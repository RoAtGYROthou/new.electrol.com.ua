<div>
    <label for="AboutUsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][AboutUs][Show]" id="AboutUsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$store['store_id']][$langCode]['Widgets']['AboutUs']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$store['store_id']][$langCode]['Widgets']['AboutUs']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionAboutus_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionAboutus_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][aboutus]" value="<?php echo $data[$store['store_id']][$langCode]['Positions']['aboutus']; ?>" />
</div>
<div>
    <label for="AboutUsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="AboutUsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][AboutUs][Title]" value="<?php echo $data[$store['store_id']][$langCode]['Widgets']['AboutUs']['Title']; ?>" />
</div>
<div>
    <label for="AboutUsText_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $aboutus_text; ?></label>
    <div class="textarea">
    <textarea id="AboutUsText_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][AboutUs][Text]" class="editable"><?php echo $data[$store['store_id']][$langCode]['Widgets']['AboutUs']['Text']; ?></textarea>
    </div>
</div>