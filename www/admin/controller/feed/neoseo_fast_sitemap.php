<?php

require_once( DIR_SYSTEM . "/engine/neoseo_controller.php");

class ControllerFeedNeoseoFastSitemap extends NeoseoController
{

	private $error = array();

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = "neoseo_fast_sitemap";
		$this->_logFile = $this->_moduleSysName . ".log";
		$this->debug = $this->config->get($this->_moduleSysName . "_debug");
	}

	public function index()
	{
		$this->checkLicense();

		$this->initLanguage('feed/' . $this->_moduleSysName);

		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {

			$this->model_setting_setting->editSetting('fast_sitemap', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			if (isset($_GET['close'])) {
				$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('feed/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
			}
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->initBreadcrumbs(array(
			array("extension/feed", "text_feed"),
			array("feed/" . $this->_moduleSysName, "heading_title_raw")
		));

		$this->initButtons();

		$this->data[$this->_moduleSysName . '_url'] = HTTP_CATALOG . 'sitemap.xml';

		$this->initParamsList(array(
			"status",
			"debug",
			"image_status",
			"seo_status",
			"seo_url_include_path",
			"addresses_status",
			"gzip_status",
			"seo_lang_status",
			"partition_status",
			"partition_volume",
			"multistore_status",
			"category_status",
			"category_brand_status",
			"filterpro_seo_status",
			"ocfilter_seo_status",
			"mfilter_seo_status",
			"category_url_end_slash",
			"category_url_date",
			"category_url_frequency",
			"category_url_priority",
			"manufacturer_status",
			"manufacturer_line_by_tima",
			"manufacturer_url_frequency",
			"manufacturer_url_priority",
			"product_status",
			"product_url_date",
			"product_url_frequency",
			"product_url_priority",
			"information_status",
			"information_url_frequency",
			"information_url_priority",
			"blog_freecart_status",
			"blog_pavo_status",
			"blog_seocms_status",
			"blog_blogmanager_status",
			"blog_category_status",
			"blog_category_url_date",
			"blog_category_url_frequency",
			"blog_category_url_priority",
			"blog_author_status",
			"blog_author_url_date",
			"blog_author_url_frequency",
			"blog_author_url_priority",
			"blog_article_status",
			"blog_article_url_date",
			"blog_article_url_frequency",
			"blog_article_url_priority",
		));

		$sql = "show tables like 'search_adress%'";

		$query = $this->db->query($sql);

		$this->data["hasAddresses"] = ( $query->num_rows > 0);

		$this->data['params'] = $this->data;

		$this->data["logs"] = $this->getLogs();

		$this->document->addStyle('view/stylesheet/' . $this->_moduleSysName . '.css');

		$this->template = 'feed/' . $this->_moduleSysName . '.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(), $this->config->get('config_compression'));
	}

	private function validate()
	{
		if (!$this->user->hasPermission('modify', 'feed/' . $this->_moduleSysName)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

?>
