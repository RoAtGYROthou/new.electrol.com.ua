<?php

require_once(DIR_SYSTEM . "/engine/neoseo_controller.php");

class ControllerFeedNeoSeoProductFeed extends NeoSeoController
{

	private $error = array();

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = "neoseo_product_feed";
		$this->_logFile = $this->_moduleSysName . ".log";
		$this->debug = $this->config->get($this->_moduleSysName . "_debug") == 1;
	}

	public function index()
	{
		$this->checkLicense();
		$this->upgrade();

		$this->initLanguage($this->_route . '/neoseo_product_feed');

		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('setting/setting');
		$this->load->model($this->_route . '/neoseo_product_feed');
		$this->load->model('tool/neoseo_product_feed');
		$this->load->model($this->_route . '/neoseo_product_feed_formats');
		$this->load->model($this->_route . '/' . $this->_moduleSysName . '_update_relations');
		$this->load->model($this->_route . '/' . $this->_moduleSysName . '_categories');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {

			$settings_array = array();
			$settings_array[$this->_moduleSysName . "_status"] = $this->request->post[$this->_moduleSysName . "_status"];
			$settings_array[$this->_moduleSysName . "_debug"] = $this->request->post[$this->_moduleSysName . "_debug"];
			$settings_array[$this->_moduleSysName . "_type"] = $this->request->post[$this->_moduleSysName . "_type"];

			$this->model_setting_setting->editSetting('neoseo_product_feed', $settings_array);

			$this->session->data['success'] = $this->language->get('text_success_options'); //add data of saved feed

			if (isset($_GET["close"])) {
				$this->redirect($this->url->link('extension/' . $this->_route, 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
			}
		}

		$this->model_feed_neoseo_product_feed->upgrade();
		$this->model_feed_neoseo_product_feed_formats->upgrade();
		$this->model_feed_neoseo_product_feed_categories->upgrade();
		$this->model_feed_neoseo_product_feed_update_relations->upgrade();

		$this->initParamsList(array(
			"status",
			"debug",
			"type",
		    ), $this->data);

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$this->initBreadcrumbs(array(array("extension/" . $this->_route, "text_feed"), array($this->_route . "/neoseo_product_feed", "heading_title_raw")));

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['page_formats'])) {
			$page_formats = $this->request->get['page_formats'];
		} else {
			$page_formats = 1;
		}

		$pagination_data = array('start' => ($page - 1) * $this->config->get('config_admin_limit'), 'limit' => $this->config->get('config_admin_limit'));
		$feeds_total = $this->model_tool_neoseo_product_feed->getTotalFeeds();
		$feeds = $this->model_tool_neoseo_product_feed->getListFeeds($pagination_data);

		foreach ($feeds as $fid => $feed) {
			$feeds[$fid]['feed_cron'] = rtrim(HTTP_CATALOG, "/") . "/index.php?route=feed/" . $this->_moduleSysName . "/download&feed=" . $feed["feed_shortname"]. ".xml";
			$feeds[$fid]['feed_cron_link'] = rtrim(HTTP_CATALOG, "/") . "/download/" . $feed["feed_shortname"] . ".xml";
			$feeds[$fid]['feed_cron_file'] = rtrim(DIR_DOWNLOAD, "/") . "/" . $feed["feed_shortname"] . ".xml";
			if (file_exists($feeds[$fid]['feed_cron_file'])) {
				$feeds[$fid]['feed_cron_date'] = date("Y-m-d H:i:s", filemtime($feeds[$fid]['feed_cron_file']));
			}
			$feeds[$fid]['feed_demand'] = rtrim(HTTP_CATALOG, "/") . "/index.php?route=feed/" . $this->_moduleSysName . "&name=" . $feed["feed_shortname"];
			$feeds[$fid]['edit'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFeed', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $feed['product_feed_id'], 'SSL');
			$feeds[$fid]['delete'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/deleteFeed', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $feed['product_feed_id'], 'SSL');
		}

		$this->data["feeds"] = $feeds;

		$this->load->model($this->_route . '/neoseo_product_feed_formats');

		$pagination_data = array('start' => ($page_formats - 1) * $this->config->get('config_admin_limit'), 'limit' => $this->config->get('config_admin_limit'));
		$formats_total = $this->model_tool_neoseo_product_feed->getTotalFormats();
		$formats = $this->model_tool_neoseo_product_feed->getFormats($pagination_data);

		foreach ($formats as $format) {
			$this->data['formats'][$format['product_feed_format_id']]['feed_format_name'] = $format['feed_format_name'];
			$this->data['formats'][$format['product_feed_format_id']]['format_xml'] = $format['format_xml'];
			$this->data['formats'][$format['product_feed_format_id']]['edit'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFormat', 'token=' . $this->session->data['token'] . '&product_feed_format_id=' . $format['product_feed_format_id'], 'SSL');
			$this->data['formats'][$format['product_feed_format_id']]['delete'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/deleteFormat', 'token=' . $this->session->data['token'] . '&product_feed_format_id=' . $format['product_feed_format_id'], 'SSL');
		}

		foreach ($formats as $key => $format) {
			$this->data["array_formats"][$format['product_feed_format_id']] = $format['feed_format_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $feeds_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();

		$pagination_formats = new Pagination();
		$pagination_formats->total = $formats_total;
		$pagination_formats->page = $page_formats;
		$pagination_formats->limit = $this->config->get('config_admin_limit');
		$pagination_formats->text = $this->language->get('text_pagination');
		$pagination_formats->url = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'] . '&page_formats={page}', 'SSL');
		$this->data['pagination_formats'] = $pagination_formats->render();

		$this->data['token'] = $this->session->data['token'];
		$this->data['add'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/addFeed', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['add_formats'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/addFormat', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['default_formats'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/defaultFormat', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['generate_url'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/generate', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['action'] = $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/' . $this->_route, 'token=' . $this->session->data['token'], 'SSL');
		$this->data['save'] = $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['save_and_close'] = $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'] . "&close=1", 'SSL');
		$this->data['close'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['clear'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/clearLogs', 'token=' . $this->session->data['token'], 'SSL');
		$this->data["cron"] = "php " . realpath(DIR_SYSTEM . "../cron/" . $this->_moduleSysName . ".php");
		$this->data["cron_debug"] = "<a href=\"" . rtrim(HTTP_CATALOG, "/") . "/cron/" . $this->_moduleSysName . ".php\" target=\"_blank\">Ручной вызов крон скрипта</a>";

		$this->document->addStyle('view/stylesheet/neoseo_product_feed.css');
		$this->data['fields'] = $this->getFields();

		$this->data["logs"] = $this->getLogs();
		$ids = array();

		$query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product`");
		foreach ($query->rows as $row) {
			$ids[] = $row['product_id'];
		}
		$this->data['ids'] = $ids;

		$this->data["params"] = $this->data;
		$this->template = $this->_route . '/neoseo_product_feed.tpl';
		$this->children = array('common/header', 'common/footer');

		$this->response->setOutput($this->render());
	}

	public function install()
	{
		$this->load->model($this->_route . "/" . $this->_moduleSysName);
		$this->model_feed_neoseo_product_feed->install(($this->config->get('config_store_id') ? $this->config->get('config_store_id') : 0));

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_formats");
		$this->model_feed_neoseo_product_feed_formats->install();

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_categories");
		$this->model_feed_neoseo_product_feed_formats->install();

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_update_relations");
		$this->model_feed_neoseo_product_feed_update_relations->install();
	}

	public function uninstall()
	{
		$this->load->model($this->_route . "/" . $this->_moduleSysName);
		$this->model_feed_neoseo_product_feed->uninstall();

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_formats");
		$this->model_feed_neoseo_product_feed_formats->uninstall();

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_categories");
		$this->model_feed_neoseo_product_feed_formats->uninstall();

		$this->load->model($this->_route . "/" . $this->_moduleSysName . "_update_relations");
		$this->model_feed_neoseo_product_feed_update_relations->uninstall();
	}

	private function validate()
	{
		if (!$this->user->hasPermission('modify', $this->_route . '/neoseo_product_feed')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function getFormFormat()
	{
		$this->initLanguage($this->_route . '/' . $this->_moduleSysName);

		$this->data['text_form'] = !isset($this->request->get['product_feed_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$this->initBreadcrumbs(array(
			array('extension/feed', "text_feed"),
			array($this->_route . '/' . $this->_moduleSysName, 'heading_title_raw'),
			array($this->_route . '/' . $this->_moduleSysName, 'text_list_formats'),
		    ), $this->data);

		if (!isset($this->request->get['product_feed_id'])) {
			$this->data['action'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/addFormat', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFormat', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $this->request->get['product_feed_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');

		$this->load->model('tool/' . $this->_moduleSysName);

		if (isset($this->request->get['product_feed_format_id'])) {

			$this->data['product_feed_format_id'] = $this->request->get['product_feed_format_id'];
			$this->data['format'] = $this->model_tool_neoseo_product_feed->getFormat($this->request->get['product_feed_format_id']);

			$this->data['breadcrumbs'][] = array(
				'text' => $this->data['format']['feed_format_name'],
				'href' => $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFormat', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $this->request->get['product_feed_format_id'], 'SSL'),
				'separator' => '::'
			);
		} else {
			$this->data['format'] = array('feed_format_name' => '', 'format_xml' => '');
		}
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = $this->_route . '/' . $this->_moduleSysName . '_format_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function addFormat()
	{
		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('tool/' . $this->_moduleSysName);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_tool_neoseo_product_feed->saveFormats($this->request->post["format"]);

			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getFormFormat();
	}

	public function editFormat()
	{
		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('tool/' . $this->_moduleSysName);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_tool_neoseo_product_feed->saveFormats($this->request->post["format"]);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getFormFormat();
	}

	public function deleteFormat()
	{

		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->load->model('tool/neoseo_product_feed');

		if (!$this->user->hasPermission('modify', $this->_route . '/neoseo_product_feed')) {
			$this->data['error_warning'] = $this->language->get('error_permission');

			$this->template = $this->_route . '/neoseo_product_feed.tpl';
			$this->children = array('common/header', 'common/footer');


			$this->data['post'] = $this->request->post;

			$this->response->setOutput($this->render(), $this->config->get('config_compression'));
			return;
		} else {

			if (isset($this->request->get["product_feed_format_id"])) {
				$this->model_tool_neoseo_product_feed->deleteFormat($this->request->get);

				$this->session->data['success'] = $this->language->get('text_success_delete');
			}


			header("location:" . str_replace('&amp;', '&', $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'], 'SSL')));
		}
	}

	public function defaultFormat()
	{

		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->load->model('feed/' . $this->_moduleSysName .'_formats');

		if (!$this->validate()) {
			$this->data['error_warning'] = $this->error['warning'];
			$this->template = $this->_route . '/neoseo_product_feed.tpl';
			$this->children = array('common/header', 'common/footer');
			$this->data['post'] = $this->request->post;
			$this->response->setOutput($this->render(), $this->config->get('config_compression'));
			return;
		} else {
			$this->model_feed_neoseo_product_feed_formats->defaultFormat();
			$this->session->data['success'] = $this->language->get('text_success_formats');
		}
		header("location:" . str_replace('&amp;', '&', $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'], 'SSL')));
	}

	protected function getForm()
	{
		$this->initLanguage($this->_route . '/' . $this->_moduleSysName);

		$this->data['text_form'] = !isset($this->request->get['product_feed_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$this->initBreadcrumbs(array(
			array('extension/feed', "text_feed"),
			array($this->_route . '/' . $this->_moduleSysName, 'heading_title_raw'),
			array($this->_route . '/' . $this->_moduleSysName, 'text_list_feeds'),
		    ), $this->data);

		if (!isset($this->request->get['product_feed_id'])) {
			$this->data['action'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/addFeed', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFeed', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $this->request->get['product_feed_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');
		$this->load->model('tool/' . $this->_moduleSysName);
		$getListFormats = $this->model_tool_neoseo_product_feed->getListFormats();
		foreach ($getListFormats as $key => $format) {
			$formats[$format['product_feed_format_id']] = $format;
		}

		$this->load->model('catalog/' . $this->_moduleSysName . '_categories');
		$this->data['feedMainCategories'] = $this->model_catalog_neoseo_product_feed_categories->getParentCategories();

		$feedCategories = array();
		foreach ($this->data['feedMainCategories'] as $mainCategories) {
			$feedCategories[$mainCategories['category_id']] = $this->model_catalog_neoseo_product_feed_categories->getCategoriesByParentId($mainCategories['category_id']);
		}
		foreach ($this->model_catalog_neoseo_product_feed_categories->getCategories(array('where' => 'c2.parent_id!=0')) as $category) {
			foreach ($feedCategories as $parent_id => $categories) {
				foreach ($categories as $item) {
					if ($item['category_id'] != $category['category_id'])
						continue;
					$category['parent_id'] = $parent_id;
					$this->data['feedCategories'][] = $category;
				}
			}
		}

		$this->data['formats'] = $formats;
		$this->load->model('localisation/language');

		$languages = array();
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$languages[$language['language_id']] = $language['name'];
		}
		$this->data['languages'] = $languages;

		$this->load->model('catalog/category');

		$categories = array();
		foreach ($this->model_catalog_category->getCategories(0) as $category) {
			$categories[$category['category_id']] = $category['name'];
		}
		$this->data['categories'] = $categories;

		$this->load->model('catalog/manufacturer');
		$manufacturers = array();
		foreach ($this->model_catalog_manufacturer->getManufacturers(0) as $manufacturer) {
			$manufacturers[$manufacturer['manufacturer_id']] = $manufacturer['name'];
		}
		$this->data['manufacturers'] = $manufacturers;
		$data['token'] = $this->session->data['token'];
		$data['product_not_unload'] = array();
		$this->load->model('catalog/product');

		$this->load->model('localisation/currency');
		$currencies = array();
		foreach ($this->model_localisation_currency->getCurrencies() as $currency) {
			$currencies[$currency['code']] = $currency['title'];
		}
		$this->data['currencies'] = $currencies;

		$this->load->model('setting/store');

		$this->data['stores'] = $this->model_setting_store->getStores();
		$this->data['product_feed_store'] = array(0);

		if (isset($this->request->post['product_feed_store'])) {
			$this->data['product_feed_store'] = $this->request->post['product_feed_store'];
		}

		/* NeoSeo Exchange1c - begin */
		$this->data['warehouses'] = array();
		$this->data['use_warehouses'] = false;
		if (($this->config->get('neoseo_exchange1c_status') == 1 && $this->config->get('neoseo_exchange1c_use_warehouse') == 1) ||
		    ($this->config->get('soforp_exchange1c_status') == 1 && $this->config->get('soforp_exchange1c_use_warehouse') == 1)) {
			$warehouses = $this->model_tool_neoseo_product_feed->getWarehouses();
		}
		if (isset($warehouses)) {
			foreach ($warehouses as $warehouse) {
				$this->data['warehouses'][$warehouse['warehouse_id']] = $warehouse['name'];
			}
			$this->data['use_warehouses'] = true;
		}
		/* NeoSeo Exchange1c - end */

		if (isset($this->request->get['product_feed_id'])) {

			$this->data['product_feed_id'] = $this->request->get['product_feed_id'];
			$getFeeds = $this->model_tool_neoseo_product_feed->getFeed($this->request->get['product_feed_id']);
			$getFeeds['feed_demand'] = rtrim(HTTP_CATALOG, "/") . "/index.php?route=" . $this->_route . "/" . $this->_moduleSysName . "&name=" . $getFeeds["feed_shortname"];
			$getFeeds["feed_cron"] = "php " . realpath(DIR_SYSTEM . "/../cron/" . $this->_moduleSysName . ".php");

			$this->load->model('catalog/product');
			$getFeeds['product_not_unload'] = array();
			$product_not_unload = explode(',', $getFeeds['not_unload']);
			if ($product_not_unload) {
				foreach ($product_not_unload as $product_id) {
					$product_info = $this->model_catalog_product->getProduct($product_id);
					if ($product_info) {
						$getFeeds['product_not_unload'][] = array(
							'product_id' => $product_info['product_id'],
							'name' => $product_info['name']
						);
					}
				}
			}

			$getFeeds['product_list'] = array();
			$products = explode(',', $getFeeds['products']);
			if ($products) {
				foreach ($products as $product_id) {
					$product_info = $this->model_catalog_product->getProduct($product_id);
					if ($product_info) {
						$getFeeds['product_list'][] = array(
							'product_id' => $product_info['product_id'],
							'name' => $product_info['name']
						);
					}
				}
			}

			$this->data['feed'] = $getFeeds;

			$this->data['product_feed_store'] = $this->model_tool_neoseo_product_feed->getProductFeedStore($this->request->get['product_feed_id']);

			$this->data['breadcrumbs'][] = array(
				'text' => $getFeeds['feed_name'],
				'href' => $this->url->link($this->_route . '/' . $this->_moduleSysName . '/editFeed', 'token=' . $this->session->data['token'] . '&product_feed_id=' . $this->request->get['product_feed_id'], 'SSL'),
				'separator' => '::'
			);
		} else {
			$this->data['feed'] = array(
				'status' => 0, 'id_format' => 1,
				'feed_name' => '',
				'feed_shortname' => '',
				'feed_demand' => rtrim(HTTP_CATALOG, "/") . "/index.php?route=" . $this->_route . "/" . $this->_moduleSysName . "&name=",
				'ip_list' => '',
				'feed_cron' => "php " . realpath(DIR_SYSTEM . "/../cron/" . $this->_moduleSysName . ".php"),
				'language_id' => 1,
				'use_main_category' => 0,
				'categories' => '',
				'manufacturers' => '',
				'currency' => 1,
				'use_original_images' => 0,
				'image_pass' => 0,
				'image_width' => '',
				'image_height' => '',
				'pictures_limit' => '',
				'cat_names_separathor' => ' > ',
				'product_markup' => 0,
				'product_markup_option' => 0,
				'product_markup_type' => 0,
				'sql_code' => 'p.quantity > 0',
				'sql_code_before' => '',
				'use_warehouse_quantity' => 0,
				'warehouses' => '',
				'product_not_unload' => array(),
				'product_list' => array(),
			);
		}
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->data['token'] = $this->session->data['token'];
		$this->template = $this->_route . '/' . $this->_moduleSysName . '_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function addFeed()
	{
		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('tool/' . $this->_moduleSysName);

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_tool_neoseo_product_feed->saveFeed($this->request->post["feed"]);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function editFeed()
	{
		$this->language->load($this->_route . '/' . $this->_moduleSysName);

		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('tool/' . $this->_moduleSysName);
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_tool_neoseo_product_feed->saveFeed($this->request->post['feed']);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			$url .= '#tab-feeds';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort = ' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order = ' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page = ' . $this->request->get['page'];
			}


			$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function deleteFeed()
	{

		$this->initLanguage($this->_route . '/neoseo_product_feed');
		$this->load->model('tool/neoseo_product_feed');

		if (!$this->user->hasPermission('modify', $this->_route . '/neoseo_product_feed')) {
			$this->data['error_warning'] = $this->language->get('error_permission');

			$this->template = $this->_route . '/neoseo_product_feed.tpl';
			$this->children = array('common/header', 'common/footer');


			$this->data['post'] = $this->request->post;

			$this->response->setOutput($this->render(), $this->config->get('config_compression'));
			return;
		} else {

			if (isset($this->request->get["product_feed_id"])) {
				$this->model_tool_neoseo_product_feed->deleteFeed($this->request->get);

				$this->session->data['success'] = $this->language->get('text_success_delete');
			}


			header("location:" . str_replace('&amp;', '&', $this->url->link($this->_route . '/neoseo_product_feed', 'token=' . $this->session->data['token'], 'SSL')));
		}
	}

	public function generate()
	{
		$product_id = $this->request->get["id"];
		$width = $this->request->get["width"];
		$height = $this->request->get["height"];

		$count = 0;
		$this->load->model("catalog/product");
		$this->load->model("tool/image");
		$images = $this->model_catalog_product->getProductImages($product_id);
		foreach ($images as $image) {
			$this->model_tool_image->resize($image['image'], $width, $height);
			$count++;
		}

		echo json_encode(array('status' => 'OK', 'count' => $count));
	}

	protected function getFields()
	{
		$result = array();

		$result['date'] = $this->language->get("field_desc_date");
		$result['url'] = $this->language->get("field_desc_url");
		$result['currency'] = $this->language->get("field_desc_currency");
		$result['categories'] = $this->language->get("field_desc_categories");
		$result['category'] = $this->language->get("field_desc_category");
		$result['category.id'] = $this->language->get("field_desc_category.id");
		$result['category.parentId'] = $this->language->get("field_desc_category.parentId");
		$result['category.name'] = $this->language->get("field_desc_category.name");
		$result['offers'] = $this->language->get("field_desc_offers");
		$result['offer'] = $this->language->get("field_desc_offer");
		$result['offer.id'] = $this->language->get("field_desc_offer.id");
		$result['offer.path'] = $this->language->get("field_desc_path");
		$result['offer.url'] = $this->language->get("field_desc_offer.url");
		$result['offer.tag'] = $this->language->get("field_desc_offer.tag");
		$result['offer.meta_title'] = $this->language->get("field_desc_offer.meta_title");
		$result['offer.meta_h1'] = $this->language->get("field_desc_offer.meta_h1");
		$result['offer.meta_description'] = $this->language->get("field_desc_offer.meta_description");
		$result['offer.meta_keyword'] = $this->language->get("field_desc_offer.meta_keyword");
		$result['offer.price'] = $this->language->get("field_desc_offer.price");
		$result['offer.currencyId'] = $this->language->get("offer.currencyId");
		$result['offer.categoryId'] = $this->language->get("offer.categoryId");
		$result['offer.name'] = $this->language->get("offer.name");
		$result['offer.description'] = $this->language->get("offer.description");
		$result['offer.description_no_html'] = $this->language->get("offer.description_no_html");
		$result['offer.model'] = $this->language->get("offer.model");
		$result['offer.vendor'] = $this->language->get("offer.vendor");
		$result['offer.vendorCode'] = $this->language->get("offer.vendorCode");
		$result['offer.image'] = $this->language->get("offer.image");
		$result['offer.discount'] = $this->language->get("offer.discount");
		$result['offer.special'] = $this->language->get("offer.special");
		$result['image'] = $this->language->get("image");
		$result['offer.options'] = $this->language->get("offer.options");
		$result['option'] = $this->language->get("option");
		$result['option.name'] = $this->language->get("option.name");
		$result['option.value'] = $this->language->get("option.value");
		$result['option.available'] = $this->language->get("option.available");
		$result['offer.attributes'] = $this->language->get("offer.attributes");
		$result['attribute'] = $this->language->get("attribute");
		$result['attributes.name'] = $this->language->get("attributes.name");
		$result['attributes.value'] = $this->language->get("attributes.value");
		$result['offer.filter_attributes'] = $this->language->get("offer.filter_attributes");
		
		return $result;
	}

}

?>
