<?php echo $header; ?>

<?php
require_once(DIR_SYSTEM . '/engine/neoseo_view.php' );
$widgets = new NeoSeoWidgets('neoseo_fast_sitemap_',$params);
$widgets->text_select_all = $text_select_all;
$widgets->text_unselect_all = $text_unselect_all;
?>

<div id="content">

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

    <?php if (isset($success) && $success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <script>jQuery(".success").slideDown().delay(2000).slideUp();</script>
    <?php } ?>

    <div class="box">
        <div class="heading">
            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>

            <h1><?php echo $heading_title_raw  . " " . $text_module_version; ?></h1>

            <div class="buttons">
                <?php if( !isset($license_error) ) { ?>
                <a onclick="$('#form').attr('action','<?php echo $save; ?>'); $('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                <a onclick="$('#form').attr('action','<?php echo $save_and_close; ?>'); $('#form').submit();" class="button"><span><?php echo $button_save_and_close; ?></span></a>
                <?php } else { ?>
                <a onclick="location = '<?php echo $recheck; ?>';" class="button"><?php echo $button_recheck; ?></a>
                <?php } ?>
                <a onclick="location = '<?php echo $close; ?>';" class="button"><span><?php echo $button_close; ?></span></a>
            </div>
        </div>

        <div class="content">
            <div id="tabs" class="htabs">
                <a href="#tab-general"><?php echo $tab_general; ?></a>
                <?php if( !isset($license_error) ) { ?>
		<a href="#tab-store"><?php echo $tab_store; ?></a>
		<a href="#tab-blog"><?php echo $tab_blog; ?></a>
                <a href="#tab-logs"><?php echo $tab_logs; ?></a>
                <?php } ?>
                <a href="#tab-support"><?php echo $tab_support; ?></a>
                <a href="#tab-license"><?php echo $tab_license; ?></a>
            </div>

            <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="tab-general">
                    <?php if( !isset($license_error) ) { ?>
                    <table class="form">
                        <?php $widgets->dropdown('status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php $widgets->dropdown('seo_status',array( 0 => $text_seo_0, 1 => $text_seo_1, 2 => $text_seo_2, 3 => $text_seo_3)); ?>
                        <?php $widgets->dropdown('seo_url_include_path',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php if( $hasAddresses ) { ?>
                        <?php $widgets->dropdown('addresses_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php } ?>
			<?php $widgets->dropdown('seo_lang_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			<?php $widgets->dropdown('partition_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			<?php $widgets->input('partition_volume',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php $widgets->dropdown('multistore_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php $widgets->dropdown('gzip_status', array( 0 => $text_gzip_0, 1 => $text_gzip_1, 2 => $text_gzip_2, 3 => $text_gzip_3, 4 => $text_gzip_4, 5 => $text_gzip_5, 6 => $text_gzip_6, 7 => $text_gzip_7, 8 => $text_gzip_8, 9 => $text_gzip_9)); ?>
                        <?php $widgets->dropdown('image_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php $widgets->text('url'); ?>
                    </table>
                    <?php } else { ?>

                        <?php echo $license_error; ?>

                    <?php } ?>
                </div>
		
		<?php if( !isset($license_error) ) { ?>
                <div id="tab-store">
		     <div id="tab-nav-store" class="vtabs">
			<a href="#tab-store-information"><?php echo $tab_store_information; ?></a>
                        <a href="#tab-store-category"><?php echo $tab_store_category; ?></a>
                        <a href="#tab-store-manufacturer"><?php echo $tab_store_manufacturer; ?></a>
                        <a href="#tab-store-product"><?php echo $tab_store_product; ?></a>
                    </div>
		    <div id="tab-store-information" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('information_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('information_url_frequency'); ?>
			    <?php $widgets->input('information_url_priority'); ?>
			</table>
		    </div>
		    <div id="tab-store-category" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('category_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('category_brand_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                <?php $widgets->dropdown('category_url_end_slash',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('filterpro_seo_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('ocfilter_seo_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('mfilter_seo_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('category_url_date', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('category_url_frequency'); ?>
			    <?php $widgets->input('category_url_priority'); ?>
			</table>
		    </div>
		    <div id="tab-store-manufacturer" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('manufacturer_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                <?php $widgets->dropdown('manufacturer_line_by_tima',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('manufacturer_url_frequency'); ?>
			    <?php $widgets->input('manufacturer_url_priority'); ?>
			</table>
		    </div>
		    <div id="tab-store-product" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('product_status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('product_url_date', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('product_url_frequency'); ?>
			    <?php $widgets->input('product_url_priority'); ?>
			</table>
		    </div>
                </div>
                <?php } ?>
		
		<?php if( !isset($license_error) ) { ?>
                <div id="tab-blog">
		    <div id="tab-nav-blog" class="vtabs">
			<a href="#tab-blog-module"><?php echo $tab_blog_module; ?></a>
                        <a href="#tab-blog-category"><?php echo $tab_blog_category; ?></a>
                        <a href="#tab-blog-author"><?php echo $tab_blog_author; ?></a>
                        <a href="#tab-blog-article"><?php echo $tab_blog_article; ?></a>
                    </div>
		    <div id="tab-blog-module" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('blog_freecart_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_seocms_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_pavo_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_blogmanager_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			</table>
		    </div>
		    <div id="tab-blog-category" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('blog_category_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_category_url_date', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('blog_category_url_frequency'); ?>
			    <?php $widgets->input('blog_category_url_priority'); ?>
			</table>
		    </div>
		    <div id="tab-blog-author" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('blog_author_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_author_url_date', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('blog_author_url_frequency'); ?>
			    <?php $widgets->input('blog_author_url_priority'); ?>
			</table>
		    </div>
		    <div id="tab-blog-article" data-src="tabs-cont">
			<table class="form-narrow">
			    <?php $widgets->dropdown('blog_article_status', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->dropdown('blog_article_url_date', array( 0 => $text_disabled, 1 => $text_enabled)); ?>
			    <?php $widgets->input('blog_article_url_frequency'); ?>
			    <?php $widgets->input('blog_article_url_priority'); ?>
			</table>
		    </div>
                </div>
                <?php } ?>

                <?php if( !isset($license_error) ) { ?>
                <div id="tab-logs">
                    <?php $widgets->debug_and_logs('debug',array( 0 => $text_disabled, 1 => $text_enabled), $clear, $button_clear_log ); ?>
                    <textarea style="width: 98%; height: 300px; padding: 5px; border: 1px solid #CCCCCC; background: #FFFFFF; overflow: scroll;"><?php echo $logs; ?></textarea>
                </div>
                <?php } ?>

                <div id="tab-support">
                    <?php echo $mail_support; ?>
                </div>

                <div id="tab-license">
                    <?php echo $module_licence; ?>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('#tabs a').tabs();
    $('#tab-nav-store a').tabs();
    $('#tab-nav-blog a').tabs();
    //--></script>
<?php echo $footer; ?>