<?php
// Heading
$_['heading_title']     = 'Фильтр';

// Text
$_['text_display']      = 'Вид:';
$_['text_list']         = 'Список';
$_['text_grid']         = 'Сетка';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Наименование (А -&gt; Я)';
$_['text_name_desc']    = 'Наименование (Я -&gt; А)';
$_['text_price_asc']    = 'От дешевых к дорогим';
$_['text_price_desc']   = 'От дорогих к дешевым';
$_['text_rating_asc']   = 'Рейтинг (по возрастанию)';
$_['text_rating_desc']  = 'Рейтинг (по убыванию)';
$_['text_model_asc']    = 'Модель (А -&gt; Я)';
$_['text_model_desc']   = 'Модель (Я -&gt; А)';
$_['text_limit']        = 'На странице:';
?>