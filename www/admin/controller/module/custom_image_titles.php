<?php
define('EXTENSION_NAME',            'Custom Image Titles');
define('EXTENSION_VERSION',         '2.6.0');
define('EXTENSION_ID',              '1243');
define('EXTENSION_COMPATIBILITY',   'OpenCart 1.5.5.x and 1.5.6.x');
define('EXTENSION_STORE_URL',       'http://www.opencart.com/index.php?route=extension/extension/info&extension_id=1243');
define('EXTENSION_SUPPORT_EMAIL',   'support@opencart.ee');
define('EXTENSION_SUPPORT_FORUM',   'http://forum.opencart.com/viewtopic.php?f=123&t=25970');
define('OTHER_EXTENSIONS',          'http://www.opencart.com/index.php?route=extension/extension&filter_username=bull5-i');

if (!defined('JSON_UNESCAPED_SLASHES')) {
    define('JSON_UNESCAPED_SLASHES', 0xFFFF);
}

class ControllerModuleCustomImageTitles extends Controller {
    private $error = array();
    protected $alert = array(
        'error'     => array(),
        'warning'   => array(),
        'success'   => array(),
        'info'      => array()
    );
    private $defaults = array(
        'cit_installed'                 => 1,
        'cit_installed_version'         => EXTENSION_VERSION,
        'cit_status'                    => 0,
        'cit_display_caption'           => 0,
        'cit_show_alt_text'             => 1,
        'cit_show_language_in_tab'      => 1,
        'cit_use_admin_language_value'  => 1,
        'cit_remove_sql_changes'        => 0
    );

    public function __construct($registry) {
        parent::__construct($registry);
        $this->load->helper('cit');

        $this->language->load('module/custom_image_titles');
        $this->load->model('module/custom_image_titles');
    }

    public function index() {
        $this->document->addStyle('view/stylesheet/cit/css/custom.min.css');

        $this->document->addScript('view/javascript/cit/custom.min.js');
        $this->document->addScript('view/javascript/jquery/superfish/js/superfish.js');

        $this->document->setTitle($this->language->get('extension_name'));

        $this->load->model('setting/setting');

        $ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && !$ajax_request && $this->validateForm($this->request->post)) {
            $this->model_setting_setting->editSetting('custom_image_titles', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_update');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        } else if ($this->request->server['REQUEST_METHOD'] == 'POST' && $ajax_request) {
            $response = array('success' => false);

            if ($this->validateForm($this->request->post)) {
                $original_settings = $this->model_setting_setting->getSetting('custom_image_titles');

                if ((int)$original_settings['cit_status'] != (int)$this->request->post['cit_status']) {
                    $response['reload'] = true;
                    $this->session->data['success'] = $this->language->get('text_success_update');
                }

                $settings = array_merge($original_settings, $this->request->post);
                $settings['cit_installed_version'] = $original_settings['cit_installed_version'];

                $this->model_setting_setting->editSetting('custom_image_titles', $settings);

                $response['success'] = true;
                $response['msg'] = $this->language->get('text_success_update');
            } else {
                $response = array_merge(array('error' => true), array('errors' => $this->error), array('alerts' => $this->alert));
            }

            $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
            return;
        }

        $this->checkVersion() && $this->model_module_custom_image_titles->checkDatabaseStructure($this->alert);

        $this->checkPrerequisites();

        $this->checkVersion();

        $this->data['heading_title'] = $this->language->get('extension_name');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_toggle_navigation'] = $this->language->get('text_toggle_navigation');
        $this->data['text_legal_notice'] = $this->language->get('text_legal_notice');
        $this->data['text_license'] = $this->language->get('text_license');
        $this->data['text_extension_information'] = $this->language->get('text_extension_information');
        $this->data['text_terms'] = $this->language->get('text_terms');
        $this->data['text_license_text'] = $this->language->get('text_license_text');
        $this->data['text_other_extensions'] = sprintf($this->language->get('text_other_extensions'), OTHER_EXTENSIONS);
        $this->data['text_support_subject'] = $this->language->get('text_support_subject');
        $this->data['text_faq'] = $this->language->get('text_faq');

        $this->data['tab_settings'] = $this->language->get('tab_settings');
        $this->data['tab_support'] = $this->language->get('tab_support');
        $this->data['tab_about'] = $this->language->get('tab_about');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_faq'] = $this->language->get('tab_faq');
        $this->data['tab_services'] = $this->language->get('tab_services');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_apply'] = $this->language->get('button_apply');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_close'] = $this->language->get('button_close');
        $this->data['button_upgrade'] = $this->language->get('button_upgrade');

        $this->data['help_tabbed_language'] = $this->language->get('help_tabbed_language');
        $this->data['help_display_caption'] = $this->language->get('help_display_caption');
        $this->data['help_use_default_value'] = $this->language->get('help_use_default_value');
        $this->data['help_remove_sql_changes'] = $this->language->get('help_remove_sql_changes');

        $this->data['entry_installed_version'] = $this->language->get('entry_installed_version');
        $this->data['entry_extension_status'] = $this->language->get('entry_extension_status');
        $this->data['entry_show_alt_text'] = $this->language->get('entry_show_alt_text');
        $this->data['entry_tabbed_language'] = $this->language->get('entry_tabbed_language');
        $this->data['entry_display_caption'] = $this->language->get('entry_display_caption');
        $this->data['entry_use_default_value'] = $this->language->get('entry_use_default_value');
        $this->data['entry_remove_sql_changes'] = $this->language->get('entry_remove_sql_changes');
        $this->data['entry_extension_name'] = $this->language->get('entry_extension_name');
        $this->data['entry_extension_compatibility'] = $this->language->get('entry_extension_compatibility');
        $this->data['entry_extension_store_url'] = $this->language->get('entry_extension_store_url');
        $this->data['entry_copyright_notice'] = $this->language->get('entry_copyright_notice');

        $this->data['error_ajax_request'] = $this->language->get('error_ajax_request');

        $this->data['ext_name'] = EXTENSION_NAME;
        $this->data['ext_version'] = EXTENSION_VERSION;
        $this->data['ext_id'] = EXTENSION_ID;
        $this->data['ext_compatibility'] = EXTENSION_COMPATIBILITY;
        $this->data['ext_store_url'] = EXTENSION_STORE_URL;
        $this->data['ext_support_email'] = EXTENSION_SUPPORT_EMAIL;
        $this->data['ext_support_forum'] = EXTENSION_SUPPORT_FORUM;
        $this->data['other_extensions_url'] = OTHER_EXTENSIONS;

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('extension_name'),
            'href'      => $this->url->link('module/custom_image_titles', 'token=' . $this->session->data['token'], 'SSL'),
            'active'    => true
        );

        $this->data['save'] = $this->url->link('module/custom_image_titles', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['upgrade'] = $this->url->link('module/custom_image_titles/upgrade', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['update_pending'] = !$this->checkVersion();

        $this->data['ssl'] = (int)$this->config->get('config_use_ssl') ? 's' : '';

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $this->data['multilingual'] = count($languages) > 1;

        $this->data['installed_version'] = $this->installedVersion();

        # Loop through all settings for the post/config values
        $unsaved_settings_warning = false;
        foreach (array_keys($this->defaults) as $setting) {
            if (isset($this->request->post[$setting])) {
                $this->data[$setting] = $this->request->post[$setting];
            } else {
                $this->data[$setting] = $this->config->get($setting);
                if ($this->data[$setting] === null) {
                    if (!$unsaved_settings_warning && $this->checkVersion())  {
                        $this->alert['warning']['unsaved'] = $this->language->get('error_unsaved_settings');
                        $unsaved_settings_warning = true;
                    }
                    if (isset($this->defaults[$setting])) {
                        $this->data[$setting] = $this->defaults[$setting];
                    }
                }
            }
        }

        if (isset($this->session->data['error'])) {
            $this->error = $this->session->data['error'];

            unset($this->session->data['error']);
        }

        if (isset($this->error['warning'])) {
            $this->alert['warning']['warning'] = $this->error['warning'];
        }

        if (isset($this->error['error'])) {
            $this->alert['error']['error'] = $this->error['error'];
        }

        if (isset($this->session->data['success'])) {
            $this->alert['success']['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        }

        $this->data['errors'] = $this->error;

        $this->data['token'] = $this->session->data['token'];

        $this->data['alerts'] = $this->alert;

        $this->template = 'module/custom_image_titles.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function install() {
        $this->model_module_custom_image_titles->applyDatabaseChanges();

        $this->load->model('localisation/language');

        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('custom_image_titles', $this->defaults);
    }

    public function uninstall() {
        if ($this->config->get('cit_remove_sql_changes')) {
            $this->model_module_custom_image_titles->revertDatabaseChanges();
        }

        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('custom_image_titles');
    }

    public function upgrade() {
        $ajax_request = isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

        $response = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateUpgrade()) {
            $this->load->model('setting/setting');

            if ($this->model_module_custom_image_titles->upgradeDatabaseStructure($this->installedVersion(), $this->alert)) {
                $settings = array();

                // Go over all settings, add new values and remove old ones
                foreach ($this->defaults as $setting => $default) {
                    $value = $this->config->get($setting);
                    if ($value === null) {
                        $settings[$setting] = $default;
                    } else {
                        $settings[$setting] = $value;
                    }
                }

                $settings['cit_installed_version'] = EXTENSION_VERSION;

                $this->model_setting_setting->editSetting('custom_image_titles', $settings);

                $this->session->data['success'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);
                $response['success'] = true;
                $response['msg'] = sprintf($this->language->get('text_success_upgrade'), EXTENSION_VERSION);
            } else {
                $this->alert['error']['database_upgrade'] = $this->language->get('error_upgrade_database');
                $response = array_merge(array('error' => true), array('errors' => $this->error), array('alerts' => $this->alert));
            }
        } else {
            $response = array_merge(array('error' => true), array('errors' => $this->error), array('alerts' => $this->alert));
        }

        if (!$ajax_request) {
            $this->redirect($this->url->link('module/custom_image_titles', 'token=' . $this->session->data['token'], 'SSL'));
        } else {
            $this->response->setOutput(json_enc($response, JSON_UNESCAPED_SLASHES));
            return;
        }
    }

    private function checkPrerequisites() {
        $errors = false;

        if (!class_exists('VQMod')) {
            $errors = true;
            $this->alert['error']['vqmod'] = $this->language->get('error_vqmod');
        }

        return !$errors;
    }

    private function checkVersion() {
        $errors = false;

        $installed_version = $this->installedVersion();

        if ($installed_version != EXTENSION_VERSION) {
            $errors = true;
            $this->alert['info']['version'] = sprintf($this->language->get('error_version'), EXTENSION_VERSION);
        }

        return !$errors;
    }

    private function validate() {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'module/custom_image_titles')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        if (!$errors) {
            return $this->checkVersion() && $this->model_module_custom_image_titles->checkDatabaseStructure($this->alert) && $this->checkPrerequisites();
        } else {
            return false;
        }
    }

    private function validateForm($data) {
        $errors = false;

        if ($errors) {
            $errors = true;
            $this->alert['warning']['warning'] = $this->language->get('error_warning');
        }

        if (!$errors) {
            return $this->validate();
        } else {
            return false;
        }
    }

    private function validateUpgrade() {
        $errors = false;

        if (!$this->user->hasPermission('modify', 'module/custom_image_titles')) {
            $errors = true;
            $this->alert['error']['permission'] = $this->language->get('error_permission');
        }

        return !$errors;
    }

    private function installedVersion() {
        $installed_version = $this->config->get('cit_installed_version');
        return $installed_version ? $installed_version : '2.5.0';
    }
}
?>
