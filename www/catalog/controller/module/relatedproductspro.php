<?php  
class ControllerModuleRelatedproductspro extends Controller {
	protected function index() {
		
		$this->load->model('tool/image');
		$this->load->model('module/relatedproductspro');
	
		$this->language->load('module/relatedproductspro');
      	$this->data['heading_title'] = $this->language->get('heading_title');
      	$this->data['button_cart'] = $this->language->get('button_cart');

		$this->data['currenttemplate'] =  $this->config->get('config_template');

		$this->data['data'] = $this->model_module_relatedproductspro->getSetting('RelatedProductsPro', $this->config->get('config_store_id'));
		
		if(!isset($this->data['data']['RelatedProductsPro']['WidgetTitle'][$this->config->get('config_language')])){
			$this->data['data']['RelatedProductsPro']['WidgetTitle'] = '';
		} else {
			$this->data['data']['RelatedProductsPro']['WidgetTitle'] = $this->data['data']['RelatedProductsPro']['WidgetTitle'][$this->config->get('config_language')];
		}
			$this->data['data']['SuperQuickCheckoutConfig'] = $this->config->get('relatedproductspro_module');
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/relatedproductspro.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/relatedproductspro.tpl';
		} else {
			$this->template = 'default/template/module/relatedproductspro.tpl';
		}
		
		$this->data['CustomRelated'] = $this->config->get('relatedproductspro_custom');
		$this->data['RelatedProductsProShowModule'] = "no";

		$productsInCart = array();
		$products_cart = $this->cart->getProducts();
		
		foreach ($products_cart as $products) {
			$productsInCart[] = $products['product_id'];
		}
		
		if (isset($productsInCart) && !empty($this->data['CustomRelated'])) {
			sort($productsInCart);
			$i = 0;
			foreach ($this->data['CustomRelated'] as $productsToCompare) {
				sort($productsToCompare['ProductsCart']);
		
				if ($productsToCompare['ProductsCart'] == $productsInCart) {
					$this->data['RelatedProducts'] = $productsToCompare['ProductsRelated'];
				}
				$i++;
			}
		}
		
		if (empty($this->data['data']['RelatedProductsPro']['PictureWidth'])) 
			$picture_width=80; 
		else
			$picture_width=$this->data['data']['RelatedProductsPro']['PictureWidth'];
			
		if (empty($this->data['data']['RelatedProductsPro']['PictureHeight'])) 
			$picture_height=80;
		else
			$picture_height=$this->data['data']['RelatedProductsPro']['PictureHeight'];
		
		$this->data['products'] = array();
		$this->data['productsCheck'] = array();
		
		$limit = $this->data['data']['RelatedProductsPro']['RelatedLimit'];
		
		$totalProducts = array();
		
		if (isset($this->data['RelatedProducts'])) {
			$totalProducts = array_merge($totalProducts, $this->data['RelatedProducts']);
		}
		
		if (($this->data['data']['RelatedProductsPro']['DefaultOCFunctionality'] == 'yes')) {
			$prods = array();
			foreach ($productsInCart as $defaultProductID) {
				$results = $this->model_catalog_product->getProductRelated($defaultProductID);
				if (!empty($results)) {
					foreach ($results as $result) {
						$prods[] = $result['product_id'];
					}
				}
			}
			$totalProducts = array_merge($totalProducts, $prods);
		}
		
		if ((empty($productsInCart)) && ($this->data['data']['RelatedProductsPro']['DefaultOCFunctionality'] == 'yes') && isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
			$prods = array();
			$results = $this->model_catalog_product->getProductRelated($product_id);
			if (!empty($results)) {
				foreach ($results as $result) {
						$prods[] = $result['product_id'];
				}
			}
			$totalProducts = array_merge($totalProducts, $prods);
		}
		
		if (isset($this->data['data']['RelatedProductsPro']['Relation']) && $this->data['data']['RelatedProductsPro']['Relation'] == 'inclusive') {
			$prods = array();
			foreach ($productsInCart as $defaultProduct) {
				foreach ($this->data['CustomRelated'] as $productsToCompare) {
					if (in_array($defaultProduct, $productsToCompare['ProductsCart'],true)) {
						$prods = array_merge($prods, $productsToCompare['ProductsRelated']);
					}
				}
			}
			$totalProducts = array_merge($totalProducts, $prods);
		}
		
		foreach ($totalProducts as $result) {
			if (sizeof($this->data['products'])==$limit)
				break;
				
			$product_info = $this->model_catalog_product->getProduct($result);
			if ($product_info['image']) {
				$image = $this->model_tool_image->resize($product_info['image'], $picture_width, $picture_height);
			} else {
				$image = false;
			}
			  
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$product_info['special']) {
				$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = (int)$product_info['rating'];
			} else {
				$rating = false;
			}
						
			if (isset($this->request->get['product_id']) && ($product_info['product_id']==$this->request->get['product_id']) ) {
				//
			} else if ((!in_array($product_info['product_id'],$productsInCart,true)) && (!in_array($product_info['product_id'],$this->data['productsCheck'],true))) {			
				$this->data['products'][] = array(
					'product_id'	   => $product_info['product_id'],
					'thumb'			=> $image,
					'name'			 => $product_info['name'],
					'price'			=> $price,
					'special'		  => $special,
					'rating'		   => $rating,
					'reviews'		  => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'			 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);
				$this->data['productsCheck'][] = $product_info['product_id'];
			}
		}

		if (!empty($this->data['products'])) $this->data['RelatedProductsProShowModule'] = "yes";
	
		$this->render();
	}
}
?>