<div>
    <label for="FacebookShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Facebook][Show]" id="FacebookShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['Facebook']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['Facebook']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionFacebook_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionFacebook_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][facebook]" value="<?php echo $data[$storeCode][$langCode]['Positions']['facebook']; ?>" />
</div>
<div>
    <label for="FacebookTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="FacebookTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Facebook][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Facebook']['Title']; ?>" />
 </div>
<div>
    <label for="FacebookURL_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $facebook_pageurl; ?></label>
    <input type="text" id="FacebookURL_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Facebook][URL]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Facebook']['URL']; ?>" />
</div>
<div>
    <label for="FacebookHeight_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $facebook_widgetheight; ?></label>
    <input type="text" id="FacebookHeight_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Facebook][Height]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Facebook']['Height']; ?>" />
</div>