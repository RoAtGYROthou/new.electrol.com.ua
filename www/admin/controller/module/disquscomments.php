<?php
class ControllerModuleDisquscomments extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/disquscomments');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$this->document->addScript('view/javascript/ckeditor/ckeditor.js');
        $this->document->addScript('view/javascript/disquscomments/bootstrap/js/bootstrap.min.js');
        $this->document->addStyle('view/javascript/disquscomments/bootstrap/css/bootstrap.min.css');
        $this->document->addStyle('view/stylesheet/disquscomments/font-awesome/css/font-awesome.css');
        $this->document->addStyle('view/stylesheet/disquscomments/disquscomments.css');			
		$this->data['error_warning'] = '';
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if (!$this->user->hasPermission('modify', 'module/disquscomments')) {
				$this->validate();
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post['DisqusComments']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }
        	if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post['DisqusComments']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }
		
			$this->model_setting_setting->editSetting('DisqusComments', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
			
			if (!empty($_GET['activate'])) {
				$this->session->data['success'] = $this->language->get('text_success_activation');
			}
			
			$selectedTab = (empty($this->request->post['selectedTab'])) ? 0 : $this->request->post['selectedTab'];
			$this->redirect($this->url->link('module/disquscomments', 'token=' . $this->session->data['token'] . '&tab='.$selectedTab, 'SSL'));
		}
						
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['entry_code'] = $this->language->get('entry_code');
			
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['entry_layouts_active'] = $this->language->get('entry_layouts_active');
		$this->data['entry_highlightcolor'] = $this->language->get('entry_highlightcolor');
				
 		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/disquscomments', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/disquscomments', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		
		if (isset($this->request->post['DisqusComments'])) {
			foreach ($this->request->post['DisqusComments'] as $key => $value) {
				$this->data['data']['DisqusComments'][$key] = $this->request->post['DisqusComments'][$key];
			}
		} else {
			$configValue = $this->config->get('DisqusComments');
			$this->data['data']['DisqusComments'] = $configValue;
			
		}
			
		$this->data['currenttemplate'] =  $this->config->get('config_template');
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['disquscomments_module'])) {
			$this->data['modules'] = $this->request->post['disquscomments_module'];
			exit;
		} elseif ($this->config->get('disquscomments_module')) { 
			$this->data['modules'] = $this->config->get('disquscomments_module');
		}
		
		$this->data['moduleData'] = $this->data['data']['DisqusComments'];				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/disquscomments.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/disquscomments')) {
			$this->error = true;
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
}
?>