<?php

$_['heading_title'] 			= 'Уведомить о наличии';
$_['text_module']	 			= 'Уведомление о наличии!';
$_['entry_name']       = 'Название модуля';

$_['text_success_avail']				= 'Настройки успешно изменены!';
$_['text_view_notices'] = 'View Notices';

$_['text_capcha'] = 'Capcha';


$_['entry_status']     = 'Cтатус:';
$_['entry_layout']        = 'Макет:';
$_['entry_position']      = 'Расположение:';
$_['entry_sort_order']    = 'Порядок сортировки:';

//Capcha

$_['entry_capcha_status'] = 'Captcha Status';
$_['avail_buttom'] = 'Заменять кнопку купить на уведомить';

$_['tab_active'] = 'Активные';
$_['tab_closed'] = 'Обработанные';
$_['button_delete'] = 'Удалить';
$_['button_send'] = 'Уведомить';
$_['text_time'] = 'Время';
$_['text_product'] = 'Товар';
$_['text_price'] = 'Цена';
$_['text_mail'] = 'E-mail';
$_['text_name'] = 'Имя';
$_['text_comment'] = 'Комментарий';
$_['text_statuse'] = 'Статус';
$_['text_quantity'] = 'В наличии';
$_['text_list'] = 'Список уведомлений';
$_['text_sender'] = 'Товар в магазине!';
$_['text_edit_product'] = 'Уведомлять при сохранении настроек товара';
$_['email_subject'] = 'Уведомление о наличии товара!';
$_['text_mail_send'] = 'Мы рады сообщить вам о наличии товара который вас заинтересовал!';
$_['text_link_page'] = 'Страница товара';
$_['text_get_availabilitylist'] = 'Перейти в список заказов на уведомление';
$_['text_mail_on'] = 'Email куда будут приходить уведомления о новых заявках на уведомление о наличии.';
$_['text_status_notprocessed'] = 'Не выполнено';
$_['text_status_processed'] = 'Выполнено';
$_['reload'] = 'Обновить';
$_['success'] = 'Ваши уведомления отправлены!';
$_['error'] = 'Нет заявок для отправки';
$_['text_cron'] = 'Ссылка для cron\'а';

$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_permission'] = 'Ошибка! Вы не имеете прав для изменения модуля';
$_['error_email'] = 'E-mail адрес введен неверно!';

?>