<?php
// Heading
$_['heading_title']          = 'SEO Editor  by <a href="http://www.apwai.net" onclick="return !window.open(this.href)"> APWAI.NET</a>'; 


// Column
$_['column_name']            = 'Name';
$_['column_type']           = 'Type';
$_['column_keyword']           = 'SEO Keyword';
$_['column_meta_keyword']           = 'Meta Keywords';
$_['column_meta_description']           = 'Meta Description';
$_['column_tags']           = 'Tags';

?>