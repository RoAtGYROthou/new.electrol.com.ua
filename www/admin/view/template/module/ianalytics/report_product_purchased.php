<?php
	$this->language->load('report/product_purchased');

	$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['fromDate'])) {
			$filter_date_start = $this->request->get['fromDate'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['toDate'])) {
			$filter_date_end = $this->request->get['toDate'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filterOrders'])) {
			$filter_order_status_id = $this->request->get['filterOrders'];
		} else {
			$filter_order_status_id = 0;
		}

	if (isset($this->request->get['page'])) {
		$page = $this->request->get['page'];
	} else {
		$page = 1;
	}

	$this->load->model('report/product');
 
	$this->data['products'] = array();

	$data2 = array(
		'filter_date_start'	  => $filter_date_start, 
		'filter_date_end'		=> $filter_date_end, 
		'filter_order_status_id' => $filter_order_status_id,
		'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
		'limit'                  => $this->config->get('config_admin_limit')
	);

	$product_total = $this->model_report_product->getTotalPurchased($data2);

	$results = $this->model_report_product->getPurchased($data2);

	foreach ($results as $result) {
		$this->data['products'][] = array(
			'name'       => $result['name'],
			'model'      => $result['model'],
			'quantity'   => $result['quantity'],
			'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
		);
	}

	$this->data['token'] = $this->session->data['token'];

	$url = '';

	if (isset($this->request->get['fromDate'])) {
		$url .= '&fromDate=' . $this->request->get['fromDate'];
	}

	if (isset($this->request->get['toDate'])) {
		$url .= '&toDate=' . $this->request->get['toDate'];
	}

	if (isset($this->request->get['filterOrders'])) {
		$url .= '&filterOrders=' . $this->request->get['filterOrders'];
	}

	$pagination = new Pagination();
	$pagination->total = $product_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_admin_limit');
	$pagination->text = $this->language->get('text_pagination');
	$pagination->url = $this->url->link('module/ianalytics', 'token=' . $this->session->data['token'] . $url . '&page={page}');

	$this->data['pagination'] = $pagination->render();		
?>

<?php if ($this->data['products']) { ?>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
	function drawVisualization() {
        var wrapper = new google.visualization.ChartWrapper({
          chartType: 'ColumnChart',
          dataTable: [['', 
		  <?php foreach ($this->data['products'] as $product) { ?>
            '<?php echo $product['name']; ?>', 
	 	 <?php } ?>
		  ], ['', 
			<?php foreach ($this->data['products'] as $product) { ?>
            <?php echo $product['quantity']; ?>, 
	 		 <?php } ?>]],
          options: {'title': 'Most ordered products',
		  			'width': $('#aftersale').width() - 270,
		  			'height': 400},
          containerId: 'visualization'
        });
        wrapper.draw();
      }
    </script>
    <div id="visualization" style="overflow:hidden;"></div>
<?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
	<table class="table-hover table table-striped">
    	<thead>
          <tr>
            <th>Product Name</th>
            <th>Model</th>
            <th>Quantity</th>
            <th>Total</th>
          </tr>
        </thead>
        
	<?php if ($this->data['products']) { ?>
	 	 <?php foreach ($this->data['products'] as $product) { ?>
              <tr>
                <td class="left"><?php echo $product['name']; ?></td>
                <td class="left"><?php echo $product['model']; ?></td>
                <td class="right"><?php echo $product['quantity']; ?></td>
                <td class="right"><?php echo $product['total']; ?></td>
              </tr>
	 	 <?php } ?>
	<?php } else { ?>
          <tr>
            <td class="center" colspan="4">There are no results yet!</td>
          </tr>
	  <?php } ?>
	  </table>
	<div class="pagination"><?php echo $this->data['pagination']; ?></div>