<?php 
$searchvar = 'search';
if (defined('VERSION') && (VERSION == '1.5.4' || VERSION == '1.5.4.1' || VERSION == '1.5.3' || VERSION == '1.5.3.1' || VERSION == '1.5.2' || VERSION == '1.5.2.1' || VERSION == '1.5.1' || VERSION == '1.5.1.3' || VERSION == '1.5.0')) {
	$searchvar = 'filter_name';
}
?>
<div style="height:5px;"></div>
<table class="form">
  <tr>
  	<td colspan="2"><h3>Dashboard</h3></td>
    <td colspan="2"><?php require('element_filter.php'); ?></td>
  </tr>
 <tr>
    <td style="width:50%;border-bottom:none;" colspan="2">
		<div class="caption">
            <h4>Daily Visitors<div style="float:right;"><a href="#daily-total-stats-more" data-toggle="tab" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-th-list"></span>&nbsp; More</a></div></h4>
		</div>
        <div class="thumbnail">
			<div class="openRateChart" style="position: relative; ">
            	<?php if (!($iAnalyticsVisitorsDataByDay[0]=="No Data Gathered Yet")) { ?>
				<script type="text/javascript">
				function drawVisualizationDashboard1() {
					var data = google.visualization.arrayToDataTable([
					  ['Part of the day', 'Unique Visits'],
					  <?php foreach($iAnalyticsVisitorsDataByDay as $j => $k): ?>
						<?php if ($j<1) { ?>
						 <?php } else { ?>
						['<?php echo $k[0]?>', <?php echo $k[1]?>],
					<?php } endforeach; ?>
					]);
			
					var options = {
					  hAxis: {title: 'Days',  titleTextStyle: {color: '#333'}, slantedText:false },
					  width: $('#dashboard1').width()/2.5,
					  height: 300,
					  chartArea: {
						  left: 50
					  }
					};
			
					var chart = new google.visualization.AreaChart(document.getElementById('drawVisualizationDashboard1'));
					chart.draw(data, options);
				  }
				</script>
				<div id="drawVisualizationDashboard1"></div>
                <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
            </div>
        </div>
    </td>
    <td style="width:25%;border-bottom:none;">
    	 <div class="caption">
            <h4>Traffic Sources<div style="float:right;"><a href="#traffic-sources-more" data-toggle="tab" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-th-list"></span>&nbsp; More</a></div></h4>
          </div>
		<div class="thumbnail">
			<div class="openRateChart" style="position: relative; ">
           		<?php if (!($iAnalyticsVisitorsDataReferersPie[1][0]==NULL && $iAnalyticsVisitorsDataReferersPie[1][1]==NULL && $iAnalyticsVisitorsDataReferersPie[1][2]==NULL && $iAnalyticsVisitorsDataReferersPie[1][3]==NULL)) { ?>
            		 <script type="text/javascript">
                        function drawVisualizationDashboard2() {
                            var data = google.visualization.arrayToDataTable([
                              ['Referer', 'Visits'],
							  <?php foreach($iAnalyticsVisitorsDataReferersPie as $j => $k): ?>
                            	<?php if ($j<1) { ?>
                           		 <?php } else { ?>
								['Direct hits', <?php echo (isset($k[0])) ? $k[0] : '0'; ?>],
								['Social networks', <?php echo (isset($k[1])) ? $k[1] : '0'; ?>],
								['Search engines', <?php echo (isset($k[2])) ? $k[2] : '0'; ?>],
								['Other', <?php echo (isset($k[3])) ? $k[3] : '0'; ?>],
							<?php } endforeach; ?>
                            ]);
                    
                            var options = {
							  width: $('#dashboard1').width()/4.6,
							  pieStartAngle: 135,
                              height: 300,
							  chartArea: {
								  left: 50
							  },
							  legend: {
								 position: 'right',
								 textStyle: {
									 fontSize: '11'
								 }
							  },
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('drawVisualizationDashboard2'));
                            chart.draw(data, options);
                          }
                        </script>
					<?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
				<div id="drawVisualizationDashboard2"></div>
            </div>
        </div>
    </td>
    <td style="width:25%;border-bottom:none;vertical-align:top;height:100%;" rowspan="2">
    	 <div class="caption">
         	<?php $var = 0; if (isset($iAnalyticsFunnelData[2][0]) && $iAnalyticsFunnelData[1][0]==0 && isset($iAnalyticsFunnelData[7][0]) && $iAnalyticsFunnelData[7][0]==6) 									
			{ $var = ($iAnalyticsFunnelData[7][1]/$iAnalyticsFunnelData[1][1])*(100); $var = number_format($var, 2); } ?>
            <h4>Conversion Rate: <?php echo ($var>0) ? $var.'%' : 'N/A'; ?><div style="float:right;"><a href="#conversion-rate-more" data-toggle="tab" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-th-list"></span>&nbsp; More</a></div></h4>
          </div>
		<div class="thumbnail">
			<center><div id="funnelDashboard"></div></center>
                        <?php if (isset($iAnalyticsFunnelData[2][0])) { ?>
							<?php $stage[0]=0; $stage[1]=0; $stage[2]=0; $stage[3]=0; $stage[4]=0; $stage[5]=0; $stage[6]=0; ?>
                            <?php foreach($iAnalyticsFunnelData as $j => $k): 
                                    if ($j<1) { 
                                    } else { 
                                          if ($k[0]==0) $stage[0]=$k[1];
                                          else if ($k[0]==1) $stage[1]=$k[1];
                                          else if ($k[0]==2) $stage[2]=$k[1];
                                          else if ($k[0]==3) $stage[3]=$k[1];
                                          else if ($k[0]==4) $stage[4]=$k[1]; 
                                          else if ($k[0]==5) $stage[5]=$k[1]; 
                                          else if ($k[0]==6) $stage[6]=$k[1];
                                    } 
                                endforeach; ?>
                            <script type="text/javascript">
                                var funnelData = [
                                    ['First Visit', <?php echo $stage[0]; ?>],
                                    ['Add to Cart', <?php echo $stage[1] ?>], 
                                    ['Login/Register', <?php echo $stage[2] ?>], 
                                    ['Delivery Method', <?php echo $stage[3] ?>], 
                                    ['Payment method', <?php echo $stage[4] ?>], 
                                    ['Confirm order', <?php echo $stage[5] ?>],  
                                    ['Successful order', <?php echo $stage[6] ?>]];
                                var chart = new FunnelChart(funnelData, 350, 585, 1/3);
                                chart.draw('#funnelDashboard', 2);
                            </script>
                         <?php } else {
							echo '<center>There is no data yet for a chart.</center>'; } ?>
        </div>
    </td>
  </tr>
  <tr>
    <td style="width:50%;border-bottom:none;" colspan="3">
    	<table style="width:100%">
        	<tr>
            	<td style="width:33%;padding-right: 20px;">
                    <div class="caption">
                        <h4>Sales<div style="float:right;"><a href="#sales-report-more" data-toggle="tab" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-th-list"></span>&nbsp; More</a></div></h4>
                    </div>
                    <div class="thumbnail">
                        <div class="openRateChart" style="position: relative; ">
                          <?php 
                            if (isset($this->request->get['fromDate'])) {
                                $filter_d_start = $this->request->get['fromDate'];
                            } else {
                                $filter_d_start = '';
                            }
                    
                            if (isset($this->request->get['toDate'])) {
                                $filter_d_end = $this->request->get['toDate'];
                            } else {
                                $filter_d_end = '';
                            }
                    
                            if (isset($this->request->get['filterOrders'])) {
                                $filter_order_st_id = $this->request->get['filterOrders'];
                            } else {
                                $filter_order_st_id = 0;
                            }
                            
                            $filter_gr = 'day';
                            $pag = 1;
                            $this->load->model('report/sale');
                
                            $this->data['ordersDashboard'] = array();
                        
                            $ordersDashboard = array(
                                'filter_date_start'		=> $filter_d_start, 
                                'filter_date_end'	     => $filter_d_end, 
                                'filter_group'           => $filter_gr,
                                'filter_order_status_id' => $filter_order_st_id,
                                'start'                  => ($pag - 1) * $this->config->get('config_admin_limit'),
                                'limit'                  => $this->config->get('config_admin_limit')
                            );
                            
                            $resultsOrders = $this->model_catalog_ianalytics->getOrders($ordersDashboard);
                        
                            foreach ($resultsOrders as $result) {
                                $this->data['ordersDashboard'][] = array(
                                    'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
                                    'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
                                    'orders'     => $result['orders'],
                                    'tax'        => $this->currency->format($result['tax'], $this->config->get('config_currency')),
                                    'tax_nocurrency'        => isset($result['tax']) ? $result['tax'] : '0',
                                    'total'      => $this->currency->format($result['total'], $this->config->get('config_currency')),
                                    'total_nocurrency'      => $result['total']
                                );
                            } 
                        ?>	
                            <?php if (isset($this->data['ordersDashboard']) && (!empty($this->data['ordersDashboard']))) { ?>
                                <script type="text/javascript">
                                function drawVisualizationDashboard3() {
                                    var data = google.visualization.arrayToDataTable([
                                      ['Day', 'Taxes', 'Revenue','Total'],
                                      <?php $reversed = array_reverse($this->data['ordersDashboard'], true); ?>
                                        <?php foreach ($reversed as $order) { ?>
                                          ['<?php echo $order['date_start']; ?>', <?php echo $order['tax_nocurrency']; ?>, <?php echo $order['total_nocurrency']-$order['tax_nocurrency']; ?>, <?php echo $order['total_nocurrency']; ?>],
                                        <?php } ?>
                                    ]);
                            
                                    var options = {
                                      hAxis: {slantedText:false },
                                      vAxis: {minValue: 0},
                                      height: 200,
									  width: $('#dashboard1').width()/3,
                                      colors: ['#3B15B5', '#FF0000', '#00A08A'],
									  chartArea: {
										  left: 50
									  }
                                    };
                            
                                    var chart = new google.visualization.AreaChart(document.getElementById('drawVisualizationDashboard3'));
                                    chart.draw(data, options);
                                  }
                                </script>
                                <div id="drawVisualizationDashboard3"></div>
                            <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
                        </div>
                    </div>
                </td>
                <td style="width:33%">
                	<div class="caption">
                        <h4>Most Searched Keywords<div style="float:right;"><a href="#searched-keywords-more" data-toggle="tab" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-th-list"></span>&nbsp; More</a></div></h4>
                    </div>
                    <div class="thumbnail">
                    <?php if (!($iAnalyticsMostSearchedKeywordsPie=="No Data Gathered Yet:0:#3F4E21;")) { ?>
                      <div class="openRateChart pieable" data-num="<?php echo $iAnalyticsMostSearchedKeywordsPie?>" style="position: relative; "></div>
                    <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
                    </div>
                </td>
            </tr>        
        </table>
    </td>
  </tr>
</table>