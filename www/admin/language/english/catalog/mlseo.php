<?php
// Heading
$_['heading_title']    = 'Multi-Language SEO by <a href="http://www.apwai.net" onclick="return !window.open(this.href)"> APWAI.NET</a>';

// Text 
$_['text_success']     = 'Success: You have saved Multi-Language SEO!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Multi-Language SEO!';
?>