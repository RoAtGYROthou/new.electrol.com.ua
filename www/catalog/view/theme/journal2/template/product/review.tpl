<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review-list"   itemtype="http://schema.org/Review">
  <div class="author"><b ><?php echo $review['author']; ?></b> <?php echo $text_on; ?> <span ><?php echo $review['date_added']; ?></span></div>
  <div class="rating"   itemtype="http://schema.org/Rating"><img width="83" height="15" src="catalog/view/theme/default/image/stars-<?php echo $review['rating'] . '.png'; ?>" alt="<?php echo $review['rating']; ?>" title="<?php echo $review['rating']; ?> "/><meta  content="1"> ( <span ><?php echo $review['rating']; ?></span> / <span >5</span> )</div>
  <div class="text" ><?php echo $review['text']; ?></div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_reviews; ?></div>
<?php } ?>
