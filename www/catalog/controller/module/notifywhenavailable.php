<?php  
class ControllerModuleNotifyWhenAvailable extends Controller {
	protected function index($setting) {
		$this->data['currenttemplate'] = $this->config->get('config_template');
	
		$this->document->addScript('catalog/view/javascript/notifywhenavailable/notifywhenavailable.js');
			
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['data']['NotifyWhenAvailable'] = str_replace('http', 'https', $this->config->get('NotifyWhenAvailable'));
		} else {
			$this->data['data']['NotifyWhenAvailable'] = $this->config->get('NotifyWhenAvailable');
		}
		$this->data['data']['NotifyWhenAvailableConfig'] = $this->config->get('notifywhenavailable_module');
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/notifywhenavailable.tpl') && file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/notifywhenavailable.css')) {
			$this->template = $this->config->get('config_template') . '/template/module/notifywhenavailable.tpl';
			$this->document->addStyle('catalog/view/theme/'.$this->data['currenttemplate'].'/stylesheet/notifywhenavailable.css');	
		} else {
			$this->template = 'default/template/module/notifywhenavailable.tpl';
			$this->document->addStyle('catalog/view/theme/default/stylesheet/notifywhenavailable.css');	
		}
		
		if(!isset($this->data['data']['NotifyWhenAvailable']['CustomTitle'][$this->config->get('config_language')])){
			$this->data['data']['NotifyWhenAvailable']['CustomTitle'] = '';
		} else {
			$this->data['data']['NotifyWhenAvailable']['CustomTitle'] = $this->data['data']['NotifyWhenAvailable']['CustomTitle'][$this->config->get('config_language')];
		}
		
		$this->response->setOutput($this->render());	
	}
	
	public function submitform() {
		$send_email = false;
		if (isset($this->request->post['NWAYourName']) && isset($this->request->post['NWAYourEmail'])) {
			$run_query = $this->db->query("INSERT INTO `" . DB_PREFIX . "notifywhenavailable` 
					(customer_email, customer_name, product_id, date_created, language)
					VALUES ('".$this->db->escape($this->request->post['NWAYourEmail'])."', '".$this->db->escape($this->request->post['NWAYourName'])."', '".$this->db->escape($this->request->post['NWAProductID'])."', NOW(), '".$this->config->get('config_language')."')
			");
			
			if ($run_query) { 
				echo "Success!";
				$send_email = true;
			}					 
		} else {
			echo "Error! There is no data!";
		}
			
		if ($send_email==true) {
			$this->data['data']['NotifyWhenAvailable'] = $this->config->get('NotifyWhenAvailable');
			if ($this->data['data']['NotifyWhenAvailable']['Notifications']=='yes')
			{
				$text = "Someone wants to be notified about a product that currently is out of stock!<br /><br />
				User Name: ".$this->request->post['NWAYourName']."<br />
				User Email: ".$this->request->post['NWAYourEmail']."<br />";	
							
				if (isset($this->request->post['NWAProductID'])) {
					$this->load->model('catalog/product');
					$product = $this->model_catalog_product->getProduct($this->request->post['NWAProductID']);
					$text .= "Selected Product: ".$product['name']."<br /><br />"; 
				}
				$text .= "You can find more information <a href='".HTTP_SERVER."admin/index.php?route=module/notifywhenavailable&token=".$this->session->data['token']."'>here</a>!";	
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject(html_entity_decode("Someone used the option NotifyWhenAvailable", ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($text);
				$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
		}
	}
		
	public function shownotifywhenavailableform() {
		$this->language->load('module/notifywhenavailable');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['NotifyWhenAvailable_Title'] = $this->language->get('NotifyWhenAvailable_Title');
		$this->data['NotifyWhenAvailable_SubmitButton'] = $this->language->get('NotifyWhenAvailable_SubmitButton');
		$this->data['NotifyWhenAvailable_Error1'] = $this->language->get('NotifyWhenAvailable_Error1');
		$this->data['NotifyWhenAvailable_Error2'] = $this->language->get('NotifyWhenAvailable_Error2');
		$this->data['NotifyWhenAvailable_Success'] = $this->language->get('NotifyWhenAvailable_Success');
		$this->data['NWA_YourName'] = $this->language->get('NWA_YourName');
		$this->data['NWA_YourEmail'] = $this->language->get('NWA_YourEmail');
		$this->data['currenttemplate'] = $this->config->get('config_template');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['data']['NotifyWhenAvailable'] = str_replace('http', 'https', $this->config->get('NotifyWhenAvailable'));
		} else {
			$this->data['data']['NotifyWhenAvailable'] = $this->config->get('NotifyWhenAvailable');
		}
		$this->data['data']['NotifyWhenAvailableConfig'] = $this->config->get('notifywhenavailable_module');
		
		if(!isset($this->data['data']['NotifyWhenAvailable']['CustomText'][$this->config->get('config_language')])){
			$this->data['data']['NotifyWhenAvailable']['CustomText'] = '';
		} else {
			$this->data['data']['NotifyWhenAvailable']['CustomText'] = $this->data['data']['NotifyWhenAvailable']['CustomText'][$this->config->get('config_language')];
		}
		
		if ((isset($this->request->get['route'])) && ($this->request->get['route']=="product/product")) {
			$this->data['NotifyWhenAvailableCurrentURL'] = $this->url->link("product/product","product_id=".$this->request->get['product_id'],"");
		} else {
			if (strpos(HTTP_SERVER,'www.') && strpos(HTTPS_SERVER,'www.')) {
				$siteName = $_SERVER["SERVER_NAME"];
			} else {
				$siteName = str_replace("www.", "", $_SERVER['SERVER_NAME']);
			}
			$this->data['NotifyWhenAvailableCurrentURL'] = "http://".$siteName.$_SERVER["REQUEST_URI"];  
		}
	
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$checker = $this->customer->getId();
		if (!empty($checker)) {
			$this->data['customer_email'] = $this->customer->getEmail();
			$this->data['customer_name'] = $this->customer->getFirstName().' '.$this->customer->getLastName();
		} else {
			$this->data['customer_email'] = '';
			$this->data['customer_name'] = '';
		}

		$this->data['NotifyWhenAvailableProductID'] = $product_id;		
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/notifywhenavailable_form.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/notifywhenavailable_form.tpl';
		} else {
			$this->template = 'default/template/module/notifywhenavailable_form.tpl';
		}
		
		$this->response->setOutput($this->render());								
	}
	
	public function sendMails() {
		$settings = $this->config->get('NotifyWhenAvailable');
		
		if (isset($settings['Enabled']) && $settings['Enabled']=='yes' && isset($settings['ScheduleEnabled']) && $settings['ScheduleEnabled']='yes') {
			$query = $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "notifywhenavailable` super 
				JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
				WHERE customer_notified=0 and language_id = " . (int)$this->config->get('config_language_id') . "
				ORDER BY `date_created` DESC");
			
			$this->load->model('catalog/product');
			$counter = 0;
			$report = array();
			foreach ($query->rows as $row) {
				$product = $this->model_catalog_product->getProduct($row['product_id']);
				if ($product['quantity']>0) {
					$this->sendEmailWhenAvailable($row);
					$counter++;	
					$report[] = $row['customer_name'].' ('.$row['customer_email'].') - '.$row['product_name'];
				}
			}
			
			$result = "Cron was executed successfully! A total of <strong>".$counter."</strong> emails were sent to the customers.<br />";
			
			if ($counter>0 && sizeof($report)>0) {
				$result .= "<br />Here is a list with the notified customers:<br /><ul>";
				foreach ($report as $rep) {
					$result .= "<li>".$rep."</li>";	
				}
				$result .= "</ul>";
			}

			if (isset($settings['CronNotify']) && $settings['CronNotify']=='yes') {
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject(html_entity_decode('NotifyWhenAvailable Sheduled Task', ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($result);
				$mail->setText(html_entity_decode($result, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			} else {
				echo $result;	
			}
			
		}
	}
	
	public function sendEmailWhenAvailable($row) {
		$this->load->model('catalog/product');	
		$this->load->model('tool/image');
		
		$product_id = $row['product_id'];
		$product_info = $this->model_catalog_product->getProduct($product_id);
		if ($product_info['image']) { $image = $this->model_tool_image->resize($product_info['image'], 200, 200); } else { $image = false; }
			
		$NotifyWhenAvailable = $this->config->get('NotifyWhenAvailable');
				
		if(!isset($NotifyWhenAvailable['EmailText'][$row['language']])){
			$EmailText = '';
			$EmailSubject = '';
		} else {
			$EmailText = $NotifyWhenAvailable['EmailText'][$row['language']];
			$EmailSubject = $NotifyWhenAvailable['EmailSubject'][$row['language']];
		}
	
		$string = html_entity_decode($EmailText);
		$patterns = array();
		$patterns[0] = '/{c_name}/';
		$patterns[1] = '/{p_name}/';
		$patterns[2] = '/{p_image}/';
		$patterns[3] = '/http:\/\/{p_link}/';
		$replacements = array();
		$replacements[0] = $row['customer_name'];
		$replacements[1] = "<a href='".HTTP_SERVER."index.php?route=product/product&product_id=".$product_id."' target='_blank'>".$product_info['name']."</a>";
		$replacements[2] = "<img src='".$image."' />";
		$replacements[3] = HTTP_SERVER."index.php?route=product/product&product_id=".$product_id;

		$text = preg_replace($patterns, $replacements, $string);
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');
		$mail->setTo($row['customer_email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($EmailSubject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($text);
		$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		$update_customers = $this->db->query("UPDATE `" . DB_PREFIX . "notifywhenavailable` SET customer_notified=1 WHERE product_id = ".$product_id."");	
	}
}
?>