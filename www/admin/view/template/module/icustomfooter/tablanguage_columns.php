<div class="columns_left_menu">
    <ul class="nav nav-stacked nav-tabs column_tabs">
        <?php foreach($columns as $index_3 => $column) : ?>
        <li<?php echo $index_3 == 0 ? ' class="active"' : ''; ?> <?php if (!empty($extraColumnAttributes[$column['var']])) echo $extraColumnAttributes[$column['var']]; ?>><a data-target="#column<?php echo $index; ?>_<?php echo $index_1; ?>_<?php echo $index_3; ?>"><?php echo $$column['var']; ?><i class="icon-chevron-right<?php echo $index_3 == 0 ? ' icon-white' : ''; ?>"></i></a></li>
        <?php endforeach; ?>
        <?php for ($i = 1; $i <= $customColumnCount; $i++) : ?>
        <li><a data-target="#column<?php echo $index; ?>_<?php echo $index_1; ?>_<?php echo ($index_3 + $i); ?>"><?php echo $customcolumn . ' ' . $i; ?><i class="icon-chevron-right"></i></a></li>
        <?php endfor; ?>
    </ul>
</div>
<div class="columns_content">
    <div class="tab-content column_content">
        <?php foreach($columns as $index_3 => $column) : ?>
        <div class="tab-pane<?php echo $index_3 == 0 ? ' active' : ''; ?>" id="column<?php echo $index; ?>_<?php echo $index_1; ?>_<?php echo $index_3; ?>">
            <h3><?php echo $$column['var']; ?></h3>
            <div class="iModuleFields">
            <?php include($column['file']); ?>
            <div class="clearfix"></div>
            </div>
        </div>
        <?php $globalSubtabsIndex++; endforeach; ?>
        <?php for ($i = 1; $i <= $customColumnCount; $i++) : ?>
        <div class="tab-pane" id="column<?php echo $index; ?>_<?php echo $index_1; ?>_<?php echo ($index_3 + $i); ?>">
            <h3><?php echo $customcolumn . ' ' . $i; ?></h3>
            <div class="iModuleFields">
            <?php include($dirName . 'columncustom_column.php'); ?>
            <div class="clearfix"></div>
            </div>
        </div>
        <?php $globalSubtabsIndex++; endfor; ?>
    </div>
</div>