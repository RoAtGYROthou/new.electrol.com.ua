<?php  
class ControllerModuleDisquscomments extends Controller {
	protected function index() {
		$this->language->load('module/disquscomments');

      	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['currenttemplate'] =  $this->config->get('config_template');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['data']['DisqusComments'] = str_replace('http', 'https', $this->config->get('DisqusComments'));
		} else {
			$this->data['data']['DisqusComments'] = $this->config->get('DisqusComments');
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/disquscomments.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/disquscomments.tpl';
		} else {
			$this->template = 'default/template/module/disquscomments.tpl';
		}
		$this->render();
	}
}
?>