<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/avail/stylesheet/availability.css">
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
<script src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
</head>
<body>
<form id="notify">
	<h4><?php echo $heading_title;?></h4>
	<div class="center">
		<label for="input-product"><?php echo $entry_product; ?></label><br>
		<input type="text" name="product" id="input-product" value="<?php echo $product_name;?>">
	</div>
	<div class="hidden">
		<label for="input-product-id"><?php echo $entry_product;?></label><br>
		<input type="text" name="product_id" id="input-product-id" value="<?php echo $product_id; ?>">
	</div>
	<div class="hidden">
		<label for="input-admin-email"><?php echo $entry_admin_mail;?></label><br>
		<input type="text" name="admin_email" id="input-admin-email" value="<?php echo $admin_email;?>">
	</div>	
	<div class="center">
		<label for="input-price"><?php echo $entry_price;?></label><br>
		<input type="text" name="price" id="input-price" value="<?php echo $price; ?>">
	</div>
	<div class="center">
		<label for="input-name"><?php echo $entry_name; ?></label><br>
		<input type="text" name="name" id="input-name">
	</div>
	<div class="center">
		<label for="input-email"><?php echo $entry_mail; ?></label><br>
		<input type="text" name="email" id="input-email">
	</div>
	<div class="center">
		<label for="textarea-enquiry"><?php echo $entry_enquiry; ?></label><br>
		<textarea name="enquiry" rows="3" cols="50" id="textarea-enquiry"></textarea>
	</div>
	<?php if ($captcha_status == '1')  { ?>
		<div class="center">
			<label for="input-captcha"><?php echo $entry_captcha;?></label><br>
			<input type="text" name="captcha" value="<?php echo $captcha; ?>"><br>
			<img src="index.php?route=information/contact/captcha" alt="" />
		</div>
	<?php } ?>
	<div class="center">
		<input type="submit" class="button center" value="<?php echo $button_send;?>">
	</div>
	
</form>
</body>
</html>
<script type="text/javascript">
	$("#notify").on("submit", function(event){
		event.preventDefault();		
		var form = $(this);
		var submitButton = form.find('input[type=submit]');
		submitButton.attr('disabled', 'disabled');
		var str = form.serialize();				
		$.ajax({
                    url : 'index.php?route=module/avail/save',
                    type : 'POST',
					dataType: 'json',
                    data : str
		})
			.success(function(json) {
								
				if (json['success']) {
					$(".error").remove();
					submitButton.attr('disabled', 'disabled');
					var message = "<div class='success-block'>" + json['success'] + "</div>";	
					$(".center").last().after(message);
				}				
				if (json['error_name']) {				
					$('input[name=name]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');					
					$('input[name=name]').after("<div class='error'>" + json['error_name'] + "</div>");
				} else {
					$('input[name=name]').parent().find(".error").remove();
				}
				if (json['error_email']) {					
					$('input[name=email]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=email]').after("<div class='error'>" + json['error_email'] + "</div>");
				} else {
					$('input[name=email]').parent().find(".error").remove();
				}
				if (json['error_product']) {					
					$('input[name=product]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=product]').after("<div class='error'>" + json['error_product'] + "</div>");
				} else {
					$('input[name=product]').parent().find(".error").remove();
				}
				if (json['error_price']) {					
					$('input[name=price]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=price]').after("<div class='error'>" + json['error_price'] + "</div>");
				} else {
					$('input[name=price]').parent().find(".error").remove();
				}
				if (json['error_captcha']) {					
					$('input[name=captcha]').parent().find(".error").remove();
					submitButton.removeAttr('disabled');
					$('input[name=captcha]').after("<div class='error'>" + json['error_captcha'] + "</div>");
				} else {
					$('input[name=captcha]').parent().find(".error").remove();
				}				
						
			})			
			.error(function(json){
					$(".error").remove();
					submitButton.attr('disabled', 'disabled');
					var message = "<div class='error'>" + json + "</div>";
					$(".center").last().after(message);
			});
	});
</script>