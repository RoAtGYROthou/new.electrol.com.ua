<table class="form">
 <tr>
  <td>Admin notification by Email:<span class="help">You will receive an email when someone wants to be notified for out of stock product</span></td>
    <td>
        <select name="NotifyWhenAvailable[Notifications]" class="NotifyWhenAvailableNotifications">
            <option value="yes" <?php echo ((isset($data['NotifyWhenAvailable']['Notifications']) && $data['NotifyWhenAvailable']['Notifications'] == 'yes')) ? 'selected=selected' : '' ?>>Enabled</option>
           <option value="no" <?php echo ((isset($data['NotifyWhenAvailable']['Notifications']) && $data['NotifyWhenAvailable']['Notifications'] == 'no')) ? 'selected=selected' : '' ?>>Disabled</option>
        </select>
   </td>
  </tr>  
   <tr>
    <td>Popup Width:<span class="help">In Pixels</span></td>
    <td>
      <div class="input-append"><input type="text" name="NotifyWhenAvailable[PopupWidth]" class="NotifyWhenAvailablePopupWidth input-mini" value="<?php echo (isset($data['NotifyWhenAvailable']['PopupWidth'])) ? $data['NotifyWhenAvailable']['PopupWidth'] : '250' ?>" /></input><span class="add-on">px</span></div><br />
   </td>
  </tr>
   <tr>
<td>Design:<span class="help">Use this codes:<br /><br />
    {name_field} - Name<br />
    {email_field} - Email <br />
    {submit_button} - Submit</span></td>
<td>
   <?php foreach ($languages as $language) { ?>
    <img src="view/image/flags/<?php echo $language['image']; ?>" style="float:left;position:absolute;margin-left:-20px;" title="<?php echo $language['name']; ?>" />
   Popup Title: <input name="NotifyWhenAvailable[CustomTitle][<?php echo $language['code']; ?>]" class="input-xxlarge" type="text" value="<?php echo (isset($data['NotifyWhenAvailable']['CustomTitle'][$language['code']])) ? $data['NotifyWhenAvailable']['CustomTitle'][$language['code']] : 'Out of stock!' ?>" />
<textarea id="description_<?php echo $language['code']; ?>" name="NotifyWhenAvailable[CustomText][<?php echo $language['code']; ?>]" style="width:300px;height:80px;"  class="NotifyWhenAvailableCustomText"><?php echo (isset($data['NotifyWhenAvailable']['CustomText'][$language['code']])) ? $data['NotifyWhenAvailable']['CustomText'][$language['code']] : '<p align="left"><span style="line-height: 1.6em;">Fill your email below and we will notify you as soon as the product is back in stock!</span></p>
<p align="left">Name: {name_field}</p>
<p align="left">Email: {email_field}</p>
<p align="left">{submit_button}</p>' ?></textarea>
 <br /><?php } ?>
</td>
</tr>
   <tr>
<td>Email Text:<span class="help">Use this codes:<br /><br />
    {c_name} - Customer Name<br />
    {p_name} - Product Name<br />
    {p_image} - Product Image<br />
    {p_link} - Product Link</span></td>
<td>
   <?php foreach ($languages as $language) { ?>
    <img src="view/image/flags/<?php echo $language['image']; ?>" style="float:left;position:absolute;margin-left:-20px;" title="<?php echo $language['name']; ?>" />
  Email Subject: <input name="NotifyWhenAvailable[EmailSubject][<?php echo $language['code']; ?>]" class="input-xxlarge" type="text" value="<?php echo (isset($data['NotifyWhenAvailable']['EmailSubject'][$language['code']])) ? $data['NotifyWhenAvailable']['EmailSubject'][$language['code']] : 'The product is back in stock!' ?>" />
<textarea id="email_text_<?php echo $language['code']; ?>" name="NotifyWhenAvailable[EmailText][<?php echo $language['code']; ?>]" style="width:300px;height:80px;"  class="NotifyWhenAvailableEmailText"><?php echo (isset($data['NotifyWhenAvailable']['EmailText'][$language['code']])) ? 				$data['NotifyWhenAvailable']['EmailText'][$language['code']] : '<p align="center"><b>Hello, {c_name}!</b></p>
<p align="center">We are happy to notify you that the product you were interested in, {p_name}&nbsp;is back in stock!</p>
<p align="center">{p_image}</p>
<p align="center"><strong>What are you waiting for?&nbsp;<a href="http://{p_link}" target="_blank">Order now</a>!</strong></p>' ?></textarea><br />
 <?php } ?>
</td>
</tr>
  <tr>
<td>Custom CSS:</td>
<td>
<textarea name="NotifyWhenAvailable[CustomCSS]" class="NotifyWhenAvailableCustomCSS"><?php echo (isset($data['NotifyWhenAvailable']['CustomCSS'])) ? $data['NotifyWhenAvailable']['CustomCSS'] : '' ?></textarea>
</td>
</tr>
</table>
<?php $token = $_GET['token']; ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description_<?php echo $language['code']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
CKEDITOR.replace('email_text_<?php echo $language['code']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>