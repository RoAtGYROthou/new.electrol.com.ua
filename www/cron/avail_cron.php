<?php
if (is_file('../admin/config.php')) {
	require_once('../admin/config.php');		
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = new Registry();

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

foreach ($query->rows as $setting) {
	if (!$setting['serialized']) {
		$config->set($setting['key'], $setting['value']);
	} else {
		$config->set($setting['key'], unserialize($setting['value']));
	}
}

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Front Controller
$controller = new Front($registry);

// Url
$url = new Url(HTTP_SERVER, $config->get('config_secure') ? HTTPS_SERVER : HTTP_SERVER);
$registry->set('url', $url);
		
// SEO URL's
if (!$seo_type = $config->get('config_seo_url_type')) {
	$seo_type = 'seo_url';
}
$controller->addPreAction(new Action('common/' . $seo_type));

// Language
$languages = array();

$query = $db->query("SELECT * FROM `" . DB_PREFIX . "language`");

foreach ($query->rows as $result) {
	$languages[$result['code']] = $result;
}

$config->set('config_language_id', $languages[$config->get('config_admin_language')]['language_id']);

// Language
$language = new Language($languages[$config->get('config_admin_language')]['directory']);
$language->load($languages[$config->get('config_admin_language')]['directory']);
$registry->set('language', $language);

//availability
		function changeMailStatus($db, $id, $status) {
			if ($db->query("UPDATE " . DB_PREFIX . "avail SET status = '" . (int) $status . "' WHERE id = '" . (int) $id . "'")) {
				return true;            
			} else {
				return false;
			}
		}
		$notify = $db->query("select * from " . DB_PREFIX . "product p, " . DB_PREFIX. "avail a where p.quantity > 0 AND p.product_id = a.product_id AND a.status = 0");
	
		$notify = $notify->rows;
		$lang = $loader->language('module/avail');
		
		if (!empty($notify)) {
			foreach ( $notify as $info ) {				
				$product_link = $url->link("product/product", "product_id=" . $info['product_id']);
				
				$mail = new Mail();				
				$mail->protocol = $config->get('config_mail_protocol');
				$mail->parameter = $config->get('config_mail_parameter');
				$mail->hostname = $config->get('config_smtp_host');
				$mail->username = $config->get('config_smtp_username');
				$mail->password = $config->get('config_smtp_password');
				$mail->port = $config->get('config_smtp_port');
				$mail->timeout = $config->get('config_smtp_timeout');
				
				$mail->setTo($info['email']);
				$mail->setFrom($config->get('config_email'));
				$mail->setSender('\''.$lang['email_subject'].'\'');
				$mail->setSubject(html_entity_decode(sprintf($lang['email_subject'], $info['name'], ENT_QUOTES, 'UTF-8')));

				$mail_text = "<!DOCTYPE html><html><head><meta http-equiv=«Content-Type» content=«text/html; charset=utf-8»></head><body>";
				$mail_text .="<p>" . html_entity_decode($info['name'].', '.$lang['text_mail_send']. "</p>");
				$mail_text .= "<p>" . $lang['text_product'] .': ' . $info['product'] . "</p>";
				$mail_text .= "<p>" . $lang['text_link_page'] . ": " . " <a href=" . $product_link . ">" . $info['product'] . "</a></p>";				
				$mail_text .= "<p>" . $lang['text_price'] . ': ' . $info['price'] . "</p></body></html>";

				$mail->setHtml($mail_text);
				$mail->send();
				changeMailStatus($db,$info['id'],1);				
			}
			echo "OK!";
		}	
 ?>