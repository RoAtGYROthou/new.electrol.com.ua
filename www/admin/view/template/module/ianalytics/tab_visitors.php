<?php 
$searchvar = 'search';
if (defined('VERSION') && (VERSION == '1.5.4' || VERSION == '1.5.4.1' || VERSION == '1.5.3' || VERSION == '1.5.3.1' || VERSION == '1.5.2' || VERSION == '1.5.2.1' || VERSION == '1.5.1' || VERSION == '1.5.1.3' || VERSION == '1.5.0')) {
	$searchvar = 'filter_name';
}
?>
<br />
<table class="form">
	<tr>
		<td style="vertical-align:top;width: 240px !important;">
			<ul class="nav nav-pills nav-stacked" id="visitsTabs">
            	<li><a href="#daily-total-stats_unique" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;&nbsp;Daily Unique Visitors</a></li>
           		<li><a href="#visitors-by-part-of-a-day" data-toggle="tab"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Visitors by Part of the day</a></li>
                <li><a href="#referer-stats" data-toggle="tab"><span class="glyphicon glyphicon-new-window"></span>&nbsp;&nbsp;Referer Stats</a></li>
            </ul>
		</td>
		<td style="vertical-align:top;">
			<div class="tab-content">
          		<div id="daily-total-stats_unique" class="tab-pane fade">
                    <h3 style="float:left;">Daily Unique Visitors</h3>
                    <?php require('element_filter.php'); ?>
                    <div class="help">This graph shows the total statistics day by day.</div>
                    
                  <?php if (!($iAnalyticsVisitorsDataByDay[0]=="No Data Gathered Yet")) { ?>
						<script type="text/javascript" src="//www.google.com/jsapi"></script>
                        <script type="text/javascript">
                        function drawVisualizationVisitorsByDay() {
                            var data = google.visualization.arrayToDataTable([
                              ['Part of the day', 'Unique Visits'],
							  <?php foreach($iAnalyticsVisitorsDataByDay as $j => $k): ?>
                            	<?php if ($j<1) { ?>
                           		 <?php } else { ?>
								['<?php echo $k[0]?>', <?php echo $k[1]?>],
							<?php } endforeach; ?>
                            ]);
                    
                            var options = {
                              title: 'Statistics',
                              hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}, slantedText:false },
                              vAxis: {minValue: 0},
                              width: $('#visitors').width() - 270,
                              height: 400
                            };
                    
                            var chart = new google.visualization.AreaChart(document.getElementById('visitsByDay'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="visitsByDay" style="overflow:hidden;"></div>
                    <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
                    	<br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsVisitorsDataByDay as $j => $k): ?>
                            <?php if ($j<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $k[0]?></th>
                                      <th><?php echo $k[1]?></th>
                                      <th><?php echo $k[2]?></th>
                                      <th><?php echo $k[3]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                            <tr>
                                 <td><?php echo $k[0]?></td>
                                 <td><?php echo $k[1]?></td>
                                 <td><?php echo $k[2]?></td>
                                 <td><?php echo $k[3]?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
					<div class="clearfix"></div>
				</div>
                
				<div id="visitors-by-part-of-a-day" class="tab-pane fade">
                    <h3 style="float:left;">Visitors by Parts of the Day</h3>
                    <?php require('element_filter.php'); ?>
                    <div class="help">This graph shows the total statistics by the parts of the day.</div>
                   <?php if (!($iAnalyticsVisitorsData[0]=="No Data Gathered Yet")) { ?>
						<script type="text/javascript" src="//www.google.com/jsapi"></script>
                        <script type="text/javascript">
                        function drawVisualizationVisitorsByPartOfTheDay() {
                            var data = google.visualization.arrayToDataTable([
                              ['Part of the day', 'Unique Visits'],
							  <?php foreach($iAnalyticsVisitorsData as $j => $k): ?>
                            	<?php if ($j<1) { ?>
                           		 <?php } else { ?>
								['<?php
                                  if ($k[0]==0) echo "Midnight";
                                  else if ($k[0]==1) echo "Morning"; 
                                  else if ($k[0]==2) echo "Noon"; 
                                  else if ($k[0]==3) echo "Evening"; 
                                  ?>', <?php echo $k[1]?>],
							<?php } endforeach; ?>
                            ]);
                    
                            var options = {
                              title: 'Statistics',
                              hAxis: {title: 'Part of the day',  titleTextStyle: {color: '#333'}, slantedText:false },
                              vAxis: {minValue: 0},
                              width: $('#visitors').width() - 270,
                              height: 400
                            };
                    
                            var chart = new google.visualization.AreaChart(document.getElementById('visitsByPartOfTheDay'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="visitsByPartOfTheDay" style="overflow:hidden;"></div>
                    <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
                    	<br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsVisitorsData as $j => $k): ?>
                            <?php if ($j<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $k[0]?></th>
                                      <th><?php echo $k[1]?></th>
                                      <th><?php echo $k[2]?></th>
                                      <th><?php echo $k[3]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                            <tr>
                                <td><?php
                                  if ($k[0]==0) echo "Midnight";
                                  else if ($k[0]==1) echo "Morning"; 
                                  else if ($k[0]==2) echo "Noon"; 
                                  else if ($k[0]==3) echo "Evening"; 
                                  ?></td>
                                 <td><?php echo $k[1]?></td>
                                 <td><?php echo $k[2]?></td>
                                 <td><?php echo $k[3]?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
					<div class="clearfix"></div>
				</div>
                
                <div id="referer-stats" class="tab-pane fade">
                    <h3 style="float:left;">Referer statistics</h3>
                    <?php require('element_filter.php'); ?>
                    <div class="help">This graph shows from where the customers come to your store.</div>
                    <?php if (!($iAnalyticsVisitorsDataReferers[0]=="No Data Gathered Yet")) { ?>
						<script type="text/javascript" src="//www.google.com/jsapi"></script>
                        <script type="text/javascript">
                        function drawVisualizationVisitorsDataReferers() {
                            var data = google.visualization.arrayToDataTable([
                              ['Referer', 'Visits'],
							  <?php foreach($iAnalyticsVisitorsDataReferersPie as $j => $k): ?>
                            	<?php if ($j<1) { ?>
                           		 <?php } else { ?>
								['Direct hits', <?php echo $k[0]?>],
								['Social networks', <?php echo $k[1]?>],
								['Search engines', <?php echo $k[2]?>],
								['Other', <?php echo $k[3]?>],
							<?php } endforeach; ?>
                            ]);
                    
                            var options = {
							  pieStartAngle: 135,
                              width: $('#visitors').width() - 270,
                              height: 400,
                            };
                    
                            var chart = new google.visualization.PieChart(document.getElementById('visitsReferers'));
                            chart.draw(data, options);
                          }
                        </script>
                        <div id="visitsReferers" style="overflow:hidden;"></div>
                    <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
                    	<br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsVisitorsDataReferers as $j => $k): ?>
                            <?php if ($j<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $k[0]?></th>
                                      <th><?php echo $k[1]?></th>
                                      <th><?php echo $k[2]?></th>
                                      <th><?php echo $k[3]?></th>
                                      <th><?php echo $k[4]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                            <tr>
                                 <td><?php echo $k[0]?></td>
                                 <td><?php echo $k[1]?></td>
                                 <td><?php echo $k[2]?></td>
                                 <td><?php echo $k[3]?></td>
                                 <td><?php echo $k[4]?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
					<div class="clearfix"></div>
				</div>
                
			</div>
		</td>
	</tr>
</table>