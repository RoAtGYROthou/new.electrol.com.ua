<?php
	$this->language->load('report/customer_order');

		if (isset($this->request->get['fromDate'])) {
			$filter_date_start = $this->request->get['fromDate'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['toDate'])) {
			$filter_date_end = $this->request->get['toDate'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filterOrders'])) {
			$filter_order_status_id = $this->request->get['filterOrders'];
		} else {
			$filter_order_status_id = 0;
		}

	if (isset($this->request->get['page'])) {
		$page = $this->request->get['page'];
	} else {
		$page = 1;
	}

	$this->load->model('report/customer');
 
	$this->data['customers'] = array();

	$data3 = array(
		'filter_date_start'	  => $filter_date_start, 
		'filter_date_end'		=> $filter_date_end, 
		'filter_order_status_id' => $filter_order_status_id,
		'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
		'limit'                  => $this->config->get('config_admin_limit')
	);

	$customer_total = $this->model_report_customer->getTotalOrders($data3); 

	$results = $this->model_catalog_ianalytics->getOrdersCustomers($data3);

	foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/customer/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customer'       => $result['customer'],
				'email'          => $result['email'],
				'customer_group' => $result['customer_group'],
				'status'         => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'orders'         => $result['orders'],
				'total'          => $this->currency->format($result['total'], $this->config->get('config_currency')),
				'action'         => $action
			);
	}

	$this->data['token'] = $this->session->data['token'];

	$url = '';

	if (isset($this->request->get['fromDate'])) {
		$url .= '&fromDate=' . $this->request->get['fromDate'];
	}

	if (isset($this->request->get['toDate'])) {
		$url .= '&toDate=' . $this->request->get['toDate'];
	}

	if (isset($this->request->get['filterOrders'])) {
		$url .= '&filterOrders=' . $this->request->get['filterOrders'];
	}

	$pagination = new Pagination();
	$pagination->total = $customer_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_admin_limit');
	$pagination->text = $this->language->get('text_pagination');
	$pagination->url = $this->url->link('module/ianalytics', 'token=' . $this->session->data['token'] . $url . '&page={page}');

	$this->data['pagination'] = $pagination->render();		
?>

<?php if ($this->data['customers']) { ?>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
	function drawVisualizationCustomers() {
        var wrapper = new google.visualization.ChartWrapper({
          chartType: 'ColumnChart',
          dataTable: [['', 
		  <?php foreach ($this->data['customers'] as $customer) { ?>
            '<?php echo $customer['customer']; ?>', 
	 	 <?php } ?>
		  ], ['', 
			<?php foreach ($this->data['customers'] as $customer) { ?>
            <?php echo $customer['orders']; ?>, 
	 		 <?php } ?>]],
          options: {'title': 'Customers with orders',
		  			'width': $('#aftersale').width() - 270,
		  			'height': 400},
          containerId: 'visualization2'
        });
        wrapper.draw();
      }
    </script>
    <div id="visualization2" style="overflow:hidden;"></div>
<?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
	<table class="table-hover table table-striped">
    	<thead>
          <tr>
            <th>Customer Name</th>
            <th>Email</th>
            <th>Group</th>
            <th>Status</th>
            <th>Orders</th>
            <th>Total</th>
            <th>Action</th>
          </tr>
        </thead>
        
	<?php if ($this->data['customers']) { ?>
	 	 <?php foreach ($this->data['customers'] as $customer) { ?>
              <tr>
                <td class="left"><?php echo $customer['customer']; ?></td>
                <td class="left"><?php echo $customer['email']; ?></td>
                <td class="left"><?php echo $customer['customer_group']; ?></td>
                <td class="left"><?php echo $customer['status']; ?></td>
                <td class="right"><?php echo $customer['orders']; ?></td>
                <td class="right"><?php echo $customer['total']; ?></td>
                <td class="right"><?php foreach ($customer['action'] as $action) { ?>
                	<a class="btn btn-default" target="_blank" href="<?php echo $action['href']; ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;<?php echo $action['text']; ?></a>
              <?php } ?></td>
              </tr>
	 	 <?php } ?>
	<?php } else { ?>
          <tr>
            <td class="center" colspan="8">There are no results yet!</td>
          </tr>
	  <?php } ?>
	  </table>
	<div class="pagination"><?php echo $this->data['pagination']; ?></div>