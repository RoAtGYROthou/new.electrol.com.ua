<?php echo $header; ?>
<div id="content">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <?php echo $breadcrumb['separator']; ?></li>
		<?php } ?>
        <li class="pull-right"><a href="<?php echo $cancel; ?>" class="btn btn-danger"><?php echo $button_cancel; ?></a></li>
        <li class="pull-right"><a onclick="$('#form').submit();" class="btn btn-success save-changes"><?php echo $button_save; ?></a></li>
	</ul>
    <?php echo (empty($data['LicensedOn'])) ? base64_decode('PGRpdiBjbGFzcz0iYWxlcnQgYWxlcnQtZXJyb3IiPjxpIGNsYXNzPSJpY29uLWV4Y2xhbWF0aW9uLXNpZ24iPjwvaT4gWW91IGFyZSBydW5uaW5nIGFuIHVubGljZW5zZWQgdmVyc2lvbiBvZiB0aGlzIG1vZHVsZSEgPGEgaHJlZj0iamF2YXNjcmlwdDp2b2lkKDApIiBvbmNsaWNrPSIkKCdhW2hyZWY9I3N1cHBvcnRdJykudHJpZ2dlcignY2xpY2snKSI+Q2xpY2sgaGVyZSB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4gdG8gZW5zdXJlIHByb3BlciBmdW5jdGlvbmluZywgYWNjZXNzIHRvIHN1cHBvcnQgYW5kIHVwZGF0ZXMuPC9kaXY+') : '' ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-error"><i class="icon-exclamation-sign"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
    <?php if (!empty($this->session->data['success'])) { ?>
    
    	<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $this->session->data['success']; unset($this->session->data['success']); ?>
        </div>
	<?php } ?>
    
	<?php if (!empty($this->session->data['error'])) { ?>
    	<div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $this->session->data['error']; unset($this->session->data['error']); ?>
        </div>
	<?php } ?>
    <div class="navbar tabbable">
        <div class="navbar-inner">
            <a class="brand"><i class="logo"></i><?php echo $heading_title; ?></a>
            <span class="tab-content">
				<?php foreach ($stores as $index => $store) : ?>
                <span class="tab-pane<?php echo $index == 0 ? ' active' : ''; ?>" id="tab_store_<?php echo $store['store_id']; ?>">
                    <ul class="nav sub_tabs">
                        <?php $index_1 = 0; foreach ($languages as $lang) : ?>
                        <?php $storeCode = (empty($data[$store['store_id']]) || empty($data[0][$lang['code']])) ? 0 : $store['store_id']; ?>
                        <?php $langCode = (empty($data[$store['store_id']]) || empty($data[0][$lang['code']])) ? 'en' : $lang['code']; ?>
                        <li<?php echo $index_1 == 0 ? ' class="active"' : ''; ?>><a data-target="#sub_tab<?php echo $index; ?>_<?php echo $index_1; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $lang['image']; ?>" title="<?php echo $lang['name']; ?>" />&nbsp;<?php echo strtoupper($lang['code']); ?></a></li>
                        <?php $index_1++; endforeach; ?>
                        <?php foreach($tabs as $index_2 => $tab): ?>
                        <li><a data-target="#sub_tab<?php echo $index; ?>_<?php echo ($index_1 + $index_2); ?>" data-toggle="tab"><?php echo $tab['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </span>
                <?php endforeach; ?>
            </span>
            <ul class="nav pull-right">
                <li class="divider-vertical"></li>
                <li class="dropdown stores_dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="store_name">Select a store</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu store_tabs">
                        <?php foreach ($stores as $index => $store) : ?>
                        <li class="store_tabs_li"><a data-toggle="tab" data-target="#tab_store_<?php echo $store['store_id']; ?>, #tab_content_store_<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
    <div class="tab-content">
        <!-- STORES -->
        <?php $globalStoreIndex = 0; $globalTabsIndex = 0; $globalSubtabsIndex = 0; 
			  foreach ($stores as $index => $store) : ?>
        <div class="tab-pane<?php echo $index == 0 ? ' active' : ''; ?>" id="tab_content_store_<?php echo $store['store_id']; ?>">
            <div class="tab-content">
                <?php $index_1 = 0; foreach ($languages as $lang) : ?>
                <?php $storeCode = (empty($data[$store['store_id']]) || empty($data[0][$lang['code']])) ? 0 : $store['store_id']; ?>
        		<?php $langCode = (empty($data[$store['store_id']]) || empty($data[0][$lang['code']])) ? 'en' : $lang['code']; ?>
                <div class="tab-pane<?php echo $index_1 == 0 ? ' active' : ''; ?>" id="sub_tab<?php echo $index; ?>_<?php echo $index_1; ?>">
                    <?php include(DIR_APPLICATION . 'view/template/module/icustomfooter/tablanguage_columns.php'); ?>
                </div>
                <?php $globalTabsIndex++; $index_1++; endforeach; ?>
                <?php foreach($tabs as $index_2 => $tab): ?>
                <div class="tab-pane" id="sub_tab<?php echo $index; ?>_<?php echo ($index_1 + $index_2); ?>">
                    <?php include($tab['file']); ?>
                </div>
                <?php $globalTabsIndex++; endforeach; ?>
            </div>
        </div>
        <?php $globalStoreIndex++; endforeach; ?>
    </div>
    <input type="hidden" name="store" value="<?php echo !empty($this->request->get['store']) ? $this->request->get['store'] : '0'; ?>" />
    <input type="hidden" name="tab" value="<?php echo !empty($this->request->get['tab']) ? $this->request->get['tab'] : '0'; ?>" />
    <input type="hidden" name="subtab" value="<?php echo !empty($this->request->get['subtab']) ? $this->request->get['subtab'] : '0'; ?>" />
    </form>
    
    <form style="display: none;" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form_payment_icons">
    	<input type="file" id="PaymentIcons" class="PaymentIcons" data-browse-text-selector="" name="paymentIcon" style="display: none;" />
        <input id="PaymentIconName" name="paymentIconName" type="hidden" value="" />
        <input type="hidden" name="store" value="<?php echo !empty($this->request->get['store']) ? $this->request->get['store'] : '0'; ?>" />
        <input type="hidden" name="tab" value="<?php echo !empty($this->request->get['tab']) ? $this->request->get['tab'] : '0'; ?>" />
        <input type="hidden" name="subtab" value="<?php echo !empty($this->request->get['subtab']) ? $this->request->get['subtab'] : '0'; ?>" />
    </form>
</div>
<?php echo $footer; ?>