<?php 
if (empty($idata)) {
	echo '<center>iCustomFooter needs to be installed from your admin panel</center>';
} else { ?>
    <div class="clearfix"></div>
    
    <?php require_once($footerPath . 'customstyles.php'); ?>
    
    <div id="iCustomFooter" class="iCustomFooter <?php echo $idata['Settings']['BackgroundPattern']; ?> <?php echo $idata['Settings']['FontFamily']; ?> <?php echo $idata['Settings']['ColumnContentOverflow']; ?> <?php echo $idata[$langcode]['Widgets']['Contacts']['IconSet']; ?>">
        <ul class="iWidgets">
        <?php if ($columns) { ?>
          <?php foreach ($columns as $column) { ?>
          <?php echo $column; ?>
          <?php } ?>
        <?php } ?>
        <li class="clearfix"></li>
        </ul>
    </div>
	
    <style type="text/css" rel="stylesheet">
	<?php echo $idata['Settings']['CustomCSS']; ?>
	</style>
    
    <script type="text/javascript">
		var responsiveDesign = '<?php echo $idata['Settings']['ResponsiveDesign']; ?>';
		var customFooterWidth = <?php echo $idata['Settings']['FooterWidth']?>;
		/** Responsive Design logic */
		var inSmallWidthMode = false;
		var respondOnWidthChange = function() {
			var currentWidth = $(window).width();
			if (currentWidth < customFooterWidth + 5) {
				$('#iCustomFooter').width(currentWidth - 5);
				$('#footer').width(currentWidth - 5);
				respondOnSmallWidth(currentWidth);
				
			} else {
				$('#iCustomFooter').width(customFooterWidth);
				$('#footer').width(customFooterWidth);
			}
			
		}
		
		$('.iWidget h2').click(function() {
			if (inSmallWidthMode == true) {
				if ($(this).parent().find('.belowTitleContainer').css('display') == 'none') {
					$('.iWidget .belowTitleContainer').slideUp();
					$(this).parent().find('.belowTitleContainer').slideDown();
				}
			}
		});
		
		var respondOnSmallWidth = function(currentWidth) {
			var columnWidth = '<?php echo $idata['Settings']['ColumnWidth'] ?>';
			if (currentWidth < (columnWidth*2+60)) {
				inSmallWidthMode = true;
				$('.iWidgets .belowTitleContainer').slideUp();
				$('.iWidgets > li').css('width','92%').css('margin-bottom','-20px');
				$('#footer').hide();
			} else {
				inSmallWidthMode = false;
				$('.iWidgets .belowTitleContainer').slideDown();
				$('.iWidgets > li').css('width',columnWidth+'px').css('margin-bottom','10px');
				$('#footer').show();
			}
		}
		
		$(document).ready(function() {
			if (responsiveDesign == 'yes') {
				$(window).resize(function() {
					respondOnWidthChange();
				});
				respondOnWidthChange();
			}
		});
		/** END */
	</script>
<?php } //ending the not activated IF ?>