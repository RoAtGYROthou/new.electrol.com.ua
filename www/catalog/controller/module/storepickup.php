<?php
class ControllerModuleStorePickup extends Controller {
	public $APIKey = 'AIzaSyA7_QNyk2UnxoHHhZRa7UU-OZaCupzN76Q';
	
	// Module-dependant variables	
	private $moduleName = 'StorePickup';
	private $moduleNameSmall = 'storepickup';
	
	public function index() {
		
		// Load the needed language files
		$this->load->language('module/storepickup');
		
		// Load the needed models
		$this->load->model('setting/setting');
		
		
		// Load script & stylesheets
						
		//$this->document->addScript('admin/view/javascript/'.$this->moduleNameSmall.'/bootstrap/js/bootstrap.min.js');
        $this->document->addScript('admin/view/javascript/ckeditor/ckeditor.js');

        //$this->document->addStyle('admin/view/javascript/'.$this->moduleNameSmall.'/bootstrap/css/bootstrap.min.css');
        $this->document->addStyle('admin/view/javascript/'.$this->moduleNameSmall.'/font-awesome/css/font-awesome.min.css');
		
		// Get the module settings
		$moduleSettings = $this->model_setting_setting->getSetting('storepickup', 0);
		$moduleData = isset($moduleSettings['StorePickup']) ? $moduleSettings['StorePickup'] : array();
		
		// Check if the module is enabled
		if (isset($moduleData['Enabled']) && $moduleData['Enabled'] == 'no') {
			$this->response->redirect($this->url->link('common/home'));	
		}
		
		// Get the breadcrumbs
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'separator' => false,
		);
		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/storepickup'),
			'separator' => $this->language->get('text_separator'),
		);
		
		// Set the title and add additional scripts
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('https://maps.googleapis.com/maps/api/js?key='.$this->APIKey);
		
		// Set the variables which will be used in the front-end page
		$this->data['heading_title'] 			= $this->language->get('heading_title');
		$this->data['button_continue'] 		= $this->language->get('button_continue');
		$this->data['continue'] 				= $this->url->link('common/home');
		$this->data['language_id']			= $this->config->get('config_language_id');
		$stores							= !empty($moduleData['StoreData']) ? $moduleData['StoreData'] : array();
		$this->data['stores']					= array();
		
		foreach ($stores as $store) {
			if (isset($store) && ($store['Enabled']=='yes')) {
				$this->data['stores'][] 		= $store;	
			}
		}
		
		$this->data['template']				= '/template/module/storepickup.tpl';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/storepickup.css')) {
			$this->document->addStyle('catalog/view/theme/'.$this->config->get('config_template').'/stylesheet/storepickup.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/storepickup.css');
		}
		
		// OpenCart layouts & header and footer
		
		$this->children = array(
				'common/header',
				'common/footer',
				'common/content_bottom',
				'common/content_top',
				'common/column_right',
				'common/column_left'
		);
		
	
		// Load the template
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/storepickup.tpl')) {
			$this->template = $this->config->get('config_template') .'/template/module/storepickup.tpl';
		} else {
			$this->template = 'default/' . '/template/module/storepickup.tpl';
		}

		$this->response->setOutput($this->render());
	}
	
}