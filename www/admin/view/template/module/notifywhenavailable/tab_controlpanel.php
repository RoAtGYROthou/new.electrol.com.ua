<table class="form">
	<tr>
		<td><span class="required">*</span> <?php echo $entry_code; ?></td>
		<td>
            <select name="NotifyWhenAvailable[Enabled]" class="NotifyWhenAvailableEnabled">
                <option value="yes" <?php echo ($data['NotifyWhenAvailable']['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
               <option value="no" <?php echo ($data['NotifyWhenAvailable']['Enabled'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
   		</td>
  	</tr>
	<tr class="NotifyWhenAvailableActiveTR">
    	<td><span class="required">*</span> <?php echo $entry_layouts_active; ?></td>
    	<td>
    		<?php $i=0; $checked=""; ?>
    		<?php foreach ($layouts as $layout) { ?>
			<?php 
			$status = null;
			foreach ($modules as $module) {
				if(!empty($module)) {
					if ($module['layout_id'] == $layout['layout_id']) {
						$status = $module['status'];
						if ((int)$status == 1) {
							$checked = ' checked=checked';
						} else { 
							$checked = '';
					   }
					}
				} 
			  
			  }
			  if (!isset($status) && $layout['name'] == 'Product') {
					$status = 1;
					$checked = ' checked=checked';	
			  }
			  if (!isset($status) && $layout['name'] == 'Sitemap') {
					$status = 0;
					$checked = '';	
			  }
            ?>
    		<div class="NotifyWhenAvailableLayout">
                <input type="checkbox" value="<?php echo $layout['layout_id']; ?>" id="NotifyWhenAvailableActive<?php echo $i?>" <?php echo $checked?> /><label for="NotifyWhenAvailableActive<?php echo $i?>"><?php echo $layout['name']; ?></label>
                <input type="hidden" name="notifywhenavailable_module[<?php echo $i?>][position]" value="content_bottom" />
                <input type="hidden" class="NotifyWhenAvailableItemLayoutIDField" name="notifywhenavailable_module[<?php echo $i?>][layout_id]" value="<?php echo $layout['layout_id']; ?>" />
                <input type="hidden" class="NotifyWhenAvailableItemStatusField" name="notifywhenavailable_module[<?php echo $i?>][status]" value="<?php echo $status ?>" />
                <input type="hidden" name="notifywhenavailable_module[<?php echo $i?>][sort_order]" value="<?php echo $i+10?>" />
            </div>
    		<?php $i++;} ?>
     	</td>
  	</tr>
	<tr>
      <td><span class="required">*</span> Scheduled tasks:<span class="help">When activated, this function will send automatically emails to the customers who are waiting for products that now are in stock.</span></td>
      <td>
      	<div class="row-fluid">
          	<div class="span3">
                <select id ="ScheduleToggle" name="NotifyWhenAvailable[ScheduleEnabled]">
                  <option value="yes" <?php echo (!empty($data['NotifyWhenAvailable']['ScheduleEnabled']) && $data['NotifyWhenAvailable']['ScheduleEnabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                  <option value="no"  <?php echo (empty($data['NotifyWhenAvailable']['ScheduleEnabled']) || $data['NotifyWhenAvailable']['ScheduleEnabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                </select>
            </div>
          	<div class="span9">
            	<div class="well">
            		<i class="icon-question-sign"></i> When you change a given product's quantity from the <strong>Products -> Edit page</strong>, <strong>NotifyWhenAvailable</strong> automatically checks if there are customers that are waiting for it. However, if you use another software to edit the products quantities, such as quick edit modules or inventory system, the module will not be triggered. In such cases, you can use the scheduling tasks in <strong>NotifyWhenAvailable</strong> which will help you alert the customers when the product that they are waiting for is back in stock.
				</div>
            </div>
		</div>
      	
      	
       </td>
    </tr>
	<tr class="nwaCron">
		<td>Receive notification when the task is executed:<span class="help">An Email will be sent to the store owner when a sheduled task is finished.</span></td>
		<td>
            <select name="NotifyWhenAvailable[CronNotify]" class="NotifyWhenAvailableCronNotify">
                <option value="yes" <?php echo ($data['NotifyWhenAvailable']['CronNotify'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
               <option value="no" <?php echo ($data['NotifyWhenAvailable']['CronNotify'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
   		</td>
  	</tr>
  	<tr class="nwaCron">
    	<td width="25%"><span class="required">*</span> Type:</td>
    	<td><select name="NotifyWhenAvailable[ScheduleType]">
            <option value="F" <?php if(!empty($data['NotifyWhenAvailable']['ScheduleType']) && $data['NotifyWhenAvailable']['ScheduleType'] == 'F') echo "selected" ?>>Fixed dates</option>
            <option value="P" <?php if(!empty($data['NotifyWhenAvailable']['ScheduleType']) && $data['NotifyWhenAvailable']['ScheduleType'] == 'P') echo "selected" ?>>Periodic</option>
          </select></td>
 	</tr>
	<tr class="nwaCron">
        <td><span class="required">*</span> Schedule:</td>
        <td><div id="FixedDateOptions">
            <div class="input-prepend">
               <span class="add-on"><i class="icon-calendar"></i></span>
               <input type="text" id="FixedDate" class="input-small" value="" placeholder="Date..." readonly />
            </div>
            <div class="input-prepend">
               <span class="add-on"><i class="icon-time"></i></span>
            <input type="text" id="FixedDateTime" class="timepicker input-small" placeholder="Time..." readonly />
            </div>
            <button class="btn addDate" style="margin-top:-10px;"><i class="icon-plus"></i> Add</button>
            <div class="scrollbox dateList">
                <?php if(isset($data['NotifyWhenAvailable']['FixedDates'])) { 
                        foreach($data['NotifyWhenAvailable']['FixedDates'] as $date) {?>
                <div id="date<?php  $id = explode( '/', $date); $id=explode('.' , $id[0]); echo $id[0].$id[1].$id[2]; ?>"><?php echo $date ?> 
                <i class="icon-minus-sign removeIcon"></i>
                <input type="hidden" name="NotifyWhenAvailable[FixedDates][]" value="<?php echo $date ?>" />
                </div>
                        <?php } } ?> 
             </div>
          </div>
          <div id="PeriodicOptions">
            <div id="CronSelector"></div>
            <input type="hidden" name="NotifyWhenAvailable[PeriodicCronValue]" value="">
          </div>
         </td>
	</tr>
	<tr class="nwaCron">
    	<td style="vertical-align:middle;">
      		<button id="TestCronAvailablity" class="btn btn-warning"><i class="icon-asterisk"></i> Test Cron</button>
		</td>
		<td>
          <div class="well">
            <i class="icon-question-sign"></i> If you want to use the scheduling features, your server has to support <strong>Cron</strong> functions.<br /><br />The cron daemon is a long running process that executes commands at specific dates and times. By clicking on the button <strong>Test Cron</strong> you can check if your server supports <strong>Cron</strong> commands<br /><br />If your server does support Cron jobs, but this script shows that the feature is disabled, this means that the automatic creation of Cron commands is disabled. In that case, you can use this URL string - <strong>vendors/notifywhenavailable/sendMails.php</strong> - in your hosting config panel.
          </div>  
    	</td>
  	</tr>
</table>
<script>
$('#TestCronAvailablity').colorbox({
	href: 'index.php?route=module/notifywhenavailable/testcron&token=' + getURLVar('token'),
	width: "50%",
	height: "65%"
});
$(document).ready(function() {	
	$('#FixedDate').datepicker({ dateFormat: "dd.mm.yy" });
	$('.timepicker').timepicker();
	$('#CronSelector').cron({
		  initial: "<?php if(!empty($data['NotifyWhenAvailable']['PeriodicCronValue'])) echo $data['NotifyWhenAvailable']['PeriodicCronValue']; else echo "* * * * *";  ?>",
    onChange: function() {
        $('input[name="NotifyWhenAvailable[PeriodicCronValue]"').val($(this).cron("value"));		 
    },
		});
});
	if($('select[name="NotifyWhenAvailable[ScheduleType]"]').val() == 'P') {
		$('#FixedDateOptions').hide();
	 	$('#PeriodicOptions').show(200);
	} else {
		$('#PeriodicOptions').hide();
		$('#fixedDateOptions').show(200);	
	}
$('select[name="NotifyWhenAvailable[ScheduleType]"]').on('change', function(e){ 
	if($(this).val() == 'P') {
		$('#FixedDateOptions').hide();
	 	$('#PeriodicOptions').show(200);	
	} else {
		$('#PeriodicOptions').hide();
		$('#FixedDateOptions').show(200);	
		}	
});
$('.btn.addDate').on('click', function(e){
		e.preventDefault();
		if($('#FixedDate').val() && $('#FixedDateTime').val() ){
		//	$('#date' + $('#FixedDate').val().replace(/\./g, '')).remove();
			$('.scrollbox.dateList').append('<div id="date' + $('#FixedDate').val().replace(/\./g,'') + '">' + $('#FixedDate').val() + '/' + $('#FixedDateTime').val() +'<i class="icon-minus-sign removeIcon"></i><input type="hidden" name="NotifyWhenAvailable[FixedDates][]" value="' + $('#FixedDate').val() + '/' + $('#FixedDateTime').val() + '" /></div>');
			$('#FixedDate').val('');
			$('#FixedDateTime').val('');
		} else {
				alert('Please fill date and time!');
			}
});
$('.scrollbox.dateList div .removeIcon').live('click', function() {
	$(this).parent().remove();
});

function showHide()  {
    var $typeSelector = $('#ScheduleToggle');
    var $toggleArea = $('.nwaCron');
	 
    $typeSelector.change(function(){
        if ($typeSelector.val() === 'yes') {
            $toggleArea.show(500) 
        }
        else {
            $toggleArea.hide(500);
        }
    });
	
	if ($typeSelector.val() === 'yes') {
		$toggleArea.show() 
	} else {
		$toggleArea.hide();
	}
}

$(document).ready(function() {
	showHide();
});

$('.NotifyWhenAvailableLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.NotifyWhenAvailableItemStatusField', $(this).parent()).val(1);
    } else {
        $('.NotifyWhenAvailableItemStatusField', $(this).parent()).val(0);
    }
});
$('.NotifyWhenAvailableEnabled').change(function() {
    toggleNotifyWhenAvailableActive(true);
});
var toggleNotifyWhenAvailableActive = function(animated) {
   if ($('.NotifyWhenAvailableEnabled').val() == 'yes') {
        if (animated) 
            $('.NotifyWhenAvailableActiveTR').fadeIn();
        else 
            $('.NotifyWhenAvailableActiveTR').show();
    } else {
        if (animated) 
            $('.NotifyWhenAvailableActiveTR').fadeOut();
        else 
            $('.NotifyWhenAvailableActiveTR').hide();
    }
}
toggleNotifyWhenAvailableActive(false);
</script>