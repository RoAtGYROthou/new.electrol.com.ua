<?php if($data['NotifyWhenAvailable']['Enabled'] != 'no'): ?>
	<div id="NotifyWhenAvailable_popup" style="display:none;width:<?php echo $data['NotifyWhenAvailable']['PopupWidth']; ?>px;" class="NWA_popover bottom">
		<div class="arrow"></div>
		<h3 class="NWA_popover-title"><?php echo $data['NotifyWhenAvailable']['CustomTitle']; ?></h3>
		<div class="NWA_popover-content">
		</div>
	</div>
	<script>
    $(document).ready( function() {
        $('body').append($('#NotifyWhenAvailable_popup'));
		
		$('a[lkmwa=true],input[lkmwa=true]').bind('click', function(e) {
		var product_id = $(this).attr('pid');
		offset = $(this).offset();
		var leftOffset = offset.left + (parseInt($(this).width()/2));
  		$('div#NotifyWhenAvailable_popup').css({
			top: offset.top,
			left: ((offset.left-$('div#NotifyWhenAvailable_popup').width()/2) + $(this).width()/2)
		});
		$('div#NotifyWhenAvailable_popup').fadeIn('slow');
		$(".NWA_popover-content").load("index.php?route=module/notifywhenavailable/shownotifywhenavailableform&product_id="+product_id);
	  });
		
        $(document).click(function(event) {
            if (!$(event.target).is("#NotifyWhenAvailable_popup, a[lkmwa=true], input[lkmwa=true], .NWA_popover-title, .arrow, .NWA_popover-content, #NWAYourName, #NWAYourEmail, #NotifyWhenAvailableSubmit, #NotifyWhenAvailable_popup p, #NotifyWhenAvailable_popup span, .NWA_popover, #NotifyWhenAvailableForm, .NWAError, input.NWA_popover_field_error")) {
                $('div#NotifyWhenAvailable_popup').fadeOut('slow');
            }
        });	
    });
    </script>
<?php endif; ?>