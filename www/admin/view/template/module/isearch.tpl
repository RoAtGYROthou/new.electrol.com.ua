<?php echo $header; ?>
<?php
  
  function is_class_method($type="public", $method, $class) { 
   // $type = mb_strtolower($type); 
    $refl = new ReflectionMethod($class, $method); 
    switch($type) { 
        case "static": 
        return $refl->isStatic(); 
        break; 
        case "public": 
        return $refl->isPublic(); 
        break; 
        case "private": 
        return $refl->isPrivate(); 
        break; 
    } 
  } 

  function vqmod_resolve($file) {

    if (class_exists('VQMod')) {
      if (is_class_method('static', 'modCheck', 'VQMod')) {
        $file = VQMod::modCheck($file);
      } else {
        $vqmod = new VQMod();
        $file = $vqmod->modCheck($file);
      }
    }

    return $file;
  }

?>
<div id="content">
  <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
        <?php echo (empty($data['iSearch']['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNzdXBwb3J0XScpLnRyaWdnZXIoJ2NsaWNrJykiPkVudGVyIHlvdXIgbGljZW5zZSBjb2RlPC9hPg0KICAgIDwvZGl2Pg==') : '' ?>
  <?php if ($error_warning) { ?><div class="alert alert-danger" > <i class="icon-exclamation-sign"></i>&nbsp;<?php echo $error_warning; ?></div><?php } ?>
    <?php if (!empty($this->session->data['success'])) { ?>
        <div class="alert alert-success autoSlideUp"> <i class="fa fa-info"></i>&nbsp;<?php echo $this->session->data['success']; ?> </div>
        <script> $('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $this->session->data['success'] = null; } ?>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  
  <!---->
  <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-search"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;"><?php echo $heading_title; ?></span></h3>
        </div>
        <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="tabbable">
                    <div class="tab-navigation form-inline">
                        <ul class="nav nav-tabs mainMenuTabs" id="mainTabs">
                            <li><a href="#position_tab" data-toggle="tab"><i class="fa fa-th"></i>&nbsp;Module Position</a></li>
                            <li><a href="#control_panel" data-toggle="tab"><i class="fa fa-power-off"></i>&nbsp;Control Panel</a></li>
                            <li id="mainSettingsTab"><a href="#customization" data-toggle="tab"><i class="fa fa-cogs"></i>&nbsp;Customization</a></li>
                            <li id="mainSettingsTab"><a href="#imprv_results" data-toggle="tab"><i class="fa fa-star"></i>&nbsp;Improving Results</a></li>
                            <li><a href="#support" data-toggle="tab"><i class="fa fa-ticket"></i>&nbsp;Support</a></li>
                        </ul>
                        <div class="tab-buttons">
                            <button type="submit" class="btn btn-success save-changes"><i class="fa fa-check"></i>&nbsp;Save Changes</button>
                            <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning">Cancel</a>
                        </div> 
                    </div><!-- /.tab-navigation --> 
                    <div class="tab-content">
                        <div id="position_tab" class="tab-pane fade"><?php require_once vqmod_resolve(DIR_APPLICATION.'view/template/module/isearch/tab_position.php'); ?></div>
                        <div id="control_panel" class="tab-pane fade"><?php require_once vqmod_resolve(DIR_APPLICATION.'view/template/module/isearch/tab_control_panel.php'); ?></div>
                        <div id="customization" class="tab-pane fade"><?php require_once vqmod_resolve(DIR_APPLICATION.'view/template/module/isearch/tab_customization.php'); ?></div>
                        <div id="imprv_results" class="tab-pane fade"><?php require_once vqmod_resolve(DIR_APPLICATION.'view/template/module/isearch/tab_improving_results.php'); ?></div>
                        <div id="support" class="tab-pane fade"><?php require_once vqmod_resolve(DIR_APPLICATION.'view/template/module/isearch/tab_support.php'); ?></div>
                    </div> <!-- /.tab-content --> 
                </div><!-- /.tabbable -->
            </form>
        </div> 
    </div>
  <!---->
  
</div>
<script type="text/javascript">
  $('#mainTabs a:first').tab('show') // Select first tab
  if (window.localStorage && window.localStorage['currentTab']) {
    $('.mainMenuTabs a[href="'+window.localStorage['currentTab']+'"]').tab('show');
  }
  if (window.localStorage && window.localStorage['currentSubTab']) {
    $('a[href="'+window.localStorage['currentSubTab']+'"]').tab('show');
  }
  $('.fadeInOnLoad').css('visibility','visible');
  $('.mainMenuTabs a[data-toggle="tab"]').click(function() {
    if (window.localStorage) {
      window.localStorage['currentTab'] = $(this).attr('href');
    }
  });
  $('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
    if (window.localStorage) {
      window.localStorage['currentSubTab'] = $(this).attr('href');
    }
  });
</script>
<?php echo $footer; ?>