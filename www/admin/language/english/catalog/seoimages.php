<?php
// Heading
$_['heading_title']          = 'SEO Images by <a href="http://www.apwai.net" onclick="return !window.open(this.href)"> APWAI.NET</a>';
$_['text_success']          = 'Success: You have successfully saved parameters!';

// Error 
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify SEO Images!';

?>