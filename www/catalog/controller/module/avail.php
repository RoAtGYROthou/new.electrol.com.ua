	<?php
	class ControllerModuleAvail extends Controller {
		private $error = array();
		
		protected function index($setting) {					
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');			
		}
		
		public function openForm() {
			$this->language->load('module/avail');		
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			$this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
            $this->data['entry_product'] = $this->language->get('entry_product');
            $this->data['entry_price'] = $this->language->get('entry_price');
            $this->data['entry_name'] = $this->language->get('entry_name');
            $this->data['entry_mail'] = $this->language->get('entry_mail');
			$this->data['entry_admin_mail'] = $this->language->get('entry_admin_mail');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');			
            $this->data['button_send'] = $this->language->get('button_send');
			
			$this->data['admin_email'] = $this->config->get('email');
			$this->data['captcha_status'] = $this->config->get('config_captcha_status');
			$this->data['product_id'] = $this->request->get['product_id_avail'];
			$this->data['product_name'] = $this->request->get['product_name'];			
			$this->data['price'] = $this->request->get['price'];
			$this->data['captcha'] = '';
			
			
			if (file_exists(DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/avail.tpl')) {		
				$this->template = $this->config->get('config_template') . '/template/module/avail.tpl';
			} else {
				$this->template = 'default/template/module/avail.tpl';
			}

			$this->response->setOutput($this->render());
		}
		public function save() {
			$this->load->model('catalog/product');
			$this->load->model('module/avail');
			$this->language->load('module/avail');
			
			$json = array();					
			
			if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() ) {
				$link = $this->url->link('product/product', '&product_id=' . $this->request->post['product_id']);
				$info = array(
                    'product' => $this->request->post['product'],
					'product_id' => $this->request->post['product_id'],
					'admin_email' => $this->request->post['admin_email'],
                    'href' => $link,
                    'price' => $this->request->post['price'],
                    'name' => $this->request->post['name'],
                    'email' => $this->request->post['email'],
                    'comment' => $this->request->post['enquiry'],
                    'status' => 0
                 );
				 
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');	
				
				$mail->setTo($info['email']);								
				$mail->setFrom($info['admin_email']);							
				$mail->setSender($this->language->get('mail_title'));
				$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $this->request->post['name'], ENT_QUOTES, 'UTF-8')));
				
				$message = "
				<!DOCTYPE html>
				<html> 
					<head> 
						<title>" . $this->language->get('heading_title') . "</title> 
					</head> 
					<body>
						<p>" . $this->language->get('mail_notify') . "</p>
						<p>" . html_entity_decode($info['name'] .", " . $this->language->get('text_mail_send')) . "</p>
						<p>" . $this->language->get('text_product') . " " . $info['product'] . "</p>
						<p>" .$this->language->get('info_product') . " " . " <a href=" . $info['href'] . ">" . $info['product'] . "</a></p>
						<p>" . $this->language->get('text_price') . " " . $info['price'] . "</p>						
					</body> 
				</html>";				
				
				 $mail->setHtml($message);
				 $mail->send();
				
				$admin_email = new Mail();
				$admin_email->protocol = $this->config->get('config_mail_protocol');
				$admin_email->parameter = $this->config->get('config_mail_parameter');
				$admin_email->hostname = $this->config->get('config_smtp_host');
				$admin_email->username = $this->config->get('config_smtp_username');
				$admin_email->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
				$admin_email->port = $this->config->get('config_smtp_port');
				$admin_email->timeout = $this->config->get('config_smtp_timeout');			
				
				$admin_email->setTo($info['admin_email']);				
				$admin_email->setFrom($info['email']);				
				$admin_email->setSender($this->language->get('admin_mail_title'));
				$admin_email->setSubject(html_entity_decode(sprintf('From: ', $this->request->post['name'], ENT_QUOTES,'UTF-8')));
				
				$admin_message = "
					<!DOCTYPE html>
						<html>
							<body>
								<p>" . $this->config->get('config_owner') . ", " . $this->language->get('admin_mail_notify') .  "</p>
							</body>
						</html>
				";
				$admin_email->setHtml($admin_message);
				$admin_email->send();
				
				
				$lastId = $this->model_module_avail->addMail($info);
				$json['success'] = $this->language->get('success');
				$this->response->setOutput(json_encode($json));
			}
			if (isset($this->error['name'])) {
					$json['error_name'] = $this->error['name'];
				} else {
					$json['error_name'] = '';
				}
				 if (isset($this->error['email'])) {
					 $json['error_email'] = $this->error['email'];
				 } else {
					 $json['error_email'] = '';
				 }
				if (isset($this->error['price'])) {
					$json['error_price'] = $this->error['price'];
				} else {
					$json['error_price'] = '';
				}
				if (isset($this->error['product'])) {
					$json['error_product'] = $this->error['product'];
				} else {
					$json['error_product'] = '';
				}
				if (isset($this->error['captcha'])) {
					$json['error_captcha'] = $this->error['captcha'];
				} else {
					$json['error_captcha'] = '';
				}
			$this->response->setOutput(json_encode($json));
		}
		
		public function validate() {
			 $this->language->load('module/avail');

			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
				$this->error['name'] = $this->language->get('error_name');
			}
			 if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			 $this->error['email'] = $this->language->get('error_email');
			 }
			 if ((utf8_strlen($this->request->post['product']) < 2)) {
                 $this->error['product'] = $this->language->get('error_product');
             }
			 if ((utf8_strlen($this->request->post['price']) < 2) || (utf8_strlen($this->request->post['price']) > 32)) {
                 $this->error['price'] = $this->language->get('error_price');
             }
			 if ($this->config->get('config_captcha_status') == '1') {
				 if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
					$this->error['captcha'] = $this->language->get('error_captcha');
				}
			 }
			 
			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}
		
	}

?>