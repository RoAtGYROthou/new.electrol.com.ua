<div>
    <label for="ContactsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Show]" id="ContactsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionContactus_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionContactus_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][contacts]" value="<?php echo $data[$storeCode][$langCode]['Positions']['contacts']; ?>" />
</div>
<div>
    <label for="ContactsIconSet_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_iconset; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][IconSet]" id="ContactsIconSet_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['IconSet'] == '') ? 'selected=selected' : '';?>><?php echo $defaultforthefooter; ?></option>
        <option value="" disabled="disabled">- - -</option>
        <option value="whiteicons" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['IconSet'] == 'whiteicons') ? 'selected=selected' : ''; ?>><?php echo $whiteicons; ?></option>
        <option value="blueicons" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['IconSet'] == 'blueicons') ? 'selected=selected' : ''; ?>><?php echo $blueicons; ?></option>
        <option value="greenicons" <?php echo ($data[$storeCode][$langCode]['Widgets']['Contacts']['IconSet'] == 'greenicons') ? 'selected=selected' : ''; ?>><?php echo $greenicons; ?></option>
    </select>
</div>
<div>
    <label for="ContactsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="ContactsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Title']; ?>" />
</div>
<div>
    <label for="ContactsText_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_text; ?></label>
    <div class="textarea">
    <textarea id="ContactUs_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Text]"><?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Text']; ?></textarea>
    </div>
</div>
<div>
    <label for="ContactsAddress1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_address; ?> 1</label>
    <input type="text" id="ContactsAddress1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Address1]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Address1']; ?>" />
</div>
<div>
    <label for="ContactsAddress2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_address; ?> 2</label>
    <input type="text" id="ContactsAddress2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Address2]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Address2']; ?>" />
</div>
<div>
    <label for="ContactsPhone1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_phone; ?> 1</label>
    <input type="text" id="ContactsPhone1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Phone1]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Phone1']; ?>" />
</div>
<div>
    <label for="ContactsPhone2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_phone; ?> 2</label>
    <input type="text" id="ContactsPhone2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Phone2]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Phone2']; ?>" />
</div>
<div>
    <label for="ContactsFax1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_fax; ?> 1</label>
    <input type="text" id="ContactsFax1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Fax1]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Fax1']; ?>" />
</div>
<div>
    <label for="ContactsFax2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_fax; ?> 2</label>
    <input type="text" id="ContactsFax2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Fax2]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Fax2']; ?>" />
</div>
<div>
    <label for="ContactsEmail1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_email; ?> 1</label>
    <input type="text" id="ContactsEmail1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Email1]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Email1']; ?>" />
</div>
<div>
    <label for="ContactsEmail2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_email; ?> 2</label>
    <input type="text" id="ContactsEmail2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Email2]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Email2']; ?>" />
</div>
<div>
    <label for="ContactsSkype1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_skype; ?> 1</label>
    <input type="text" id="ContactsSkype1_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Skype1]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Skype1']; ?>" />
</div>
<div>
    <label for="ContactsSkype2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactus_skype; ?> 2</label>
    <input type="text" id="ContactsSkype2_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Contacts][Skype2]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Contacts']['Skype2']; ?>" />
</div>
<script>

CKEDITOR.replace( 'ContactUs_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>' );
setTimeout(function() {
CKEDITOR.instances.ContactUs_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>.document.on('keyup', function(event) {
	$('textarea#ContactUs_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>').val(CKEDITOR.instances.ContactUs_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>.getData());
});
}, 2000);
</script>