<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><h1 class="heading-title"><?php echo $heading_title; ?></h1><?php echo $content_top; ?>
  <?php echo $description; ?>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
<?php echo $microdatapro; $microdatapro_main_flag = 1; //microdatapro 7.5 - 1 - main ?>
  <?php echo $content_bottom; ?></div>
<?php if(!isset($microdatapro_main_flag)){echo $microdatapro;} //microdatapro 7.5 - 2 - extra ?>
<?php echo $footer; ?>