<table class="form wideBoxes">
  <tr>
    <td><span class="required">*</span> Site short-name<span class="help">Get short-name for your site from http://disqus.com/admin/create/.</span></td>
    <td>
        <div class="col-xs-3">
            <input type="text" name="DisqusComments[ModName]" class="form-control" value="<?php echo(!empty($data['DisqusComments']['ModName'])) ? $data['DisqusComments']['ModName'] : '' ; ?>" />
        </div>
   </td>
  </tr>
 <tr>
    <td><span class="required">*</span> Width<span class="help">In Pixels</span></td>
    <td>
        <div class="col-xs-3">
            <input type="text" name="DisqusComments[Width]" class="form-control" value="<?php echo(!empty($data['DisqusComments']['Width'])) ? $data['DisqusComments']['Width'] : '730' ; ?>" />
        </div>
   </td>
  </tr>
  <tr>
    <td>URL Route<span class="help">Specify URL route for which exact page you want to have the Disqus Comments enabled. For example for the product detail page specify <em>product/product</em></span></td>
    <td valign="top">
        <div class="col-xs-3">
            <input type="text" name="DisqusComments[URLRoute]" class="form-control" value="<?php echo(!empty($data['DisqusComments']['URLRoute'])) ? $data['DisqusComments']['URLRoute'] : '' ; ?>" />
        </div>
   </td>
  </tr>
  <tr>
    <td>Hide Tabs</td>
    <td>
        <div class = "col-xs-3">
            <select name="DisqusComments[HideTabs]" class="form-control">
                <option value="yes" <?php echo($data['DisqusComments']['HideTabs'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
                <option value="no" <?php echo($data['DisqusComments']['HideTabs'] == 'no') ? 'selected' : '' ; ?>>No</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td>Show Disqus Comments in product tab</td>
    <td>
        <div class="col-xs-3">
            <select name="DisqusComments[showInTab]" class="form-control">
                <option value="yes" <?php echo($data['DisqusComments']['showInTab'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
                <option value="no" <?php echo($data['DisqusComments']['showInTab'] == 'no') ? 'selected' : '' ; ?>>No</option>
            </select>
        </div>
   </td>
  </tr>
    <tr>
    <td>Disable mobile version of Disqus<span class="help">Tells the Disqus service to never use the mobile optimized version of Disqus.</span></td>
    <td>
        <div class="col-xs-3">
            <select name="DisqusComments[DisableMobile]" class="form-control">
                <option value="yes" <?php echo($data['DisqusComments']['DisableMobile'] == 'yes') ? 'selected' : '' ; ?>>Yes</option>   
                <option value="no" <?php echo($data['DisqusComments']['DisableMobile'] == 'no') ? 'selected' : '' ; ?>>No</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td>Custom CSS</td>
    <td>
        <div class="col-xs-4">
            <textarea name="DisqusComments[CustomCSS]" class="form-control" style="width:320px; height:70px;"><?php echo (!empty($data['DisqusComments']['CustomCSS'])) ? $data['DisqusComments']['CustomCSS'] : '' ; ?></textarea>
        </div>
   </td>
  </tr>
</table>