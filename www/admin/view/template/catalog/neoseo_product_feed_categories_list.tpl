<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
                <h1><?php echo $heading_title . " " . $text_module_version  . " " .$heading_title_raw; ?></h1>
                <div class="buttons">
                    <a href="<?php echo $add; ?>" class="button"><?php echo $button_insert; ?></a>
                    <a onclick="$('#form').submit();" class="button"><?php echo $button_delete; ?></a></div>
            </div>
      
            <div class="content">
	      
                <div id="update-categories" style="display: none;">
                    <form action="<?php echo $action_import; ?>" method="post" enctype="multipart/form-data" id="import" class="form-horizontal">
                        <table class="list">
                            <thead>
                            <tr>
                                <td class="center"><?php echo $entry_name_parent;?></td>
                                <td class="center"><?php echo $entry_import_categories; ?></td>
                                <td class="left"> <?php echo $column_action; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">
                                    <select name="parent_id" style="width: 800px">
                                        <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                        <?php foreach ($all_categories as $category) { ?>
                                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td class="center" style="width: 20%">
                                    <input type="file" name="upload" class="form-control"/>
                                </td>
                                <td class="left" style="width: 10%">
                                    <a onclick="$('#import').submit()" data-toggle="tooltip" title="<?php echo $button_process; ?>" class="button"> <?php echo $button_process; ?></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
	
                <div style="margin-bottom: 10px; text-align: right;">
                    <a onclick="$('#update-categories').toggle()" data-toggle="tooltip" title="<?php echo $button_update_categories; ?>">[<?php echo $button_update_categories; ?>]</a>
                </div>
	
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                    <table class="list">
                        <thead>
                        <tr>
                            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                            <td class="left"><?php echo $column_name; ?></td>
                            <td class="right"><?php echo $column_action; ?></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($categories) { ?>
                            <?php foreach ($categories as $category) { ?>
                                <tr>
                                    <td style="text-align: center;"><?php if ($category['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
                                        <?php } ?></td>
                                    <?php if ($category['href']) { ?>
                                        <td class="left"><?php echo $category['indent']; ?><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></td>
                                    <?php } else { ?>
                                        <td class="left"><?php echo $category['indent']; ?><?php echo $category['name']; ?></td>
                                    <?php } ?>
                                    <td class="right"><?php foreach ($category['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                        <?php } ?></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>