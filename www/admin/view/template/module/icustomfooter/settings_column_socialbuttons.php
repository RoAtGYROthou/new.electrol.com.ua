<div>
    <label for="ButtonsShow_<?php echo $store['store_id']; ?>"><?php echo $showsocialbuttons; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][SocialButtons][Show]" id="ButtonsShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="FacebookLikeShow_<?php echo $store['store_id']; ?>"><?php echo $facebooklikebutton; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][SocialButtons][FacebookLike][Show]" id="FacebookLikeShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['FacebookLike']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['FacebookLike']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['FacebookLike']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['FacebookLike']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="TwitterPinShow_<?php echo $store['store_id']; ?>"><?php echo $twittertweet; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][SocialButtons][TwitterPin][Show]" id="TwitterPinShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['TwitterPin']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['TwitterPin']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['TwitterPin']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['TwitterPin']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="PinterestPinShow_<?php echo $store['store_id']; ?>"><?php echo $pinterestpin; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][SocialButtons][PinterestPin][Show]" id="PinterestPinShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['PinterestPin']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['PinterestPin']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['PinterestPin']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['PinterestPin']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="GooglePlusShow_<?php echo $store['store_id']; ?>"><?php echo $googleplusbutton; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][SocialButtons][GooglePlus][Show]" id="GooglePlusShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['GooglePlus']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['GooglePlus']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['SocialButtons']['GooglePlus']['Show']) && $data[$storeCode]['Settings']['SocialButtons']['GooglePlus']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>