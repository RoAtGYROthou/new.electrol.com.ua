<div>
    <label for="PaymentIconsShow_<?php echo $store['store_id']; ?>"><?php echo $paymenticons_showpaymenticons; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][PaymentIcons][Show]" id="PaymentIconsShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo (!empty($data[$storeCode]['Settings']['PaymentIcons']['Show']) && $data[$storeCode]['Settings']['PaymentIcons']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo (!empty($data[$storeCode]['Settings']['PaymentIcons']['Show']) && $data[$storeCode]['Settings']['PaymentIcons']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div class="control-group" id="paymentIconNameParent_<?php echo $store['store_id']; ?>">
    <label for="PaymentIconsList" class="control-label"><?php echo $paymenticons_uploadicons; ?></label>
    <div class="paymenticons_container">
    	<div>
        	<input name="paymentIconName<?php echo $store['store_id']; ?>" id="paymentIconName_<?php echo $store['store_id']; ?>" type="text" placeholder="<?php echo $paymenticons_titleoftheicon; ?>" /> <span class="paymentIconsUploadButton btn btn-success" data-name-source-selector="#paymentIconNameParent_<?php echo $store['store_id']; ?>" data-name-selector="#paymentIconName_<?php echo $store['store_id']; ?>"><i class="icon-upload icon-white"></i> <?php echo $paymenticons_upload; ?></span>
        </div>
        <div>
            <span data-click-selector="#PaymentIcons" class="paymentIconsBrowse btn btn-primary" data-browse-text-selector="#PaymentIconsValue_<?php echo $store['store_id']; ?>"><i class="icon-picture icon-white"></i> <?php echo $paymenticons_addfiles; ?></span><span id="PaymentIconsValue_<?php echo $store['store_id']; ?>" class="PaymentIconsValues"></span>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div>
    <label for="PaymentIconsList"><?php echo $paymenticons_paymenticons; ?></label>
    <div class="paymenticons_container">
        <?php foreach ($images as $icon_index => $icon) : ?>
        <div class="clearfix paymenticon">
        	<div class="paymenticon_move"><?php if ($icon_index > 0) : ?><a href="<?php echo str_replace(array('{STORE}', '{TAB}', '{SUBTAB}'), array($globalStoreIndex, $globalTabsIndex, $globalSubtabsIndex), $icon['moveup']); ?>"><i class="icon-chevron-up"></i></a><?php endif; ?></div>
            <div class="paymenticon_move"><?php if ($icon_index < count($images) - 1) : ?><a href="<?php echo str_replace(array('{STORE}', '{TAB}', '{SUBTAB}'), array($globalStoreIndex, $globalTabsIndex, $globalSubtabsIndex), $icon['movedown']); ?>"><i class="icon-chevron-down"></i></a><?php endif; ?></div>
        	<div class="paymenticon_image"><img src="<?php echo $icon['path']; ?>" alt="<?php echo $icon['name']; ?>" /></div>
            <div class="paymenticon_name"><?php echo $icon['name']; ?></div>
            <div class="paymenticon_remove"><a class="btn btn-danger" onclick="return confirm('<?php echo str_replace('{IMAGE}', $icon['name'], $paymenticons_confirm_delete); ?>');" href="<?php echo str_replace(array('{STORE}', '{TAB}', '{SUBTAB}'), array($globalStoreIndex, $globalTabsIndex, $globalSubtabsIndex), $icon['delete']); ?>"><i class="icon-remove-sign icon-white"></i> <?php echo $paymenticons_delete; ?></a></div>
        </div>
        <?php endforeach; ?>
    </div>
</div>