<table id="ordersWrapper" class="table table-bordered table-hover" width="100%">
<thead>
	<tr class="table-header">
    	<th width="20%">Customer Email</th>
        <th width="15%">Customer Name</th>
		<th width="25%">Product</th>
        <th width="10%">Date</th>
        <th width="5%">Language</th>
		<th width="5%">Actions</th>
    </tr>
    </thead>
    <tbody>
<?php foreach($sources as $source) { ?>
<tr>
<td><?php echo $source['customer_email']; ?></td>
<td><?php echo $source['customer_name']; ?></td>
<td><a href="<?php echo '../index.php?route=product/product&product_id='.$source['product_id']; ?>" target="_blank"><strong><?php echo $source['product_name']; ?></strong></a></td>

<td><?php echo $source['date_created']; ?></td>
<td><?php echo $source['language']; ?></td>
<td><a onclick="removeCustomer('<?php echo $source['notifywhenavailable_id']; ?>')" class="btn btn-small btn-danger"><i class="icon-remove"></i> Remove</a><?php $limit--; ?></td>
</tr>

<?php }
    if($limit)
    while($limit--)
    {  ?>
    <?php }
?>
</tbody>
<tfoot><tr><td colspan="6"><div class="pagination"><?php echo $pagination; ?></div></td></tr></tfoot>
</table>
<div style="float:right;padding: 5px;">
	<a onclick="removeAll()" class="btn btn-small btn-info"><i class="icon-trash"></i>&nbsp;&nbsp;Remove all</a>
</div>
<script>
	function removeCustomer(notifywhenavailableID) {      
				var r=confirm("Are you sure you want to remove the customer?");
				if (r==true) {
					$.ajax({
						url: 'index.php?route=module/notifywhenavailable/removecustomer&token=<?php echo $this->session->data['token']; ?>',
						type: 'post',
						data: {'notifywhenavailable_id': notifywhenavailableID},
						success: function(response) {
						location.reload();
					}
				});
			 }
			}
	function removeAll() {      
				var r=confirm("Are you sure you want to remove all records?");
				if (r==true) {
					$.ajax({
						url: 'index.php?route=module/notifywhenavailable/removeallcustomers&token=<?php echo $this->session->data['token']; ?>',
						type: 'post',
						data: {'remove': r},
						success: function(response) {
						location.reload();
					}
				});
			 }
			}
	$(document).ready(function(){
		 $('#ordersWrapper .links a').click(function(e){
				e.preventDefault();
				$.ajax({
					url: this.href,
					type: 'get',
					dataType: 'html',
					success: function(data) {				
						$("#ordersWrapper").html(data);
					}
				});
			 });		 
		   });
</script>