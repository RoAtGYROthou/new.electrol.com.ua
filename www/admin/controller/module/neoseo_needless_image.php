<?php

require_once(DIR_SYSTEM . "/engine/neoseo_controller.php");

class ControllerModuleNeoSeoNeedlessImage extends NeoSeoController
{

    private $error = array();

    public function __construct( $registry )
    {
        parent::__construct($registry);
        $this->_moduleSysName = "neoseo_needless_image";
        $this->_logFile = $this->_moduleSysName . ".log";
        $this->debug = $this->config->get($this->_moduleSysName . "_debug") == 1;
    }

    public function index()
    {
        $this->checkLicense();
        $this->upgrade();

        $this->initLanguage('module/' . $this->_moduleSysName);
        $this->load->model('module/' . $this->_moduleSysName);
        $this->load->model('tool/' . $this->_moduleSysName);
        $this->document->setTitle($this->language->get('heading_title_raw'));

        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            if (isset($this->request->post['directory']) && is_array($this->request->post['directory'])) {
                $directories = array();

                foreach ($this->request->post['directory'] as $directory) {
                    $directories[] = array('path' => str_replace('../', '', $directory['path']), 'recursive' => $directory['recursive'] ? 1 : 0);
                }

                $this->model_tool_neoseo_needless_image->setDirectoriesDb($directories);
                if (isset($this->request->post['neoseo_needless_image_debug'])) {
                    $post = $this->request->post;
                    unset($post['directory']);
                    $this->model_setting_setting->editSetting($this->_moduleSysName, $post);
                }
            }
            $this->session->data['success'] = $this->language->get('text_success');
            if (isset($this->request->get['close'])) {
                $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
            } else {
                $this->redirect($this->url->link('module/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }
        $this->initBreadcrumbs(array(
            array("extension/module", "text_module"),
            array("module/" . $this->_moduleSysName, "heading_title_raw")
        ));

        $this->data['action_delete'] = $this->url->link('module/' . $this->_moduleSysName . '/delete', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['action'] = $this->url->link('module/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');
        $this->data['close'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['save'] = $this->url->link('module/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');
        $this->data['save_and_close'] = $this->url->link('module/' . $this->_moduleSysName, 'token=' . $this->session->data['token'] . "&close=1", 'SSL');
        $this->data['clear'] = $this->url->link('module/' . $this->_moduleSysName . '/clear', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['recheck'] = $this->url->link('module/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');

        $this->data['directories'] = $this->model_tool_neoseo_needless_image->getDirectoriesDb();
        $this->data['directories_fs'] = $this->model_tool_neoseo_needless_image->getDirectoriesFs();

        $this->initButtons();

        $this->initParamsList(array(
            "debug",
        ));

        $this->data["logs"] = $this->getLogs();

        $this->data['token'] = $this->session->data['token'];
        $this->template = 'module/' . $this->_moduleSysName . '.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/' . $this->_moduleSysName)) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function checkPermission()
    {
        return $this->user->hasPermission('modify', 'module/neoseo_needless_image') ? true : false;
    }

    public function analyze()
    {
        $files_to_delete = array();

        if (isset($this->request->post['directory']) && is_array($this->request->post['directory'])) {
            foreach ($this->request->post['directory'] as $key => $directory) {
                if ($directory['path']) {
                    $files_to_delete[$key] = $this->getNeedlessImages($directory['path'], $directory['recursive'] ? true : false);
                } else {
                    $files_to_delete[$key] = array();
                }
            }
        }

        $this->response->setOutput(json_encode($files_to_delete));
    }

    public function delete()
    {
        $this->load->language('module/neoseo_needless_image');
        $this->load->model('module/neoseo_needless_image');
        $json = array(
            'message' => '',
            'data' => array(),
        );
        if ($this->checkPermission()) {
            if (isset($this->request->post['path']) && $this->request->post['path']) {
                $recursive = isset($this->request->post['recursive']) && $this->request->post['recursive'] ? true : false;

                $deleted_files = 0;
                if (isset($this->request->post['delete']) && is_array($this->request->post['delete'])) {
                    $files_temp = $this->getNeedlessImages($this->request->post['path'], $recursive);
                    $files_to_delete_all = array();

                    foreach ($files_temp as $file) {
                        $files_to_delete_all[] = $file['path'];
                    }

                    foreach ($this->request->post['delete'] as $file) {
                        if (in_array($file, $files_to_delete_all)) {
                            if (unlink(rtrim(DIR_IMAGE . str_replace('../', '', $file), '/'))) {
                                $deleted_files++;
                            }
                        }
                    }
                }

                $json['message'] = '<div class="' . ($deleted_files ? 'success' : 'attention') . '">' . sprintf($this->language->get('text_deleted'), $deleted_files) . '</div>';
                $json['data'] = $this->getNeedlessImages($this->request->post['path'], $recursive);
            } else {
                $json['message'] = '<div class="warning">' . $this->language->get('error_directory') . '</div>';
            }
        } else {
            $json['message'] = '<div class="warning">' . $this->language->get('error_permission') . '</div>';
        }

        $this->response->setOutput(json_encode($json));
    }

    private function getNeedlessImages( $directory, $recursive )
    {
        $http_image = defined('HTTP_IMAGE') ? HTTP_IMAGE : HTTPS_CATALOG . 'image/';
        $this->load->model('tool/neoseo_needless_image');

        $used_files = $this->model_tool_neoseo_needless_image->getImagesDb();
        $files = $this->model_tool_neoseo_needless_image->getImagesFs($directory, '*', $recursive);
        $files_to_delete = array();

        foreach ($files as $file) {
            if (!in_array($file, $used_files)) {
                $files_to_delete[] = array(
                    'name' => '<a href="' . $http_image . $file . '" target="_blank">' . $file . '</a>',
                    'path' => $file,
                );

                $file_info = pathinfo($file);

                $cached_files = $this->model_tool_neoseo_needless_image->getImagesFsCached($file);
                foreach ($cached_files as $cached_file) {
                    $cached_file_info = pathinfo($cached_file);
                    $cached_file_size = str_replace($file_info['filename'] . '-', '', $cached_file_info['filename']);

                    $files_to_delete[] = array(
                        'name' => '<a href="' . $http_image . $cached_file . '" target="_blank">' . $file . '</a> <b>(cache) [' . $cached_file_size . ']</b>',
                        'path' => $cached_file,
                    );
                }
            }
        }

        return $files_to_delete;
    }

}

?>
