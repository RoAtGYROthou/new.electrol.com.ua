<?php $passURLRoute = (!empty($data['DisqusComments']['URLRoute'])) ? ($data['DisqusComments']['URLRoute'] == $this->request->get['route']) : true; ?>
<?php if($passURLRoute) { ?>
<?php if($data['DisqusComments']['Enabled'] != 'no'): ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $currenttemplate?>/stylesheet/disquscomments.css" />
<script type="text/javascript">	
        var disqus_shortname = '<?php echo $data['DisqusComments']['ModName']; ?>';
		<?php if (isset($this->request->get['product_id'])) { ?>
			var disqus_identifier = 'prod-<?php echo $this->request->get['product_id']; ?>';
		<?php } ?>
		<?php if($data['DisqusComments']['DisableMobile'] == 'yes') { ?>
			var disqus_disable_mobile = true;
		<?php } ?>
	       (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php if($data['DisqusComments']['showInTab'] == 'no') { ?>
<!-- START DisqusComments -->
<div id="disqus_thread" style="width: <?php echo $data['DisqusComments']['Width']; ?>px;"></div>
<?php } ?>
<style type="text/css">
<?php if($data['DisqusComments']['HideTabs'] != 'no'): ?>
.htabs, .tab-content {
	display:none !important;	
}
<?php endif; ?>
<?php echo htmlspecialchars_decode($data['DisqusComments']['CustomCSS']); ?>
</style>
<!-- END DisqusComments -->

<?php endif; ?>
<?php } ?>