<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <div class="box">
            <div class="heading">
                <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
                <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>
                <div class="buttons">
                    <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                    <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
            </div>
            <div class="content">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <table class="form">
                        <tr>
                            <td><?php echo $entry_format_name; ?></td>
                            <td> <input name="format[feed_format_name]"
                                        value="<?php echo $format['feed_format_name']?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_format_xml_code; ?></td>
                            <td> <textarea name="format[format_xml]" cols="100" rows='20'><?php echo $format['format_xml'];?></textarea>
                            </td>
                        </tr>
                        <input type="hidden"
                               name="<?php echo isset($format['product_feed_format_id'])? 'format[product_feed_format_id]': '';?>"
                               value="<?php echo isset($format['product_feed_format_id'])?$format['product_feed_format_id']: '';?>"/>
                    </table>

                </form>
            </div>
        </div>
    </div>
<?php echo $footer; ?>