<div>
    <label for="TwitterShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Twitter][Show]" id="TwitterShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['Twitter']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['Twitter']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionTwitter_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionTwitter_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][twitter]" value="<?php echo $data[$storeCode][$langCode]['Positions']['twitter']; ?>" />
</div>
<div>
    <label for="TwitterTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="TwitterTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Twitter][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Twitter']['Title']; ?>" />
</div>
<div>
    <label for="TwitterProfile_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $twitter_profile; ?></label>
    <input type="text" id="TwitterProfile_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Twitter][Profile]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['Twitter']['Profile']; ?>" />
</div>
<div>
    <label for="TwitterWidgetID_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $twitter_widget_id; ?></label>
    <input type="text" id="TwitterWidgetID_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][Twitter][WidgetID]" value="<?php echo !empty($data[$storeCode][$langCode]['Widgets']['Twitter']['WidgetID']) ? $data[$storeCode][$langCode]['Widgets']['Twitter']['WidgetID'] : ''; ?>" />
</div>