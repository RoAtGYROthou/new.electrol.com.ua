<div>
    <label for="ContactFormShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][Show]" id="ContactFormShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['ContactForm']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['ContactForm']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionContactform_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionContactform_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][contactform]" value="<?php echo $data[$storeCode][$langCode]['Positions']['contactform']; ?>" />
</div>
<div>
    <label for="ContactFormUseCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_captchaspamprotection; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][UseCaptcha]" id="ContactFormUseCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['ContactForm']['UseCaptcha'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['ContactForm']['UseCaptcha'] == 'false') ? 'selected=selected' : '';?>><?php echo $no?></option>
    </select>
</div>
<div>
    <label for="ContactFormTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="ContactFormTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['Title']; ?>" />
</div>
<div>
    <label for="ContactFormEmail_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_sendemailsto; ?></label>
    <input type="text" id="ContactFormEmail_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][Email]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['Email']; ?>" />
</div>
<div>
    <label for="ContactFormEmailSubject_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_emailsubject; ?></label>
    <input type="text" id="ContactFormEmailSubject_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][EmailSubject]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['EmailSubject']; ?>" />
</div>
<div>
    <label for="ContactFormLabelName_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_nameboxlabel; ?></label>
    <input type="text" id="ContactFormLabelName_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelName]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelName']; ?>" />
</div>
<div>
    <label for="ContactFormLabelEmail_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_emailboxlabel; ?></label>
    <input type="text" id="ContactFormLabelEmail_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelEmail]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelEmail']; ?>" />
</div>
<div>
    <label for="ContactFormLabelMessage_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_messageboxlabel; ?></label>
    <input type="text" id="ContactFormLabelMessage_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelMessage]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelMessage']; ?>" />
</div>
<div>
    <label for="ContactFormMaxMessageLength_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_message_max_length; ?></label>
    <input type="text" id="ContactFormMaxMessageLength_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][MaxMessageLength]" value="<?php echo !empty($data[$storeCode][$langCode]['Widgets']['ContactForm']['MaxMessageLength']) ? $data[$storeCode][$langCode]['Widgets']['ContactForm']['MaxMessageLength'] : 1000; ?>" />
</div>
<div>
    <label for="ContactFormLabelCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_captchaboxlabel; ?></label>
    <input type="text" id="ContactFormLabelCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelCaptcha]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelCaptcha']; ?>" />
</div>
<div>
    <label for="ContactFormLabelSend_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_sendbuttonlabel; ?></label>
    <input type="text" id="ContactFormLabelSend_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelSend]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelSend']; ?>" />
</div>
<div>
    <label for="ContactFormLabelSuccess_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_successfulsentmessage; ?></label>
    <input type="text" id="ContactFormLabelSuccess_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelSuccess]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelSuccess']; ?>" />
</div>
<div>
    <label for="ContactFormLabelRequired_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_requiredfieldmessage; ?></label>
    <input type="text" id="ContactFormLabelRequired_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelRequired]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelRequired']; ?>" />
</div>
<div>
    <label for="ContactFormLabelNotValid_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_notvalidmessage; ?></label>
    <input type="text" id="ContactFormLabelNotValid_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelNotValid]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelNotValid']; ?>" />
</div>
<div>
    <label for="ContactFormLabelInvalidCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $contactform_notvalidcaptcha; ?></label>
    <input type="text" id="ContactFormLabelInvalidCaptcha_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][ContactForm][LabelInvalidCaptcha]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['ContactForm']['LabelInvalidCaptcha']; ?>" />
</div>