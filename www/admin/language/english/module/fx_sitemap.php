<?php
$_['heading_title']    = '<b>#<span style="color:#29D">F</span><span style="color:#F55">X</span> Sitemap⁴</b>';
$_['text_module']      = 'Modules';
$_['text_edit']        = 'Settings';
$_['text_success']        = 'Saved';
$_['text_modules']        = 'Modules';
$_['text_defalt']        = 'Default parameters';
$_['text_key']        = 'File-generate protect key';
$_['produsts']        = 'Products';
$_['categories']        = 'Categories';
$_['brands']        = 'Manufacturers';
$_['other']        = 'Other Pages';
$_['status']        = 'Status';
$_['service']        = 'Service';
$_['text_only_seo_url']        = 'Only with SEO URL';
$_['text_postfix']        = 'URL Postfix';
$_['text_sort']        = 'Sorting';
$_['information']        = 'Information';