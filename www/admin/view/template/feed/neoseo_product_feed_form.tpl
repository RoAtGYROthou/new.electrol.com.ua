<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <div class="box">
        <div class="heading">
            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
            <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="feed[status]">
                                <?php if ($feed["status"]) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_format; ?></td>
                        <td><select name="feed[id_format]">
                                <?php  foreach($formats as $format){ ?>
                                <option value=<?php echo $format['product_feed_format_id']?>" <?php if (isset($feed["id_format"]) && $feed["id_format"]==$format['product_feed_format_id'] ) { ?>selected="selected" <?php } ?>><?php echo $format['feed_format_name']?></option>
                                <?php }?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_feed_name; ?></td>
                        <td><input name="feed[feed_name]" type="text"
                                   value="<?php echo $feed["feed_name"]; ?>" size="40" maxlength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_feed_shortname; ?></td>
                        <td><input name="feed[feed_shortname]"
                                   type="text" value="<?php echo $feed['feed_shortname']; ?>" size="40" maxlength="20" />
                            <br/>
                            <br/>
                        </td>
                    </tr>

                    <tr>
                        <td><label class="col-sm-4"><?php echo $entry_store; ?></label></td>
                        <td>
                            <div style="height: 150px; overflow: auto; margin-bottom:0;">
                                <div class="checkbox">
                                    <label>
                                        <?php if (in_array(0, $product_feed_store)) { ?>
                                        <input type="checkbox" name="feed[product_feed_store][]" value="0" checked="checked" />
                                        <?php echo $text_default; ?>
                                        <?php } else { ?>
                                        <input type="checkbox" name="feed[product_feed_store][]" value="0" />
                                        <?php echo $text_default; ?>
                                        <?php } ?>
                                    </label>
                                </div>

                                <?php foreach ($stores as $store) { ?>
                                <div class="checkbox">
                                    <label>
                                        <?php if (in_array($store['store_id'], $product_feed_store)) { ?>
                                        <input type="checkbox" name="feed[product_feed_store][]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                                        <?php echo $store['name']; ?>
                                        <?php } else { ?>
                                        <input type="checkbox" name="feed[product_feed_store][]" value="<?php echo $store['store_id']; ?>" />
                                        <?php echo $store['name']; ?>
                                        <?php } ?>
                                    </label>
                                </div>
                                <?php } ?>
                            </div>

                            <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a>
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_ip_list; ?></td>
                        <td><textarea rows="8" cols="80" name="feed[ip_list]"><?php echo $feed["ip_list"]; ?></textarea></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_feed_language; ?></td>
                        <td>
                            <select name="feed[language_id]">
                                <?php foreach($languages as $language_id=> $language){ ?>
                                    <option value="<?php echo $language_id;?>"
                                        <?php echo ($language_id==$feed['language_id'])? 'selected="selected"': '';?>><?php echo $language; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_use_main_category; ?></td>
                        <td><select name="feed[use_main_category]">
                                <?php if ($feed["use_main_category"]) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <!-- NeoSeo Exchange1c - begin -->
                    <?php if($use_warehouses) { ?>
                        <tr>
                        <td><?php echo $entry_use_warehouse_quantity; ?></td>
                        <td>
                            <select name="feed[use_warehouse_quantity]" class='form-control' id="use_warehouse_quantity">
                                <option value="0"<?php if( !$feed['use_warehouse_quantity'] ) echo 'selected="selected"';?>><?php echo $text_disabled; ?></option>
                                <option value="1"<?php if( $feed['use_warehouse_quantity'] ) echo 'selected="selected"';?>><?php echo $text_enabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr class="show_warehouses">
                        <td><?php echo $entry_warehouse; ?></td>
                        <td><div class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php foreach( $warehouses as $warehouse_id => $warehouse_name) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div class="<?php echo $class; ?>">
                                        <input class="category" type="checkbox" name='feed[warehouses][]' value="<?php echo $warehouse_id;?>" data="<?php echo $warehouse_name;?>"
                                        <?php if( in_array($warehouse_id, explode(',',$feed['warehouses']))) echo ' checked="checked"'; ?>/>
                                        <?php echo $warehouse_name;?>
                                    </div>
                                <?php } ?>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                    </tr>
                    <?php } ?>
                    <!-- NeoSeo Exchange1c - end -->
                    <tr>
                        <td><?php echo $entry_use_categories; ?></td>
                        <td>
                            <select name="feed[use_categories]" id="use_categories">
                                <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                <?php foreach ($feedMainCategories as $category) { ?>
                                    <?php if ($category['category_id'] == $feed['use_categories']) { ?>
                                        <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr id="list_categories_default">
                        <td><?php echo $entry_category; ?></td>
                        <td>
                            <div class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php foreach ($categories as $category_id=> $category) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div class="<?php echo $class; ?>">
                                        <?php if (in_array($category_id, explode(',',$feed["categories"])) && $feed['use_categories'] == 0) { ?>
                                            <input type="checkbox" name="feed[categories][]" value="<?php echo $category_id; ?>" checked="checked" />
                                            <?php echo $category; ?>
                                        <?php } else { ?>
                                            <input type="checkbox" name="feed[categories][]" value="<?php echo $category_id; ?>" />
                                            <?php echo $category; ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                    </tr>
                    <?php foreach ($feedMainCategories as $main_category) { ?>
                        <tr id="list_categories_<?php echo $main_category['category_id']; ?>">
                            <td><?php echo $entry_category; ?></td>
                            <td>
                                <div class="scrollbox">
                                    <?php $class = 'odd'; ?>
                                    <?php foreach($feedCategories as $category) { ?>
                                        <?php if($category['parent_id']==$main_category['category_id']) { ?>
                                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                            <div class="<?php echo $class; ?>">
                                                <?php if (in_array($category['category_id'], explode(',',$feed["categories"])) && $feed['use_categories'] == $main_category['category_id']) { ?>
                                                    <input type="checkbox" name="feed[categories][]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                                                    <?php echo $category['name']; ?>
                                                <?php } else { ?>
                                                    <input type="checkbox" name="feed[categories][]" value="<?php echo $category['category_id']; ?>" />
                                                    <?php echo $category['name']; ?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td><?php echo $entry_manufacturers; ?></td>
                        <td><div class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php foreach ($manufacturers as $manufacturer_id => $manufacturer) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div class="<?php echo $class; ?>">
                                        <?php if (in_array($manufacturer_id, explode(',',$feed["manufacturers"]))) { ?>
                                            <input type="checkbox" name="feed[manufacturers][]" value="<?php echo $manufacturer_id; ?>" checked="checked" />
                                            <?php echo $manufacturer; ?>
                                        <?php } else { ?>
                                            <input type="checkbox" name="feed[manufacturers][]" value="<?php echo $manufacturer_id; ?>" />
                                            <?php echo $manufacturer; ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_products; ?></td>
                        <td><input type="text" name="product" value="" size="40"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><div id="product-list" class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php if (isset($feed['product_list']))  { ?>
                                <?php foreach ($feed['product_list'] as $product) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div id="product-list-<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"> <?php echo $product['name']; ?><img src="view/image/delete.png" alt="" />
                                    <input type="hidden" name="feed[product_list][]" value="<?php echo $product['product_id']; ?>" />
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_not_unload; ?></td>
                        <td><input type="text" name="not_unload" value="" size="40"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><div id="product-not-unload" class="scrollbox">
                                <?php $class = 'odd'; ?>
                                <?php if (isset($feed['product_not_unload']))  { ?>
                                <?php foreach ($feed['product_not_unload'] as $product) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div id="product-not-unload-<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"> <?php echo $product['name']; ?><img src="view/image/delete.png" alt="" />
                                    <input type="hidden" name="feed[product_not_unload][]" value="<?php echo $product['product_id']; ?>" />
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_currency; ?></td>
                        <td><select name="feed[currency]">
                                <?php foreach($currencies as $currency_id=> $currency){ ?>
                                    <option value="<?php echo $currency_id;?>"
                                        <?php echo ($currency_id==$feed['currency'])? 'selected="selected"': '';?>
                                    ><?php echo $currency; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_use_original_images; ?></td>
                        <td><select name="feed[use_original_images]" id="use_original_images">
                                <?php if ($feed["use_original_images"]) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_image_pass; ?></td>
                        <td><select name="feed[image_pass]" id="image_pass">
                                <?php if ($feed["image_pass"]) { ?>
                                    <option value="1" selected="selected"><?php echo $text_image_pass[1]; ?></option>
                                    <option value="0"><?php echo $text_image_pass[0]; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_image_pass[1]; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_image_pass[0]; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="show_params_image">
                        <td><?php echo $entry_image_width; ?></td>
                        <td><input name="feed[image_width]" type="text" value="<?php echo $feed["image_width"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr class="show_params_image">
                        <td><?php echo $entry_image_height; ?></td>
                        <td><input name="feed[image_height]" type="text" value="<?php echo $feed["image_height"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_pictures_limit; ?></td>
                        <td><input name="feed[pictures_limit]" type="text" value="<?php echo $feed["pictures_limit"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_cat_names_separathor; ?></td>
                        <td><input name="feed[cat_names_separathor]" type="text" value="<?php echo $feed["cat_names_separathor"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_product_markup_type; ?></td>
                        <td><select name="feed[product_markup_type]" id="product_markup_type">
                                <?php if ($feed["product_markup_type"]) { ?>
                                <option value="1" selected="selected"><?php echo $text_product_markup_type[1]; ?></option>
                                <option value="0"><?php echo $text_product_markup_type[0]; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_product_markup_type[1]; ?></option>
                                <option value="0" selected="selected"><?php echo $text_product_markup_type[0]; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_product_markup; ?></td>
                        <td><input name="feed[product_markup]" type="text" value="<?php echo $feed["product_markup"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_product_markup_option; ?></td>
                        <td><input name="feed[product_markup_option]" type="text" value="<?php echo $feed["product_markup_option"]; ?>" size="40" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_replace; ?></td>
                        <td>
                            <?php if (!isset($feed['replace_break'])) { $feed['replace_break'] = 0;  } ?>
                            <select id="active" name="feed[replace_break]" class="form-control">

                                <option value="0" <?php if (!$feed['replace_break']) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                                <option value="1" <?php if ($feed['replace_break']) { ?>selected<?php } ?>><?php echo $text_enabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_sql_code; ?></td>
                        <td><textarea rows="8" cols="80" name="feed[sql_code]"><?php echo $feed["sql_code"]; ?></textarea></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_sql_code_before; ?></td>
                        <td><textarea rows="8" cols="80" name="feed[sql_code_before]"><?php echo $feed["sql_code_before"]; ?></textarea></td>
                    </tr>
                    <input type="hidden" name="<?php echo isset($feed['product_feed_id'])? 'feed[product_feed_id]': '';?>" value="<?php echo isset($feed['product_feed_id'])?$feed['product_feed_id']: '';?>"/>
        </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script>
    $("input[name*='feed_name']").blur(function () {
        var value = convertCyr($(this).val());
        $("input[name*='feed_shortname']").val(value);
    });
    var rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g);
    var eng = "shh sh ch cz yu ya yo zh _ y e a b v g d e z i j k l m n o p r s t u f x _".split(/ +/g);
    function convertCyr(text) {
        var x;
        text = text.toLowerCase();
        for (x = 0; x < rus.length; x++) {
            text = text.split(rus[x]).join(eng[x]);
        }

        text = text.replace(/[^a-z0-9_\.\s]/g, "");
        text = text.replace(/\./g, "_");
        text = text.replace(/\s/g, "_");
        return text;
    }
    $(function () {
        showCategories($('#use_categories').val());
        showParamsImage($('#use_original_images').val());
    });

    $('#use_categories').change(function () {
        $('[id^=list_categories_]').find(':checkbox').prop('checked', false);
        showCategories($(this).val());
    });

    $('#use_original_images').change(function () {
        showParamsImage($(this).val());
    });
    function showCategories(value) {
        $('[id^=list_categories_]').hide();
        if (value != 0) {
            $('#list_categories_' + value).show();
        } else {
            $('#list_categories_default').show();
        }
    }

    function showParamsImage(value) {
        if (value != 0) {
            $('.show_params_image').hide();
        } else {
            $('.show_params_image').show();
        }
    }

    /* NeoSeo Exchange1c - begin */
    $('#use_warehouse_quantity').change(function () {
        showWarehouses($(this).val());
    });

    $(function () {
        showWarehouses($('#use_warehouse_quantity').val());
    });

     function showWarehouses(value) {
        if (value == 0) {
            $('.show_warehouses').hide();
        } else {
            $('.show_warehouses').show();
        }
    }
    /* NeoSeo Exchange1c - end */

    $('input[name=\'not_unload\']').autocomplete({
        delay: 500,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('#product-not-unload-' + ui.item.value).remove();

            $('#product-not-unload').append('<div id="product-not-unload-' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" />' +
                '<input type="hidden" name="feed[product_not_unload][]" value="' + ui.item.value + '" />' +
                '</div>');

            $('#product-not-unload div:odd').attr('class', 'odd');
            $('#product-not-unload div:even').attr('class', 'even');

            return false;
        },
        focus: function(event, ui) {
            return false;
        }
    });

    //Products
    $('input[name=\'product\']').autocomplete({
        delay: 500,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('#product-list-' + ui.item.value).remove();

            $('#product-list').append('<div id="product-list-' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" />' +
                '<input type="hidden" name="feed[product_list][]" value="' + ui.item.value + '" />' +
                '</div>');

            $('#product-list div:odd').attr('class', 'odd');
            $('#product-list div:even').attr('class', 'even');

            return false;
        },
        focus: function(event, ui) {
            return false;
        }
    });

    $('#product-list div img').live('click', function() {
        $(this).parent().remove();

        $('#product-list div:odd').attr('class', 'odd');
        $('#product-list div:even').attr('class', 'even');
    });
</script>