<?php echo $header; ?>
<div class="content">
    <ul class="breadcrumb store_pickup_breadcrumb">
    	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
    		<li><?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    	<?php } ?>
    </ul>
    <div class="row">
    	<?php echo $column_left; ?>
    	<?php if ($column_left && $column_right) { ?>
   			<?php $class = 'col-sm-6'; ?>
    	<?php } elseif ($column_left || $column_right) { ?>
    		<?php $class = 'col-sm-9'; ?>
    	<?php } else { ?>
    		<?php $class = 'col-sm-12'; ?>
    	<?php } ?>
    	<div id="content" class="<?php echo $class; ?>">
        	<?php echo $content_top; ?>
   			<h1><?php echo $heading_title; ?></h1>
			<div class="map-index row">
            	<div class="col-xs-12 col-sm-3  storepickup-data">
                	<ul class="list-group store-list" data-name="store-select-dropdown">
                    	<?php $i=0; foreach ($stores as $store) { ?>
                            <li class="list-group-item store-list-item<?php echo ($i==0) ? ' active' : ''; ?>" data-value="store-<?php echo $store['id']; ?>">    
                                <h3 class="store-name"><?php echo $store['Name'][$language_id]; ?></h3>
                                <p><?php echo $store['Address'][$language_id]; ?>&nbsp;</p>
                                <div class="store-office">
                                    <span class="phone-label"><i class="fa fa-phone"></i></span>
                                    <span class="phone-number"><?php echo $store['Phone']; ?></span>
                                </div>
                            </li>
                        <?php $i++; } ?>
					</ul>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="widget-map-holder">
                        <div id="currency_widget_map"></div>
                    </div>
                    <script type="text/javascript">
                    (function($) {
                        function initialize_offices_map() {
									<?php $i=0; foreach ($stores as $store) { ?>
										<?php if ($i==0) { ?>
											var LAT_LNG = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);
											var mapOptions = {
												center: LAT_LNG,
												zoom: 15
											};
											offices_map = new google.maps.Map(document.getElementById("currency_widget_map"), mapOptions);
										<?php } ?>
									
										LAT_LNG_<?php echo $store['id']; ?> = new google.maps.LatLng(<?php echo $store['Longtitude']; ?>, <?php echo $store['Latitude']; ?>);
										office_marker_362 = new google.maps.Marker({
											position: LAT_LNG_<?php echo $store['id']; ?>,
											map: offices_map,
											title: '<?php echo $store['Name'][$language_id]; ?>'
										});
									<?php $i++; } ?>
                            }
    
                        google.maps.event.addDomListener(window, 'load', initialize_offices_map);
    
                        $(document).ready(function() {
						   $('.store-list-item').on("click", function(event) {
								$('.store-list-item').removeClass('active');
								var office_id = $(this).data('value');
								$(this).addClass('active');
					
								// Map Office Marker Switch
								if (typeof window["LAT_LNG_" + office_id.replace('store-','')] !== 'undefined') {
									var LAT_LNG = window["LAT_LNG_" + office_id.replace('store-','')];
									offices_map.panTo(LAT_LNG);
								}
							});
						});
                    })(jQuery); 
                    </script>
            	</div>
            </div>
            <div class="buttons" style="border:none;">
            	<div class="pull-right"><a href="<?php echo $continue; ?>" class="button btn-info" style="color:white;"><?php echo $button_continue; ?></a></div>
          	</div>
            <?php echo $content_bottom; ?>
        </div>
    	<?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?> 