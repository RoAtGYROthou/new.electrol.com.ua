<div class="box">
  <?php if ($title) { ?>
  <div class="box-heading"><?php echo $title; ?></div>
  <?php } ?>
  <div class="box-content">
    <?php echo $message; ?>
  </div>
</div>