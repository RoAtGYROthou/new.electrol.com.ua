<?php

class ModelModuleAvail extends Model {	

    public function getAvailabilities($data = array()) {
        if ($data) {
            $sql = "SELECT a.*,p.quantity FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '0' ";


            $sort_data = array(
                'id',
                'status'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {

            $query = $this->db->query("SELECT a.*,p.quantity FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '0' ");
            
            return $query->rows;
        }
    }
    public function getProcessed($data = array()) {
        if ($data) {
            $sql = "SELECT a.*,p.quantity  FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '1' ";


            $sort_data = array(
                'id',
                'status'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {

            $query = $this->db->query("SELECT a.*,p.quantity  FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '1' ");
            
            return $query->rows;
        }
    }
	public function getProductQuantity($product_id) {
        $query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product where product_id = " . $product_id);
		
		foreach ( $query->row as $key => $value ) {
			return $value;
		}        
    }
	public function notify() {

        $query = $this->db->query("select * from " . DB_PREFIX . "product p, " . DB_PREFIX. "avail a where p.quantity > 0 AND p.product_id = a.product_id AND a.status = 0");

        return $query->rows;
    }	
	public function notifyByProductId($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "avail where product_id = ". $product_id . " AND status = 0");
		
		$result = $query->rows;
		
		$this->language->load('module/avail');
			
		
		if (!empty($result)) {
			foreach ( $result as $info ) {
			    $link = HTTP_CATALOG . 'index.php?route=product/product&product_id='. $info['product_id'];	
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				
				$mail->setTo($info['email']);
				$mail->setFrom($info['admin_email']);
				$mail->setSender('\''.$this->language->get('email_subject').'\'');
				$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $info['name'], ENT_QUOTES, 'UTF-8')));
							
				$mail_text = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Document</title></head><body>";
				$mail_text .="<p>" . html_entity_decode($info['name'].', '.$this->language->get('text_mail_send'). "</p>");
				$mail_text .= "<p>" . $this->language->get('text_product') .': ' . $info['product'] . "</p>";
				$mail_text .= "<p>" . $this->language->get('text_link_page') . ": " . " <a href=" . $link . ">" . $info['product'] . "</a></p>";
				$mail_text .= "<p>" . $this->language->get('text_price') . ': ' . $info['price'] . "</p></body></html>";

				$mail->setHtml($mail_text);
				$mail->send();
				$this->model_module_avail->changeMailStatus($info['id'], 1);
			}		
		}		
		
	}
     public function getAvailability($id) {

         if ($id) {
             $sql = "SELECT * FROM " . DB_PREFIX . "avail where id = ". $id ."";


             $query = $this->db->query($sql);

             return $query->row;
         }
     }


    public function getTotalCalls() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "avail");

        return $query->row['total'];
    }
    

    
    public function changeMailStatus($id, $status) {
        if ($this->db->query("UPDATE " . DB_PREFIX . "avail SET status = '" . (int) $status . "' WHERE id = '" . (int) $id . "'")) {
            return true;            
        } else {
            return false;
        }
    }
	public function deleteNotifications($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "avail WHERE id = '" . (int)$id . "'");		
		
	}
	
    public function install() {

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "avail` (
            `id` int(6) NOT NULL AUTO_INCREMENT,
            `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `email` varchar(50) NOT NULL,
			`admin_email` varchar(50) NOT NULL,
			`product` varchar(50) NOT NULL,
			`product_id` int(11) NOT NULL,
			`price` varchar(50) NOT NULL,
            `link_page` varchar(255) NOT NULL,
			`name` varchar(50) NOT NULL,
            `comment` text NOT NULL,
            `status` varchar(50) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
    }

}

?>