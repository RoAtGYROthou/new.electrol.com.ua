<?php
// Heading
$_['heading_title']    = 'Extended SEO by <a href="http://www.apwai.net" onclick="return !window.open(this.href)"> APWAI.NET</a>';

// Text 
$_['text_success']     = 'Success: You have saved Extended SEO!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Extended SEO!';
?>