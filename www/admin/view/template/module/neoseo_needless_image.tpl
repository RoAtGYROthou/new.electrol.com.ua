<?php echo $header; ?>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if (isset($success) && $success) { ?>
        <div class="success" style="display: none;"><?php echo $success; ?></div>
        <script>jQuery(".success").slideDown().delay(1000).slideUp();</script>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>

            <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>

            <div class="buttons">
                <?php if( !isset($license_error) ) { ?>
                    <a onclick="beforeSaveForm();  $('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                    <a onclick="beforeSaveForm(); $('#form').attr('action','<?php echo $save_and_close; ?>'); $('#form').submit();" class="button"><span><?php echo $button_save_and_close; ?></span></a>
                <?php } else { ?>
                    <a onclick="location = '<?php echo $recheck; ?>';" class="button"><?php echo $button_recheck; ?></a>
                <?php } ?>
                <a onclick="location = '<?php echo $close; ?>';" class="button"><?php echo $button_close; ?></a>
            </div>
        </div>
        <div class="content">
            <div id="tabs" class="htabs">
                <a class="tabs" href="#tab-params"><?php echo $tab_params; ?></a>
                <a class="tabs" href="#tab-logs"><?php echo $tab_logs; ?></a>                        
                <a href="#tab-support"><?php echo $tab_support; ?></a>
                <a class="tabs" href="#tab-license"><?php echo $tab_license; ?></a>
            </div>

                          
            <?php if( !isset($license_error) ) { ?>
                <div id="tab-params">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                        <input type="hidden" name="neoseo_needless_image_debug"  id='debug_hidden'/>
                        <table id="directory" class="list">
                            <thead>
                            <tr>
                                <td class="left"><?php echo $entry_directory; ?></td>
                                <td class="left"><?php echo $entry_recursive; ?></td>
                                <td></td>
                            </tr>
                            </thead>
                            <?php foreach ($directories as $directory_row => $directory) { ?>
                                <tbody id="directory-row<?php echo $directory_row; ?>">
                                <tr>
                                    <td class="left">
                                        <select name="directory[<?php echo $directory_row ?>][path]">
                                            <option value=""><?php echo $text_select_dir; ?></option>
                                            <?php foreach($directories_fs as $directory_fs) { ?>
                                                <option value="<?php echo $directory_fs; ?>"<?php echo $directory_fs == $directory['path'] ? ' selected="selected"' : ''; ?>><?php echo $directory_fs; ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php //print_r($directories_fs); ?>
                                    </td>
                                    <td class="left">
                                        <select name="directory[<?php echo $directory_row ?>][recursive]">
                                            <option value="0"><?php echo $text_no; ?></option>
                                            <option value="1"<?php echo $directory['recursive'] ? ' selected="selected"' : ''; ?>><?php echo $text_yes; ?></option>
                                        </select>
                                    </td>
                                    <td class="left">
                                        <a onclick="$('#directory-row<?php echo $directory_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
                                    </td>
                                </tr>
                                </tbody>
                            <?php } ?>
                            <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td class="left"><a onclick="addDirectory();" class="button"><?php echo $button_add_dir; ?></a></td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                    <table id="analyze" class="list">
                        <tfoot>
                        <tr>
                            <td class="center">
                                <a onclick="analyze();" class="button" style="font-size:1.3em;"><?php echo $button_analyze; ?></a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                          
            <?php } else { ?>
                <div><?php echo $license_error; ?></div>
            <?php } ?>
                          
                          
                          
            <div id="tab-logs">
                <div class="buttons" style="float: right"><a onclick="$('#form').attr('action', '<?php echo $clear; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button delete" ><?php echo $text_clear_log; ?></a></div>
                <div style="clear:both"><br></div>
                <textarea style="width: 98%; height: 300px; padding: 5px; border: 1px solid #CCCCCC; background: #FFFFFF; overflow: scroll;"><?php echo $logs; ?></textarea>
            </div>                

            <div id="tab-support">
           
                <table class="form">
                    <tr>
                        <td><?php echo $entry_debug; ?></td>
                        <td>
                            <select name="neoseo_needless_image_debug_select" id='debug_select'>
                                <option <?php if ($neoseo_needless_image_debug == 0) { ?> selected="selected" <?php } ?> value="0"><?php echo $text_disabled; ?></option>
                                <option <?php if ($neoseo_needless_image_debug == 1) { ?> selected="selected" <?php } ?> value="1"><?php echo $text_enabled; ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><?php echo $mail_support; ?></td>
                    </tr>
                </table>
              
            </div>  

            <div id="tab-license">
                <?php echo $module_licence; ?>
            </div>
        
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('#tabs a').tabs();

    window.token = '<?php echo $token; ?>';

    //--></script>    
<script type="text/javascript"><!--
    var directory_row = <?php echo isset($directory_row) ? ++$directory_row : 0; ?>;
    function beforeSaveForm(){
        var value=$('#debug_select').val();
        $('#debug_hidden').val(value);
    }

    function addDirectory() {
        var html = '';
	
        html  = '<tbody id="directory-row' + directory_row + '">';
        html += '	<tr>';
        html += '		<td class="left">';
        html += '			<select name="directory[' + directory_row + '][path]" id="directory' + directory_row + '">';
        html += '				<option value=""><?php echo $text_select_dir; ?></option>';
        <?php foreach($directories_fs as $directory_fs) { ?>
        html += '				<option value="<?php echo addslashes($directory_fs); ?>"><?php echo addslashes($directory_fs); ?></option>';
        <?php } ?>
        html += '			</select>';
        html += '		</td>';
        html += '		<td class="left">';
        html += '			<select name="directory[' + directory_row + '][recursive]">';
        html += '				<option value="0"><?php echo $text_no; ?></option>';
        html += '				<option value="1"><?php echo $text_yes; ?></option>';
        html += '			</select>';
        html += '		</td>';
        html += '		<td class="left">';
        html += '			<a onclick="$(\'#directory-row' + directory_row + '\').remove();" class="button"><?php echo $button_remove; ?></a>';
        html += '		</td>';
        html += '	</tr>';
        html += '</tbody>';
	
        $('#directory tfoot').before(html);
	
        directory_row++;
    }

    function analyze() {
        var post_data = $('#form').serialize();
        var html = '';
        var inner_html = '';
	
        $('#analyze thead, #analyze tbody').remove();
	
        $("#form select[name*='[path]']").each(function(index){
            html += '<thead id="analyze-head-' + index + '"' + ($(this).val() == '' ? ' style="display:none;"' : '') + '>';
            html += '	<tr>';
            html += '		<td class="left">' + $(this).val() + ($("#form select[name*='[" + index + "][recursive]']").val() == 1 ? ' <span style="font-weight:normal;">(+ <?php echo utf8_strtolower($entry_recursive); ?>)</span>' : '') + '</td>';
            html += '	</tr>';
            html += '</thead>';
            html += '<tbody id="analyze-body-' + index + '"' + ($(this).val() == '' ? ' style="display:none;"' : '') + '>';
            html += '	<tr>';
            html += '		<td class="center"><img src="view/image/loading.gif" class="loading"></td>';
            html += '	</tr>';
            html += '</tbody>';
        });
	
        $('#analyze tfoot').before(html);
	
        $.ajax({
            url: 'index.php?route=module/neoseo_needless_image/analyze&token=<?php echo $token; ?>',
            type: 'post',
            data: post_data,
            dataType: 'json',
            success: function(json) {
                if (json) {
                    var dir_length = json.length;
				
                    for (var i = 0; i < dir_length; i++) {
                        files_length = json[i].length;
                        if (files_length) {
                            inner_html  = prepareCheckboxesForm(json[i], i);
                        } else {
                            inner_html = '<div class="attention" style="display: inline-block;"><?php echo $text_no_files_to_delete; ?></div>'
                        }
					
                        $('#analyze-body-' + i + ' td').html(inner_html);
                    }
                } else {
                    inner_html  = '<tbody>';
                    inner_html += '	<tr>';
                    inner_html += '		<td class="center"><div class="warning" style="display: inline-block;"><?php echo $error_error; ?></div></td>';
                    inner_html += '	</tr>';
                    inner_html += '</tbody>';
				
                    $('#analyze thead, #analyze tbody').remove();
                    $('#analyze tfoot').before(inner_html);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function deleteFiles(index) {
        var post_data = $('#form-delete-' + index).serialize();
        var html = '';
	
        $('#analyze-body-' + index + ' td').html('<img src="view/image/loading.gif" class="loading">');
	
        $.ajax({
            url: 'index.php?route=module/neoseo_needless_image/delete&token=<?php echo $token; ?>',
            type: 'post',
            data: post_data,
            dataType: 'json',
            success: function(json) {
                if (json.data.length) {
                    html = prepareCheckboxesForm(json.data, index, json.message);
                } else {
                    html = '<div style="display: inline-block;">' + json.message + '</div>';
                }
			
                $('#analyze-body-' + index + ' td').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    function prepareCheckboxesList(checkboxes) {
        var output = '';
        var odd = 'odd';
	
        for (var i = 0; i < checkboxes.length; i++) {
            odd = odd == 'odd' ? 'even' : 'odd';
            output += '		<div class="' + odd + '">';
            output += '			<input name="delete[]" type="checkbox" value="' + checkboxes[i]['path'] + '"> ' + checkboxes[i]['name'];
            output += '		</div>';
        }
	
        return output;
    }

    function prepareCheckboxesForm(data, index, message) {
        var output = '';
	
        if (typeof message === 'undefined') {
            message = '';
        }
	
        output += '<form id="form-delete-' + index + '" action="<?php echo $action_delete ?>" method="post" enctype="multipart/form-data" class="left" style="display:inline-block;">';
        output += message;
        output += '	<input type="hidden" value="' + $("#form select[name*='[" + index + "][path]']").val() + '" name="path">';
        output += '	<input type="hidden" value="' + $("#form select[name*='[" + index + "][recursive]']").val() + '" name="recursive">';
        output += '	<div class="scrollbox" style="width:700px;height:200px;">';
        output += prepareCheckboxesList(data);
        output += '	</div>';
        output += '	<div class="right">';
        output += '		<a onclick="deleteFiles(' + index + ');" class="button"><?php echo $button_delete_selected; ?></a> <a onclick="selectAll(\'#form-delete-' + index + '\');" class="button" style="background-color: #fff; color: #000; border: 1px solid #ddd; font-weight: bold;"><?php echo $button_select_all; ?></a> <a onclick="unselectAll(\'#form-delete-' + index + '\');" class="button" style="background-color: #fff; color: #000; border: 1px solid #ddd; font-weight: bold;"><?php echo $button_unselect_all; ?></a>';
        output += '	</div>';
        output += '</form>';
	
        return output;
    }

    function selectAll(form_id) {
        $(form_id).find(':checkbox').attr('checked', true);
    }

    function unselectAll(form_id) {
        $(form_id).find(':checkbox').attr('checked', false);
    }
//--></script> 
<?php echo $footer; ?>                    