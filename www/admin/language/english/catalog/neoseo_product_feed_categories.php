<?php

// Heading
$_['heading_title'] = 'NeoSeo Мульти Экспорт';
$_['heading_title_raw'] = 'Категории мультиэкспорта';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Список категорий обновлен!';
$_['text_list'] = 'Категории мультиэкспорта';
$_['text_add'] = 'Добавление категории';
$_['text_edit'] = 'Редактирование категории';

// Column
$_['column_name'] = 'Название категории';
$_['column_sort_order'] = 'Порядок сортировки';
$_['column_action'] = 'Действие';

// Entry
$_['entry_name'] = 'Название категории:';
$_['entry_name_parent'] = 'Родительская категория:';
$_['entry_product_to_feed_category'] = 'Категория для';
$_['entry_import_categories'] = 'Импорт справочника категорий:';

//Button
$_['button_process'] = 'Обработать';
$_['button_update_categories'] = 'Обновить справочник';

//Error
$_['error_empty_file'] = 'Файл не был загружен!';
$_['error_upload'] = 'Ошибка загрузки файла!';
$_['error_type'] = 'Неверный формат файла!';
