<?php  
class ControllerCommonHome extends Controller {
	public function index() {

					//microdatapro 7.5 start
					$this->data['microdatapro_data']['meta_description'] = $this->config->get('config_meta_description');
					$this->data['description'] = $this->config->get('config_meta_description');
					$this->data['heading_title'] = $this->config->get('config_meta_title');
					$this->data['breadcrumbs'] = array(array("href" => $this->url->link('common/home')));
					$this->data['microdatapro_data']['image'] = is_file(DIR_IMAGE . $this->config->get('config_logo'))?$this->config->get('config_logo'):'';
					$this->document->setTc_og($this->getChild('module/microdatapro/tc_og', $this->data));
					$this->document->setTc_og_prefix($this->getChild('module/microdatapro/tc_og_prefix'));
					//microdatapro 7.5 end
				
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->data['heading_title'] = $this->config->get('config_title');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
										
		$this->response->setOutput($this->render());
	}
}
?>