<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Мульти Экспорт</span>';
$_['heading_title_raw'] = 'NeoSeo Мульти Экспорт';

//Tabs
$_['tab_general'] = 'Общие';
$_['tab_feeds'] = 'Экспорты';
$_['tab_formats'] = 'Форматы';
$_['tab_fields'] = 'Поля';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';
$_['tab_usefull'] = 'Usefull links';

//Columns
$_['column_feed_name'] = 'Экспорт';
$_['column_format_name'] = 'Формат';
$_['column_feed_status'] = 'Статус экспорта';
$_['column_feed_demand'] = 'Ссылка';
$_['column_generate'] = 'Генерация кеша изображений';
$_['column_action'] = 'Действие';
$_['column_xml_code'] = 'XML код';
$_['column_update_relationship'] = 'Обновить связи мультиэкпорта';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Перепроверить';
$_['button_clear_log'] = 'Очистить лог';
$_['button_generate'] = 'Сгенерировать';
$_['button_add'] = 'Добавить';
$_['button_cache_generate'] = 'Сгенерировать кэш изображений';
$_['button_default_format'] = 'Default Formats';

// Text
$_['text_module_version'] = '';
$_['text_module'] = 'Модули';
$_['text_feed'] = 'Каналы продвижения';
$_['text_success_options'] = 'Настройки модуля обновлены!';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_select_all'] = 'Выделить всё';
$_['text_unselect_all'] = 'Снять выделение';
$_['text_bigmir'] = 'bigmir';
$_['text_hotline'] = 'hotline.ua';
$_['text_prom'] = 'prom.ua';
$_['text_yml'] = 'YML';
$_['text_google'] = 'google merchant';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_success_delete'] = 'Запись экспорта успешно удалена!';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_new_feed'] = 'Новый экспорт';
$_['text_del_feed'] = 'Удалить экспорт';
$_['text_link'] = 'По запросу';
$_['text_cron'] = 'По расписанию';
$_['text_new_format'] = 'Новый формат';
$_['text_del_format'] = 'Удалить формат';
$_['text_description'] = '<p>Описание</p>';
$_['text_none'] = 'Встроенные';
$_['text_action_delete'] = 'Удалить';
$_['text_action_edit'] = 'Редактировать';
$_['text_list_feeds'] = 'Экпорты';
$_['text_list_formats'] = 'Форматы';
$_['text_default_format_confirm'] = 'This action will overwrite the format settings and restore their default values. Do you want to continue?';
$_['text_success_formats'] = 'Formats successfully restored';
$_['text_image_pass'][0] = 'Skip';
$_['text_image_pass'][1] = 'Give no_image.png';
$_['text_product_markup_type'][0] = 'Percent';
$_['text_product_markup_type'][1] = 'Fixed value';
$_['text_default_format'] = 'Restore Default Formats';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_format'] = 'Формат:';
$_['entry_image_pass'] = '<label class="control-label">Images pass</label><br>What to do if image file is not available';
$_['entry_product_markup'] = '<label class = "control-label"> Product mark-up</label> <br> The price of the product when unloaded will be changed by the specified value or percents. Negative values are allowed.';
$_['entry_product_markup_option'] = '<label class = "control-label">Product option mark-up</label> <br> The price of the product option when unloaded will be changed by the specified value or percents. Negative values are allowed.';
$_['entry_product_markup_type'] = '<label class = "control-label">Type of product mark-ups</label> <br>Type of markup:  percents or fixed value.';
$_['entry_debug'] = 'Отладка:';
$_['entry_type'] = 'Формирование экспорта:';
$_['entry_format_name'] = 'Название формата:';
$_['entry_format_xml_code'] = 'XML код:';
$_['entry_field_list_name'] = 'Переменная шаблона';
$_['entry_field_list_desc'] = 'Описание';
$_['entry_feed_name'] = 'Название экспорта:<br/><span class="help">Название экспорта, будет отображаться во вкладке экспортов</span>';
$_['entry_feed_shortname'] = 'Название файла:<br/><span class="help">Будет отображаться в урле, по которому будет доступен экспорт</span>';
$_['entry_feed_status'] = 'Статус экспорта:<br/><span class="help">Отключение будет блокировать экспорт как по запросу, так и по расписанию</span>';
$_['entry_data_feed'] = 'Адрес:';
$_['entry_category'] = 'Категории:<br/><span class="help">Отметьте категории из которых надо экспортировать предложения для prom.ua</span>';
$_['entry_currency'] = 'Валюта предложений:<br/><span class="help">Пересчет будет выполнен по курсу, заданному в админке</span>';
$_['entry_ip_list'] = 'Список доверенных ip-адресов:<br/><span class="help">Укажите свой личный айпи и айпи сервера прайс-агрегатора, чтобы больше никто не имел доступ к вашим товарам</span>';
$_['entry_image_width'] = 'Ширина картинки товара:<br/><span class="help">Измеряется в пикселях, рекомендуемое значение - 600</span>';
$_['entry_image_height'] = 'Высота картинки товара:<br/><span class="help">Измеряется в пикселях, рекомендуемое значение - 600</span>';
$_['entry_feed_language'] = 'Язык экспорта:<br/><span class="help">Укажите язык описаний товаров и категорий</span>';
$_['entry_pictures_limit'] = 'Макс. кол-во картинок:<br/><span class="help">Чаще всего агрегаторы принимают не более 10 картинок на товар. Укажите тут ограничение чтобы прайс агрегатор не сыпал ошибками</span>';
$_['entry_cat_names_separathor'] = '<label class="control-label">Разделитель имен категорий</label><br>Данный символ используется для разделения имен категорий в переменной path';
$_['entry_use_original_images'] = 'Использовать оригиналы изображений';
$_['entry_cron'] = 'Файл cron';
$_['entry_sql_code'] = 'SQL для тонкой настройки<br/><span class="help">Укажите любое SQL выражение с использованием префикса p.<br>"Например p.price > 100 или p.price > 100 and p.quantity > 0"</span>';
$_['entry_manufacturers'] = 'Производители:<br/><span class="help">Отметьте производителей из которых надо экспортировать предложения</span>';
$_['entry_use_main_category'] = 'Использовать главную категорию<br/><span class="help">Использовать поле "Главная категория", а не "Показывать в категориях"</span>';
$_['entry_use_categories'] = 'Используемые категории';
$_['entry_sql_code_before'] = 'SQL для подготовки данных<br><span class="help">Укажите серию SQL запросов, которые будут выполнены перед формированием данных. Все запросы необходимо разделять точкой с запятой  - ";"</span>';
$_['entry_store'] = 'Store';
$_['entry_use_warehouse_quantity'] = 'Использовать количество товара по складам';
$_['entry_warehouse'] = 'Склады:<br/><span class="help">Отметьте склады из которых надо брать остаток предложения. Если выбранно несколько складов, остаток будет суммироваться.</span>';
$_['entry_replace'] = 'Заменять перенос строки &lt;br&gt,&lt;br/&gt на \n';
$_['entry_not_unload'] = 'Не выгружать товары <br/><span class="help">Черный список товаров</span>';
$_['entry_products'] = 'Выгружать товары';
$_['entry_instruction'] = 'Read the module instruction:';
$_['entry_history'] = 'Changes history:';
$_['entry_faq'] = 'Frequency Asked Questions:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления модулем NeoSeo Мульти Экспорт!';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";

//Success
$_['success_update_product_feed_categories'] = 'Связи товаров с категориями мультиэкспорта обновлены!';

//fields_desc
$_['field_desc_date'] = 'Дата экспорта. По умолчанию текущая дата';
$_['field_desc_url'] = 'URL адрес по которому доступен экспорт';
$_['field_desc_currency'] = 'Валюта предложений (устанавливается на вкладке экспорты)';
$_['field_desc_categories'] = 'Массив категорий экспорта';
$_['field_desc_category'] = 'Массив категории экспорта';
$_['field_desc_category.id'] = 'Идентификатор категории';
$_['field_desc_category.parentId'] = 'Инентификатор родительской категории';
$_['field_desc_category.name'] = 'Название категории';
$_['field_desc_path'] = 'Array of full product path in format <main category>/<child category>/<child category 2>...';
$_['field_desc_offers'] = 'Массив товаров экспорта';
$_['field_desc_offer'] = 'Массив товара экспорта';
$_['field_desc_offer.id'] = 'Идентификатор экспорта';
$_['field_desc_offer.url'] = 'URL адрес товара';
$_['field_desc_offer.tag'] = 'Мета теги товара';
$_['field_desc_offer.meta_title'] = 'Мета заголовок товара';
$_['field_desc_offer.meta_h1'] = 'Поле мета Н1 товара (будьте внимательны: поле meta_h1 есть не во всех версиях Опенкарт)';
$_['field_desc_offer.meta_description'] = 'Мета описание товара';
$_['field_desc_offer.meta_keyword'] = 'Ключевые слова товара';
$_['field_desc_offer.price'] = 'Цена товара';

//offer
$_['offer.currencyId'] = 'Валюта товара (устанавливается на вкладке экспорты)';
$_['offer.categoryId'] = 'Идентификатор категории товара';
$_['offer.name'] = 'Наименование товара';
$_['offer.description'] = 'Описание товара';
$_['offer.description_no_html'] = 'Product description with html tag cleanup';
$_['offer.model'] = 'Модель товара';
$_['offer.vendor'] = 'Продавец товара';
$_['offer.vendorCode'] = 'Код продавца';
$_['offer.image'] = 'Массив ссылок на изображения товара';
$_['offer.discount'] = 'Массив скидок товара';
$_['offer.special'] = 'Массив акций товара';
$_['offer.options'] = 'Массив опций товаров';
$_['offer.attributes'] = 'Массив атрибутов товаров';
$_['offer.filter_attributes'] = 'Array of product attributes from filter';
$_['image'] = 'Ссылка на изображение товара';

//option
$_['option'] = 'Массив опции товара';
$_['option.name'] = 'Наименование опции товара';
$_['option.value'] = 'Значение опции товара';
$_['option.available'] = 'Наличие товара';

//attribute
$_['attribute'] = 'Массив атрибута товара';
$_['attributes.name'] = 'Наименование атрибута';
$_['attributes.value'] = 'Значение атрибута';

$_['mail_support'] = "";
$_['module_licence'] = "";

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-vygruzka-na-prays-agregatory">https://neoseo.com.ua/nastroyka-modulya-neoseo-vygruzka-na-prays-agregatory</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/vygruzka-na-prajs-agregatory#module_history">https://neoseo.com.ua/vygruzka-na-prajs-agregatory#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/vygruzka-na-prajs-agregatory#faqBox">https://neoseo.com.ua/vygruzka-na-prajs-agregatory#faqBox</a>';$_['text_module_version']='85';
$_['error_license_missing']='<h3 style = "color: red"> Missing file with key! </h3>

<p> To obtain a file with a key, contact NeoSeo by email <a href="mailto:license@neoseo.com.ua"> license@neoseo.com.ua </a>, with the following: </p>

<ul>
	<li> the name of the site where you purchased the module, for example, https://neoseo.com.ua </li>
	<li> the name of the module that you purchased, for example: NeoSeo Sharing with 1C: Enterprise </li>
	<li> your username (nickname) on this site, for example, NeoSeo</li>
	<li> order number on this site, e.g. 355446</li>
	<li> the main domain of the site for which the key file will be activated, for example, https://neoseo.ua</li>
</ul>

<p>Put the resulting key file at the root of the site, that is, next to the robots.txt file and click the "Check again" button.</p>';
$_['error_ioncube_missing']='<h3 style="color: red">No IonCube Loader! </h3>

<p>To use our module, you need to install the IonCube Loader.</p>

<p>For installation please contact your hosting TS</p>

<p>If you can not install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:info@neoseo.com.ua"> info@neoseo.com.ua </a> </p>';
$_['module_licence']='<h2>NeoSeo Software License Terms</h2>
<p>Thank you for purchasing our web studio software.</p>
<p>Below are the legal terms that apply to anyone who visits our site and uses our software products or services. These Terms and Conditions are intended to protect your interests and interests of LLC NEOSEO and its affiliated entities and individuals (hereinafter referred to as "we", "NeoSeo") acting in the agreements on its behalf.</p>
<p><strong>1. Introduction</strong></p>
<p>These Terms of Use of NeoSeo (the "Terms of Use"), along with additional terms that apply to a number of specific services or software products developed and presented on the NeoSeo website (s), contain terms and conditions that apply to each and every one of them. the visitor or user ("User", "You" or "Buyer") of the NeoSeo website, applications, add-ons and components offered by us along with the provision of services and the website, unless otherwise noted (all services and software, software Modules offered through the NeoSeo website or auxiliary servers Isa, web services, etc. Applications on behalf NeoSeo collectively referred to as - "NeoSeo Service" or "Services").</p>
<p>NeoSeo Terms are a binding contract between NeoSeo and you - so please carefully read them.</p>
<p>You may visit and/or use the NeoSeo Services only if you fully agree to the NeoSeo Terms: By using and/or signing up to any of the NeoSeo Services, you express and agree to these Terms of Use and other NeoSeo terms, for example, provide programming services in the context of typical and non-typical tasks that are outlined here: <a href = "https://neoseo.com.ua/vse-chto-nujno-znat-klienty "target ="_blank" class ="external"> https://neoseo.com.ua/vse-chto-nujno-znat-klienty </a>, (hereinafter the NeoSeo Terms).</p>
<p>If you are unable to read or agree to the NeoSeo Terms, you must immediately leave the NeoSeo Website and not use the NeoSeo Services.</p>
<p>By using our Software products, Services, and Services, you acknowledge that you have read our Privacy Policy at <a href = "https://neoseo.com.ua/policy-konfidencialnosti "target ="_blank " class ="external"> https://neoseo.com.ua/politika-konfidencialnosti </a> (" Privacy Policy ")</p>
<p>This document is a license agreement between you and NeoSeo.</p>
<p>By agreeing to this agreement or using the software, you agree to all these terms.</p>
<p>This agreement applies to the NeoSeo software, any fonts, icons, images or sound files provided as part of the software, as well as to all NeoSeo software updates, add-ons or services, if not applicable to them. miscellaneous. This also applies to NeoSeo apps and add-ons for the SEO-Store, which extend its functionality.</p>
<p>Prior to your use of some of the application features, additional NeoSeo and third party terms may apply. For the correct operation of some applications, additional agreements are required with separate terms and conditions of privacy, for example, with services that provide SMS-notification services.</p>
<p>Software is not sold, but licensed.</p>
<p>NeoSeo retains all rights (for example, the rights provided by intellectual property laws) that are not explicitly granted under this agreement. For example, this license does not entitle you to:</p>
<li> <span> </span> <span> </span> separately use or virtualize software components; </li>
<li> publish or duplicate (with the exception of a permitted backup) software, provide software for rental, lease or temporary use; </li>
<li> transfer the software (except as provided in this agreement); </li>
<li> Try to circumvent the technical limitations of the software; </li>
<li> study technology, decompile or disassemble the software, and make appropriate attempts, other than those to the extent and in cases where (a) it provides for the right; (b) authorized by the terms of the license to use the components of the open source code that may be part of this software; (c) necessary to make changes to any libraries licensed under the small GNU General Public License, which are part of the software and related; </li>
<p> You have the right to use this software only if you have the appropriate license and the software was properly activated using the genuine product key or in another permissible manner.
</p>
<p> The cost of the SEO-Shop license does not include installation services, settings, and more of its stylization, as well as other paid/free add-ons. These services are optional, the cost depends on the number of hours required for the implementation of the hours, here: <a href = "https://neoseo. com.ua/vse-chto-nujno-znat-klienty "target =" _ blank "class =" external "> https://neoseo.com.ua/vse-chto-nujno-znat-klienty </a>
</p>
<p> The complete version of the document can be found here:
</p>
<p> <a href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank" class="external"> https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya </a>
</p>';
$_['mail_support']='<h2>Terms of free and paid information and technical support in <a class="external" href="https://neoseo.com.ua/" target="_blank"> NeoSeo</a>.</h2>

<p>Since we are confident that any quality work must be paid, all consultations requiring preliminary preparation of the answer, pay, including and case studies: &quot; look, and why your module is not working here? &quot;</p>

<p>If the answer to your question is already ready, you will receive it for free. But if you need to spend time answering the question, studying files, finding a bug and analyzing it, then we&#39;ll ask you to make a payment before you can answer.</p>

<p>We are <strong>helping to install</strong> and <strong> fix bugs when installing </strong>our modules in our order.</p>

<p>For any questions, please contact www.opencartmasters.com.</p>

<p>See the full version of the license agreement here:<strong> </strong><a class="external" href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank"> https://neoseo.com .ua/usloviya-licenzionnogo-soglasheniya</a></p>

<p><strong>Special offer: write review - get an add-on as a gift :)</strong></p>

<p>Dear Customers of web studio NeoSeo,</p>

<p>Tell us, what could be better for the development of the company than public reviews? This is a great way to hear your Client and make your products and service even better.</p>

<p>Please, leave a review about cooperation with our web studio or about our software solutions (add-ons) on our Facebook, Google, pages, Google, Yandex and OpenCartForum.com. pages.</p>

<p>Write as it is, it is important for us to hear an honest and objective assessment, and as a sign of gratitude for the time spent writing reviews, we have prepared a nice bonus. Detailed conditions are here: <a href="https://neoseo.com.ua/akciya-modul-v-podarok " target="_blank">https://neoseo.com.ua/akciya-modul-v-podarok </a></p>

<p>Once again, thank you very much for being with us!</p>

<p>The NeoSeo Team</p>';
