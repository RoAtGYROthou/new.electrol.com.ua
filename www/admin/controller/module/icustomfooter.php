<?php
class ControllerModuleICustomFooter extends Controller {
	public function index() {
		$this->language->load('module/icustomfooter');
		$this->load->model('module/icustomfooter');
		
		//Settings and Layouts
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post)) {		
			if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
				$this->session->data['error'] = $this->language->get('error_permission');
				$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'));
			} else {
                foreach (array(
                    'vqmod_icustomfooter.xml',
                    'vqmod_icustomfooter_151x.xml'
                ) as $file_name) {
                    if (is_writable(IMODULE_ROOT . 'vqmod/xml/' . $file_name . '_uninstalled')) {
                        rename(IMODULE_ROOT . 'vqmod/xml/' . $file_name . '_uninstalled', IMODULE_ROOT . 'vqmod/xml/' . $file_name);
                    }
                }

				if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post['data']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
				}
				if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
					$this->request->post['data']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']),true);
				}
				if (!empty($this->request->post['paymentIconName'])) {
					$this->model_module_icustomfooter->uploadIcon();
				} else {
					if (!empty($this->request->post['data'])) {
						$data = $this->request->post['data'];
						$this->model_module_icustomfooter->editSetting('icustomfooter', $data);
						$this->session->data['success'] = $this->language->get('text_success');
						$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'));
					}
				}
			}
		}
		
		// Initialize
		$this->document->addStyle('view/javascript/icustomfooter/bootstrap/css/bootstrap.min.css');
		$this->document->addStyle('view/stylesheet/icustomfooter.css');
		$this->document->addStyle('view/stylesheet/icustomfooter/font-awesome/css/font-awesome.min.css');
		$this->document->addScript('view/javascript/ckeditor/ckeditor.js');
		$this->document->addScript('view/javascript/icustomfooter/bootstrap/js/bootstrap.min.js');
		$this->document->addScript('view/javascript/icustomfooter.js');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		// Columns
		$dirName = DIR_APPLICATION . 'view/template/module/icustomfooter/';
		$this->data['dirName'] = $dirName;
		$column_files = scandir($dirName); 
		
		foreach ($column_files as $key => $file) {
			if (strpos($file, 'column_') === 0) {
				$this->data['columns'][] = array(
					'file' => $dirName . $file,
					'var' => substr($file, 7, strripos($file, '.') - 7)
				);
			}
			if (strpos($file, 'settings_column_') === 0) {
				$this->data['settings_columns'][] = array(
					'file' => $dirName . $file,
					'var' => substr($file, 16, strripos($file, '.') - 16)
				);
			}
		}
		
		$this->data['extraColumnAttributes'] = array(
			'googlemaps' => ' onclick="$(\'.GoogleMapsPreviewButton\').click();"'
		);
		foreach (array_merge($this->data['columns'], $this->data['settings_columns']) as $column) {
			$this->data[$column['var']] = $this->language->get($column['var']);	
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		$this->data['customColumnCount'] = 5;
		$this->data['customcolumn'] = $this->language->get('customcolumn');
		$this->data['footercustomization'] = $this->language->get('footercustomization');
		$this->data['assistance'] = $this->language->get('assistance');
		$this->data['globalsettings'] = $this->language->get('globalsettings');
		$this->data['assistancetitle'] = $this->language->get('assistancetitle');
		$this->data['assistancetext'] = $this->language->get('assistancetext');
		$this->data['yes'] = $this->language->get('yes');
		$this->data['no'] = $this->language->get('no');
		$this->data['white'] = $this->language->get('white');
		$this->data['dark'] = $this->language->get('dark');
		$this->data['hidewhatunfits'] = $this->language->get('hidewhatunfits');
		$this->data['showscrollers'] = $this->language->get('showscrollers');
		$this->data['dashed'] = $this->language->get('dashed');
		$this->data['dotted'] = $this->language->get('dotted');
		$this->data['solid'] = $this->language->get('solid');
		$this->data['double'] = $this->language->get('double');
		$this->data['groove'] = $this->language->get('groove');
		$this->data['ridge'] = $this->language->get('ridge');
		$this->data['inset'] = $this->language->get('inset');
		$this->data['outset'] = $this->language->get('outset');
		$this->data['noline'] = $this->language->get('noline');
		$this->data['defaultfont'] = $this->language->get('defaultfont');
		$this->data['greenicons'] = $this->language->get('greenicons');
		$this->data['whiteicons'] = $this->language->get('whiteicons');
		$this->data['blueicons'] = $this->language->get('blueicons');
		$this->data['preview'] = $this->language->get('preview');
		$this->data['defaultforthefooter'] = $this->language->get('defaultforthefooter');
		$this->data['titleofthecolumn'] = $this->language->get('titleofthecolumn');
		$this->data['aboutus_text'] = $this->language->get('aboutus_text');
		$this->data['showcolumn'] = $this->language->get('showcolumn');
		$this->data['columnposition'] = $this->language->get('columnposition');
		$this->data['contactus_iconset'] = $this->language->get('contactus_iconset');
		$this->data['contactus_title'] = $this->language->get('contactus_title');
		$this->data['contactus_text'] = $this->language->get('contactus_text');
		$this->data['contactus_address'] = $this->language->get('contactus_address');
		$this->data['contactus_phone'] = $this->language->get('contactus_phone');
		$this->data['contactus_fax'] = $this->language->get('contactus_fax');
		$this->data['contactus_email'] = $this->language->get('contactus_email');
		$this->data['contactus_skype'] = $this->language->get('contactus_skype');
		$this->data['maps_longlat'] = $this->language->get('maps_longlat');
		$this->data['maps_preview'] = $this->language->get('maps_preview');
		$this->data['maps_apikey'] = $this->language->get('maps_apikey');
		$this->data['contactform_sendemailsto'] = $this->language->get('contactform_sendemailsto');
		$this->data['twitter_numberoftweets'] = $this->language->get('twitter_numberoftweets');
		$this->data['twitter_keyword'] = $this->language->get('twitter_keyword');
		$this->data['twitter_widget_id'] = $this->language->get('twitter_widget_id');
		$this->data['twitter_fetch_tweets_from'] = $this->language->get('twitter_fetch_tweets_from');
		$this->data['twitter_profile'] = $this->language->get('twitter_profile');
		$this->data['twitter_keyword_mentioned_in_twitter'] = $this->language->get('twitter_keyword_mentioned_in_twitter');
		$this->data['facebook_pageurl'] = $this->language->get('facebook_pageurl');
		$this->data['facebook_widgetheight'] = $this->language->get('facebook_widgetheight');
		$this->data['youtube_url'] = $this->language->get('youtube_url');
		$this->data['youtube_width'] = $this->language->get('youtube_width');
		$this->data['youtube_height'] = $this->language->get('youtube_height');
		$this->data['custom_text'] = $this->language->get('custom_text');
		$this->data['contactform_captchaspamprotection'] = $this->language->get('contactform_captchaspamprotection');
		$this->data['contactform_emailsubject'] = $this->language->get('contactform_emailsubject');
		$this->data['contactform_nameboxlabel'] = $this->language->get('contactform_nameboxlabel');
		$this->data['contactform_emailboxlabel'] = $this->language->get('contactform_emailboxlabel');
		$this->data['contactform_messageboxlabel'] = $this->language->get('contactform_messageboxlabel');
		$this->data['contactform_captchaboxlabel'] = $this->language->get('contactform_captchaboxlabel');
		$this->data['contactform_sendbuttonlabel'] = $this->language->get('contactform_sendbuttonlabel');
		$this->data['contactform_successfulsentmessage'] = $this->language->get('contactform_successfulsentmessage');
		$this->data['contactform_requiredfieldmessage'] = $this->language->get('contactform_requiredfieldmessage');
		$this->data['contactform_notvalidmessage'] = $this->language->get('contactform_notvalidmessage');
		$this->data['contactform_notvalidcaptcha'] = $this->language->get('contactform_notvalidcaptcha');
		$this->data['contactform_message_max_length'] = $this->language->get('contactform_message_max_length');
		$this->data['paymenticons_showpaymenticons'] = $this->language->get('paymenticons_showpaymenticons');
		$this->data['paymenticons_paymenticons'] = $this->language->get('paymenticons_paymenticons');
		$this->data['paymenticons_addfiles'] = $this->language->get('paymenticons_addfiles');
		$this->data['paymenticons_uploadicons']	= $this->language->get('paymenticons_uploadicons');
		$this->data['paymenticons_titleoftheicon'] = $this->language->get('paymenticons_titleoftheicon');
		$this->data['paymenticons_upload'] = $this->language->get('paymenticons_upload');
		$this->data['paymenticons_confirm_delete'] = $this->language->get('paymenticons_confirm_delete');
		$this->data['paymenticons_delete'] = $this->language->get('paymenticons_delete');
		$this->data['showsocialbuttons'] = $this->language->get('showsocialbuttons');
		$this->data['facebooklikebutton'] = $this->language->get('facebooklikebutton');
		$this->data['pinterestpin'] = $this->language->get('pinterestpin');
		$this->data['googleplusbutton'] = $this->language->get('googleplusbutton');
		$this->data['twittertweet'] = $this->language->get('twittertweet');
		$this->data['showcustomfooter'] = $this->language->get('showcustomfooter');
		$this->data['footerwidth'] = $this->language->get('footerwidth');
		$this->data['footerusefooterwith'] = $this->language->get('footerusefooterwith');
		$this->data['footerusewithdefaultocwithicons'] = $this->language->get('footerusewithdefaultocwithicons');
		$this->data['footerusewiththemefooter'] = $this->language->get('footerusewiththemefooter');
		$this->data['footerusewithicons'] = $this->language->get('footerusewithicons');
		$this->data['hidepoweredby'] = $this->language->get('hidepoweredby');
		$this->data['footerfontfamily'] = $this->language->get('footerfontfamily');
		$this->data['footerbackgroundstyle'] = $this->language->get('footerbackgroundstyle');
		$this->data['footerbackgroundcolor'] = $this->language->get('footerbackgroundcolor');
		$this->data['usebackgroundcolor'] = $this->language->get('usebackgroundcolor');
		$this->data['footertextcolor'] = $this->language->get('footertextcolor');
		$this->data['whencontentoverflows'] = $this->language->get('whencontentoverflows');
		$this->data['columnheight'] = $this->language->get('columnheight');
		$this->data['columntitlecolor'] = $this->language->get('columntitlecolor');
		$this->data['columnlinestyle'] = $this->language->get('columnlinestyle');
		$this->data['responsivedesign'] = $this->language->get('responsivedesign');
		$this->data['columnheight'] = $this->language->get('columnheight');
		$this->data['columnwidth'] = $this->language->get('columnwidth');
		$this->data['customcss'] = $this->language->get('customcss');
		
		$data = $this->model_module_icustomfooter->getSetting('icustomfooter');
		$data = empty($data) ? $this->model_module_icustomfooter->getDefaultSettings() : $data;
		$this->data['data'] = $data; 
		
		$tab_files = scandir($dirName); 
		$this->data['tabs'] = array();
		foreach ($tab_files as $key => $file) {
			if (strpos($file,'tab_') !== false) {
				$this->data['tabs'][] = array(
					'file' => $dirName . $file,
					'name' => ucwords(str_replace('.php', '', str_replace('_', ' ', str_replace('tab_', '', $file))))
				);
			} 
		}
		
		// Images
		
		$raw_image_files = scandir(PAYMENTICONS_FOLDER);
		$imgurl = dirname(IMODULE_SERVER) . '/image/icustomfooter/paymenticons/';
		
		$image_files = array();
		foreach ($raw_image_files as $key => $value) {
			if (strstr($value, '.png') !== false || strstr($value, '.jpg') !== false || strstr($value, '.jpeg') !== false || strstr($value, '.gif') !== false) {
				$name = $value;
				$name = str_replace('.png', '', $name);
				$name = str_replace('.jpg', '', $name);
				$name = str_replace('.jpeg', '', $name);
				$name = str_replace('.gif', '', $name);
				$name = preg_replace('/[^a-z ]/i', '', $name);
				array_push($image_files, array(
					'path' => $imgurl . $value,
					'file' => PAYMENTICONS_FOLDER . $value,
					'name' => $name,
					'origname' => $value,
					'delete' => $this->url->link('module/icustomfooter/image_delete', 'token=' . $this->session->data['token'] . '&image=' . $key . $this->model_module_icustomfooter->generateTabs(true), 'SSL'),
					'moveup' => $this->url->link('module/icustomfooter/image_moveup', 'token=' . $this->session->data['token'] . '&moveup=' . $key . $this->model_module_icustomfooter->generateTabs(true), 'SSL'),
					'movedown' => $this->url->link('module/icustomfooter/image_movedown', 'token=' . $this->session->data['token'] . '&movedown=' . $key  . $this->model_module_icustomfooter->generateTabs(true), 'SSL')
				));
			}
		}
		$this->data['images'] = $image_files;
		
		$this->data['stores'] = $this->model_module_icustomfooter->getSystemStores();
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->template = 'module/icustomfooter.tpl';
		
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' <span class="divider">/</span> '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' <span class="divider">/</span> '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'),
      		'separator' => false
   		);
		
		$this->data['action'] = $this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['languages'] = $this->model_module_icustomfooter->getSystemLanguages();
		
		$this->response->setOutput($this->render());
	}
	
	public function image_moveup() {
		$this->language->load('module/icustomfooter');
		$this->load->model('module/icustomfooter');
		
		if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} else {
			$this->model_module_icustomfooter->normalizeImages();
			
			$raw_image_files = scandir(PAYMENTICONS_FOLDER);
			
			$moveUp = $this->request->get['moveup']; 
			$oldName = $raw_image_files[$this->request->get['moveup']];
			$newName = str_pad($moveUp - 1, strlen(count($raw_image_files) - 1), '0', STR_PAD_LEFT) . '_' . preg_replace('/[^a-z .]/i', '', $oldName);
			rename(PAYMENTICONS_FOLDER . $oldName, PAYMENTICONS_FOLDER . $newName);
			
			$oldName = $raw_image_files[$moveUp - 1];
			$newName = str_pad($moveUp, strlen(count($raw_image_files) - 1), '0', STR_PAD_LEFT) . '_' . preg_replace('/[^a-z .]/i', '', $oldName);
			rename(PAYMENTICONS_FOLDER . $oldName, PAYMENTICONS_FOLDER . $newName);
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'));
	}
	
	public function image_movedown() {
		$this->language->load('module/icustomfooter');
		$this->load->model('module/icustomfooter');
		
		if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} else {
			$this->model_module_icustomfooter->normalizeImages();
			
			$raw_image_files = scandir(PAYMENTICONS_FOLDER);
			
			$moveDown = $this->request->get['movedown']; 
			$oldName = $raw_image_files[$this->request->get['movedown']];
			$newName = str_pad($moveDown + 1, strlen(count($raw_image_files) - 1), '0', STR_PAD_LEFT) . '_' . preg_replace('/[^a-z .]/i', '', $oldName);
			rename(PAYMENTICONS_FOLDER . $oldName, PAYMENTICONS_FOLDER . $newName);
			
			$oldName = $raw_image_files[$moveDown + 1];
			$newName = str_pad($moveDown, strlen(count($raw_image_files) - 1), '0', STR_PAD_LEFT) . '_' . preg_replace('/[^a-z .]/i', '', $oldName);
			rename(PAYMENTICONS_FOLDER . $oldName, PAYMENTICONS_FOLDER . $newName);
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'));
	}
	
	public function image_delete() {
		$this->language->load('module/icustomfooter');
		$this->load->model('module/icustomfooter');
		
		if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} else {
			$raw_image_files = scandir(PAYMENTICONS_FOLDER);
			
			unlink(PAYMENTICONS_FOLDER . $raw_image_files[$this->request->get['image']]);
			$this->model_module_icustomfooter->normalizeImages();
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->model_module_icustomfooter->generateTabs(), 'SSL'));
	}
	
	public function install() {
		if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
			$this->language->load('module/icustomfooter');
			$this->session->data['error'] = $this->language->get('error_permission');
			$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->load->model('module/icustomfooter');
			$this->model_module_icustomfooter->editSetting('icustomfooter', $this->model_module_icustomfooter->getDefaultSettings());

            foreach (array(
                'vqmod_icustomfooter.xml',
                'vqmod_icustomfooter_151x.xml'
            ) as $file_name) {
                if (is_writable(IMODULE_ROOT . 'vqmod/xml/' . $file_name . '_uninstalled')) {
                    rename(IMODULE_ROOT . 'vqmod/xml/' . $file_name . '_uninstalled', IMODULE_ROOT . 'vqmod/xml/' . $file_name);
                }
            }
		}
	}
	
	public function uninstall() {
		if (!$this->user->hasPermission('modify', 'module/icustomfooter')) {
			$this->language->load('module/icustomfooter');
			$this->session->data['error'] = $this->language->get('error_permission');
			$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->load->model('module/icustomfooter');
			$this->model_module_icustomfooter->deleteSetting('icustomfooter');
			
            foreach (array(
                'vqmod_icustomfooter.xml',
                'vqmod_icustomfooter_151x.xml'
            ) as $file_name) {
                if (is_writable(IMODULE_ROOT . 'vqmod/xml/' . $file_name)) {
                    rename(IMODULE_ROOT . 'vqmod/xml/' . $file_name, IMODULE_ROOT . 'vqmod/xml/' . $file_name . '_uninstalled');
                }
            }
		}
	}
} ?>