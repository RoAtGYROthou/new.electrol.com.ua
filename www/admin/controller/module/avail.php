<?php
	class ControllerModuleAvail extends Controller {
		private $error = array();
		
		public function install() {
			$this->load->model('module/avail');
			$this->model_module_avail->install();
		}
		public function index() {
			$this->language->load('module/avail');
			$this->document->setTitle($this->language->get('heading_title'));
			$this->load->model('setting/setting');
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {			
				$this->model_setting_setting->editSetting('avail', $this->request->post);		

				$this->session->data['success'] = $this->language->get('text_success');

				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			$this->data['text_get_availabilitylist'] = $this->language->get('text_get_availabilitylist');
			$this->data['text_mail_on'] = $this->language->get('text_mail_on');
			$this->data['text_mail'] = $this->language->get('text_mail');
            $this->data['text_enabled'] = $this->language->get('text_enabled');
            $this->data['text_disabled'] = $this->language->get('text_disabled');
            $this->data['entry_status'] = $this->language->get('entry_status');
			$this->data['text_capcha'] = $this->language->get('text_capcha');
			$this->data['text_edit_product'] = $this->language->get('text_edit_product');
			$this->data['help_google_captcha'] = $this->language->get('help_google_captcha');
			$this->data['entry_google_captcha_public'] = $this->language->get('entry_google_captcha_public');
			$this->data['config_google_captcha_public'] = $this->language->get('config_google_captcha_public');
			$this->data['entry_google_captcha_secret'] = $this->language->get('entry_google_captcha_secret');
			$this->data['config_google_captcha_secret'] = $this->language->get('config_google_captcha_secret');
			$this->data['entry_capcha_status'] = $this->language->get('entry_capcha_status');			
			$this->data['avail_buttom'] = $this->language->get('avail_buttom');
			$this->data['text_cron'] = $this->language->get('text_cron');
			$this->data['entry_layout'] = $this->language->get('entry_layout');
			$this->data['entry_position'] = $this->language->get('entry_position');
			$this->data['entry_status'] = $this->language->get('entry_status');
			$this->data['text_enabled'] = $this->language->get('text_enabled');
			$this->data['text_disabled'] = $this->language->get('text_disabled');
			$this->data['text_content_top'] = $this->language->get('text_content_top');
			$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
			$this->data['text_column_left'] = $this->language->get('text_column_left');
			$this->data['text_column_right'] = $this->language->get('text_column_right');
			$this->data['cron_link'] = 'wget -q -O - ' . HTTP_CATALOG . 'cron/avail_cron.php';
			$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
			
			$this->data['button_save'] = $this->language->get('button_save');
			$this->data['button_cancel'] = $this->language->get('button_cancel');
			$this->data['button_add_module'] = $this->language->get('button_add_module');
			$this->data['button_remove'] = $this->language->get('button_remove');
			
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
			if (isset($this->error['email'])) {
                $this->data['error_email'] = $this->error['email'];
            } else {
                $this->data['error_email'] = '';
            }			
			
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/avail', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);
			
			if (isset($this->request->post['email'])) {
                $this->data['email'] = $this->request->post['email'];
            } elseif ($this->config->get('email')) {
				$this->data['email'] = $this->config->get('email');
			} else {
                $this->data['email'] = '';
            }
			if (isset($this->request->post['config_captcha_status'])) {
                $this->data['config_captcha_status'] = $this->request->post['config_captcha_status'];
            } elseif ($this->config->get('config_captcha_status')) {
				$this->data['config_captcha_status'] = $this->config->get('config_captcha_status');
			} else {
                $this->data['config_captcha_status'] = '';
            }
			if (isset($this->request->post['avail_buttom_status'])) {
                $this->data['avail_buttom_status'] = $this->request->post['avail_buttom_status'];
            } elseif ($this->config->get('avail_buttom_status')) {
				$this->data['avail_buttom_status'] = $this->config->get('avail_buttom_status');
			} else {
                $this->data['avail_buttom_status'] = '';
            }
			if (isset($this->request->post['config_product_edit'])) {
                $this->data['config_product_edit'] = $this->request->post['config_product_edit'];
            } elseif ($this->config->get('config_product_edit')) {
				$this->data['config_product_edit'] = $this->config->get('config_product_edit');
			} else {
                $this->data['config_product_edit'] = '';
            }
			
			$this->data['getAvailabilityList'] = $this->url->link('module/avail/getAvailabilityList', 'token=' . $this->session->data['token'], 'SSL');
			
			$this->data['action'] = $this->url->link('module/avail', 'token=' . $this->session->data['token'], 'SSL');

			$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

			$this->data['token'] = $this->session->data['token'];
			
			$this->data['modules'] = array();

			if (isset($this->request->post['avail_module'])) {
				$this->data['modules'] = $this->request->post['avail_module'];
			} elseif ($this->config->get('avail_module')) { 
				$this->data['modules'] = $this->config->get('avail_module');
			}		
			
			$this->document->addStyle('view/avail/stylesheet/avail.css');
			
			$this->load->model('design/layout');

			$this->data['layouts'] = $this->model_design_layout->getLayouts();

			$this->template = 'module/availability.tpl';
			$this->children = array(
				'common/header',
				'common/footer'
			);

			$this->response->setOutput($this->render());
		}
		protected function validate() {
			if (!$this->user->hasPermission('modify', 'module/featured')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
			if ( isset($this->request->post['email']) ) {
				if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
					$this->error['email'] = $this->language->get('error_email');
				}
			}
			
			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}
		public function getAvailabilityList() {
			$this->language->load('module/avail');
			$this->load->model('module/avail');
			$this->load->model('catalog/product');
			
			$this->data['heading_title'] = $this->language->get('heading_title');
            $this->data['text_yes'] = $this->language->get('text_yes');
            $this->data['text_no'] = $this->language->get('text_no');
			$this->data['text_time'] = $this->language->get('text_time');
			$this->data['text_product'] = $this->language->get('text_product');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_mail'] = $this->language->get('text_mail');
			$this->data['text_name'] = $this->language->get('text_name');
			$this->data['text_comment'] = $this->language->get('text_comment');
			$this->data['text_statuse'] = $this->language->get('text_statuse');
			$this->data['text_quantity'] = $this->language->get('text_quantity');
			$this->data['text_list'] = $this->language->get('text_list');
			$this->data['tab_active'] = $this->language->get('tab_active');
			$this->data['tab_closed'] = $this->language->get('tab_closed');
			$this->data['button_delete'] = $this->language->get('button_delete');
            $this->data['button_send'] = $this->language->get('button_send');
			$this->data['button_save'] = $this->language->get('button_save');
            $this->data['button_cancel'] = $this->language->get('button_cancel');
			$this->data['id'] = "№";
			$this->data['reload'] = $this->language->get('reload');
			
			$availability = array();
			$availability  = $this->model_module_avail->getAvailabilities();
			
			foreach ($availability as $availabilities) {
				$this->data['availabilities'][] = array (
					'id' => $availabilities['id'],
					'time'=>  $availabilities['time'],
					'product' => $availabilities['product'],
					'price' => $availabilities['price'],
					'email' => $availabilities['email'],
					'name' => $availabilities['name'],
					'status' =>$availabilities['status'],
					'quantity' =>$availabilities['quantity'],
					'comment' =>  mb_strlen($availabilities['comment'])> 50 ? substr($availabilities['comment'],0,50) : $availabilities['comment'],
					'full_comment' => mb_strlen($availabilities['comment'])> 50 ? $availabilities['comment'] : ''   
				);
			}
			$processed = array();
			$processed  = $this->model_module_avail->getProcessed();		
			
		    foreach ($processed as $proces) {
				$this->data['processed'][] = array (
					'id' => $proces['id'],
					'time'=>  $proces['time'],
					'product' => $proces['product'],
					'price' => $proces['price'],
					'email' => $proces['email'],
					'name' => $proces['name'],
					'status' =>$proces['status'],
					'quantity' =>$proces['quantity'],
					'comment' =>  mb_strlen($proces['comment'])> 50 ? substr($proces['comment'],0,50) : $proces['comment'],
					'full_comment' => mb_strlen($proces['comment'])> 50 ? $proces['comment'] : ''   
					);
			}
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/avail', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);
			$this->data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_list'),
					'href' => $this->url->link('module/avail/getAvailabilityList', 'token=' . $this->session->data['token'], 'SSL'),
					'separator' => ' :: '
			);
			
			$this->document->addStyle('view/avail/stylesheet/avail.css');
            $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');            

            $this->data['token'] = $this->session->data['token'];
			
			$this->data['statuses'] = array(
				'0' => $this->language->get('text_status_notprocessed'),
				'1' => $this->language->get('text_status_processed')
			);
			$this->template = 'module/availabilitylist.tpl';
			$this->children = array(
				'common/header',
				'common/footer'
			);

			$this->response->setOutput($this->render());
		}
		public function changeMailStatus() {
			$this->load->model('module/avail');
			$this->data['token'] = $this->session->data['token'];
			$id = $this->request->post['id'];
            $status = $this->request->post['status'];			
            if($this->model_module_avail->changeMailStatus($id, $status)){
                echo 'ok';
            } else {
                echo 'error';
            }
		}
		public function notify() {
			$store = HTTP_CATALOG;
            $this->load->model('module/avail');
			$this->language->load('module/avail');			
			$result = $this->model_module_avail->notify();
			$this->data['token'] = $this->session->data['token'];
			
			$json = array();
			
			if(!empty($result)){
				foreach ($result as $info ) {
					$link = HTTP_CATALOG . 'index.php?route=product/product&product_id='. $info['product_id'];
					
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->hostname = $this->config->get('config_smtp_host');
					$mail->username = $this->config->get('config_smtp_username');
					$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->port = $this->config->get('config_smtp_port');
					$mail->timeout = $this->config->get('config_smtp_timeout');
					
					$mail->setTo($info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender('\''.$this->language->get('email_subject').'\'');
					$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $info['name'], ENT_QUOTES, 'UTF-8')));
					
					$mail_text = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Document</title></head><body>";
					$mail_text .="<p>" . html_entity_decode($info['name'].', '.$this->language->get('text_mail_send'). "</p>");
					$mail_text .= "<p>" . $this->language->get('text_product') .': ' . $info['product'] . "</p>";
					$mail_text .= "<p>" . $this->language->get('text_link_page') . ": " . " <a href=" . $link . ">" . $info['product'] . "</a></p>";
					$mail_text .= "<p>" . $this->language->get('text_price') . ': ' . $info['price'] . "</p></body></html>";

					$mail->setHtml($mail_text);
					$mail->send();
					$this->model_module_avail->changeMailStatus($info['id'], 1);
				}
				$json['success'] = $this->language->get('success');
				$this->response->setOutput(json_encode($json));
			} else {
				$json['error'] = $this->language->get('error');
                $this->response->setOutput(json_encode($json));
			}
		}
		public function deleteNotifications() {
			$this->load->model('module/avail');
			$this->data['token'] = $this->session->data['token'];
			$selected = $this->request->post['idArray'];
			
			foreach ($selected as $value ) {
				$this->model_module_avail->deleteNotifications($value);
			}		
		}
}
?>