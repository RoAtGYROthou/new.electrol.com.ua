<?php
/* 
 * @package NVisitors
 * @version 0.2.1
 */  
class ControllerModulenvisitors extends Controller {
	protected function index() {
		$this->load->language('module/nvisitors');		
		
		$this->data['nvisitors_username'] = $this->config->get('nvisitors_username');
		$this->data['nvisitors_password'] = $this->config->get('nvisitors_password');
		$this->data['nvisitors_enable'] = $this->config->get('nvisitors_enable');		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/nvisitors.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/nvisitors.tpl';
		} else {
			$this->template = 'default/template/module/nvisitors.tpl';
		}
		
		$this->render();
	}
}
?>
