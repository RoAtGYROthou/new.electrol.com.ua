<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
                <h1><?php echo $heading_title . " " . $text_module_version  . " " .$heading_title_raw; ?></h1>
                <div class="buttons">
                    <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                    <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
            </div>
            <div class="content">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <table class="form">
                        <tr>
                            <td style="width: 30%"> <?php echo $entry_name; ?></td>
                            <td><input type="text" name="item[name]" maxlength="255" size="100" value="<?php echo isset($item['name'])? $item['name']: ''; ?>" /></td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_name_parent; ?></td>
                            <td><select name="item[parent_id]">
                                    <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                    <?php foreach ($categories as $category) { ?>
                                        <?php if ($category['category_id'] == $item['parent_id']) { ?>
                                            <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select></td>
                        </tr>
                    </table>
	  
                </form>
            </div>
        </div>
    </div>
<?php echo $footer; ?>