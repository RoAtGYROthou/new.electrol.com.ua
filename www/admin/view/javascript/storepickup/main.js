var includedMapsAPIs = [];
var initialize_map = function(previewDiv, longitude, latitude) {
	var myLatlng = new google.maps.LatLng(longitude, latitude);
	
	var mapOptions = {
		center: myLatlng,
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	var map = new google.maps.Map(previewDiv, mapOptions);
	
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: ""
	});
}

var displayMaps = function() {
	$('.GoogleMapsPreviewDiv').each(function(index, previewDiv) {
		var apikey = $($(previewDiv).attr('data-apikey-selector')).val();
		var longitude = parseFloat($($(previewDiv).attr('data-longitude-selector')).val().replace(/,/g, '.'));
		var latitude = parseFloat($($(previewDiv).attr('data-latitude-selector')).val().replace(/,/g, '.'));
		if (apikey == '') return;
		if (includedMapsAPIs.indexOf(apikey) == -1) {
			includedMapsAPIs.push(apikey);
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.src = 'https://maps.googleapis.com/maps/api/js?key=' + apikey + '&sensor=false&callback=displayMaps';
			document.body.appendChild(script);
		} else {
			if (document.readyState === "complete") initialize_map(previewDiv, longitude, latitude);
			else $(window).load(function() { initialize_map(previewDiv, longitude, latitude); });
		}
	});
}

$(document).ready(function() {
	displayMaps();
	$('.GoogleMapsPreviewButton').click(function(e) {
		e.preventDefault();
		displayMaps();
		return false;
	});
});

$(document).on('shown.bs.tab','a[data-toggle="tab"]', function (e) {
	displayMaps();
})