<?php
// Heading
$_['heading_title']    = 'SEO Replacer by <a href="http://www.apwai.net" onclick="return !window.open(this.href)"> APWAI.NET</a>';

// Text 
$_['text_success']     = 'Success: You have saved the changes and replaced the text in the selected SEO data.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify SEO Replacer!';
?>