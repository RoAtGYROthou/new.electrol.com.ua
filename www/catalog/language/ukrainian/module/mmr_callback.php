<?php
// Text
$_['text_mmr_callback_caption']               = 'Передзвоніть мені!';
$_['text_mmr_callback_title']                 = 'Зворотній дзвінок';
$_['text_mmr_callback_button']                = 'Передзвоніть мені';

$_['text_mmr_callback_processing']            = 'Обробка';
$_['text_mmr_callback_success']               = 'Успішно: ваш запит було відправлено, ми зв’яжемося з вами якомога швидше!';

$_['text_mmr_callback_email_subject']         = 'Новий запит Зворотнього дзвінка';
$_['text_mmr_callback_email_order_received']  = 'Ви отримали запит зворотнього дзвінка';
$_['text_mmr_callback_email_order_detail']    = 'Деталі зворотнього дзвінка';
$_['text_mmr_callback_email_url']             = 'Адреса сторінки';

//Errors
$_['error_mmr_callback_fields']               = 'Увага: Будь ласка, заповніть обов’язкові поля';
?>