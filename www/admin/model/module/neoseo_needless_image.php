<?php

require_once(DIR_SYSTEM . "/engine/neoseo_model.php");

class ModelModuleNeoSeoNeedlessImage extends NeoSeoModel
{

    public function __construct( $registry )
    {
        parent::__construct($registry);
        $this->_logFile = "neoseo_needless_image.log";
        $this->debug = $this->config->get("neoseo_needless_image") == 1;
    }

    public function install()
    {

        $this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'neoseo_needless_image_dir` (`directory_id` int(11) NOT NULL AUTO_INCREMENT, `path` varchar(255) COLLATE utf8_bin DEFAULT NULL,  `recursive` TINYINT(1) NOT NULL DEFAULT 0, PRIMARY KEY (`directory_id`)) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin');
    }

    public function upgrade()
    {

    }

    public function uninstall()
    {

        $this->db->query('DROP TABLE IF EXISTS `' . DB_PREFIX . 'neoseo_needless_image_dir`');
    }
}

?>