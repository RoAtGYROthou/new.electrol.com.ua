<?php  
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

			if ($this->config->get('hb_snippets_og_enable') == '1'){
			$this->document->setOpengraph('og:title', $this->config->get('config_title'));
            $this->document->setOpengraph('og:type', 'website');
            $this->document->setOpengraph('og:site_name', $this->config->get('config_name'));
            $this->document->setOpengraph('og:image', HTTP_SERVER . 'image/' . $this->config->get('config_logo'));
            $this->document->setOpengraph('og:url', $this->config->get('config_url'));
            $this->document->setOpengraph('og:description', $this->config->get('config_meta_description'));
			//$this->document->setOpengraph('article:publisher', $this->config->get('hb_snippets_fb_page'));
			}
			

		$this->data['heading_title'] = $this->config->get('config_title');

			$this->data['store_name'] = $store_name = $this->config->get('config_name');
			$this->data['store_url'] = $store_url = HTTPS_SERVER;
			
			$hb_snippets_kg_data = $this->config->get('hb_snippets_kg_data');
			
			if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
				$logo = $store_url . 'image/' . $this->config->get('config_logo');
			} else {
				$logo = '';
			}
			
			$hb_snippets_kg_data = str_replace('{store_name}',$store_name, $hb_snippets_kg_data);
			$hb_snippets_kg_data = str_replace('{store_logo}',$logo, $hb_snippets_kg_data);
			$hb_snippets_kg_data = str_replace('{store_url}',$store_url, $hb_snippets_kg_data);
			
			$this->data['hb_snippets_kg_data'] = $hb_snippets_kg_data ;
			$this->data['hb_snippets_kg_enable'] = $this->config->get('hb_snippets_kg_enable');
				
			$this->data['hb_snippets_local_enable'] = $this->config->get('hb_snippets_local_enable');
			$this->data['hb_snippets_local_snippet'] = $this->config->get('hb_snippets_local_snippet');
			
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
										
		$this->response->setOutput($this->render());
	}
}
?>