<?php  
class ControllerModuleIcustomfooter extends Controller {
	protected function index() {
		$this->load->model('module/icustomfooter');
		
		$data = $this->model_module_icustomfooter->getSetting('icustomfooter');
		
		$data = $data[$this->config->get('config_store_id')];
		
		if (!empty($data) && $data['Settings']['Show'] == 'true') {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/icustomfooter.css')) {
				$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/icustomfooter.css');
			} else {
				$this->document->addStyle('catalog/view/theme/default/stylesheet/icustomfooter.css');
			}
			
			if (is_dir(DIR_TEMPLATE . $this->config->get('config_template').'/template/module/icustomfooter/')) {
				$this->data['themefoldername'] = $this->config->get('config_template');
				$this->data['themepath'] = 'catalog/view/theme/' . $this->config->get('config_template');
			} else {
				$this->data['themefoldername'] = 'default';
				$this->data['themepath'] = 'catalog/view/theme/default/';
			}
			
			$this->data['columns'] = array();
			$this->data['idata'] = $data;
			
			$this->data['footerPath'] = is_dir(DIR_TEMPLATE . $this->config->get('config_template').'/template/module/icustomfooter/') ? DIR_TEMPLATE . $this->config->get('config_template').'/template/module/icustomfooter/' : DIR_TEMPLATE . '/default/template/module/icustomfooter/';
			
			$config_language_id = $this->config->get('config_language_id');
			$this->load->model('localisation/language');
			$language = $this->model_localisation_language->getLanguage($config_language_id);
			$langcode = $language['code'];
			
			$this->data['langcode'] = $langcode;
						
			$positions = $data[$langcode]['Positions'];
			asort($positions);
			
			foreach ($positions as $file => $pos) {
				$column = $this->getChild('module/icustomfooter/column_' . strtolower($file), array('idata' => $data, 'langcode' => $langcode));
				
				if ($column) {
					$this->data['columns'][] = $column;
				}
			}
					
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/icustomfooter.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/icustomfooter.tpl';
			} else {
				$this->template = 'default/template/module/icustomfooter/icustomfooter.tpl';
			}
			
			$this->render();
		}
	}
	
	protected function column_aboutus($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_custom1($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_custom2($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_custom3($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_custom4($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_custom5($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_contacts($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_googlemaps($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_contactform($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		$config = $setting['idata'][$setting['langcode']]['Widgets']['ContactForm'];
		
		$val = intval($config['MaxMessageLength']);
		if (empty($val)) $val = 1000;
		$this->data['messageLimit'] = $val;
		$this->data['flash'] = '';
		
		if (isset($this->request->post['iContactForm'])) {
			$pass = true;
			if ($config['UseCaptcha'] == 'true') {
				if (empty($this->session->data['icustomfooter_captcha']) || empty($this->request->post['iContactForm']['Captcha']) || $this->request->post['iContactForm']['Captcha'] != $this->session->data['icustomfooter_captcha']) $pass = false;
				if (!$pass) $this->data['flash'] = '<div class="redflashmessage" style="display:block;">'.$config['LabelInvalidCaptcha'].'</div>';
			}
			
			if (empty($this->request->post['iContactForm']['Message']) || empty($this->request->post['iContactForm']['Name']) || empty($this->request->post['iContactForm']['Email'])) $pass = false;
			
			if ($pass) {
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($config['Email']);
				$mail->setFrom(substr($this->request->post['iContactForm']['Email'], 0, 255));
				$mail->setSender(substr($this->request->post['iContactForm']['Name'], 0, 255));
				$mail->setSubject($config['EmailSubject']);
				$mail->setText(strip_tags(substr($this->request->post['iContactForm']['Message'], 0, $val)));
				$mail->send();
				
				$this->data['flash'] = '<div class="flashmessage">'.$config['LabelSuccess'].'</div>';							
			}
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_twitter($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_facebook($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	protected function column_youtube($setting) {
		$this->data['idata'] = $setting['idata'];
		$this->data['langcode'] = $setting['langcode'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		} else {
			$this->template = 'default/template/module/icustomfooter/' . __FUNCTION__ . '.tpl';
		}
		
		$this->render();
	}
	
	public function captcha() {
		$this->load->library('icustomfooter_captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['icustomfooter_captcha'] = $captcha->getCode();
		
		$this->load->model('module/icustomfooter');
		$data = $this->model_module_icustomfooter->getSetting('icustomfooter');
		$data = $data[$this->config->get('config_store_id')];
		
		if ($data['Settings']['BackgroundPattern'] == 'whitebgpattern') {
			$escape = array(255, 255, 255);
			$transparent = true;
		} else if ($data['Settings']['BackgroundPattern'] == 'darkbgpattern') {
			$escape = array(0, 0, 0);
			$transparent = true;
		} else {
			$escape = $this->hex2rgb($data['Settings']['BackgroundColor']);
			$transparent = false;
		}
		
		$captcha->showImage($escape, $transparent);
	}
	
	private function hex2rgb($hex) { // from http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
		$hex = str_replace("#", "", $hex);
		
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		//return implode(",", $rgb); // returns the rgb values separated by commas
		return $rgb; // returns an array with the rgb values
	}
}
?>