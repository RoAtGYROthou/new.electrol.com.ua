<?php
class ControllerModuleRelatedproductspro extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/relatedproductspro');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('localisation/language');
		$this->load->model('setting/setting');
		$this->load->model('setting/store');
		$this->load->model('module/relatedproductspro');
		$this->load->model('design/layout');
	
		$this->document->addScript('view/javascript/relatedproductspro/bootstrap/js/bootstrap.min.js');
		$this->document->addStyle('view/javascript/relatedproductspro/bootstrap/css/bootstrap.min.css');
		$this->document->addStyle('view/javascript/relatedproductspro/font-awesome/css/font-awesome.min.css');
		$this->document->addStyle('view/stylesheet/relatedproductspro.css');	
		
		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0; 
        }
	
        $store = $this->getCurrentStore($this->request->get['store_id']);
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if (!$this->user->hasPermission('modify', 'module/relatedproductspro')) {
				$this->validate();
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post['RelatedProductsPro']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
				$this->request->post['RelatedProductsPro']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']),true);
			}
			$store = $this->getCurrentStore($this->request->post['store_id']);

            $this->model_module_relatedproductspro->editSetting('relatedproductspro', $this->request->post, $this->request->post['store_id']);
				
			$this->session->data['success'] = $this->language->get('text_success');
			
			if (!empty($_GET['activate'])) {
				$this->session->data['success'] = $this->language->get('text_success_activation');
			}
			
			$selectedTab = (empty($this->request->post['selectedTab'])) ? 0 : $this->request->post['selectedTab'];
			$this->redirect($this->url->link('module/relatedproductspro', 'token=' . $this->session->data['token'] . '&tab='.$selectedTab.'&store_id='.$store['store_id'], 'SSL'));
		}
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_load_in_selector'] = $this->language->get('text_load_in_selector');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_layout_options'] = $this->language->get('entry_layout_options');
		$this->data['entry_position_options'] = $this->language->get('entry_position_options');
		$this->data['entry_enable_disable']	= $this->language->get('entry_enable_disable');
		$this->data['entry_yes'] = $this->language->get('entry_yes');
		$this->data['entry_no'] = $this->language->get('entry_no');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
	
		$this->data['error_warning'] = '';
		$languages = $this->model_localisation_language->getLanguages();
		$this->data['languages'] = $languages;
		$firstLanguage = array_shift($languages);
		$this->data['firstLanguageCode'] = $firstLanguage['code'];
		
		$store = $this->getCurrentStore($this->request->get['store_id']);
		$this->data['stores'] = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' . $this->data['text_default'].')', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
		$this->data['store'] = $store;
		
 		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}
  		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/relatedproductspro', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		$this->data['action'] = $this->url->link('module/relatedproductspro', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$moduleSettings	= $this->model_module_relatedproductspro->getSetting('relatedproductspro', $store['store_id']);
		$this->data['data']['RelatedProductsPro'] = isset($moduleSettings['RelatedProductsPro']) ? $moduleSettings['RelatedProductsPro'] : array(); 
		$this->data['CustomRelated'] = isset($moduleSettings['relatedproductspro_custom']) ? $moduleSettings['relatedproductspro_custom'] : array(); 
		$this->data['modules'] = isset($moduleSettings['relatedproductspro_module']) ? $moduleSettings['relatedproductspro_module'] : array(); 

		$this->data['currenttemplate'] =  $this->config->get('config_template');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/relatedproductspro.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);		
		$this->load->model('catalog/product');
		
		$this->response->setOutput($this->render());
	}
	
 	public function install() {
    }
	
    public function uninstall() {
    }
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/relatedproductspro')) {
			$this->error = true;
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_SERVER;
        } else {
            $storeURL = HTTP_SERVER;
        } 
        return $storeURL;
    }

    private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }	
}
?>