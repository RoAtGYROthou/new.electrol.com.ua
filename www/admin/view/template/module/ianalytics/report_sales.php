<?php
	$this->language->load('report/sale_order');

		if (isset($this->request->get['fromDate'])) {
			$filter_date_start = $this->request->get['fromDate'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['toDate'])) {
			$filter_date_end = $this->request->get['toDate'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filterOrders'])) {
			$filter_order_status_id = $this->request->get['filterOrders'];
		} else {
			$filter_order_status_id = 0;
		}
		
		$filter_group = 'day';

	if (isset($this->request->get['page'])) {
		$page = $this->request->get['page'];
	} else {
		$page = 1;
	}

	$this->load->model('report/sale');

	$this->data['orders'] = array();

	$data4 = array(
		'filter_date_start'	     => $filter_date_start, 
		'filter_date_end'	     => $filter_date_end, 
		'filter_group'           => $filter_group,
		'filter_order_status_id' => $filter_order_status_id,
		'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
		'limit'                  => $this->config->get('config_admin_limit')
	);

	$order_total = $this->model_report_sale->getTotalOrders($data4);

	$results = $this->model_catalog_ianalytics->getOrders($data4);

	foreach ($results as $result) {
		$this->data['orders'][] = array(
			'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
			'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
			'orders'     => $result['orders'],
			'tax'        => $this->currency->format($result['tax'], $this->config->get('config_currency')),
			'tax_nocurrency'        => isset($result['tax']) ? $result['tax'] : '0',
			'total'      => $this->currency->format($result['total'], $this->config->get('config_currency')),
			'total_nocurrency'      => $result['total']
		);
	}

	$this->data['token'] = $this->session->data['token'];

	$url = '';

	if (isset($this->request->get['fromDate'])) {
		$url .= '&fromDate=' . $this->request->get['fromDate'];
	}

	if (isset($this->request->get['toDate'])) {
		$url .= '&toDate=' . $this->request->get['toDate'];
	}

	if (isset($this->request->get['filterOrders'])) {
		$url .= '&filterOrders=' . $this->request->get['filterOrders'];
	}

	$pagination = new Pagination();
	$pagination->total = $order_total;
	$pagination->page = $page;
	$pagination->limit = $this->config->get('config_admin_limit');
	$pagination->text = $this->language->get('text_pagination');
	$pagination->url = $this->url->link('module/ianalytics', 'token=' . $this->session->data['token'] . $url . '&page={page}');

	$this->data['pagination'] = $pagination->render();		
?>

<?php if ($this->data['orders']) { ?>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
	function drawVisualizationOrders() {
      	var data = google.visualization.arrayToDataTable([
          ['Day', 'Taxes', 'Revenue','Total'],
		  <?php $reversed = array_reverse($this->data['orders'], true); ?>
			<?php foreach ($reversed as $order) { ?>
		 	  ['<?php echo $order['date_start']; ?>', <?php echo $order['tax_nocurrency']; ?>, <?php echo $order['total_nocurrency']-$order['tax_nocurrency']; ?>, <?php echo $order['total_nocurrency']; ?>],
	 		<?php } ?>
        ]);

        var options = {
          title: 'Sales Performance',
          hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}, slantedText:true },
          vAxis: {minValue: 0},
		  width: $('#aftersale').width() - 270,
		  height: 400,
		  colors: ['#3B15B5', '#FF0000', '#00A08A']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('visualization3'));
        chart.draw(data, options);
      }
    </script>
    <div id="visualization3" style="overflow:hidden;"></div>
<?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
	<table class="table-hover table table-striped">
    	<thead>
          <tr>
            <th>Date</th>
            <th>Orders</th>
            <th>Tax</th>
            <th>Total</th>
          </tr>
        </thead>
        
	<?php if ($this->data['orders']) { ?>
	 	 <?php foreach ($this->data['orders'] as $order) { ?>
              <tr>
                <td class="left"><?php echo $order['date_start']; ?></td>
                <td class="right"><?php echo $order['orders']; ?></td>
                <td class="right"><?php echo $order['tax']; ?></td>
                <td class="right"><?php echo $order['total']; ?></td>
              </tr>
	 	 <?php } ?>
	<?php } else { ?>
          <tr>
            <td class="center" colspan="5">There are no results yet!</td>
          </tr>
	  <?php } ?>
	  </table>
	<div class="pagination"><?php echo $this->data['pagination']; ?></div>