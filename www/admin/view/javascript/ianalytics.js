google.load("visualization", "1.1", {packages:["corechart"]});

// Tabs start
$('a[href=#daily-total-stats-more]').on('click', function() {
	$('a[href=#visitors]').click();
	$('a[href=#daily-total-stats]').click();
});
$('a[href=#traffic-sources-more]').on('click', function() {
	$('a[href=#visitors]').click();
	$('a[href=#referer-stats]').click();
});
$('a[href=#conversion-rate-more]').on('click', function() {
	$('a[href=#aftersale]').click();
	$('a[href=#customer-funnel]').click();
});
$('a[href=#sales-report-more]').on('click', function() {
	$('a[href=#aftersale]').click();
	$('a[href=#sales-report]').click();
});
$('a[href=#ordered-products-more]').on('click', function() {
	$('a[href=#aftersale]').click();
	$('a[href=#ordered-products]').click();
});
$('a[href=#searched-keywords-more]').on('click', function() {
	$('a[href=#presale]').click();
	$('a[href=#most-searched-products]').click();
});
// Tab End

$(window).resize(function(){
	loadcharts();
});

$(window).load(function(){
	loadcharts();
});

$('a[href=#dashboard1]').on('click', function() {
	setTimeout(function() { loadcharts(); }, 15);
});
$('a[href=#presale]').on('click', function() {
	setTimeout(function() { loadcharts(); }, 15);
});
$('a[href=#aftersale]').on('click', function() {
	setTimeout(function() { loadcharts(); }, 15);
});
$('a[href=#visitors]').on('click', function() {
	setTimeout(function() { loadcharts(); }, 15);
});

function loadcharts() {
	if (typeof drawChart == 'function') {
		drawChart();
	}
	if (typeof drawLineChart == 'function') {
		drawLineChart();
	}
	if (typeof drawVisualizationDashboard1 == 'function') {
		drawVisualizationDashboard1();
	}
	if (typeof drawVisualizationDashboard3 == 'function') {
		drawVisualizationDashboard3();
	}
	if (typeof drawVisualizationDashboard2 == 'function') {
		drawVisualizationDashboard2();
	}
	if (typeof drawVisualizationDashboard4 == 'function') {
		drawVisualizationDashboard4();
	}
	if (typeof drawVisualizationCustomers == 'function') {
		drawVisualizationCustomers();
	}
	if (typeof drawVisualizationOrders == 'function') {
		drawVisualizationOrders();
	}
	if (typeof drawVisualizationVisitorsByDay == 'function') {
		drawVisualizationVisitorsByDay();
	}
	if (typeof drawVisualizationVisitorsByPartOfTheDay == 'function') {
		drawVisualizationVisitorsByPartOfTheDay();
	}
	if (typeof drawVisualizationVisitorsDataReferers == 'function') {
		drawVisualizationVisitorsDataReferers();
	}
	if (typeof drawVisualization == 'function') {
		drawVisualization();
	}	
	drawVisualization
};

function drawLineChart() {
	if(typeof(monthlySearchesGraph) != 'undefined') {
		var data = google.visualization.arrayToDataTable(monthlySearchesGraph);
		var options = {
		  hAxis: {title: '',  titleTextStyle: {color: 'blue'}, slantedText:true},
		  chartArea: {left: 70},
		  width: $('#presale').width() - 260,
		  height: 400
		};
		
		var chart = new google.visualization.AreaChart(document.getElementById('searchedFound'));
		chart.draw(data, options);
	}
}

function drawChart() {
	var rawDataToArray = function(num) {
		if (!num) return null;
		var rawrows = num.split(';');
		var rows = [];
		$(rawrows).each(function(i) {
			var expression = $.trim($(this)[0]);
			if (expression != '') {
				var spl = expression.split(':');
				if (spl[1]) { spl[1] = parseInt(spl[1])}
				rows.push(spl);
			}
		});
		
		return rows;
	}
	
	// Pieable
	$('.pieable').each(function(index) {

		var data = new google.visualization.DataTable();
		var title = $(this).data('title'); if (!title) { title = ''; }
		var options = {
		  title: title,
		  width: $('#dashboard1').width()/4,
		  backgroundColor: 'transparent',
		  is3D: false,
		  chartArea: {
			  width: '100%',
			  height: '75%'
		  },
		  legend: {
			 position: 'right',
			 textStyle: {
				 fontSize: '11'
			 }
		  },
		  pieSliceTextStyle: {
			 fontSize: '11'
		  }
		};
		
		var rows = rawDataToArray($(this).data('num'));
		var colors = [];
		$(rows).each(function(i) {
			if (rows[i])
				if (rows[i][2]) {
					colors.push(rows[i].pop());
				}
		});
		if (colors.length > 0) {
			options.colors = colors;
		}
		//var colors = rawDataToArray($(this).data('colors'));
		
		data.addColumn('string', 'Label');
		data.addColumn('number', 'Value');
		data.addRows(rows);
		
		var chart = new google.visualization.PieChart($(this)[0]);
		chart.draw(data, options);
	});

}

var nowDate = new Date();

$('.iAnalyticsDateFilter input').datepicker({
	dateFormat:'yy-mm-dd',
	changeMonth:true,
	changeYear:true,
	maxDate:'+0 d',
	minDate:iAnalyticsMinDate
});

$('.fromDate').datepicker("option", "onSelect", function( selectedDate ) {
	iAnalyticsUpdateSelect($(this));
	$( ".iAnalyticsDateFilter input.toDate" ).datepicker( "option", "minDate", selectedDate );
});

$('.toDate').datepicker("option", "minDate", $( ".iAnalyticsDateFilter input.fromDate" ).datepicker( "getDate" ));
$('.toDate').datepicker("option", "onSelect", function( selectedDate ) {
	iAnalyticsUpdateSelect($(this));
	$( ".iAnalyticsDateFilter input.fromDate" ).datepicker( "option", "maxDate", selectedDate );
});

$('.iAnalyticsSelectBox').change(function(e) {
	
	var fromDate = new Date(); fromDate.setTime(nowDate.getTime());
	var toDate = new Date(); toDate.setTime(nowDate.getTime());
	var substract = 0;
	if ($(this).val() == 'last-week') {
		substract = 6*24*3600*1000;
	} else if ($(this).val() == 'last-month') {
		substract = 29*24*3600*1000;	
	} else if ($(this).val() == 'last-year') {
		substract = 364*24*3600*1000;	
	}
	
	fromDate.setTime(fromDate.getTime() - substract);
	
	if (substract > 0) {
		$('.iAnalyticsDateFilter .toDate').datepicker('setDate', toDate);
		$('.iAnalyticsDateFilter .fromDate').datepicker('setDate', fromDate);
	}
});

$('.iAnalyticsDateFilter button.dateFilterButton').click(function(event) {
	var fromDate = $(this).parent().parent().children('td:eq(1)').children('input').val();
	var toDate = $(this).parent().parent().children('td:eq(2)').children('input').val();
	//
	if ($(this).parent().parent().children('td:eq(3)').children('select').val()) { 
		var filter = $(this).parent().parent().children('td:eq(3)').children('select').val();
	}
	else var filter = 0;
	//
	iAnalyticsFilterDates(fromDate, toDate);
	
	fromDate = $.datepicker.formatDate('yy-mm-dd', $(this).parent().parent().children('td:eq(1)').children('input').datepicker( "getDate" ));
	toDate =  $.datepicker.formatDate('yy-mm-dd', $(this).parent().parent().children('td:eq(2)').children('input').datepicker( "getDate" ));

	var newURL = document.location.search;

	if (newURL.match(/fromDate=/) != null) {
		newURL = newURL.replace(/fromDate=(.*)(&|$)/g, "fromDate=" + fromDate + "&");	
	} else { 
		newURL +='&fromDate=' + fromDate; 
	}
	
	if (newURL.match(/toDate=/) != null) {
		newURL = newURL.replace(/toDate=(.*)(&|$)/g, "toDate=" + toDate + "&");	
	} else { 
		newURL +='&toDate=' + toDate; 
	}
	//
	if (newURL.match(/filterOrders=/) != null) {
		newURL = newURL.replace(/filterOrders=(.*)(&|$)/g, "filterOrders=" + filter + "&");	
	} else { 
		newURL +='&filterOrders=' + filter; 
	}
	//
	newURL = newURL.replace(/&+/g, "&");
	newURL = newURL.replace(/(&$)/g, "");
	
	document.location.search = newURL ;
});

function iAnalyticsUpdateSelect(field) {
	var fromDate = $(field).parent().parent().children('td:eq(1)').children('input').datepicker('getDate').getTime()/(24*3600*1000);
	var toDate = $(field).parent().parent().children('td:eq(2)').children('input').datepicker('getDate').getTime()/(24*3600*1000);
	var interval = null;
	
	if ($.datepicker.formatDate('yy-mm-dd', $(field).parent().parent().children('td:eq(2)').children('input').datepicker('getDate')) == $.datepicker.formatDate('yy-mm-dd', nowDate)) {
		interval = toDate - fromDate + 1;	
	}
	
	if (interval == 7) {
		$('.iAnalyticsSelectBox').val('last-week');
	} else if (interval == 30) {
		$('.iAnalyticsSelectBox').val('last-month');
	} else if (interval == 365) {
		$('.iAnalyticsSelectBox').val('last-year');
	} else {
		$('.iAnalyticsSelectBox').val('custom');
	}
}

function iAnalyticsFilterDates(fromDate, toDate) {
	try {
		$.datepicker.parseDate('yy-mm-dd', toDate);
	} catch(err) {
		$( ".iAnalyticsDateFilter input.fromDate" ).datepicker( "setDate", $('.iAnalyticsDateFilter input.toDate').datepicker("option", "maxDate"));
	}
	
	try {
		$.datepicker.parseDate('yy-mm-dd', fromDate);
	} catch(err) {
		$( ".iAnalyticsDateFilter input.fromDate" ).datepicker( "setDate", iAnalyticsMinDate );
	}
}