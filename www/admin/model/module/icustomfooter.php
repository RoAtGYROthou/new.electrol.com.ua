<?php class Modelmoduleicustomfooter extends Model {
	public function __construct($register) {
		if (!defined('IMODULE_ROOT')) define('IMODULE_ROOT', substr(DIR_APPLICATION, 0, strrpos(DIR_APPLICATION, '/', -2)) . '/');
		if (!defined('IMODULE_SERVER_NAME')) define('IMODULE_SERVER_NAME', substr((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER), 7, strlen((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER)) - 8));
		if (!defined('IMODULE_SERVER')) define('IMODULE_SERVER', isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER);
		if (!defined('PAYMENTICONS_FOLDER')) define('PAYMENTICONS_FOLDER', DIR_IMAGE . 'icustomfooter/paymenticons/');
		
		parent::__construct($register);
	}
	
	public function getSetting($group, $store_id = 0) {
		$data = array(); 
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
		
		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = unserialize($result['value']);
			}
		}

		return $data;
	}
	
	public function editSetting($group, $data, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
		
		foreach ($data as $key => $value) {
			if (!is_array($value)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'");
			}
		}
	}
	
	public function deleteSetting($group, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
	}
	
	public function findTextBetween($word1, $word2, $content) {
		$between = str_replace($word1, '',substr($content, strpos($content, $word1), strpos($content, $word2) - strpos($content, $word1)));
		return $between;
	}
	
	public function normalizeImages() {
		$raw_image_files = scandir(PAYMENTICONS_FOLDER);
		foreach ($raw_image_files as $index => $file) {
			if (in_array($file, array('.', '..'))) continue;
			$oldName = $file;
			$newName = str_pad($index, strlen(count($raw_image_files) - 1), '0', STR_PAD_LEFT) . '_' . preg_replace('/[^a-z .]/i', '', $oldName);
			rename(PAYMENTICONS_FOLDER . $oldName, PAYMENTICONS_FOLDER . $newName);
		}
	}
	
	public function generateTabs($keywords = false) {
		$store = $keywords ? '{STORE}' : '0';
		if (!$keywords) {
			if (isset($this->request->post['store'])) {
				$store = $this->request->post['store'];
			} else if (!empty($this->request->get['store'])) {
				$store = $this->request->get['store'];
			}
		}
		
		$tab = $keywords ? '{TAB}' : '0';
		if (!$keywords) {
			if (isset($this->request->post['tab'])) {
				$tab = $this->request->post['tab'];
			} else if (!empty($this->request->get['tab'])) {
				$tab = $this->request->get['tab'];
			}
		}
		
		$subtab = $keywords ? '{SUBTAB}' : '0';
		if (!$keywords) {
			if (isset($this->request->post['subtab'])) {
				$subtab = $this->request->post['subtab'];
			} else if (!empty($this->request->get['subtab'])) {
				$subtab = $this->request->get['subtab'];
			}
		}
		
		return '&store=' . $store . '&tab=' . $tab . '&subtab=' . $subtab;
	}
	
	public function uploadIcon() {
		$files = $this->request->files['paymentIcon'];
		$dir = DIR_IMAGE . 'icustomfooter/paymenticons/';
		
		if ((($files["type"] == "image/gif") || ($files["type"] == "image/jpeg") || ($files["type"] == "image/png")) && $files["size"] < 102400) {
			if ($files["error"] > 0) {
				$this->session->data['error'] = $this->language->get('error_upload');
				$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->generateTabs(), 'SSL'));
			} else {
				$extension = pathinfo($files['name'], PATHINFO_EXTENSION);
				$target_name = $dir . '00_' . $this->request->post['paymentIconName'] . '.' . $extension;
				
				if (file_exists($target_name)) {
					$this->session->data['error'] = $this->language->get('error_upload_exists');
					$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->generateTabs(), 'SSL'));
				} else {
					move_uploaded_file($files["tmp_name"], $target_name);
					$this->session->data['success'] = $this->language->get('success_upload_icon');
					$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->generateTabs(), 'SSL'));
				}
			}
		} else {
			$this->session->data['error'] = $this->language->get('error_upload');
			$this->redirect($this->url->link('module/icustomfooter', 'token=' . $this->session->data['token'] . $this->generateTabs(), 'SSL'));
		}
	}
	
	protected function redirect($url, $status = 302) {
		header('Status: ' . $status);
		header('Location: ' . str_replace(array('&amp;', "\n", "\r"), array('&', '', ''), $url));
		exit();				
	}
	
	public function getSystemStores() {
		$this->load->model('setting/store');
		return array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' .$this->language->get('text_default') . ')', 'url' => NULL, 'ssl' => NULL)), $this->model_setting_store->getStores());
	}
	
	public function getSystemLanguages() {
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages(array());
		return $languages;
	}
	
	public function getDefaultSettings() {
		$settings_json = '[{"en":{"Widgets":{"AboutUs":{"Show":"true","Title":"About us","Text":"<p>iCustomFooter is a widely popular custom footer creation module. It is light-weight piece of software that comes with preset Twitter, Facebook, YouTube and Pinterest integrations. Other options include text editing via Rich Text Editor and controlling the visibility of each widget. Should you have any concerns or questions please visit our support page from the link in the module.<\/p>\r\n\r\n<p>Enjoy iCustomFooter!<\/p>\r\n"},"ContactForm":{"Show":"true","UseCaptcha":"true","Title":"Drop us a line","Email":"conicgp@gmail.com","EmailSubject":"iCustomFooter Message","LabelName":"Your name:","LabelEmail":"Your email:","LabelMessage":"Message","MaxMessageLength":"1000","LabelCaptcha":"Code:","LabelSend":"Send message","LabelSuccess":"Your message was sent, thanks!","LabelRequired":"is a required field","LabelNotValid":"is not valid","LabelInvalidCaptcha":"Invalid CAPTCHA code"},"Contacts":{"Show":"true","IconSet":"","Title":"Contact us","Text":"<p>Work days 9am - 6pm<\/p>\r\n","Address1":"1 Infinite Loop","Address2":"Cupertino, CA 95014","Phone1":"800-692-7753","Phone2":"1-800-MY-APPLE","Fax1":"800-692-7753","Fax2":"1-800-MY-APPLE","Email1":"some@apple.com","Email2":"other@apple.com","Skype1":"appleskype","Skype2":"petermarretc"},"Facebook":{"Show":"true","Title":"Facebook","URL":"http:\/\/www.facebook.com\/iSenseLabs","Height":"270"},"GoogleMaps":{"Show":"true","Title":"Google Maps","Longitude":"23.333333","Latitude":"42.7","APIKey":""},"Twitter":{"Show":"true","NumberOfTweets":"2","Title":"Tweet feed","Method":"profile","Profile":"iSenseLabs","Keyword":"isense"},"YouTube":{"Show":"true","Title":"YouTube","URL":"ShkYDPN5Knc","Width":"","Height":""},"Custom1":{"Show":"true","Title":"Custom column 1","Text":"<p style=\"font-size: 13px;\">In this column you can put any custom content you want via the admin panel. You can edit it though a rich text editor which means you can format the text, upload images, add flash, links and others. You can also change the title of each column in iCustomFooter.<\/p>\r\n\r\n<p style=\"font-size: 13px;\">As Norman Vincent Pealse says,&nbsp;<em>\u201cEvery problem has in it the seeds of its own solution. If you don\u2019t have any problems, you don\u2019t get any seeds.\u201d<\/em><\/p>\r\n\r\n<p style=\"font-size: 13px;\">Enjoy the custom column!<\/p>\r\n"},"Custom2":{"Show":"false","Title":"Custom column 2","Text":"<p>In this column you can put any custom content you want via the admin panel. You can edit it though a rich text editor which means you can format the text, upload images, add flash, links and others. You can also change the title of each column in iCustomFooter.<\/p>\r\n\r\n<p>As Norman Vincent Pealse says,&nbsp;<em>\u201cEvery problem has in it the seeds of its own solution. If you don\u2019t have any problems, you don\u2019t get any seeds.\u201d<\/em><\/p>\r\n\r\n<p>Enjoy the custom column!<\/p>\r\n"},"Custom3":{"Show":"false","Title":"Custom column 3","Text":"<p>In this column you can put any custom content you want via the admin panel. You can edit it though a rich text editor which means you can format the text, upload images, add flash, links and others. You can also change the title of each column in iCustomFooter.<\/p>\r\n\r\n<p>As Norman Vincent Pealse says,&nbsp;<em>\u201cEvery problem has in it the seeds of its own solution. If you don\u2019t have any problems, you don\u2019t get any seeds.\u201d<\/em><\/p>\r\n\r\n<p>Enjoy the custom column!<\/p>\r\n"},"Custom4":{"Show":"false","Title":"Custom column 4","Text":"<p>In this column you can put any custom content you want via the admin panel. You can edit it though a rich text editor which means you can format the text, upload images, add flash, links and others. You can also change the title of each column in iCustomFooter.<\/p>\r\n\r\n<p>As Norman Vincent Pealse says,&nbsp;<em>\u201cEvery problem has in it the seeds of its own solution. If you don\u2019t have any problems, you don\u2019t get any seeds.\u201d<\/em><\/p>\r\n\r\n<p>Enjoy the custom column!<\/p>\r\n"},"Custom5":{"Show":"false","Title":"Custom column 5","Text":"<p>In this column you can put any custom content you want via the admin panel. You can edit it though a rich text editor which means you can format the text, upload images, add flash, links and others. You can also change the title of each column in iCustomFooter.<\/p>\r\n\r\n<p>As Norman Vincent Pealse says,&nbsp;<em>\u201cEvery problem has in it the seeds of its own solution. If you don\u2019t have any problems, you don\u2019t get any seeds.\u201d<\/em><\/p>\r\n\r\n<p>Enjoy the custom column!<\/p>\r\n"}},"Positions":{"aboutus":"1","contactform":"4","contacts":"2","facebook":"6","googlemaps":"3","twitter":"5","youtube":"8","Custom1":"1","Custom2":"2","Custom3":"3","Custom4":"4","Custom5":"5"}},"Settings":{"PaymentIcons":{"Show":"true"},"SocialButtons":{"Show":"true","FacebookLike":{"Show":"true"},"TwitterPin":{"Show":"true"},"PinterestPin":{"Show":"true"},"GooglePlus":{"Show":"true"}},"Show":"true","ResponsiveDesign":"yes","FooterWidth":"980","UseFooterWith":"defaultocwithicons","HidePoweredBy":"","FontFamily":"fontfamilyinherit","BackgroundPattern":"whitebgpattern","ColumnContentOverflow":"overflowhidden","ColumnHeight":"330","ColumnWidth":"220","ColumnColor":"#FFFFFF","BackgroundColor":"#FFFFFF","TextColor":"#FFFFFF","ColumnLineStyle":"dashed","CustomCSS":"\/* Write your Custom CSS here *\/\r\n"}}]';
		$settings = json_decode($settings_json, true);
		$languages = $this->getSystemLanguages();
		$stores = $this->getSystemStores();
		foreach($languages as $l) {
			foreach ($stores as $s) {
				if (empty($settings[$s['store_id']])) {
					$settings[$s['store_id']] = $settings[0];
				}
				if (strtolower($l['code']) != 'en') {
					$settings[$s['store_id']][$l['code']] = $settings[0]['en'];
				}
			}
		}
		
		return $settings;
	}
}
?>