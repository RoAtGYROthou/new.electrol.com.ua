<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
	<div class="buttons"><a onclick="location = '<?php echo $this->url->link('catalog/redirect', 'token=' . $this->session->data['token'], 'SSL'); ?>';" class="button">Create Redirect</a></div>    
  </div>
  <div class="content">

	 <table class="list">
          <thead>
            <tr>              
              <td class="left">Not Found Link</td>
              <td class="left">Date</td>              
            </tr>
          </thead>
          <tbody>
            
            <?php if (!empty($pages)) { ?>
            <?php foreach ($pages as $page) { ?>
            <tr>              
              <td class="left"><?php echo $page['link']; ?></td>
              <td class="left"><?php echo $page['date']; ?></td>                            			  
            </tr>
			<?php } ?>			
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
		</table>
		 <div class="pagination"><?php echo $pagination; ?></div>
	</div>

	<span style="color:red" class="help">*</span>	
  </div>
</div>
<script type="text/javascript"><!--

var route_row = <?php echo $route_row; ?>;

function addroute() {	
        
	html  = '<tbody id="route-row' + route_row + '">';
	html += '  <tr>';
	html += '    <td><input type="text" name="redirect[' + route_row + '][title]" size="80" value="" /></td>';
	html += '    <td><input type="text" name="redirect[' + route_row + '][url]" size="80" value="" /></td>';
	html += '    <td class="left"><a onclick="$(\'#route-row' + route_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#redirect tfoot').before(html);
	
	route_row++;
}

//--></script>
</script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script>
<?php echo $footer; ?>