<?php echo $header; ?>
<div id="content">
    <ol class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
    		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
    </ol>
    <?php if (empty($data['iAnalytics']['LicensedOn'])) { 
        echo base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNzdXBwb3J0XScpLnRyaWdnZXIoJ2NsaWNrJykiPkVudGVyIHlvdXIgbGljZW5zZSBjb2RlPC9hPg0KICAgIDwvZGl2Pg==');
	 } ?>
	<?php if ($error_warning) { ?>
	<div class="alert alert-warning fade in"><span class="glyphicon glyphicon-warning-sign"></span> <?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if (!empty($this->session->data['success'])) { ?>
    <div class="alert alert-success autoSlideUp"><i class="icon-ok-sign"></i> <?php echo $this->session->data['success']; ?></div>
    <script> $('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $this->session->data['success'] = null; } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<strong><?php echo $heading_title; ?></strong></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="tabbable">
				<div class="tab-navigation">
                    <ul class="nav nav-tabs mainMenuTabs" id="mainTabs">
                        <li><a href="#dashboard1" data-toggle="tab"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                        <li><a href="#presale" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Pre-Sale</a></li>
                        <li><a href="#aftersale" data-toggle="tab"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span> After-Sale</a></li>
                        <li><a href="#visitors" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Visitors</a></li>
                        <li><a href="#controlpanel" data-toggle="tab"><i class="fa fa-cogs"></i> Settings</a></li>
                        <li><a href="#support" data-toggle="tab"><i class="fa fa-external-link"></i> Support</a></li>        
                    </ul>
                    <div class="tab-buttons">
                        <div class="btn-group">
							<button type="button" onClick="javascript:void(0)" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
								<span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;Data&nbsp; <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
                                <?php if ((!empty($iAnalyticsStatus['status']) && $iAnalyticsStatus['status'] == 'run') || empty($iAnalyticsStatus)){ ?>
                                <li><a href="javascript:void(0)" onclick="document.location='index.php?route=module/ianalytics/pausegatheringdata&token=<?php echo $_GET['token']; ?>'"><span class="glyphicon glyphicon-pause"></span>&nbsp;Pause Gathering Data</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" onclick="document.location='index.php?route=module/ianalytics/resumegatheringdata&token=<?php echo $_GET['token']; ?>'"><span class="glyphicon glyphicon-play"></span>&nbsp;Resume Gathering Data</a></li>
                                <?php } ?>
                                <li class="divider"></li>
                                <li><a onclick="return confirm('Are you sure you wish to delete all analytics data?');" href="index.php?route=module/ianalytics/deleteanalyticsdata&token=<?php echo $this->session->data['token']; ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;Clear All Analytics Data</a></li>
							</ul>
                        </div>
                        <button type="submit" class="btn btn-success save-changes"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Save changes</button>
                        <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;<?php echo $button_cancel?></a>
                  	</div>
          		</div>
                <div class="tab-content">
                    <div id="dashboard1" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_dashboard.php'); ?>                        
                    </div>
                    <div id="presale" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_presale.php'); ?>                        
                    </div>
                    <div id="aftersale" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_aftersale.php'); ?>                        
                    </div>
                    <div id="visitors" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_visitors.php'); ?>                        
                    </div>
                    <div id="controlpanel" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_controlpanel.php'); ?>                        
                    </div>
                    <div id="support" class="tab-pane fade">
                      <?php require_once(DIR_APPLICATION.'view/template/module/ianalytics/tab_support.php'); ?>                        
                    </div>
                </div><!-- /.tab-content -->
			</div>
        </form>   
      </div>
    </div>
</div>
<script type="text/javascript">
var iAnalyticsMinDate = '<?php echo $this->data['iAnalyticsMinDate']; ?>';
$(function() {
	$('#mainTabs a:first').tab('show'); // Select first tab
	$('#preSaleTabs').children().first().children('a').click();
	$('#afterSaleTabs').children().first().children('a').click();
	$('#visitsTabs').children().first().children('a').click();	
	if (window.localStorage && window.localStorage['currentTab']) {
		$('.mainMenuTabs a[href="'+window.localStorage['currentTab']+'"]').tab('show');
	}
	if (window.localStorage && window.localStorage['currentSubTab1']) {
		$('a[href="'+window.localStorage['currentSubTab1']+'"]').tab('show');
	}
	if (window.localStorage && window.localStorage['currentSubTab2']) {
		$('a[href="'+window.localStorage['currentSubTab2']+'"]').tab('show');
	}
	if (window.localStorage && window.localStorage['currentSubTab3']) {
		$('a[href="'+window.localStorage['currentSubTab3']+'"]').tab('show');
	}
	$('.fadeInOnLoad').css('visibility','visible');
	$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
		if (window.localStorage) {
			window.localStorage['currentTab'] = $(this).attr('href');
		}
	});
	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"], #afterSaleTabs a[data-toggle="tab"], #visitsTabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab1'] = $(this).attr('href');
		}
	});
	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"], #preSaleTabs a[data-toggle="tab"], #visitsTabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab2'] = $(this).attr('href');
		}
	});
	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"], #preSaleTabs a[data-toggle="tab"], #afterSaleTabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab3'] = $(this).attr('href');
		}
	});
});
</script>
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript" src="view/javascript/ianalytics.js"></script>
<?php echo $footer; ?>