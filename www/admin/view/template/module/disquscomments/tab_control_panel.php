<table class="form">
  <tr>
    <td><span class="required">*</span> <?php echo $entry_code; ?></td>
    <td>
        <div class="col-xs-3">
            <select name="DisqusComments[Enabled]" class="DisqusCommentsEnabled form-control">
                <option value="yes" <?php echo ($data['DisqusComments']['Enabled'] == 'yes') ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo ($data['DisqusComments']['Enabled'] == 'no') ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
  <tr class="DisqusCommentsActiveTR">
    <td><span class="required">*</span> <?php echo $entry_layouts_active; ?></td>
    <td>
    <?php $checked = '';	$i=0;?>
    <?php foreach ($layouts as $layout) { ?>
    <?php 
	    $status = null;
        foreach ($modules as $module) {
            if(!empty($module)) {
                if ($module['layout_id'] == $layout['layout_id']) {
                    $status = $module['status'];
                    if ((int)$status == 1) {
                        $checked = ' checked=checked';
                    } else { 
                        $checked = '';
                    }
                }
            } 
          
          }
          if (!isset($status) && $layout['name'] == 'Product') {
                $status = 1;
                $checked = ' checked=checked';	
          }
		  
		  
    ?>
    <div style="padding-left:15px;">
   		<div class="DisqusCommentsLayout">
            <input type="checkbox" value="<?php echo $layout['layout_id']; ?>" id="DisqusCommentsActive<?php echo $i?>" <?php echo $checked?> /><label class="checkbox-inline lbl" for="DisqusCommentsActive<?php echo $i?>"><?php echo $layout['name']; ?></label>
            <input type="hidden" name="disquscomments_module[<?php echo $i?>][position]" value="content_bottom" />
            <input type="hidden" class="DisqusCommentsItemLayoutIDField" name="disquscomments_module[<?php echo $i?>][layout_id]" value="<?php echo $layout['layout_id']; ?>" />
            <input type="hidden" class="DisqusCommentsItemStatusField" name="disquscomments_module[<?php echo $i?>][status]" value="<?php echo $status ?>" />
            <input type="hidden" name="disquscomments_module[<?php echo $i?>][sort_order]" value="<?php echo $i+10?>" />
        </div>
    </div>
    <?php $i++;} ?>
     </td>
  </tr>
</table>
<script>
$('.DisqusCommentsLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.DisqusCommentsItemStatusField', $(this).parent()).val(1);
    } else {
        $('.DisqusCommentsItemStatusField', $(this).parent()).val(0);
    }
});

$('.DisqusCommentsEnabled').change(function() {
    toggleDisqusCommentsActive(true);
});

var toggleDisqusCommentsActive = function(animated) {
    if ($('.DisqusCommentsEnabled').val() == 'yes') {
        if (animated) 
            $('.DisqusCommentsActiveTR').fadeIn();
        else 
            $('.DisqusCommentsActiveTR').show();
    } else {
        if (animated) 
            $('.DisqusCommentsActiveTR').fadeOut();
        else 
            $('.DisqusCommentsActiveTR').hide();
    }
}

toggleDisqusCommentsActive(false);
</script>