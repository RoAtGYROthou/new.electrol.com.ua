<table class="form">
  <tr>
    <td><span class="required">*</span> <?php echo $entry_code; ?></td>
    <td valign="top">
        <div class="col-xs-3">
            <select name="iSearch[Enabled]" class="iSearchEnabled form-control">
                <option value="yes" <?php echo ($data['iSearch']['Enabled'] == 'yes') ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo ($data['iSearch']['Enabled'] == 'no') ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>    
   </td>
  </tr>
  <tr>
    <td valign="top"><span class="required">*</span> Search in: <span class="help">Choose only the parameters you need.</span></td>
    <td>
    <div style="padding-left:15px;">    
        <div class="searchInSpan">
            <input type="checkbox" id="searchIn1" name="iSearch[SearchIn][ProductName]" <?php if(!empty($data['iSearch']['SearchIn']['ProductName'])) { echo ($data['iSearch']['SearchIn']['ProductName'] == 'on') ? 'checked=checked' : ''; } else { echo ''; } ?> /><label class="checkbox-inline lbl" for="searchIn1">Product Name</label>
        </div>
        <div class="searchInSpan">
            <input type="checkbox" id="searchIn2" name="iSearch[SearchIn][ProductModel]"  <?php if(!empty($data['iSearch']['SearchIn']['ProductModel'])) echo ($data['iSearch']['SearchIn']['ProductModel'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn2">Product Model</label>
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn3" name="iSearch[SearchIn][UPC]" <?php if(!empty($data['iSearch']['SearchIn']['UPC'])) echo ($data['iSearch']['SearchIn']['UPC'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn3">UPC</label>        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn10" name="iSearch[SearchIn][SKU]" <?php if(!empty($data['iSearch']['SearchIn']['SKU'])) echo ($data['iSearch']['SearchIn']['SKU'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn10">SKU</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn15" name="iSearch[SearchIn][EAN]" <?php if(!empty($data['iSearch']['SearchIn']['EAN'])) echo ($data['iSearch']['SearchIn']['EAN'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn15">EAN</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn16" name="iSearch[SearchIn][JAN]" <?php if(!empty($data['iSearch']['SearchIn']['JAN'])) echo ($data['iSearch']['SearchIn']['JAN'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn16">JAN</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn17" name="iSearch[SearchIn][ISBN]" <?php if(!empty($data['iSearch']['SearchIn']['ISBN'])) echo ($data['iSearch']['SearchIn']['ISBN'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn17">ISBN</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn18" name="iSearch[SearchIn][MPN]" <?php if(!empty($data['iSearch']['SearchIn']['MPN'])) echo ($data['iSearch']['SearchIn']['MPN'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn18">MPN</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn4" name="iSearch[SearchIn][Manufacturer]" <?php if(!empty($data['iSearch']['SearchIn']['Manufacturer'])) echo ($data['iSearch']['SearchIn']['Manufacturer'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn4">Manufacturer</label>        
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn7" name="iSearch[SearchIn][AttributeNames]" <?php if(!empty($data['iSearch']['SearchIn']['AttributeNames'])) echo ($data['iSearch']['SearchIn']['AttributeNames'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn7">Attribute Names</label>       </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn7_1" name="iSearch[SearchIn][AttributeValues]" <?php if(!empty($data['iSearch']['SearchIn']['AttributeValues'])) echo ($data['iSearch']['SearchIn']['AttributeValues'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn7_1">Attribute Values</label>       </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn8" name="iSearch[SearchIn][Categories]" <?php if(!empty($data['iSearch']['SearchIn']['Categories'])) echo ($data['iSearch']['SearchIn']['Categories'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn8">Categories</label>        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn19" name="iSearch[SearchIn][Filters]" <?php if(!empty($data['iSearch']['SearchIn']['Filters'])) echo ($data['iSearch']['SearchIn']['Filters'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn19">Filters</label>      </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn9" name="iSearch[SearchIn][Description]" <?php if(!empty($data['iSearch']['SearchIn']['Description'])) echo ($data['iSearch']['SearchIn']['Description'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn9">Description</label>        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn5" name="iSearch[SearchIn][Tags]" <?php if(!empty($data['iSearch']['SearchIn']['Tags'])) echo ($data['iSearch']['SearchIn']['Tags'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn5">Tags</label>        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn6" name="iSearch[SearchIn][Location]" <?php if(!empty($data['iSearch']['SearchIn']['Location'])) echo ($data['iSearch']['SearchIn']['Location'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn6">Location</label>        
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn11" name="iSearch[SearchIn][OptionName]" <?php if(!empty($data['iSearch']['SearchIn']['OptionName'])) echo ($data['iSearch']['SearchIn']['OptionName'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn11">Option Name</label>     
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn12" name="iSearch[SearchIn][OptionValue]" <?php if(!empty($data['iSearch']['SearchIn']['OptionValue'])) echo ($data['iSearch']['SearchIn']['OptionValue'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn12">Option Value</label>     
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn13" name="iSearch[SearchIn][MetaDescription]" <?php if(!empty($data['iSearch']['SearchIn']['MetaDescription'])) echo ($data['iSearch']['SearchIn']['MetaDescription'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn13">Meta Description</label>     
        </div>
        <div class="searchInSpan onlyUseAJAX">
            <input type="checkbox" id="searchIn14" name="iSearch[SearchIn][MetaKeyword]" <?php if(!empty($data['iSearch']['SearchIn']['MetaKeyword'])) echo ($data['iSearch']['SearchIn']['MetaKeyword'] == 'on') ? 'checked=checked' : ''?> /><label class="checkbox-inline lbl" for="searchIn14">Meta Keyword</label>     
        </div>
    </div>   
    </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Responsive Design <span class="help">Select &quot;Yes&quot; if you want to make iSearch Results width fit your responsive design theme Search Field</span></td>
    <td valign="top">
        <div class="col-xs-3">
            <select name="iSearch[ResponsiveDesign]" class="ResponsiveDesign form-control">
                <option value="no" <?php echo ($data['iSearch']['ResponsiveDesign'] == 'no') ? 'selected=selected' : ''?>>No</option>
                <option value="yes" <?php echo ($data['iSearch']['ResponsiveDesign'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
            </select>
        </div>    
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Use AJAX <span class="help">Select &quot;Yes&quot; if you want to load the search results asynchronously from the server on typing, or &quot;No&quot; to cache them on page load first (some features are limited in non-AJAX mode due to performance considerations)</span></td>
    <td valign="top">
        <div class="col-xs-3">
            <select name="iSearch[UseAJAX]" class="UseAJAX form-control">
                <option value="yes" <?php echo ($data['iSearch']['UseAJAX'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
                <option value="no" <?php echo ($data['iSearch']['UseAJAX'] == 'no') ? 'selected=selected' : ''?>>No</option>
            </select>
        </div>    
        
        <script>
        $('select.UseAJAX').change(function() {
            if($(this).val() == 'no') {
                $('.onlyUseAJAX').slideUp();
            } else { 
                $('.onlyUseAJAX').slideDown();
            }
        });
        
        var useAJAX = '<?php echo $data['iSearch']['UseAJAX']; ?>';
        if (useAJAX == 'no') {
            $('.onlyUseAJAX').hide();
        }
        
        </script>
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Use Strict Search<span class="help">Strict Search searches for your query strictly the whole phrase as-it-is (example: &quot;blue jeans&quot; will match all products that have the full phrase &quot;blue jeans&quot;). If set to &quot;No&quot;, it will search the whole phrase as well as the separate words (example: &quot;blue jeans&quot; will match all products that have &quot;blue&quot; and/or &quot;jeans&quot;).</span></td>
    <td valign="top">
        <div class="col-xs-3">
            <select name="iSearch[UseStrictSearch]" class="UseStrictSearch form-control">
                <option value="yes" <?php echo ($data['iSearch']['UseStrictSearch'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
                <option value="no" <?php echo ($data['iSearch']['UseStrictSearch'] == 'no') ? 'selected=selected' : ''?>>No</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Search Engine on hitting 'Enter' <span class="help">Choose which search engine you prefer to be used. If you choose the default OpenCart search engine, the module will do the instant search, and on submit will produce your original OpenCart search results.</span></td>
    <td valign="top">
       <div class="row" style="margin-left:0;">
        <div class="col-xs-3">
            <select name="iSearch[AfterHittingEnter]" class="AfterHittingEnter form-control">
                <option value="default" <?php echo ($data['iSearch']['AfterHittingEnter'] == 'default') ? 'selected=selected' : ''?>>Default OpenCart engine</option>
                <option value="isearchengine1551" <?php echo ($data['iSearch']['AfterHittingEnter'] == 'isearchengine1551') ? 'selected=selected' : ''?>>iSearch engine for OpenCart 1.5.5.1+</option>
                <option value="isearchengine1541" <?php echo ($data['iSearch']['AfterHittingEnter'] == 'isearchengine1541') ? 'selected=selected' : ''?>>iSearch engine for OpenCart 1.5.0 - 1.5.4.1</option>
            </select>
        </div>
      </div>
        <div class="row" style="margin-left:0;">
      <div class="col-xs-12">
      <div class="help" style="display:block;">
        <br/><p>Disclaimer:<br/><br/>
        In case your theme is heavily modified and you have chosen iSearch engine for OpenCart, there may be conflicts between the files of iSearch and the theme files.</p>
        </div>
        </div>
 </div>
        
   </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Load Images on Instant Search<span class="help"></span></td>
    <td>
        <div class="col-xs-3">
            <select name="iSearch[LoadImagesOnInstantSearch]" class="iSearchLoadImages form-control">
                <option value="yes" <?php echo ($data['iSearch']['LoadImagesOnInstantSearch'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
                <option value="no" <?php echo ($data['iSearch']['LoadImagesOnInstantSearch'] == 'no') ? 'selected=selected' : ''?>>No</option>
            </select>
        </div>    
   </td>
  </tr>
</table>
<script>
$('.iSearchLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.iSearchItemStatusField', $(this).parent()).val(1);
    } else {
        $('.iSearchItemStatusField', $(this).parent()).val(0);
    }
});

$('.iSearchEnabled').change(function() {
    toggleiSearchActive(true);
});

var toggleiSearchActive = function(animated) {
    if ($('.iSearchEnabled').val() == 'yes') {
        if (animated) 
            $('.iSearchActiveTR').fadeIn();
        else 
            $('.iSearchActiveTR').show();
    } else {
        if (animated) 
            $('.iSearchActiveTR').fadeOut();
        else 
            $('.iSearchActiveTR').hide();
    }
}

toggleiSearchActive(false);
</script>
