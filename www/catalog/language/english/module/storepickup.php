<?php
// Text
$_['heading_title'] 	= 'Our stores';
$_['footer_link'] 		= 'Our stores';
$_['heading_error'] 	= 'Not found!';
$_['text_error'] 		= 'There is no data for a store with this ID!';