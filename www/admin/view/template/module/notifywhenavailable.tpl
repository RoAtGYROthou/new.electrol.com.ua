<?php echo $header; ?>
<div id="content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php echo (empty($data['NotifyWhenAvailable']['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNpc2Vuc2Utc3VwcG9ydF0nKS50cmlnZ2VyKCdjbGljaycpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-error"><i class="icon-exclamation-sign"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
    <?php if (!empty($this->session->data['success'])) { ?>
    <div class="alert alert-success autoSlideUp"><i class="icon-ok-sign"></i> <?php echo $this->session->data['success']; ?></div>
    <script> $('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $this->session->data['success'] = null; } ?>
  <div class="box">
    <div class="box-heading">
      <h1><i class="icon-tag"></i> <?php echo $heading_title; ?></h1>
    </div>
    <div id="mainSettings" class="content fadeInOnLoad">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div class="tabbable">
		  <div class="tab-navigation">        
          <ul class="nav nav-tabs mainMenuTabs">
            <li class="active"><a href="#orders" data-toggle="tab"><i class="icon-list"></i> Waiting List</a></li>
            <li><a href="#archive" data-toggle="tab"><i class="icon-hdd"></i> Archive</a></li>
            <li><a href="#statistics" data-toggle="tab"><i class="icon-signal"></i> Statistics</a></li>
            <li><a href="#controlpanel" data-toggle="tab"><i class="icon-off"></i> Control Panel</a></li>
            <li><a href="#settings" data-toggle="tab"><i class="icon-wrench"></i> Settings</a></li>
            <li><a href="#isense-support" data-toggle="tab"><i class="icon-share"></i> Support</a></li>        
          </ul>
          <div class="tab-buttons">
            <button type="submit" class="btn btn-primary save-changes"><i class="icon-ok"></i> Save changes</button> 	          
          </div>
          </div>
         <div class="tab-content">
			<div id="orders" class="tab-pane active">
              <?php require_once(DIR_APPLICATION.'view/template/module/notifywhenavailable/tab_viewcustomers.php'); ?>                        
            </div>
			<div id="settings" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/notifywhenavailable/tab_settings.php'); ?>                        
            </div>
            <div id="statistics" class="tab-pane">
              <div style="overflow:hidden;">
              <?php if (sizeof($products)>0) { ?>
              <script type="text/javascript" src="https://www.google.com/jsapi"></script>
				<script type="text/javascript">
                  google.load("visualization", "1", {packages:["corechart"]});
                  function drawChart() {
                    var data = google.visualization.arrayToDataTable([
						['Product', 'Waiting List', 'Archive', { role: 'annotation' } ],
						<?php 
					  foreach($products as $pid => $notified) {
					   $productInfo = $this->model_catalog_product->getProduct($pid);
						echo "['".htmlspecialchars($productInfo['name'], ENT_QUOTES)."', ".(isset($notified[0]) ? $notified[0] : '0').", ".(isset($notified[1]) ? $notified[1] : '0').", ''],";
						}
						?>
					  ]);
                    var options = {
                      title: 'Products Performance',
					  isStacked: true,
					  legend: { position: 'top', maxLines: 3 },
					  height: 400
                    };
            
                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                  }
                </script>
                
                <?php } else { echo "<center>There is no data yet for a chart.</center>"; }?>
             	<div id='chart_div'></div>
             </div>                      
            </div>
            <div id="controlpanel" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/notifywhenavailable/tab_controlpanel.php'); ?>                        
            </div>
            <div id="archive" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/notifywhenavailable/tab_archive.php'); ?>                        
            </div>           
			<div id="isense-support" class="tab-pane">
              <?php require_once(DIR_APPLICATION.'view/template/module/notifywhenavailable/tab_support.php'); ?>                        
            </div>
          </div><!-- /.tab-content -->
        </div><!-- /.tabbable -->
      </form>
    </div>
  </div>
</div>
<script>
if (window.localStorage && window.localStorage['currentTab']) {
	$('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
}
if (window.localStorage && window.localStorage['currentSubTab']) {
	$('a[href='+window.localStorage['currentSubTab']+']').trigger('click');  
}
$('.fadeInOnLoad').css('visibility','visible');
$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
	if (window.localStorage) {
		window.localStorage['currentTab'] = $(this).attr('href');
	}
});
$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
	if (window.localStorage) {
		window.localStorage['currentSubTab'] = $(this).attr('href');
	}
});
if (typeof drawChart == 'function') { 
    google.setOnLoadCallback(drawChart);
}
$('a[href=#statistics]').on('click', function() {
	if (typeof drawChart == 'function') { 
		setTimeout(function() { drawChart(); }, 250);
	}
});
</script>
<?php echo $footer; ?>