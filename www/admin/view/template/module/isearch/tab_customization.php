<table class="form">
  <tr>
    <td><span class="required">*</span> <?php echo $entry_highlightcolor; ?></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="text" name="iSearch[HighlightColor]" value="<?php echo (empty($data['iSearch']['HighlightColor'])) ? '#F7FF8C' : $data['iSearch']['HighlightColor']?>" />
        </div>
    </td>
  </tr>
  <tr>
    <td><span class="required">*</span> Limit Results to <span class="help">Denotes on which result to cut off the instant box when iSearch-ing</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="text" name="iSearch[ResultsLimit]" value="<?php echo (empty($data['iSearch']['ResultsLimit'])) ? '5' : $data['iSearch']['ResultsLimit']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Results Box Width (px)<span class="help">Width of the box in pixels. Default is &quot;278px&quot;.</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="text" name="iSearch[ResultsBoxWidth]" value="<?php echo (empty($data['iSearch']['ResultsBoxWidth'])) ? '278px' : $data['iSearch']['ResultsBoxWidth']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Results Box Height (px)<span class="help">Height of the box in pixels. Leave empty for &quot;auto&quot;.</span></td>
    <td>
    <div class="col-xs-3">
        <input class="form-control" type="text" name="iSearch[ResultsBoxHeight]" value="<?php echo (empty($data['iSearch']['ResultsBoxHeight'])) ? '' : $data['iSearch']['ResultsBoxHeight']?>" /></td>
  </tr>
  <tr>
    <td>Instant Results Image Width (px)<span class="help">Width of the instant result images in pixels. Default is 80.</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="number" name="iSearch[InstantResultsImageWidth]" value="<?php echo (empty($data['iSearch']['InstantResultsImageWidth'])) ? '80' : $data['iSearch']['InstantResultsImageWidth']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Instant Results Image Height (px)<span class="help">Height of the instant result images in pixels. Default is 80.</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="number" name="iSearch[InstantResultsImageHeight]" value="<?php echo (empty($data['iSearch']['InstantResultsImageHeight'])) ? '80' : $data['iSearch']['InstantResultsImageHeight']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Results Title Width<span class="help">Width of the title, typically in %.</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="text" name="iSearch[ResultsBoxTitleWidth]" value="<?php echo (empty($data['iSearch']['ResultsBoxTitleWidth'])) ? '42%' : $data['iSearch']['ResultsBoxTitleWidth']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Result Title Font Size (px)<span class="help">Leave empty for your site default font size.</span></td>
    <td>
        <div class="col-xs-3">
            <input class="form-control" type="text" name="iSearch[ResultsTitleFontSize]" value="<?php echo (empty($data['iSearch']['ResultsTitleFontSize'])) ? '' : $data['iSearch']['ResultsTitleFontSize']?>" /></td>
        </div>
  </tr>
  <tr>
    <td>Result Title Font Weight<span class="help">Choose one</span></td>
    <td>
        <div class="col-xs-3">
            <select class="form-control" name="iSearch[ResultsTitleFontWeight]" class="ResultsTitleFontWeight">
                <option value="bold" <?php echo ($data['iSearch']['ResultsTitleFontWeight'] == 'bold') ? 'selected=selected' : ''?>>Bold</option>
                <option value="normal" <?php echo ($data['iSearch']['ResultsTitleFontWeight'] == 'normal') ? 'selected=selected' : ''?>>Normal</option>
            </select>
        </div>
    </td>
  </tr>
  <tr>
    <td>Show Images<span class="help">Show product images in the results box</span></td>
    <td>
        <div class="col-xs-3">
            <select class="form-control" name="iSearch[ResultsShowImages]" class="ResultsShowImages">
                <option value="yes" <?php echo ($data['iSearch']['ResultsShowImages'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
                <option value="no" <?php echo ($data['iSearch']['ResultsShowImages'] == 'no') ? 'selected=selected' : ''?>>No</option>
            </select>
        </div>
    </td>
  </tr>
  <tr>
    <td>Show Models<span class="help">Show product models in the results box</span></td>
    <td>
        <div class="col-xs-3">
            <select class="form-control" name="iSearch[ResultsShowModels]" class="ResultsShowModels">
                <option value="no" <?php echo ($data['iSearch']['ResultsShowModels'] == 'no') ? 'selected=selected' : ''?>>No</option>
                <option value="yes" <?php echo ($data['iSearch']['ResultsShowModels'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
            </select>
        </div>
    </td>
  </tr>
  <tr>
    <td>Show Prices<span class="help">Show product prices in the results box</span></td>
    <td>
        <div class="col-xs-3">
            <select class="form-control" name="iSearch[ResultsShowPrices]" class="ResultsShowPrices">
                <option value="no" <?php echo ($data['iSearch']['ResultsShowPrices'] == 'no') ? 'selected=selected' : ''?>>No</option>
                <option value="yes" <?php echo ($data['iSearch']['ResultsShowPrices'] == 'yes') ? 'selected=selected' : ''?>>Yes</option>
            </select>
        </div>
    </td>
  </tr>
  <tr>
    <td>More Results Title<span class="help">This is the label that shows up if more results than the results limit are found</span></td>
    <td>
        <div class="col-xs-3">
            <?php foreach ($languages as $language) : ?>
            <div class="form-group">
                <div class="input-group">
                   <div class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></div>
                   <input class="form-control" type="text" name="iSearch[<?php echo $language['language_id']; ?>][ResultsMoreResultsLabel]" value="<?php echo (empty($data['iSearch'][$language['language_id']]['ResultsMoreResultsLabel'])) ? 'View All Results' : $data['iSearch'][$language['language_id']]['ResultsMoreResultsLabel']; ?>" /><br />
                </div>
            </div>   
            <?php endforeach; ?>
        </div>
    </td>
  </tr>
  <tr>
    <td>Not Found Text<span class="help">The text that appears when there are no search results.</span></td>
    <td>
        <div class="col-xs-3">
            <?php foreach ($languages as $language) : ?>
            <div class="form-group">
                <div class="input-group">
                   <div class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></div>
                   <input class="form-control" type="text" name="iSearch[<?php echo $language['language_id']; ?>][ResultsNoResultsLabel]" value="<?php echo (empty($data['iSearch'][$language['language_id']]['ResultsNoResultsLabel'])) ? 'No Results Found' : $data['iSearch'][$language['language_id']]['ResultsNoResultsLabel']; ?>" /><br />
                </div>
            </div>   
            <?php endforeach; ?>
        </div>
    </td>
  </tr>
  <tr>
    <td valign="top">Custom CSS<span class="help">Put your custom CSS here</span></td>
    <td>
        <div class="col-xs-4">
            <textarea class="form-control" name="iSearch[CustomCSS]" style="width:320px;height:200px;"><?php echo (empty($data['iSearch']['CustomCSS'])) ? '' : $data['iSearch']['CustomCSS']?></textarea>
        </div>                    
    </td>
  </tr>
</table>
