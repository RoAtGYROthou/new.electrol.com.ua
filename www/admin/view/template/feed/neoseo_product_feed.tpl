<?php echo $header; ?>
<?php
require_once(DIR_SYSTEM . '/engine/neoseo_view.php' );
$widgets = new NeoSeoWidgets('neoseo_product_feed_',$params);
$widgets->text_select_all = $text_select_all;
$widgets->text_unselect_all = $text_unselect_all;
?>

    <div id="content">
        <style>
            .scrollbox {
                width: 550px;
            }
        </style>
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a
                href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if (isset($success) && $success) { ?>
            <div class="success" style="display: none;"><?php echo $success; ?></div>
            <script>jQuery(".success").slideDown().delay(1000).slideUp();</script>
        <?php } ?>

        <div class="box">
            <div class="heading">
                <img src="view/image/neoseo.png" width="36" height="36" style="float:left" alt=""/>
                <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>
                <div class="buttons">
                    <?php if( !isset($license_error) ) { ?>
                        <a onclick="$('#form').attr('action', '<?php echo $save; ?>'); $('#form').submit();"
                           class="button"><span><?php echo $button_save; ?></span></a>
                        <a onclick="$('#form').attr('action', '<?php echo $save_and_close; ?>'); $('#form').submit();"
                           class="button"><span><?php echo $button_save_and_close; ?></span></a>
                    <?php } else { ?>
                        <a onclick="location = '<?php echo $recheck; ?>';"
                           class="button"><span><?php echo $button_recheck; ?></span></a>
                    <?php } ?>
                    <a onclick="location = '<?php echo $close; ?>';"
                       class="button"><span><?php echo $button_close; ?></span></a>
                </div>
            </div>

            <div class="content">
                <form action="" method="post" enctype="multipart/form-data" id="form">
                    <div id="tabs" class="htabs">
                        <a class="tabs" href="#tab-general"><?php echo $tab_general; ?></a>
                        <?php if( !isset($license_error) ) { ?>
                            <a class="tabs" href="#tab-feeds"><?php echo $tab_feeds; ?></a>
                            <a class="tabs" href="#tab-formats"><?php echo $tab_formats; ?></a>
                            <a class="tabs" href="#tab-fields"><?php echo $tab_fields; ?></a>
                            <a class="tabs" href="#tab-logs"><?php echo $tab_logs; ?></a>
                        <?php } ?>
                        <a class="tabs" href="#tab-support"><?php echo $tab_support; ?></a>
                        <a class="tabs" href="#tab-license"><?php echo $tab_license; ?></a>
                        <a class="tabs" href="#tab-usefull"><?php echo $tab_usefull; ?></a>
                    </div>
                    <div id="tab-general">
                        <?php if( !isset($license_error) ) { ?>
                            <table class="form">
                                <?php $widgets->dropdown('status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                                <?php $widgets->dropdown('type',array( 0 => $text_link, 1 => $text_cron)); ?>
                                <tr id='field_cron'>
                                    <td><?php echo $entry_cron; ?></td>
                                    <td>
                                        <span id="cron_link" style="margin: 0 0 0 20px;"><?php echo $cron; ?></span>
                                    </td>
                                </tr>
                            </table>

                        <?php } else { ?>
                            <div><?php echo $license_error; ?></div>
                        <?php } ?>
                    </div>


                    <?php if( !isset($license_error) ) { ?>
                        <div id="tab-feeds">

                            <div style="text-align:right;">
                                <a href="<?php echo $add; ?>" data-toggle="tooltip"
                                   title="<?php echo $button_add; ?>" class="button">
                                    <span><?php echo $text_new_feed; ?></span>
                                </a>
                            </div>
                            <br>
                            <table class="list display" id="items-table" width="30%">
                                <thead>
                                <tr>
                                    <td class="center"><?php echo $column_feed_name; ?></td>
                                    <td class="center"><?php echo $column_format_name; ?></td>
                                    <td class="center"><?php echo $column_feed_status; ?></td>
                                    <td class="center"><?php echo $column_feed_demand; ?></td>
                                    <td class="center"><?php echo $column_generate; ?></td>
                                    <td class="center"><?php echo $column_action; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($feeds)) { ?>
                                    <?php foreach ($feeds as $feed) { ?>
                                        <tr>
                                            <td class="center"><?php echo $feed['feed_name']; ?></td>
                                            <td class="center"><?php echo (isset($array_formats[$feed['id_format']])) ? $array_formats[$feed['id_format']] : ''; ?></td>
                                            <td class="center"><?php echo ($feed['status'] == 1) ? 'Включено' : 'Отключено'; ?></td>
                                            <td class="center">
                                                <a target="_blank" class="by_demand"
                                                   href="<?php echo $feed['feed_demand']; ?>"><?php echo $feed['feed_demand']; ?></a>
                                                <div class="by_cron">
                                                    <?php if( isset($feed['feed_cron_date']) ) { ?>
                                                        <p>1. <a target="_blank" href="<?php echo $feed['feed_cron']; ?>"><?php echo $feed['feed_cron']; ?></a>
                                                        </p>
                                                        <p>2. <a target="_blank" href="<?php echo $feed['feed_cron_link']; ?>"><?php echo $feed['feed_cron_link']; ?></a>
                                                        </p>
                                                        <p style="color:green">Последнее изменение: <?php echo $feed['feed_cron_date']; ?></p>
                                                    <?php } else { ?>
                                                        <p>1. <?php echo $feed['feed_cron']; ?></p>
                                                        <p>2. <?php echo $feed['feed_cron_link']; ?></p>
                                                        <p style="color:red">файл еще не создан</p>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td class="center">
                                                <a onclick="generate('#progress-<?php echo $feed['product_feed_id']; ?>', <?php echo $feed['image_width']; ?> , <?php echo $feed['image_height']; ?> ); return false;"
                                                   title="<?php echo $button_generate; ?>" class="button"><i
                                                            class="fa fa-cog"></i> <?php echo $button_cache_generate; ?></a>
                                                <div id="progress-<?php echo $feed['product_feed_id']; ?>" class="progress" style="margin-top:20px; display:none">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped"
                                                         role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                                         aria-valuemax="100" style="width:0%">0%
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="center">
                                                <a href="<?php echo $feed['edit']; ?>" data-toggle="tooltip"
                                                   title="<?php echo $button_edit; ?>" class="btn btn-primary">[<?php echo $text_action_edit?>]</a>
                                                <br>
                                                <a onclick="deleteItem('<?php echo $feed['delete']; ?>')"
                                                   data-toggle="tooltip" title="<?php echo $button_delete; ?>"
                                                   class="btn btn-danger">[<?php echo $text_action_delete?>]</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="pagination"><?php echo $pagination; ?></div>
                        </div>
                    <?php } ?>


                    <?php if( !isset($license_error) ) { ?>
                        <div id="tab-formats">

                            <div style="text-align:right;">
                                <a href="<?php echo $add_formats; ?>" data-toggle="tooltip"
                                   title="<?php echo $button_add; ?>" class="button">
                                    <span><?php echo $text_new_format; ?></span>
                                </a>
                                <a href="<?php echo $default_formats; ?>" id="default_formats" data-toggle="tooltip"
                                   title="<?php echo $text_default_format; ?>" class="button">
                                    <?php echo $button_default_format; ?>
                                </a>
                            </div>
                            <br>
                            <table class="list display" id="items-table" width="30%">
                                <thead>
                                <tr>
                                    <td class="center"><?php echo $column_format_name; ?></td>
                                    <td class="center"><?php echo $column_xml_code; ?></td>
                                    <td class="center"><?php echo $column_action; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($formats) { ?>
                                    <?php foreach ($formats as $format) { ?>
                                        <tr>
                                            <td class="center"><?php echo $format['feed_format_name']; ?></td>
                                            <td  class="center">
                                                <textarea rows="10" cols="100" disabled><?php echo $format['format_xml']; ?></textarea>

                                            <td class="center">
                                                <a href="<?php echo $format['edit']; ?>" data-toggle="tooltip"
                                                   title="<?php echo $button_edit; ?>" class="btn btn-primary">[<?php echo $text_action_edit?>]</a>
                                                <br>
                                                <a onclick="deleteItem('<?php echo $format['delete']; ?>')"
                                                   data-toggle="tooltip" title="<?php echo $button_delete; ?>"
                                                   class="btn btn-danger">[<?php echo $text_action_delete?>]</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="pagination"><?php echo $pagination_formats; ?></div>
                        </div>
                    <?php } ?>
                    <?php if( !isset($license_error) ) { ?>
                        <div id="tab-fields">
                            <table class="list display" id="items-table" width="30%">
                                <thead>
                                <tr>
                                    <td width="200px" class="left"><?php echo $entry_field_list_name; ?></td>
                                    <td><?php echo $entry_field_list_desc; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach( $fields as $field_name => $field_desc ) { ?>
                                    <tr>
                                        <td class="left">{<?php echo $field_name ?>}</td>
                                        <td><?php echo $field_desc ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    <?php } ?>

                    <?php if( !isset($license_error) ) { ?>
                        <div id="tab-logs">
                            <table class="form">
                                <?php if( !isset($license_error) ) { ?>
                                <tr>
                                    <td><?php echo $entry_debug; ?></td>
                                    <td>
                                        <select name="neoseo_product_feed_debug">
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                            <option <?php if ($neoseo_product_feed_debug == 1) { ?> selected="selected" <?php } ?> value="1"><?php echo $text_enabled; ?></option>
                                        </select>
                                        <span id="cron_link_debug" style="margin: 0 0 0 20px;"><?php echo $cron_debug; ?></span>
                                    </td>
                                    <?php } ?>
                                    <td><div class="buttons" style="float: right"><a onclick="$('#form').attr('action', '<?php echo $clear; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button delete" ><span><?php echo $button_clear_log; ?></span></a></div></td>
                                </tr>
                            </table>
                            <textarea style="width: 98%; height: 300px; padding: 5px; border: 1px solid #CCCCCC; background: #FFFFFF; overflow: scroll;"><?php echo $logs; ?></textarea>
                        </div>
                    <?php } ?>
                    <div id="tab-support">
                        <?php echo $mail_support; ?>
                    </div>
                    <div id="tab-license">
                        <?php echo $module_licence; ?>
                    </div>
                    <div id="tab-usefull">
                        <?php $widgets->usefullLinks(); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--

        $('#tabs a.tabs').tabs();
        $('#tab-cells a').tabs();
        $('#tab-format a').tabs();
        $("#neoseo_product_feed_type").change(function(){

            if (Number($(this).val()) == 1) {
                $("#field_cron").show();
                $(".by_demand").hide();
                $(".by_cron").show();
            } else {
                $("#field_cron").hide();
                $(".by_demand").show();
                $(".by_cron").hide();
            }
        });
        $("#neoseo_product_feed_type").trigger("change");
        $("[name=neoseo_product_feed_debug]").change(function(){

            if (Number($("[name=neoseo_product_feed_debug]").val()) === 0 || Number($("#neoseo_product_feed_type").val()) == 0)
                $("#cron_link_debug").hide();
            else
                $("#cron_link_debug").show();
        });
        $("[name=neoseo_product_feed_debug]").trigger("change");
        //--></script>
    <script type="text/javascript"><!--
        function deleteItem(href) {
            if(confirm('Удалить?')){
                location = href;
            }else{
                return false;
            }
        }

        <?php
        if (!isset($license_error)) { ?>
        var products = <?php echo json_encode($ids); ?>;
        var productsCurrent = [];
        var generateProgressId = 0;
        var generateWidth = 600;
        var generateHeight = 600;
        function generateNext() {
            var product = productsCurrent.shift();
            if (!product) {
                $(generateProgressId).hide();
                return;
            }
            var index = products.length - productsCurrent.length;
            var total = products.length;
            var percent = Number(index * 100 / total).toFixed(0);
            $(generateProgressId + " .progress-bar").prop("aria-valuenow", percent);
            $(generateProgressId + " .progress-bar").css("width", percent + "%");
            $(generateProgressId + " .progress-bar").html(index + " из " + total);
            $.ajax({
                url: '<?php echo str_replace("&amp;", "&", $generate_url); ?>',
                data: {id: product, width: generateWidth, height: generateHeight},
                dataType: 'json'
            }).done(function () {
                generateNext();
            });
        }

        function generate(progressId, width, height) {
            productsCurrent = products.slice(0);
            generateProgressId = progressId;
            generateWidth = width;
            generateHeight = height;
            $(generateProgressId).show();
            generateNext();
        }
        <?php } ?>
        //--></script>
    <script type="text/javascript">
        $('#default_formats').click(function (e) {
            e.preventDefault();
            if(confirm("<?php echo $text_default_format_confirm ?>")){
                window.location = $(this).prop('href');
            }
        });
    </script>
<?php echo $footer; ?>