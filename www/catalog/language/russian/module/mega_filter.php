<?php
$_['heading_title'] = 'Mega Filter PRO';
$_['name_price'] = 'Цена';
$_['name_manufacturers'] = 'Производители';
$_['name_rating'] = 'Рейтинг';
$_['name_search'] = 'Поиск';
$_['name_stock_status'] = 'Статус наличия';
$_['text_button_apply'] = 'Применить';
$_['text_reset_all'] = 'Сброс';
$_['text_show_more'] = 'Показать ещё (%s)';
$_['text_show_less'] = 'Показать меньше';
$_['text_display'] = 'Показать';
$_['text_grid'] = 'Сетка';
$_['text_list'] = 'Список';
$_['text_loading'] = 'Загрузка...';
$_['text_select'] = 'Выберите...';
$_['text_go_to_top'] = 'Перейти к началу';
$_['text_init_filter'] = 'Поиск с фильтрацией';
$_['text_initializing'] = 'Инициализация...';
?>