<div>
    <label for="YouTubeShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][YouTube][Show]" id="YouTubeShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['YouTube']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['YouTube']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionYouTube_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionYouTube_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][youtube]" value="<?php echo $data[$storeCode][$langCode]['Positions']['youtube']; ?>" />
</div>
<div>
    <label for="YouTubeTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="YouTubeTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][YouTube][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['YouTube']['Title']; ?>" />
</div>
<div>
    <label for="YouTubeURL_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $youtube_url; ?></label>
    <input type="text" id="YouTubeURL_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][YouTube][URL]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['YouTube']['URL']; ?>" />
</div>
<div>
    <label for="YouTubeWidth_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $youtube_width; ?></label>
    <input type="text" id="YouTubeWidth_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][YouTube][Width]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['YouTube']['Width']; ?>" />
</div>
<div>
    <label for="YouTubeHeight_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $youtube_height; ?></label>
    <input type="text" id="YouTubeHeight_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][YouTube][Height]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['YouTube']['Height']; ?>" />
</div>