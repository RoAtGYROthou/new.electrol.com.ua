<?php
// Language strings
$_['heading_title']               = 'StorePickup';
$_['error_permission']            = 'Warning: You do not have permission to modify module StorePickup!';
$_['text_success']                = 'Success: You have modified module StorePickup!';
$_['text_enabled']                = 'Enabled';
$_['text_disabled']               = 'Disabled';
$_['button_cancel']				  = 'Cancel';
$_['save_changes']				  = 'Save changes';
$_['text_default']				  = 'Default';
$_['text_module']				  = 'Module';

$_['text_module_status'] = 'Module status: <span class="help"><i class="fa fa-info-circle"></i> Enable or disable the module.</br></span>';     
$_['text_store_listing'] = 'Store listing: <span class="help"><i class="fa fa-info-circle"></i> This is the url for the store listing.</br></span>';     
$_['text_store_status'] = 'Store {store_id} Status:  <span class="help"><i class="fa fa-info-circle"></i> Enable or disable the selected store.</br></span>';     
$_['text_store_name'] = 'Store Name:  <span class="help"><i class="fa fa-info-circle"></i> Enter the store name here:</br></span>';   
$_['text_store_address'] = 'Store Address:  <span class="help"><i class="fa fa-info-circle"></i> Enter the store address here:</br></span>';   
$_['text_store_phone'] = 'Store Phone:  <span class="help"><i class="fa fa-info-circle"></i> Enter the phone of the store:</br></span>';   
$_['text_long_lat'] = 'Longtitude/Latidude:  <span class="help"><i class="fa fa-info-circle"></i> Enter the store\'s longtitude & latitute coordinates.</br></span>';
$_['text_map'] = 'Map preview:  <span class="help"><i class="fa fa-info-circle"></i> Here you can see how the shop will appear on the map.</br></span>';   
  
$_['text_multilingual'] = 'Multi-lingual settings:';  
$_['text_location_information'] = 'Store location information:';     
    
?>