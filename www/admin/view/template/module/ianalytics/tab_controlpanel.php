<br />
<table class="form">
  <tr>
    <td><h5><span class="required">*</span> <?php echo $entry_code; ?></h5><span class="help">Enable or disable iAnalytics</span></td>
    <td>
    	<div class="col-xs-2">
            <select name="iAnalytics[Enabled]" class="form-control iAnalyticsEnabled">
                <option value="yes" <?php echo ($data['iAnalytics']['Enabled'] == 'yes') ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo ($data['iAnalytics']['Enabled'] == 'no') ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
  <tr class="iAnalyticsActiveTR">
    <td><h5><span class="required">*</span> <?php echo $entry_layouts_active; ?></h5><span class="help">Choose which pages iAnalytics to be active</span></td>
    <td>
    <?php $i=0;?>
    <?php foreach ($layouts as $layout) { ?>
    <?php 
        foreach ($modules as $module) {
            if(!empty($module)) {
                if ($module['layout_id'] == $layout['layout_id']) {
                    $status = $module['status'];
                    if ((int)$status == 1) {
                        $checked = ' checked=checked';
                    } else { 
                        $checked = '';
                    }
                    
                }
            } 
          
          }
          if (!isset($status)) {
                $status = 1;
                $checked = ' checked=checked';
          }
    ?>
    <div class="iAnalyticsLayout">
        <input type="checkbox" value="<?php echo $layout['layout_id']; ?>" id="iAnalyticsActive<?php echo $i?>" <?php echo $checked?> /><label for="iAnalyticsActive<?php echo $i?>"><?php echo $layout['name']; ?></label>
        <input type="hidden" name="ianalytics_module[<?php echo $i?>][position]" value="content_bottom" />
        <input type="hidden" class="iAnalyticsItemLayoutIDField" name="ianalytics_module[<?php echo $i?>][layout_id]" value="<?php echo $layout['layout_id']; ?>" />
        <input type="hidden" class="iAnalyticsItemStatusField" name="ianalytics_module[<?php echo $i?>][status]" value="<?php echo $status ?>" />
        <input type="hidden" name="ianalytics_module[<?php echo $i?>][sort_order]" value="<?php echo $i+10?>" />
    </div>
    <?php $i++;} ?>
     </td>
  </tr>
    <tr>
    <td><h5>Show statistics in the OpenCart dashboard:</h5><span class="help">Enable or disable the statistics in the OpenCart default dashboard</span></td>
    <td>
    	<div class="col-xs-2">
            <select name="iAnalytics[Dashboard]" class="form-control iAnalyticsDashboard">
                <option value="yes" <?php echo ($data['iAnalytics']['Dashboard'] == 'yes') ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo ($data['iAnalytics']['Dashboard'] == 'no') ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
	<tr>
    <td><h5>Collect After-Sale data:</h5><span class="help"></span></td>
    <td>
    	<div class="col-xs-2">
            <select name="iAnalytics[AfterSaleData]" class="form-control iAnalyticsAfterSaleData">
                <option value="yes" <?php echo (isset($data['iAnalytics']['AfterSaleData']) && ($data['iAnalytics']['AfterSaleData'] == 'yes')) ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo (isset($data['iAnalytics']['AfterSaleData']) && ($data['iAnalytics']['AfterSaleData'] == 'no')) ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
  <tr>
    <td><h5>Google Analytics e-commerce tracking:</h5></td>
    <td>
    	<div class="col-xs-2">
            <select name="iAnalytics[GoogleAnalytics]" id="GA_ecommerce" class="form-control iAnalyticsGoogleAnalytics">
                <option value="yes" <?php echo (isset($data['iAnalytics']['GoogleAnalytics']) && ($data['iAnalytics']['GoogleAnalytics'] == 'yes')) ? 'selected=selected' : ''?>>Enabled</option>
                <option value="no" <?php echo (isset($data['iAnalytics']['GoogleAnalytics']) && ($data['iAnalytics']['GoogleAnalytics'] == 'no')) ? 'selected=selected' : ''?>>Disabled</option>
            </select>
        </div>
   </td>
  </tr>
  <tr id="custom_settings">
    <td><h5>Google Account ID Number:</h5><span class="help">UA-XXXXXXX-XX variable from your Google Analytics account</span></td>
    <td><div class="col-xs-2">
             <div class="form-group">
                <input type="text" class="form-control" placeholder="UA-XXXXXXX-XX" name="iAnalytics[GoogleAnalyticsIDNumber]"  id="moduleGA_id" value="<?php echo !empty($data['iAnalytics']['GoogleAnalyticsIDNumber']) ? $data['iAnalytics']['GoogleAnalyticsIDNumber'] : ''?>">
            </div>
		</div>
        <div class="col-xs-10">
             <div class="form-group">
             <script language="javascript" type="text/javascript">
<!--
function popitup(url) {
	newwindow=window.open(url,'name','height=750,width=950,toolbar=no,menubar=no,scrollbars=no,location=no,directories=no');
	if (window.focus) {newwindow.focus()}
	return false;
}

// -->
</script>
               <a class="btn btn-primary" onclick="return popitup('https://www.google.com/analytics/web/#report/conversions-ecommerce-overview/')">Open Google eCommerce Tracking</a>
            </div>
		</div>
   </td>
  </tr>
  <tr>
    <td><h5>Clear analytics data:</h5></td>
    <td>
    	<a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete all analytics data?');" href="index.php?route=module/ianalytics/deleteanalyticsdata&token=<?php echo $this->session->data['token']; ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp; Clear All Analytics Data</a>
    </td>
  </tr>
  <tr>
    <td><h5>Blacklisted IP's</h5><span class="help">These are the IP addresses whose analytics data will not be listed. Keep in mind that data from these IP addresses will still be logged in the database. Type in one IP address per line.</span></td>
    <td>
    	<textarea style="width: 400px; height: 100px;" class="form-control" name="iAnalytics[BlacklistedIPs]"><?php echo !empty($data['iAnalytics']['BlacklistedIPs']) ? $data['iAnalytics']['BlacklistedIPs'] : ''; ?></textarea>
    </td>
  </tr>
</table>
<script>
$('.iAnalyticsLayout input[type=checkbox]').change(function() {
    if ($(this).is(':checked')) { 
        $('.iAnalyticsItemStatusField', $(this).parent()).val(1);
    } else {
        $('.iAnalyticsItemStatusField', $(this).parent()).val(0);
    }
});

$('.iAnalyticsEnabled').change(function() {
    toggleiAnalyticsActive(true);
});

var toggleiAnalyticsActive = function(animated) {
    if ($('.iAnalyticsEnabled').val() == 'yes') {
        if (animated) 
            $('.iAnalyticsActiveTR').fadeIn();
        else 
            $('.iAnalyticsActiveTR').show();
    } else {
        if (animated) 
            $('.iAnalyticsActiveTR').fadeOut();
        else 
            $('.iAnalyticsActiveTR').hide();
    }
}

toggleiAnalyticsActive(false);
</script>
<script>
 $(function() {
    var $typeSelector = $('#GA_ecommerce');
    var $toggleArea = $('#custom_settings');
	 if ($typeSelector.val() === 'yes') {
            $toggleArea.show(); 
        }
        else {
            $toggleArea.hide(); 
        }
    $typeSelector.change(function(){
        if ($typeSelector.val() === 'yes') {
            $toggleArea.show(500); 
        }
        else {
            $toggleArea.hide(400); 
        }
    });
});
 </script>