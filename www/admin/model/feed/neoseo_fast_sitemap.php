<?php

require_once(DIR_SYSTEM . "/engine/neoseo_model.php");

class ModelFeedNeoseoFastSitemap extends NeoseoModel
{

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = 'neoseo_fast_sitemap';
		$this->_logFile = $this->_moduleSysName . '.log';
		$this->debug = $this->config->get($this->_moduleSysName . '_debug');
	}

	// Install/Uninstall
	public function install()
	{
		// Значения параметров по умолчанию
		$this->initParamsDefaults(array(
			'status' => 1,
			'debug' => 1,
			'image_status' => 0,
			'seo_status' => 1,
			'seo_url_include_path' => 0,
			'addresses_status' => 0,
			'gzip_staes_tus' => 0,
			'seo_lang_status' => 0,
			'partition_status' => 1,
			'partition_volume' => 50000,
			'multistore_status' => 0,
			'category_status' => 1,
			'category_brand_status' => 0,
			'filterpro_seo_status' => 0,
			'ocfilter_seo_status' => 0,
			'mfilter_seo_status' => 0,
			'category_url_date' => 1,
			'category_url_frequency' => "weekly",
			'category_url_priority' => "0.7",
			'manufacturer_status' => 1,
			'manufacturer_line_by_tima' => 0,
			'manufacturer_url_date' => 1,
			'manufacturer_url_frequency' => "weekly",
			'manufacturer_url_priority' => "0.7",
			'product_status' => 1,
			'product_url_date' => 1,
			'product_url_frequency' => "weekly",
			'product_url_priority' => "1",
			'information_status' => 1,
			'information_url_date' => 1,
			'information_url_frequency' => "weekly",
			'information_url_priority' => "1",
			'blog_freecart_status' => 0,
			'blog_pavo_status' => 0,
			'blog_seocms_status' => 0,
			'blog_blogmanager_status' => 0,
			'blog_category_status' => 1,
			'blog_category_url_date' => 1,
			'blog_category_url_frequency' => "weekly",
			'blog_category_url_priority' => "0.7",
			'blog_author_status' => 1,
			'blog_author_url_date' => 1,
			'blog_author_url_frequency' => "weekly",
			'blog_author_url_priority' => "0.7",
			'blog_article_status' => 1,
			'blog_article_url_date' => 1,
			'blog_article_url_frequency' => "weekly",
			'blog_article_url_priority' => "0.7",
		));

		// Пермишены
		$this->load->model('user/user_group');
		$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'feed/' . $this->_moduleSysName);
		$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'feed/' . $this->_moduleSysName);
		$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'tool/' . $this->_moduleSysName);
		$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'tool/' . $this->_moduleSysName);

		return TRUE;
	}

	public function upgrade()
	{
		
	}

	public function uninstall()
	{
		
	}

}

?>