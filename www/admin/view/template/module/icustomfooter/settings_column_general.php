<div>
    <label for="SettingsShow_<?php echo $store['store_id']; ?>"><?php echo $showcustomfooter; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][Show]" id="SettingsShow_<?php echo $store['store_id']; ?>">
        <option value="true" <?php echo ($data[$storeCode]['Settings']['Show'] == 'true') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$storeCode]['Settings']['Show'] == 'false') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="SettingsResponsiveDesign_<?php echo $store['store_id']; ?>"><?php echo $responsivedesign; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][ResponsiveDesign]" id="SettingsResponsiveDesign_<?php echo $store['store_id']; ?>">
        <option value="no" <?php echo ($data[$storeCode]['Settings']['ResponsiveDesign'] == 'no') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
        <option value="yes" <?php echo ($data[$storeCode]['Settings']['ResponsiveDesign'] == 'yes') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
    </select>                            
</div>
<div>
    <label for="SettingsFooterWidth_<?php echo $store['store_id']; ?>"><?php echo $footerwidth; ?></label>
    <input type="text" id="SettingsFooterWidth_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][FooterWidth]" value="<?php echo $data[$storeCode]['Settings']['FooterWidth']; ?>" />
</div>
<div>
    <label for="SettingsUseFooterWith_<?php echo $store['store_id']; ?>"><?php echo $footerusefooterwith; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][UseFooterWith]" id="SettingsUseFooterWith_<?php echo $store['store_id']; ?>">
        <option value="defaultocwithicons" <?php echo (!empty($data[$storeCode]['Settings']['UseFooterWith']) && $data[$storeCode]['Settings']['UseFooterWith'] == 'defaultocwithicons') ? 'selected=selected' : ''; ?>><?php echo $footerusewithdefaultocwithicons; ?></option>
        <option value="themefooter" <?php echo (!empty($data[$storeCode]['Settings']['UseFooterWith']) && $data[$storeCode]['Settings']['UseFooterWith'] == 'themefooter') ? 'selected=selected' : ''; ?>><?php echo $footerusewiththemefooter; ?></option>
        <option value="icons" <?php echo (!empty($data[$storeCode]['Settings']['UseFooterWith']) && $data[$storeCode]['Settings']['UseFooterWith'] == 'icons') ? 'selected=selected' : ''; ?>><?php echo $footerusewithicons; ?></option>
    </select>
</div>
<div>
    <label for="SettingsHidePoweredBy_<?php echo $store['store_id']; ?>"><?php echo $hidepoweredby; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][HidePoweredBy]" id="SettingsHidePoweredBy_<?php echo $store['store_id']; ?>">
        <option value="" <?php echo ($data[$storeCode]['Settings']['HidePoweredBy'] == '') ? 'selected=selected' : ''; ?>><?php echo $no; ?></option>
        <option value="display:none;" <?php echo ($data[$storeCode]['Settings']['HidePoweredBy'] == 'display:none;') ? 'selected=selected' : ''; ?>><?php echo $yes; ?></option>
    </select>
</div>
<div>
    <label for="SettingsFontFamily_<?php echo $store['store_id']; ?>"><?php echo $footerfontfamily; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][FontFamily]" id="SettingsFontFamily_<?php echo $store['store_id']; ?>">
        <option value="fontfamilyinherit" <?php echo ($data[$storeCode]['Settings']['FontFamily'] == 'fontfamilyinherit') ? 'selected=selected' : ''; ?>><?php echo $defaultfont; ?></option>
        <option value="fontfamilyarialhelvetica" <?php echo ($data[$storeCode]['Settings']['FontFamily'] == 'fontfamilyarialhelvetica') ? 'selected=selected' : ''; ?>>Arial, Helvetica</option>
        <option value="fontfamilygeorgiatimesnewroman" <?php echo ($data[$storeCode]['Settings']['FontFamily'] == 'fontfamilygeorgiatimesnewroman') ? 'selected=selected' : ''; ?>>Georgia, Times New Roman</option>
        <option value="fontfamilytrebuchetms" <?php echo ($data[$storeCode]['Settings']['FontFamily'] == 'fontfamilytrebuchetms') ? 'selected=selected' : ''; ?>>Trebuchet MS</option>
    </select>
</div>
<div>
    <label for="SettingsBackgroundPattern_<?php echo $store['store_id']; ?>"><?php echo $footerbackgroundstyle; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][BackgroundPattern]" id="SettingsBackgroundPattern_<?php echo $store['store_id']; ?>">
        <option value="whitebgpattern" <?php echo ($data[$storeCode]['Settings']['BackgroundPattern'] == 'whitebgpattern') ? 'selected=selected' : ''; ?>><?php echo $white; ?></option>
        <option value="darkbgpattern" <?php echo ($data[$storeCode]['Settings']['BackgroundPattern'] == 'darkbgpattern') ? 'selected=selected' : ''; ?>><?php echo $dark; ?></option>
        <option value="usebackgroundcolor" <?php echo ($data[$storeCode]['Settings']['BackgroundPattern'] == 'usebackgroundcolor') ? 'selected=selected' : ''; ?>><?php echo $usebackgroundcolor; ?></option>
    </select>
</div>
<div>
	<label for="SettingsBackgroundColor_<?php echo $store['store_id']; ?>"><?php echo $footerbackgroundcolor; ?></label>
    <input type="text" id="SettingsBackgroundColor_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][BackgroundColor]" value="<?php echo !empty($data[$storeCode]['Settings']['BackgroundColor']) ? $data[$storeCode]['Settings']['BackgroundColor'] : ''; ?>" />
</div>
<div>
    <label for="SettingsColumnColor_<?php echo $store['store_id']; ?>"><?php echo $columntitlecolor; ?></label>
    <input type="text" id="SettingsColumnColor_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][ColumnColor]" value="<?php echo $data[$storeCode]['Settings']['ColumnColor']; ?>" />
</div>
<div>
    <label for="SettingsTextColor_<?php echo $store['store_id']; ?>"><?php echo $footertextcolor; ?></label>
    <input type="text" id="SettingsTextColor_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][TextColor]" value="<?php echo !empty($data[$storeCode]['Settings']['TextColor']) ? $data[$storeCode]['Settings']['TextColor'] : ''; ?>" />
</div>
<div>
    <label for="SettingsColumnContentOverflow_<?php echo $store['store_id']; ?>"><?php echo $whencontentoverflows; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][ColumnContentOverflow]" id="SettingsColumnContentOverflow_<?php echo $store['store_id']; ?>">
        <option value="overflowhidden" <?php echo ($data[$storeCode]['Settings']['ColumnContentOverflow'] == 'overflowhidden') ? 'selected=selected' : ''; ?>><?php echo $hidewhatunfits; ?></option>
        <option value="overflowauto" <?php echo ($data[$storeCode]['Settings']['ColumnContentOverflow'] == 'overflowauto') ? 'selected=selected' : ''; ?>><?php echo $showscrollers; ?></option>
    </select>
</div>
<div>
    <label for="SettingsColumnHeight_<?php echo $store['store_id']; ?>"><?php echo $columnheight; ?></label>
    <input type="text" id="SettingsColumnHeight_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][ColumnHeight]" value="<?php echo $data[$storeCode]['Settings']['ColumnHeight']; ?>" />
</div>
<div>
    <label for="SettingsColumnWidth_<?php echo $store['store_id']; ?>"><?php echo $columnwidth; ?></label>
    <input type="text" id="SettingsColumnWidth_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][ColumnWidth]" value="<?php echo $data[$storeCode]['Settings']['ColumnWidth']; ?>" />
</div>
<div>
    <label for="SettingsColumnLineStyle_<?php echo $store['store_id']; ?>"><?php echo $columnlinestyle; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][Settings][ColumnLineStyle]" id="SettingsColumnLineStyle_<?php echo $store['store_id']; ?>">
        <option value="dotted" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'dotted') ? 'selected=selected' : ''; ?>><?php echo $dotted; ?></option>
        <option value="dashed" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'dashed') ? 'selected=selected' : ''; ?>><?php echo $dashed; ?></option>
        <option value="solid" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'solid') ? 'selected=selected' : ''; ?>><?php echo $solid; ?></option>
        <option value="double" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'double') ? 'selected=selected' : ''; ?>><?php echo $double; ?></option>
        <option value="groove" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'groove') ? 'selected=selected' : ''; ?>><?php echo $groove; ?></option>
        <option value="ridge" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'ridge') ? 'selected=selected' : ''; ?>><?php echo $ridge; ?></option>
        <option value="inset" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'inset') ? 'selected=selected' : ''; ?>><?php echo $inset; ?></option>
        <option value="outset" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'outset') ? 'selected=selected' : ''; ?>><?php echo $outset; ?></option>
        <option value="none" <?php echo ($data[$storeCode]['Settings']['ColumnLineStyle'] == 'none') ? 'selected=selected' : ''; ?>><?php echo $noline; ?></option>
    </select>
</div>
<div>
    <label for="SettingsCustomCSS_<?php echo $store['store_id']; ?>"><?php echo $customcss; ?></label>
    <div class="textarea">
    	<textarea id="SettingsCustomCSS_<?php echo $store['store_id']; ?>" name="data[<?php echo $store['store_id']; ?>][Settings][CustomCSS]"><?php echo $data[$storeCode]['Settings']['CustomCSS']; ?></textarea>
    </div>
</div>