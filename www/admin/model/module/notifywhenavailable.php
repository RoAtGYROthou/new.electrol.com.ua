<?php 
class ModelModuleNotifyWhenAvailable extends Model {
	public function __construct($register) {
		if (!defined('IMODULE_ROOT')) define('IMODULE_ROOT', substr(DIR_APPLICATION, 0, strrpos(DIR_APPLICATION, '/', -2)) . '/');
		if (!defined('IMODULE_SERVER_NAME')) define('IMODULE_SERVER_NAME', substr((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER), 7, strlen((defined('HTTP_CATALOG') ? HTTP_CATALOG : HTTP_SERVER)) - 8));
		parent::__construct($register);
	}
	
	public function install(){
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "notifywhenavailable`
	(`notifywhenavailable_id` INT(11) NOT NULL AUTO_INCREMENT, 
	 `customer_email` VARCHAR(200) NULL DEFAULT NULL,
	 `customer_name` VARCHAR(100) NULL DEFAULT NULL,
	 `product_id` INT(11) NULL DEFAULT '0',
	 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00',
	 `customer_notified` TINYINT(1) NOT NULL DEFAULT '0',
	 `language` VARCHAR(100) NULL DEFAULT '".$this->config->get('config_language')."',
	  PRIMARY KEY (`notifywhenavailable_id`));");	
	}
	
	public function uninstall()	{
		  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "notifywhenavailable`");
	}
	
	public function viewcustomers($page=1, $limit=8, $sort="id", $order="DESC") {	
		if ($page) {
				$start = ($page - 1) * $limit;
			}
			$query =  $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "notifywhenavailable` super 
			JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
			WHERE customer_notified=0 and language_id = " . (int)$this->config->get('config_language_id') . "
			ORDER BY `date_created` DESC
			LIMIT ".$start.", ".$limit);
			
		return $query->rows; 
	}
	
	public function viewnotifiedcustomers($page=1, $limit=8, $sort="id", $order="DESC") {	
		if ($page) {
				$start = ($page - 1) * $limit;
			}
			$query =  $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "notifywhenavailable` super 
			JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
			WHERE customer_notified=1 and language_id = " . (int)$this->config->get('config_language_id') . "
			ORDER BY `date_created` DESC
			LIMIT ".$start.", ".$limit);
			
		return $query->rows; 
	}
	
	public function getTotalCustomers(){
			$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "notifywhenavailable` WHERE customer_notified=0");
		return $query->row['count']; 
	}
	
	public function getTotalNotifiedCustomers(){
			$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "notifywhenavailable` WHERE customer_notified=1");
		return $query->row['count']; 
	}
	
	public function sendEmailWhenAvailable($product_id) {	
		$product_info = $this->model_catalog_product->getProduct($product_id);
		$this->load->model('tool/image');
		if ($product_info['image']) { $image = $this->model_tool_image->resize($product_info['image'], 200, 200); } else { $image = false; }
			
		$customers = $this->db->query("SELECT * FROM `" . DB_PREFIX . "notifywhenavailable` 
			WHERE product_id = ".$product_id." AND customer_notified=0
			ORDER BY `date_created` DESC");					
								
		$NotifyWhenAvailable = $this->config->get('NotifyWhenAvailable');
				
		foreach($customers->rows as $cust) {
			if(!isset($NotifyWhenAvailable['EmailText'][$cust['language']])){
				$EmailText = '';
				$EmailSubject = '';
			} else {
				$EmailText = $NotifyWhenAvailable['EmailText'][$cust['language']];
				$EmailSubject = $NotifyWhenAvailable['EmailSubject'][$cust['language']];
			}
		
			$string = html_entity_decode($EmailText);
			$patterns = array();
			$patterns[0] = '/{c_name}/';
			$patterns[1] = '/{p_name}/';
			$patterns[2] = '/{p_image}/';
			$patterns[3] = '/http:\/\/{p_link}/';
			$replacements = array();
			$replacements[0] = $cust['customer_name'];
			$replacements[1] = "<a href='".HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id."' target='_blank'>".$product_info['name']."</a>";
			$replacements[2] = "<img src='".$image."' />";
			$replacements[3] = HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id;

			$text = preg_replace($patterns, $replacements, $string);
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($cust['customer_email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($EmailSubject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($text);
			$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
		
		$update_customers = $this->db->query("UPDATE `" . DB_PREFIX . "notifywhenavailable` 
			SET customer_notified=1
			WHERE product_id = ".$product_id."");	
	}
}
?>