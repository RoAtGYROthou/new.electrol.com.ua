<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
                <h1><?php echo $heading_title . " " . $text_module_version  . " " .$heading_title_raw; ?></h1>
                <div style="text-align: right; margin-top: 10px;">
                    <span><b><?php echo $entry_feed;?></b></span>
                    <select name="product_feed_id" style='width: 350px'>
                        <?php foreach ($listFeeds as $feed_id => $feed) { ?>
                            <option value="<?php echo $feed_id; ?>"  <?php if($activeFeed['product_feed_id']==$feed_id) echo 'selected="selected"'; ?>><?php echo $feed['feed_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php if(count($listFeeds)>0) { ?>
                <div id='update-product-feed'>
                    <table class="list">
                        <thead>
                        <tr>
                            <td class="right" style='width: 90%'><?php echo $entry_update_all; ?></td>
                            <td class="left" style='width: 10%'>
                                <select name="all_feed_category_id" id="input-status" style='width: 340px'>
                                    <option value="0"><?php echo $text_none;?></option>
                                    <?php foreach($feedCategories as $category) { ?>
                                        <option value="<?php echo $category['category_id'] ?>" ><?php echo $category['name'] ?></option>
                                    <?php }?>
                                </select>
                            </td>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div class="content">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                        <input hidden id="page" value="<?php echo $page ?>">
                        <input hidden id="sort" value="<?php echo $sort ?>">
                        <input hidden id="order" value="<?php echo $order ?>">
                        <input hidden name="feed_id" value="<?php echo $activeFeed['product_feed_id'];?>">
                        <table class="list product">
                            <thead>
                            <tr id="head">
                                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                                <td class="center"><?php echo $column_image; ?></td>
                                <td class="left"><?php if ($sort == 'pd.name') { ?>
                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                    <?php } ?></td>
                                <td class="left"><?php if ($sort == 'p.price') { ?>
                                        <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                                    <?php } ?></td>
                                <td class="left">
                                    <?php echo $column_category; ?>
                                </td>
                                <td class="center">
                                    <?php echo $column_feed_category.' '.$activeFeed['feed_name']; ?>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="filter">
                                <td></td>
                                <td></td>
                                <td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
                                <td align="left"><input type="text" name="filter_price" value="<?php echo $filter_price; ?>" size="8"/></td>
                                <td><select name="filter_category_id" style="width: 100px;" class="filter">
                                        <option value="*"></option>
                                        <option value="null">-</option>
                                        <?php foreach($categories as $category) { ?>
                                            <option value="<?php echo $category['category_id'] ?>" <?php if($filter_category_id == $category['category_id']) echo 'selected="selected"'; ?>><?php echo $category['name'] ?></option>
                                        <?php }?>
                                    </select></td>
                                <td></td>
                            </tr>
                            <?php if ($products) { ?>
                                <?php foreach ($products as $product) { ?>
                                    <tr>
                                        <td style="text-align: center;"><?php if ($product['selected']) { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                                            <?php } else { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                                            <?php } ?></td>
                                        <td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
                                        <td class="left"><?php echo $product['name']; ?></td>
                                        <td class="left"><?php echo $product['price']; ?></td>
                                        <td class="left"><?php foreach ($product['category'] as $cat) echo $cat['name'] . '<br />'; ?></td>
                                        <td class="center">
                                            <select name="feed_category_id" style="width: 350px;" onchange="updateProductToFeedCategory($(this).val(), <?php echo $product['product_id']; ?> )">
                                                <option value="0"><?php echo $text_none;?></option>
                                                <?php foreach($feedCategories as $category) { ?>
                                                    <option value="<?php echo $category['category_id'] ?>" <?php if($product['feed_category_id'] == $category['category_id']) echo 'selected="selected"'; ?>><?php echo $category['name'] ?></option>
                                                <?php }?>
                                            </select>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </form>
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
            <?php } else { ?>
                <div class="content">
                    <div class="warning"><?php echo $error_empty_feed; ?></div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php if(count($listFeeds)>0) { ?>
    <script id="productTemplate" type="text/x-jquery-tmpl">
<tr>
              <td style="text-align: center;">
                <input type="checkbox" name="selected[]" value="${product_id}" />
              </td>
              <td class="center"><img src="${image}" alt="${name}" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
              <td class="left">${name}</td>
              <td class="left">${price}</td>
	      <td class="left">{{each(i, cat) category}}${cat['name']}<br/>{{/each}}</td>
      	      <td class="center">
	      		    <select name="feed_category_id" style="width: 350px;" onchange="updateProductToFeedCategory($(this).val(),${product_id})">
	    <option value="0"><?php echo $text_none;?></option>  	
	    <?php foreach($feedCategories as $category) { ?>
    {{if feed_category_id == <?php echo $category['category_id'] ?>}}
		    <option value="<?php echo $category['category_id'] ?>" selected="selected"><?php echo $category['name'] ?></option>
    {{else}}
		  <option value="<?php echo $category['category_id'] ?>"><?php echo $category['name'] ?></option>
    {{/if}}
		  <?php }?>
	       </select>
	      </td>

            </tr>
</script>
    <script type="text/javascript" src="view/javascript/jquery/jquery.tmpl.min.js"></script>
    <script type="text/javascript"><!--
        function filter() {
            url = 'index.php?route=catalog/neoseo_product_feed_update_relations/filter&token=<?php echo $token; ?>';
            url += '&page=' + $('#page').val();
            if ($('#sort').val()) {
                url += '&sort=' + $('#sort').val();
            }
            if ($('#order').val()) {
                url += '&order=' + $('#order').val();
            }

            var filter_name = $('input[name=\'filter_name\']').attr('value');
            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name);
            }

            var filter_price = $('input[name=\'filter_price\']').attr('value');
            if (filter_price) {
                url += '&filter_price=' + encodeURIComponent(filter_price);
            }

            var category_id = $('select[name=\'filter_category_id\']').attr('value');
            if (category_id != '*') {
                url += '&filter_category_id=' + encodeURIComponent(category_id);
            }
            url += "&activeFeed=<?php echo $activeFeed['product_feed_id']?>";
   
            $.ajax({
                url: url,
                dataType: 'json',
                success: function(json) {
                    $('table.list.product tr:gt(1)').empty();
                    $("#productTemplate").tmpl(json.products).appendTo("table.list.product");
                    $('.pagination').html(json.pagination);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
//--></script> 
    <script type="text/javascript"><!--
        function gsUV(e, t, v) {
            var n = String(e).split("?");
            var r = "";
            if (n[1]) {
                var i = n[1].split("&");
                for (var s = 0; s <= i.length; s++) {
                    if (i[s]) {
                        var o = i[s].split("=");
                        if (o[0] && o[0] == t) {
                            r = o[1];
                            if (v != undefined) {
                                i[s] = o[0] + '=' + v;
                            }
                            break;
                        }
                    }
                }
            }
            if (v != undefined) {
                return n[0] + '?' + i.join('&');
            }
            return r
        }
        $('#form input').keydown(function(e) {
            if (e.keyCode == 13) {
                $('#page').val(1);
                filter();
            }
        });
        $('#form input').bind("input", function() {
            if ($(this).val() == '') {
                $('#page').val(1);
                filter();
            }
        });
        $('#form select.filter').bind("change", function() {
            $('#page').val(1);
            filter();
        });
        $('select[name=\product_feed_id\]').bind("change", function() {
            $('input[name=feed_id]').attr('value', $(this).val());
            $('#form').submit();
        });
        $('select[name=\all_feed_category_id\]').bind("change", function() {
            var products_id = [];
            $("input[name^='selected']").each(function() {
                if ($(this).is(':checked')) {
                    products_id.push($(this).val());
                }
            });
            updateProductToFeedCategory($(this).val(), products_id)
        });

        $('#head a').live("click", function() {

            var sort = gsUV($(this).attr('href'), 'sort');
            $('#sort').val(sort);
            var order = gsUV($(this).attr('href'), 'order');
            $('#order').val(order);
            $(this).attr('href', gsUV($(this).attr('href'), 'order', order == 'DESC' ? 'ASC' : 'DESC'));
            $('#head a').removeAttr('class');
            this.className = order.toLowerCase();
            filter();
            return false;
        });

        function clear_filter() {
            $('tr.filter select option:selected').prop('selected', false);
            $('tr.filter input').val('');
            filter();
            return false;
        }

        function updateProductToFeedCategory(category_id, product_id) {
            $.ajax({
                url: 'index.php?route=catalog/neoseo_product_feed_update_relations/updateProductToFeedCategory&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    'category_id': category_id,
                    'feed_id': <?php echo $activeFeed['product_feed_id'] ?> ,
                    'products_id': product_id
                },
                success: function(json) {
                    $('.warning,.success').remove();
                    if (json['error']) {
                        $('.box').before('<div class="warning"> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        $('.box').before('<div class="success"> ' + json['success'] + '</div>');
                        filter();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
//--></script> 
    <script type="text/javascript"><!--
        $('.filter input').autocomplete({
            delay: 500,
            source: function(request, response) {
                filter();
            }
        });
//--></script> 
<?php } ?>
<?php echo $footer; ?>