<div class="row-fluid">
	<div class="span12">
        <div class="minification-tabbable-parent">
<div class="tabbable tabs-left"> 
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#general" data-toggle="tab"><i class="icon-edit"></i> General Options</a></li>
                    <li><a href="#relatedProducts" data-toggle="tab"><i class="icon-align-justify"></i> Related products</a></li>
                  </ul>
                 <div class="tab-content">
                    <div id="general" class="tab-pane active">
                        <div class="box-heading">
                            <h1 style="float:left;">General</h1>
                            <br /><br />
                        </div>
         				<table class="form">
                        	<tr>
                              <td>Module title:</td>
                                <td>
                                    <?php foreach ($languages as $language) { ?>
                                <div class="input-prepend">   
                                  <span class="add-on"><?php echo $language['name']; ?></span>   
									<input name="RelatedProductsPro[WidgetTitle][<?php echo $language['code']; ?>]" class="input-xxlarge" type="text" value="<?php echo (isset($data['RelatedProductsPro']['WidgetTitle'][$language['code']])) ? $data['RelatedProductsPro']['WidgetTitle'][$language['code']] : 'Complement goods' ?>" />
								</div>
                             <?php } ?>
                               </td>
                            </tr>
                        	<tr>
                              <td>Wrap in widget:</td>
                                <td>
                                    <select name="RelatedProductsPro[WrapInWidget]" class="RelatedProductsProWrapInWidget">
                                        <option value="yes" <?php echo (isset($data['RelatedProductsPro']['WrapInWidget']) && $data['RelatedProductsPro']['WrapInWidget'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                                       <option value="no" <?php echo (isset($data['RelatedProductsPro']['WrapInWidget']) && $data['RelatedProductsPro']['WrapInWidget'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                                    </select>
                               </td>
                         	</tr>
							<tr>
                                <td>Show Add to Cart button:</td>
                                <td>
                                    <select name="RelatedProductsPro[AddToCart]" class="RelatedProductsProAddToCart">
                                        <option value="yes" <?php echo (isset($data['RelatedProductsPro']['AddToCart']) && $data['RelatedProductsPro']['AddToCart'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                                       <option value="no" <?php echo (isset($data['RelatedProductsPro']['AddToCart']) && $data['RelatedProductsPro']['AddToCart'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                                    </select>
                               </td>
							</tr>
							<tr>
                                <td>Additional positioning:</td>
                                <td>
                                        <label class="checkbox">
                                          <input type="checkbox" name="RelatedProductsPro[AdditionalPositioning][UnderCart]" value="yes" <?php echo !empty($data['RelatedProductsPro']['AdditionalPositioning']['UnderCart']) ? 'checked="checked"' : ''; ?>/>
                                        Show under Shopping Cart in Checkout page
                                        </label>
                               </td>
							</tr>
							<tr>
                            <td>Picture Width & Height:<span class="help">In Pixels</span></td>
                            <td>
                                Width:&nbsp;&nbsp;&nbsp;<div class="input-append"><input type="text" name="RelatedProductsPro[PictureWidth]" class="RelatedProductsProPictureWidth input-mini" value="<?php echo (isset($data['RelatedProductsPro']['PictureWidth'])) ? $data['RelatedProductsPro']['PictureWidth'] : '80' ?>" /></input><span class="add-on">px</span></div><br />Height:&nbsp;<div class="input-append"><input type="text" name="RelatedProductsPro[PictureHeight]" class="RelatedProductsProPictureHeight input-mini" value="<?php echo (isset($data['RelatedProductsPro']['PictureHeight'])) ? $data['RelatedProductsPro']['PictureHeight'] : '80' ?>" /></input><span class="add-on">px</span></div>
                           </td>
                          </tr>
						  <tr>
                            <td>Custom CSS:</td>
                            <td>
                                <textarea name="RelatedProductsPro[CustomCSS]" placeholder="Custom CSS" class="RelatedProductsProCustomCSS"><?php echo (isset($data['RelatedProductsPro']['CustomCSS'])) ? $data['RelatedProductsPro']['CustomCSS'] : '' ?></textarea>
                           </td>
                          </tr>
						</table>
                        <div class="clearfix"></div>
                    </div>
                    <div id="relatedProducts" class="tab-pane">                          
                            <div class="box-heading">
                                <h1 style="float:left;">Related products</h1>
								<br /><br />
                            </div>					
                     <!-- Related Products tab - START -->
                     <br />
                     <table class="form">
						<tr>
                            <td style="vertical-align:top;"><strong>Type of relation</strong>:<span class="help">Choose how the products will be related.</span></td>
                            <td>
                                <div class="well" style="float:right;text-align:left;width:65%">
                                 <i class="icon-info-sign"></i> <strong>Exclusive</strong> - The customers will see related products based on exact matches in the Related Product Pairs - meaning results only from one of the pairs.<div style="padding:5px;" ></div>
                                 <i class="icon-info-sign"></i> <strong>Inclusive</strong> - The customers will see related products based on a similar matches in the Related Product Pairs, e.g. there could be results from two, three or more pairs.
                                </div>
                                <select name="RelatedProductsPro[Relation]">
									<option value="exclusive" <?php echo (isset($data['RelatedProductsPro']['Relation']) && $data['RelatedProductsPro']['Relation'] == 'exclusive') ? 'selected=selected' : '' ?>>Exclusive</option>
									<option value="inclusive" <?php echo (isset($data['RelatedProductsPro']['Relation']) && $data['RelatedProductsPro']['Relation'] == 'inclusive') ? 'selected=selected' : '' ?>>Inclusive</option>
                                </select>
                           </td>
						</tr>
                     	<tr>
							<td style="vertical-align:top;" ><strong>Automatic Related Products</strong>:<span class="help">The system will try to intelligently guess related products based on cart content and your current OpenCart-native related products.</span></td>
							<td>
                                <div class="well" style="float:right;text-align:left;width:65%">
                                 <i class="icon-warning-sign"></i> If you enable this option, the system will use the default functionality in OpenCart to find related products. The results will be displayed after the ones that are specified in this module (if there are any).<br /><br />
                                 If <strong>Automatic Related Products</strong> is enabled, the module can display related products even if there are not products in the cart. The data is taken from the default OpenCart functionality.
                                </div>
                                <select name="RelatedProductsPro[DefaultOCFunctionality]" class="RelatedProductsProDefaultOCFunctionality">
                                    <option value="yes" <?php echo (isset($data['RelatedProductsPro']['DefaultOCFunctionality']) && $data['RelatedProductsPro']['DefaultOCFunctionality'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                                   <option value="no" <?php echo (isset($data['RelatedProductsPro']['DefaultOCFunctionality']) && $data['RelatedProductsPro']['DefaultOCFunctionality'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
                                </select>
                           </td>
                        </tr>
                        <tr>
                        	<td style="vertical-align:top;" ><strong>Limit</strong>:<span class="help">This applies on both results (from the module + default OpenCart related functionality)</span></td>
                            <td>
                                <input name="RelatedProductsPro[RelatedLimit]" class="input-mini" type="number" value="<?php echo (isset($data['RelatedProductsPro']['RelatedLimit'])) ? $data['RelatedProductsPro']['RelatedLimit'] : '5' ?>" />
                            </td>
                        </tr>
                     </table>
                      <div class="box-heading">
                                <h1 style="float:left;">Related Products Pairs</h1><br /><br />
                            </div><br />
					 <?php $token = $_GET['token']; ?>
                         <table id="module" class="table table-bordered table-hover" width="100%" >
                          <thead>
                            <tr class="table-header">
                              <td class="left"><strong>Cart products</strong></td>
                              <td class="left"><strong>Related products</strong></td>
                              <td><strong>Actons:</strong></td>
                            </tr>
                          </thead>
                          <?php $module_row = 0; ?>
                          <?php if (isset($CustomRelated)) {
							  foreach ($CustomRelated as $module) { ?>
                              <?php if (!isset($module['id'])) { $module['id']=mt_rand(10000, 99999);} ?>
                          <tbody id="module-row<?php echo $module['id']; ?>">
                            <tr>
                              <td class="left">
					<input type="hidden" class="bundle_id" name="relatedproductspro_custom[<?php echo $module['id']; ?>][id]" value="<?php echo $module['id']; ?>" />
                          <span style="vertical-align:middle;">Add product:</span> <input type="text" name="productsCart" value="" />
                            <div id="products-cart_<?php echo $module['id']; ?>" class="scrollbox first">
                                   <?php $class1 = 'odd'; ?>
                                   <?php if (!empty($module['ProductsCart'])) {
                                        foreach ($module['ProductsCart'] as $pr) { ?>
                                          <?php $class1 = ($class1 == 'even' ? 'odd' : 'even'); ?>
                                          <?php $product_info = $this->model_catalog_product->getProduct($pr); ?>
                                          <div id="products-cart_<?php echo $module['id']; ?>_<?php echo $pr; ?>" class="<?php echo $class1; ?>"> <?php echo $product_info['name']; ?><img src="view/image/delete.png" alt="" />
                                          <input type="hidden" name="relatedproductspro_custom[<?php echo $module['id']; ?>][ProductsCart][]" value="<?php echo $pr; ?>" />
                                         </div>
                                   <?php }
                                   } ?>
                            </div>
                        </td>
						<td class="left">
                           <span style="vertical-align:middle;">Add product:</span> <input type="text" name="productsRelated" value="" />
                            <div id="products-related_<?php echo $module['id']; ?>" class="scrollbox second">
                                   <?php $class1 = 'odd'; ?>
                                   <?php if (!empty($module['ProductsRelated'])) {
                                        foreach ($module['ProductsRelated'] as $pr) { ?>
                                          <?php $class1 = ($class1 == 'even' ? 'odd' : 'even'); ?>
                                          <?php $product_info = $this->model_catalog_product->getProduct($pr); ?>
                                          <div id="products-related_<?php echo $module['id']; ?>_<?php echo $pr; ?>" class="<?php echo $class1; ?>"> <?php echo $product_info['name']; ?><img src="view/image/delete.png" alt="" />
                                          <input type="hidden" name="relatedproductspro_custom[<?php echo $module['id']; ?>][ProductsRelated][]" value="<?php echo $pr; ?>" />
                                         </div>
                                   <?php }
                                   } ?>
                            </div>
                          
                        </td>
                             <td class="left" style="vertical-align:middle;"><a onclick="$('#module-row<?php echo $module['id']; ?>').remove();" class="btn btn-small btn-danger" style="text-decoration:none;"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>
                           </tr>
                          </tbody>
                          <?php $module_row++; ?>
                          <?php } } ?>
                          <tfoot>
                            <tr>
                              <td colspan="2"></td>
                              <td class="left"><a onclick="addModule();" class="btn btn-small btn-primary"><i class="icon-plus"></i> Add New</a></td>
                            </tr>
                          </tfoot>
                        </table>
                        
                        <script type="text/javascript"><!--
                        function addModule() {
							var module_row=Math.floor(Math.random() * 99999) + 10000;
                            html  = '<tbody style="display:none;" id="module-row' + module_row + '">';
                            html += '  <tr>';
                            html += '    <td class="left">';
							html += '<input type="hidden" class="bundle_id" name="relatedproductspro_custom[' + module_row + '][id]" value="' + module_row + '" />';
                            html += '<span style="vertical-align:middle;">Add product:</span> <input type="text" name="productsCart" value="" />';
                            html += '<div id="products-cart_' + module_row + '" class="scrollbox first">';
                            html += '</div>';	
                            html += ' ';
                            html += '    </td>';
                            html += '    <td class="left">';
                            html += '<span style="vertical-align:middle;">Add product:</span> <input type="text" name="productsRelated" value="" />';
                            html += '<div id="products-related_' + module_row + '" class="scrollbox second">';
                            html += '</div>';	html += '    </td>';
                            html += '    <td class="left" style="vertical-align:middle;"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-small btn-danger" style="text-decoration:none;"><i class="icon-remove"></i> <?php echo $button_remove; ?></a></td>';
                            html += '  </tr>';
                            html += '</tbody>';
                            
                            $('#module tfoot').before(html);
                            $('#module-row' + module_row).fadeIn();
							initializeAutocomplete();
                            
                        }
                        //-->
						
						var initializeAutocomplete = function () {
							// Show in Products
							$('input[name=\'productsCart\']').autocomplete({
								delay: 500,
								source: function(request, response) {
									$.ajax({
										url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
										dataType: 'json',
										success: function(json) {		
											response($.map(json, function(item) {
												return {
													label: item.name,
													value: item.product_id
												}
											}));
										}
									});
								}, 
								select: function(event, ui) {
									var bundle_id = $(this).parents('tr').find('.bundle_id').val();
									$(this).parent().find('#products-cart_' + bundle_id + '_' + ui.item.value).remove();
									$(this).parent().find('.first').append('<div id="products-cart_' + bundle_id + '_' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="relatedproductspro_custom[' + bundle_id + '][ProductsCart][]" value="' + ui.item.value +'" /></div>');
									$(this).parent().find('#products-cart_' + bundle_id + ' div:odd').attr('class', 'odd');
									$(this).parent().find('#products-cart_' + bundle_id + ' div:even').attr('class', 'even');		
									return false;
								},
								focus: function(event, ui) {
								  return false;
							   }
							});
										
							$('.scrollbox.first div img').live('click', function() {
								var bundle_id = $(this).parents('tr').find('.bundle_id').val();
								$(this).parent().remove();
								$(this).parent().find('#products-cart_' + bundle_id + ' div:odd').attr('class', 'odd');
								$(this).parent().find('#products-cart_' + bundle_id + ' div:even').attr('class', 'even');	
							});
							
							// ProductsRelated
							$('input[name=\'productsRelated\']').autocomplete({
								delay: 500,
								source: function(request, response) {
									$.ajax({
										url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
										dataType: 'json',
										success: function(json) {		
											response($.map(json, function(item) {
												return {
													label: item.name,
													value: item.product_id
												}
											}));
										}
									});
								}, 
								select: function(event, ui) {
									var bundle_id = $(this).parents('tr').find('.bundle_id').val();
									$(this).parent().find('#products-related_' + bundle_id + '_' + ui.item.value).remove();
									$(this).parent().find('.second').append('<div id="products-related_' + bundle_id + '_' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="relatedproductspro_custom[' + bundle_id + '][ProductsRelated][]" value="' + ui.item.value +'" /></div>');
									$(this).parent().find('#products-related_' + bundle_id + ' div:odd').attr('class', 'odd');
									$(this).parent().find('#products-related_' + bundle_id + ' div:even').attr('class', 'even');		
									return false;
								},
								focus: function(event, ui) {
								  return false;
							   }
							});
										
							$('.scrollbox.second div img').live('click', function() {
								var bundle_id = $(this).parents('tr').find('.bundle_id').val();
								$(this).parent().remove();
								$(this).parent().find('#products-related_' + bundle_id + ' div:odd').attr('class', 'odd');
								$(this).parent().find('#products-related_' + bundle_id + ' div:even').attr('class', 'even');	
							});
							
						}
						initializeAutocomplete();
                        </script>
                     <!-- Related Products tab - END -->
                            <div class="clearfix"></div>
                    </div>
                 </div>
               </div>
            </div>
	</div>
</div>