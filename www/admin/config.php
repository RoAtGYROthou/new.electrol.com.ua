<?php
// HTTP
define('HTTP_SERVER', 'http://new.electrol.com.ua/admin/');
define('HTTP_CATALOG', 'http://new.electrol.com.ua/');
define('HTTP_IMAGE', 'http://new.electrol.com.ua/image/');

// HTTPS
define('HTTPS_SERVER', 'http://new.electrol.com.ua/admin/');
define('HTTPS_CATALOG', 'http://new.electrol.com.ua/');
define('HTTPS_IMAGE', 'http://new.electrol.com.ua/image/');
// DIR
$dir = dirname(dirname(__FILE__));
define('DIR_APPLICATION', $dir . '/admin/');
define('DIR_SYSTEM', $dir . '/system/');
define('DIR_DATABASE', $dir . '/system/database/');
define('DIR_LANGUAGE', $dir . '/admin/language/');
define('DIR_TEMPLATE', $dir . '/admin/view/template/');
define('DIR_CONFIG', $dir . '/system/config/');

define('DIR_IMAGE', $dir . '/image/');
define('DIR_CACHE', $dir . '/system/cache/');
define('DIR_DOWNLOAD', $dir . '/download/');
define('DIR_LOGS', $dir . '/system/logs/');
define('DIR_CATALOG', $dir . '/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'new.electrol.com.ua');
define('DB_PASSWORD', '7Q5z1H4i');
define('DB_DATABASE', 'new.electrol.com.ua');
define('DB_PREFIX', '');
?>
