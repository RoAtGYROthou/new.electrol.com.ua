<div>
    <label for="GoogleMapsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $showcolumn; ?></label>
    <select name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][GoogleMaps][Show]" id="GoogleMapsShow_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>">
        <option value="true" <?php echo ($data[$storeCode][$langCode]['Widgets']['GoogleMaps']['Show'] == 'true') ? 'selected=selected' : '';?>><?php echo $yes; ?></option>
        <option value="false" <?php echo ($data[$storeCode][$langCode]['Widgets']['GoogleMaps']['Show'] == 'false') ? 'selected=selected' : '';?>><?php echo $no; ?></option>
    </select>
</div>
<div>
    <label for="ColumnPositionGoogleMaps_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $columnposition; ?></label>
    <input id="ColumnPositionGoogleMaps_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" type="text" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Positions][googlemaps]" value="<?php echo $data[$storeCode][$langCode]['Positions']['googlemaps']; ?>" />
</div>
<div>
    <label for="GoogleMapsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $titleofthecolumn; ?></label>
    <input type="text" id="GoogleMapsTitle_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][GoogleMaps][Title]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['GoogleMaps']['Title']; ?>" />
</div>
<div>
    <label for="GoogleMapsAPIKey_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $maps_apikey; ?></label>
    <input type="text" id="GoogleMapsAPIKey_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][GoogleMaps][APIKey]" value="<?php echo !empty($data[$storeCode][$langCode]['Widgets']['GoogleMaps']['APIKey']) ? $data[$storeCode][$langCode]['Widgets']['GoogleMaps']['APIKey'] : ''; ?>" />
</div>
<div>
    <label for="GoogleMapsLongitude_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $maps_longlat; ?></label>
    <input type="text" class="GoogleMapsLongitude semiField" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][GoogleMaps][Longitude]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['GoogleMaps']['Longitude']; ?>" id="GoogleMapsLongitude_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" />
    <input type="text" class="GoogleMapsLatitude semiField" name="data[<?php echo $store['store_id']; ?>][<?php echo $lang['code']; ?>][Widgets][GoogleMaps][Latitude]" value="<?php echo $data[$storeCode][$langCode]['Widgets']['GoogleMaps']['Latitude']; ?>" id="GoogleMapsLatitude_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" />
    <button class="btn GoogleMapsPreviewButton" id="GoogleMapsPreviewButton_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $preview; ?></button>
</div>
<div>
    <label for="GoogleMapsPreviewDiv_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"><?php echo $maps_preview; ?></label>
    <div class="GoogleMapsPreviewDiv" data-longitude-selector="#GoogleMapsLongitude_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" data-latitude-selector="#GoogleMapsLatitude_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" data-apikey-selector="#GoogleMapsAPIKey_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>" id="GoogleMapsPreviewDiv_<?php echo $store['store_id']; ?>_<?php echo $lang['code']; ?>"></div>
</div>