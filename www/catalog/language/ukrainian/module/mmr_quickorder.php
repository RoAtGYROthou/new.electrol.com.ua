<?php
// Text
$_['text_mmr_quickorder_button']                = 'Купити в 1 клік';
$_['text_mmr_quickorder_callback_button']       = 'Замовити';
$_['text_mmr_quickorder_processing']            = 'Обробка';
$_['text_mmr_quickorder_success']               = 'Успішно: ваш запит було відправлено, ми зв’яжемося з вами якомога швидше!';

$_['text_mmr_quickorder_email_subject']         = 'Новий запит Швидкого Замовлення';
$_['text_mmr_quickorder_email_order_received']  = 'Ви отримали замовлення';
$_['text_mmr_quickorder_email_order_detail']    = 'Деталі замовлення';
$_['text_mmr_quickorder_email_url']             = 'Адреса сторінки';

//Errors
$_['error_mmr_quickorder_fields']               = 'Увага: Будь ласка, заповніть обов’язкові поля';
?>