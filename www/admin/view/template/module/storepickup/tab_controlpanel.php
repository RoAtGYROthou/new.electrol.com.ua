<div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <h5><strong><span class="required">* </span><?php echo $text_module_status; ?></strong></h5>
      </div>
      <div class="col-md-3">
        <select id="Checker" name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
              <option value="yes" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
              <option value="no"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
        </select>
      </div>
    </div>
    <br />
	<div class="row">
      <div class="col-md-3"> 
        <h5><strong><?php echo $text_store_listing; ?></strong></h5>
      </div>
      <div class="col-md-9">
		<a href="<?php echo $store_listing; ?>" class="well well-sm" target="_blank"><?php echo $store_listing; ?></a>
      </div>
    </div>
    <br />
</div>
<div class="tabbable tabs-left" id="store_tabs">

    <ul class="nav nav-tabs store-list">
        <li class="static"><a class="addNewStoreData"><i class="fa fa-plus"></i> Add New Store</a></li>
        <?php if (isset($moduleData['StoreData'])) { ?>
            <?php foreach ($moduleData['StoreData'] as $storedata) { ?>
            <li><a href="#storedata_<?php echo $storedata['id']; ?>" data-toggle="tab" data-store-id="<?php echo $storedata['id']; ?>"><i class="fa fa-pencil-square-o"></i> <?php echo $storedata['Name'][$language_id]; ?> (<?php echo $storedata['id']; ?>) <i class="fa fa-minus-circle removeStoreData"></i>
                <input type="hidden" name="<?php echo $moduleName; ?>[StoreData][<?php echo $storedata['id']; ?>][id]" value="<?php echo $storedata['id']; ?>" />
                </a> </li>
            <?php } ?>
        <?php } ?>
    </ul>
    <div class="tab-content store-settings">
        <?php if (isset($moduleData['StoreData'])) { ?>
            <?php foreach ($moduleData['StoreData'] as $storedata) { 
                require(DIR_APPLICATION.'view/template/module/'.$moduleNameSmall.'/tab_storetab.tpl');
            } ?>
        <?php } ?>
    </div>
</div>
        
<script type="text/javascript"><!--
// Add Template
function addNewStoreData() {
	count = $('.store-list li:last-child > a').data('store-id') + 1 || 1;
	var ajax_data = {};
	ajax_data.token = '<?php echo $token; ?>';
	ajax_data.store_id = '<?php echo $store['store_id']; ?>';
	ajax_data.storedata_id = count;

	$.ajax({
		url: 'index.php?route=module/<?php echo $moduleNameSmall; ?>/get_store_settings',
		data: ajax_data,
		dataType: 'html',
		beforeSend: function() {
			NProgress.start();
		},
		success: function(settings_html) {
			
			$('.store-settings').append(settings_html);
			
			if (count == 1) { $('a[href="#storedata_'+ count +'"]').tab('show'); }
			tpl 	= '<li>';
			tpl 	+= '<a href="#storedata_'+ count +'" data-toggle="tab" data-store-id="'+ count +'">';
			tpl 	+= '<i class="fa fa-pencil-square-o"></i> Store '+ count;
			tpl 	+= '<i class="fa fa-minus-circle removeStoreData"></i>';
			tpl 	+= '<input type="hidden" name="<?php echo $moduleName; ?>[StoreData]['+ count +'][id]" value="'+ count +'"/>';
			tpl 	+= '</a>';
			tpl	+= '</li>';
			
			$('.store-list').append(tpl);
			
			NProgress.done();
			$('.store-list').children().last().children('a').trigger('click');
			window.localStorage['currentSubTab'] = $('.store-list').children().last().children('a').attr('href');
		}
	});
}

// Remove Template
function removeStoreData() {
	tab_link = $(event.target).parent();
	tab_pane_id = tab_link.attr('href');
	
	var confirmRemove = confirm('Are you sure you want to remove ' + tab_link.text().trim() + '?');
	
	if (confirmRemove == true) {
		tab_link.parent().remove();
		$(tab_pane_id).remove();
		
		if ($('.store-list').children().length > 1) {
			$('.store-list > li:nth-child(2) a').tab('show');
			window.localStorage['currentSubTab'] = $('.store-list > li:nth-child(2) a').attr('href');
		}
	}
}

// Events for the Add and Remove buttons
$(document).ready(function() {
	// Add New Store
	$('.addNewStoreData').click(function(e) { addNewStoreData(); });
	// Remove Store
	$('.store-list').delegate('.removeStoreData', 'click', function(e) { removeStoreData(); });
});

// Show the EDITOR
<?php 
if (isset($moduleData['StoreData'])) { 
	foreach ($moduleData['StoreData'] as $storedata) {
		foreach ($languages as $language) { ?>
			$('#message_<?php echo $storedata['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
				height: 200
			});
			$('#messageD_<?php echo $storedata['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
				height: 200
			});
<?php	}
	}
} ?>
</script>