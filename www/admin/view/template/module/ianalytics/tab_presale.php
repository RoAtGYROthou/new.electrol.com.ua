<?php 
$searchvar = 'search';
if (defined('VERSION') && (VERSION == '1.5.4' || VERSION == '1.5.4.1' || VERSION == '1.5.3' || VERSION == '1.5.3.1' || VERSION == '1.5.2' || VERSION == '1.5.2.1' || VERSION == '1.5.1' || VERSION == '1.5.1.3' || VERSION == '1.5.0')) {
	$searchvar = 'filter_name';
}
?>
<br />
<table class="form">
	<tr>
		<td style="vertical-align:top;width: 240px !important;">
			<ul class="nav nav-pills nav-stacked" id="preSaleTabs">
				<li><a href="#search-queries1" data-toggle="tab"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search Queries</a></li>
                <li><a href="#search-keywords" data-toggle="tab"><span class="glyphicon glyphicon-font"></span>&nbsp;&nbsp;Search Keywords</a></li>
                <li><a href="#most-searched-products" data-toggle="tab"><span class="glyphicon glyphicon-tags"></span>&nbsp;&nbsp;Most Searched Products</a></li>
                <li><a href="#opened-products" data-toggle="tab"><span class="glyphicon glyphicon-map-marker"></span>&nbsp;&nbsp;Opened Products</a></li>
                <li><a href="#added-to-cart" data-toggle="tab"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Added to Cart Products</a></li>
                <li><a href="#added-to-wishlist" data-toggle="tab"><span class="glyphicon glyphicon-heart"></span>&nbsp;&nbsp;Added to Wishlist Products</a></li>
				<li><a href="#compared-products" data-toggle="tab"><span class="glyphicon glyphicon-transfer"></span>&nbsp;&nbsp;Compared Products</a></li>
            </ul>
		</td>
		<td style="vertical-align:top;">
			<div class="tab-content">
                <div id="search-queries1" class="tab-pane fade">
                    <h3 style="float:left;">Search Queries Graph</h3>
                    <?php require('element_filter.php'); ?>
                    <div class="help">This graph depicts what part of your users' search queries has derived results</div>
                    <div class="iModuleFields">
                        <script type="text/javascript">
                            var monthlySearchesGraph = $.parseJSON('<?php echo json_encode($iAnalyticsMonthlySearchesGraph)?>'); 
                        </script>
                        <div id="searchedFound" style="width:100%;overflow:hidden;"></div>
                        <br />
                        <h3>Search Queries in Numbers</h3>
                        <table class="table-hover table table-striped">
                            <?php foreach($iAnalyticsMonthlySearchesTable as $index => $day): ?>
							<?php if ($index<1) { ?>
                                <thead>
                                    <tr>
                                      <th><?php echo $day[0]?></th>
                                      <th><?php echo $day[1]?></th>
                                      <th><?php echo $day[2]?></th>
                                      <th><?php echo $day[3]?></th>
                                    </tr>
                                </thead>
                            <?php } else { ?>
                                <tr><td><?php echo $day[0]?></td><td><?php echo $day[1]?></td><td><?php echo $day[2]?></td><td><?php echo $day[3]?></td></tr>
                            <?php } ?>
                            <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div id="search-keywords" class="tab-pane fade">
                    <div class="iModuleFields">
                        <h3 style="float:left;">Keywords in Search - History</h3>
                        <?php require('element_filter.php'); ?>
                        <div class="help">This table shows the search queries of your website users starting from the most recent.</div><br />
                        <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsKeywordSearchHistory as $index => $k): ?>
						<?php if ($index<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
								  <th><?php echo $k[2]?></th>
                                  <th><?php echo $k[3]?></th>
                                  <th><?php echo $k[4]?></th>
                                  <th><?php echo $k[5]?></th>
                                  <th></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <tr><td><?php echo strlen($k[0]) > 30 ? substr($k[0], 0, 30) . '...' : $k[0];?></td><td><?php echo $k[1]?></td><td><?php echo $k[2]?></td><td><?php echo $k[3]?></td><td><?php echo $k[4]?></td><td><?php echo $k[5]?></td>
                            <td style="text-align:right;"><?php if ($index > 0) : ?><a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete the record?');" href="index.php?route=module/ianalytics/deletesearchkeyword&token=<?php echo $this->session->data['token']; ?>&searchValue=<?php echo $k[6]; ?>"><span class="glyphicon glyphicon-minus-sign"></span>&nbsp;Delete record</a>&nbsp;&nbsp;<a class="btn btn-default" onclick="return confirm('Are you sure you wish to delete all of the searches of this keyword?');" href="index.php?route=module/ianalytics/deleteallsearchkeyword&token=<?php echo $this->session->data['token']; ?>&searchValue=<?php echo $k[0]; ?>"><span class="glyphicon glyphicon-remove"></span>&nbsp;Delete keyword</a><?php endif; ?></td>
                            </tr>
                            <?php } ?>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div id="most-searched-products" class="tab-pane fade">
                    <h3 style="float:left;">Most Searched Keywords</h3>
                    <?php require('element_filter.php'); ?>
                    <span class="help">This indicates what your visitors search the most on your site using the OpenCart search engine</span>
                    <br />
                    <table class="table-hover table table-striped">
                        <?php foreach($iAnalyticsMostSearchedKeywords as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                  <th></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                            <tr><td><?php echo strlen($k[0]) > 30 ? substr($k[0], 0, 30) . '...' : $k[0];?></td><td><?php echo $k[1]?></td><td align="right"><?php if ($j > 0) {  ?><div><a href="../index.php?route=product/search&<?php echo $searchvar; ?>=<?php echo $k[0]?>" target="_blank" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span>&nbsp; Preview</a></div><?php } ?></td></tr>
						<?php } ?>
                        <?php endforeach; ?>
                    </table>
                    <div class="iModuleFields">
                        <div class="clearfix"></div>
                    </div>
                 </div>
                 <div id="opened-products" class="tab-pane fade">
					<h3 style="float:left;">Opened Products</h3>
					<?php require('element_filter.php'); ?>
					<div class="help">This table shows the products your visitors viewed starting from the most viewed.</div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostOpenedProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
                 <div id="added-to-cart" class="tab-pane fade">
					<h3 style="float:left;">Most Added to Cart Products</h3>
					<?php require('element_filter.php'); ?>
					<div class="help">This table shows the most added to cart products from your customers.</div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostAddedtoCartProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
				<div id="added-to-wishlist" class="tab-pane fade">
					<h3 style="float:left;">Most Added to Wishlist Products</h3>
					<?php require('element_filter.php'); ?>
					<div class="help">This table shows the most added to wishlist products from your customers.</div>
                    <br />
					<table class="table-hover table table-striped">
					<?php foreach($iAnalyticsMostAddedtoWishlistProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
						<tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
					<?php endforeach; ?>
					</table>
					<div class="clearfix"></div>
				</div>
                <div id="compared-products" class="tab-pane fade">
					<h3 style="float:left;">Compared Products</h3>
					<?php require('element_filter.php'); ?>
                      <span class="help">This table shows the products your visitors compared starting from the most compared.</span>
                      <br />
                      <table class="table-hover table table-striped">
                      <?php foreach($iAnalyticsMostComparedProducts as $j => $k): ?>
						<?php if ($j<1) { ?>
                            <thead>
                                <tr>
                                  <th><?php echo $k[0]?></th>
                                  <th><?php echo $k[1]?></th>
                                </tr>
                            </thead>
                        <?php } else { ?>
                          <tr><td><?php echo $k[0]?></td><td><?php echo $k[1]?></td></tr>
						<?php } ?>
                      <?php endforeach; ?>
                      </table>
                      <div class="clearfix"></div>
				</div>
			</div>
		</td>
	</tr>
</table>