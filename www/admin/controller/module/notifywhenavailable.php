<?php
class ControllerModuleNotifyWhenAvailable extends Controller {
	private $error = array(); 
	public function index() {   
		$this->language->load('module/notifywhenavailable');
        $this->load->model('module/notifywhenavailable');
		$this->load->model('catalog/product');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		$this->data['error_warning'] = '';
	
		$this->document->addScript('view/javascript/notifywhenavailable/bootstrap/js/bootstrap.min.js');
		$this->document->addStyle('view/javascript/notifywhenavailable/bootstrap/css/bootstrap.min.css');
		$this->document->addStyle('view/javascript/notifywhenavailable/font-awesome/css/font-awesome.min.css');
		$this->document->addStyle('view/stylesheet/notifywhenavailable.css');	
		
		$this->document->addScript('view/javascript/notifywhenavailable/cron.js');
		$this->document->addScript('view/javascript/notifywhenavailable/timepicker.js');
		
		$catalogURL = $this->getCatalogURL();
		$this->document->addStyle($catalogURL.'catalog/view/javascript/jquery/colorbox/colorbox.css');
		$this->document->addScript($catalogURL.'catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
		
		// check for the multi-lingual variable
		$check_update = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "notifywhenavailable` LIKE 'language'");
		if (!$check_update->rows)
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "notifywhenavailable`
				ADD `language` VARCHAR(100) NULL DEFAULT '".$this->config->get('config_language')."'");
		// check for the multi-lingual variable
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if (!$this->user->hasPermission('modify', 'module/notifywhenavailable')) {
				$this->validate();
				$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post['NotifyWhenAvailable']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
				$this->request->post['NotifyWhenAvailable']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']),true);
			}
		
			$this->model_setting_setting->editSetting('NotifyWhenAvailable', $this->request->post);		
				
			$this->session->data['success'] = $this->language->get('text_success');
			
			if (!empty($_GET['activate'])) {
				$this->session->data['success'] = $this->language->get('text_success_activation');
			}
			
			if ($this->request->post['NotifyWhenAvailable']["ScheduleEnabled"] == 'yes') {
                $cronCommands   = array();
                $cronFolder     = dirname(DIR_APPLICATION) . '/vendors/notifywhenavailable/';
                $dateForSorting = array();
                if (isset($this->request->post['NotifyWhenAvailable']["ScheduleType"]) && $this->request->post['NotifyWhenAvailable']["ScheduleType"] == 'F') {
                    if (isset($this->request->post['NotifyWhenAvailable']["FixedDates"])) {
                        foreach ($this->request->post['NotifyWhenAvailable']["FixedDates"] as $date) {
                            $buffer           = explode('/', $date);
                            $bufferDate       = explode('.', $buffer[0]);
                            $bufferTime       = explode(':', $buffer[1]);
                            $cronCommands[]   = (int) $bufferTime[1] . ' ' . (int) $bufferTime[0] . ' ' . (int) $bufferDate[0] . ' ' . (int) $bufferDate[1] . ' * php ' . $cronFolder . 'sendMails.php';
                            $dateForSorting[] = $bufferDate[2] . '.' . $bufferDate[1] . '.' . $bufferDate[0] . '.' . $buffer[1];
                        }
                        asort($dateForSorting);
                        $sortedDates = array();
                        foreach ($dateForSorting as $date) {
                            $newDate       = explode('.', $date);
                            $sortedDates[] = $newDate[2] . '.' . $newDate[1] . '.' . $newDate[0] . '/' . $newDate[3];
                        }
                        $this->request->post['NotifyWhenAvailable']["FixedDates"] = $sortedDates;
                    }
                }
                if (isset($this->request->post['NotifyWhenAvailable']["ScheduleType"]) && $this->request->post['NotifyWhenAvailable']["ScheduleType"] == 'P') {
                    $cronCommands[] = $this->request->post['NotifyWhenAvailable']['PeriodicCronValue'] . ' php ' . $cronFolder . 'sendMails.php';
                }
                if (isset($cronCommands)) {
                    $cronCommands      = implode(PHP_EOL, $cronCommands);
                    $currentCronBackup = shell_exec('crontab -l');
                    $currentCronBackup = explode(PHP_EOL, $currentCronBackup);
                    foreach ($currentCronBackup as $key => $command) {
                        if (strpos($command, ' php ' . $cronFolder . 'sendMails.php') || empty($command)) {
                            unset($currentCronBackup[$key]);
                        }
                    }
                    $currentCronBackup = implode(PHP_EOL, $currentCronBackup);
                    file_put_contents($cronFolder . 'cron.txt', $currentCronBackup . PHP_EOL . $cronCommands . PHP_EOL);
                    exec('crontab -r');
                    exec('crontab ' . $cronFolder . 'cron.txt');
                }
            } else {
				
			}
			
			$selectedTab = (empty($this->request->post['selectedTab'])) ? 0 : $this->request->post['selectedTab'];
			$this->redirect($this->url->link('module/notifywhenavailable', 'token=' . $this->session->data['token'] . '&tab='.$selectedTab, 'SSL'));
		}
		
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();;
		$this->data['languages'] = $languages;
		$firstLanguage = array_shift($languages);
		$this->data['firstLanguageCode'] = $firstLanguage['code'];
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_layout_options'] = $this->language->get('entry_layout_options');
		$this->data['entry_layouts_active'] = $this->language->get('entry_layouts_active');
		$this->data['entry_position_options'] = $this->language->get('entry_position_options');
		$this->data['entry_enable_disable']	= $this->language->get('entry_enable_disable');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

 		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}
  		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/notifywhenavailable', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		$this->data['action'] = $this->url->link('module/notifywhenavailable', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		if (isset($this->request->post['notifywhenavailable'])) {
			foreach ($this->request->post['notifywhenavailable'] as $key => $value) {
				$this->data['data']['notifywhenavailable'][$key] = $this->request->post['NotifyWhenAvailable'][$key];
			}
		} else {
			$configValue = $this->config->get('NotifyWhenAvailable');
			$this->data['data']['NotifyWhenAvailable'] = $configValue;		
		}
		
		$this->data['currenttemplate'] =  $this->config->get('config_template');
		
		$this->data['modules'] = array();
		if (isset($this->request->post['notifywhenavailable_module'])) {
			$this->data['modules'] = $this->request->post['notifywhenavailable_module'];
			exit;
		} elseif ($this->config->get('notifywhenavailable_module')) { 
			$this->data['modules'] = $this->config->get('notifywhenavailable_module');
		}	
		
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->template = 'module/notifywhenavailable.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);	
		
		$run_query = $this->db->query("SELECT `product_id`,`customer_notified`, COUNT(`customer_notified`) as cust_count FROM `".DB_PREFIX."notifywhenavailable` GROUP BY `product_id`,`customer_notified`");
		$this->data['products'] = array();
		if (isset($run_query->rows)) {
			foreach ($run_query->rows as $row)
			{
				$this->data['products'][$row['product_id']][$row['customer_notified']] = $row['cust_count'];
			}			
		}
			
		$this->response->setOutput($this->render());
	}
	
 	public function install()
    {
        $this->load->model('module/notifywhenavailable');
        $this->model_module_notifywhenavailable->install();
    }
    public function uninstall()
    {
        $this->load->model('module/notifywhenavailable');
        $this->model_module_notifywhenavailable->uninstall();
    }
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/notifywhenavailable')) {
			$this->error = true;
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	 public function getcustomers()
    {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        }
        $this->load->model('module/notifywhenavailable');
        $pagination               = new Pagination();
        $pagination->num_links    = 2;
        $pagination->total        = $this->model_module_notifywhenavailable->getTotalCustomers();
        $pagination->page         = $page;
        $pagination->limit        = 10;
        $pagination->text         = $this->language->get('text_pagination');
        $pagination->url          = $this->url->link('module/notifywhenavailable/getcustomers', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
        $this->data['sources']    = $this->model_module_notifywhenavailable->viewcustomers($page, $pagination->limit);
        $this->data['limit']      = $pagination->limit;
        $this->template           = 'module/notifywhenavailable/viewcustomers.tpl';
        $this->data['pagination'] = $pagination->render();
        $this->response->setOutput($this->render());
    }
	
	public function getarchive()
    {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        }
        $this->load->model('module/notifywhenavailable');
        $pagination               = new Pagination();
        $pagination->num_links    = 2;
        $pagination->total        = $this->model_module_notifywhenavailable->getTotalNotifiedCustomers();
        $pagination->page         = $page;
        $pagination->limit        = 10;
        $pagination->text         = $this->language->get('text_pagination');
        $pagination->url          = $this->url->link('module/notifywhenavailable/getarchive', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
        $this->data['sources']    = $this->model_module_notifywhenavailable->viewnotifiedcustomers($page, $pagination->limit);
        $this->data['limit']      = $pagination->limit;
        $this->template           = 'module/notifywhenavailable/archive.tpl';
        $this->data['pagination'] = $pagination->render();
        $this->response->setOutput($this->render());
    }
	
	public function removecustomer() {
		if (isset($_POST['notifywhenavailable_id'])) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "notifywhenavailable` WHERE `notifywhenavailable_id`=".(int)$_POST['notifywhenavailable_id']);
			if ($run_query) echo "Success!";
		}
	}
		
	public function removeallcustomers() {
		if (isset($_POST['remove']) && ($_POST['remove']==true)) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "notifywhenavailable` WHERE `customer_notified`='0'");
			if ($run_query) echo "Success!";
		}
	}
		
	public function removeallarchive() {
		if (isset($_POST['remove']) && ($_POST['remove']==true)) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "notifywhenavailable` WHERE `customer_notified`='1'");
			if ($run_query) echo "Success!";
		}
	}
		
	private function getCatalogURL(){
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }
	
	public function testcron()
    {
        if (function_exists('shell_exec') && trim(shell_exec('echo EXEC')) == 'EXEC') {
            $this->data['shell_exec_status'] = 'Enabled';
        } else {
            $this->data['shell_exec_status'] = 'Disabled';
        }
        if ($this->data['shell_exec_status'] == 'Enabled') {
		   $cronFolder = dirname(DIR_APPLICATION) . '/vendors/notifywhenavailable/';
            if (shell_exec('crontab -l')) {
                $this->data['cronjob_status']    = 'Enabled';
                $curentCronjobs                  = shell_exec('crontab -l');
                $this->data['current_cron_jobs'] = explode(PHP_EOL, $curentCronjobs);
                file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
            } else {
				file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
                if (file_exists($cronFolder . 'cron.txt')) {
                    exec('crontab ' . $cronFolder . 'cron.txt');
                    if (shell_exec('crontab -l')) {
                        $this->data['cronjob_status'] = 'Enabled';
                        shell_exec('crontab -r');
                    } else {
                        $this->data['cronjob_status'] = 'Disabled';
                    }
                }
            }
            if (file_exists($cronFolder . 'cron.txt')) {
                $this->data['folder_permission'] = "Writable";
                unlink($cronFolder . 'cron.txt');
            } else {
                $this->data['folder_permission'] = "Unwritable";
            }
        }
        $this->data['cron_folder'] = $cronFolder;
        $this->template            = 'module/notifywhenavailable/test_cron.php';
        $this->response->setOutput($this->render());
    }
}
?>