<?php

$sapi = php_sapi_name();
// Version
define('VERSION', '1.5.0.0');
function log1($message)
{
	file_put_contents(DIR_LOGS . "neoseo_product_feed.log", date("Y-m-d H:i:s - ") . "NeoSeo Product Feed " . $message . "\r\n", FILE_APPEND);
}

// Configuration
if (is_file(dirname(__FILE__) . "/../config_local.php")) {
	require_once(dirname(__FILE__) . "/../config_local.php");
} elseif (is_file(dirname(__FILE__) . "/../config.php")) {
	require_once(dirname(__FILE__) . "/../config.php");
} else {
	log1("Отсутствует файл конфигурации");
	exit();
}

log1("Начинаем формирование экспорта по расписанию");

log1("Подключаем движок опенкарт");

if (file_exists(dirname(__FILE__) . '/../vqmod/vqmod.php')) {
	// VirtualQMOD
	require_once(dirname(__FILE__) . '/../vqmod/vqmod.php');
	if (method_exists('VQMod', 'bootup')) {
		VQMod::bootup();
		// VQMODDED Startup
		require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

		// Application Classes
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/customer.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/affiliate.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/currency.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/tax.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/weight.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/length.php'));
		require_once(VQMod::modCheck(DIR_SYSTEM . 'library/cart.php'));
		if (file_exists(VQMod::modCheck(DIR_SYSTEM . 'library/ocstore.php'))) {
			require_once(VQMod::modCheck(DIR_SYSTEM . 'library/ocstore.php'));
		}
	} else {
		$vqmod = new VQMod();
		// VQMODDED Startup
		require_once($vqmod->modCheck(DIR_SYSTEM . 'startup.php'));

		// Application Classes
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/customer.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/affiliate.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/currency.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/tax.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/weight.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/length.php'));
		require_once($vqmod->modCheck(DIR_SYSTEM . 'library/cart.php'));
	}
} else {
	// NO VQMOD
	require_once(DIR_SYSTEM . 'startup.php');

	// Application Classes
	require_once(DIR_SYSTEM . 'library/customer.php');
	require_once(DIR_SYSTEM . 'library/affiliate.php');
	require_once(DIR_SYSTEM . 'library/currency.php');
	require_once(DIR_SYSTEM . 'library/tax.php');
	require_once(DIR_SYSTEM . 'library/weight.php');
	require_once(DIR_SYSTEM . 'library/length.php');
	require_once(DIR_SYSTEM . 'library/cart.php');
	if (file_exists(DIR_SYSTEM . 'library/ocstore.php')) {
		require_once(DIR_SYSTEM . 'library/ocstore.php');
	}
}

log1("Инициализируем движок опенкарт");

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);


// Store
$config->set('config_store_id', 0); // todo: сделать параметром
$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int) $config->get('config_store_id') . "' ORDER BY store_id ASC");
foreach ($query->rows as $setting) {
	if (!$setting['serialized']) {
		$config->set($setting['key'], $setting['value']);
	} else {
		$config->set($setting['key'], unserialize($setting['value']));
	}
}

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);


// Request
$request = new Request();
$registry->set('request', $request);


// Cache
$cache = new Cache();
$registry->set('cache', $cache);

// Session
$session = new Session();
$registry->set('session', $session);


// Language Detection
$languages = array();

$query = $db->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");
foreach ($query->rows as $result) {
	$languages[$result['code']] = $result;
}

$code = $config->get('config_language');
$session->data['language'] = $code;

$config->set('config_language_id', $languages[$code]['language_id']);
$config->set('config_language', $languages[$code]['code']);


// Language
$language = new Language($languages[$code]['directory']);
$language->load($languages[$code]['filename']);
$registry->set('language', $language);


// Customer
$registry->set('customer', new Customer($registry));

if (isset($request->get['tracking'])) {
	setcookie('tracking', $request->get['tracking'], time() + 3600 * 24 * 1000, '/');
}

$registry->set('currency', new Currency($registry));

// Tax
$registry->set('tax', new Tax($registry));

function error_handler($errno, $errstr, $errfile, $errline)
{
	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$error = 'Notice';
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$error = 'Warning';
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$error = 'Fatal Error';
			break;
		default:
			$error = 'Unknown';
			break;
	}

	log1('Произошла ошибка PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	return true;
}

// Error Handler
set_error_handler('error_handler');

log1("Инициализируем сео-компонент");

if (!$seo_type = $config->get('config_seo_url_type')) {
	$seo_type = 'seo_url';
}
$seoAction = new Action('common/' . $seo_type);
if (file_exists(dirname(__FILE__) . '/../vqmod/vqmod.php')) {
	if (method_exists('VQMod', 'bootup')) {
		require_once(VQMod::modCheck($seoAction->getFile()));
	} else {
		require_once($vqmod->modCheck($seoAction->getFile()));
	}
} else {
	require_once($seoAction->getFile());
}
$seoClass = $seoAction->getClass();
$seoController = new $seoClass($registry);
$url->addRewrite($seoController);

putenv("SERVER_NAME=localhost"); // это чтобы почта работала
log1("Загружаем модуль мультиэкспорта");
$loader->load->model('feed/neoseo_product_feed');
log1("Запускаем формирование экспорта");
$loader->model_feed_neoseo_product_feed->saveFeeds();

