<?php
// Heading
$_['heading_title']       = 'RelatedProductsPro 1.4';
// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module RelatedProductsPro!';
$_['text_success_activation']        = 'ACTIVATED: You have successfully activated RelatedProductsPro!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
// Entry
$_['entry_code']          = 'RelatedProductsPro status:<br /><span class="help">Enable or disable RelatedProductsPro</span>';
$_['entry_highlightcolor']        = 'Highlight color:<br /><span class="help">This is the color the keyword in the results highlights in.<br/><br/><em>Examples: red, blue, #F7FF8C</em></span>';
// Entry
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_layout_options']      = 'Layout Options:';
$_['entry_position_options']    = 'Position Options:';
$_['entry_enable_disable']		= 'RelatedProductsPro status:<span class="help">Enable or disable RelatedProductsPro for this store</span>';
$_['entry_yes']					= 'Yes';
$_['entry_no']					= 'No';
// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module RelatedProductsPro!';
?>