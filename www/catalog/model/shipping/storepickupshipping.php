<?php
class ModelShippingStorePickupShipping extends Model {
	function getQuote($address) {
		$this->load->language('shipping/storepickupshipping');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('storepickupshipping_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('storepickupshipping_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();

		$this->load->model('setting/setting');
		$StorePickupSettings = $this->model_setting_setting->getSetting('storepickup');
		$moduleData = !empty($StorePickupSettings['StorePickup']) ? $StorePickupSettings['StorePickup'] : array(); 
		$storeData = isset($StorePickupSettings['StorePickup']['StoreData']) ? $StorePickupSettings['StorePickup']['StoreData'] : array();

		if ($status && !empty($storeData) && $moduleData['Enabled'] == 'yes') {
			$quote_data = array();
			
			foreach ($storeData as $sData) {
				if ($sData['Enabled'] == 'yes') {
					$quote_data['storepickupshipping'.$sData['id']] = array(
						'code'         => 'storepickupshipping.storepickupshipping'.$sData['id'],
						'title'        => $sData['Name'][$this->config->get('config_language_id')].' - '.$sData['Address'][$this->config->get('config_language_id')],
						'cost'         => 0.00,
						'tax_class_id' => 0,
						'text'         => '0.00'
					);
				}
			}

			$method_data = array(
				'code'       => 'storepickupshipping',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('storepickupshipping_sort_order'),
				'error'      => false
			);
			
		}

		return $method_data;
	}
}