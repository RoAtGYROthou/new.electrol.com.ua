<?php echo $header; ?>

<div id="content"><h1 class="heading-title"><?php echo $heading_title; ?></h1><?php echo $content_top; ?>
	<img class="image404" src="https://electrol.com.ua/image/data/404/404-error-electrol-min.jpg" title="404 Страница не найдена" alt="404 Страница не найдена">
  
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>